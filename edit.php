<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | General Includes
// +----------------------------------------------------------------------

    include_once("functions.php");
        $Timer->start('init');
    include_once("db.php");
    include_once("session.php");
    include_once("prg_account.php");
    include_once("prg_lesson.php");


// +----------------------------------------------------------------------
// | Cache Control
// +----------------------------------------------------------------------
    header ("Content-type: text/html");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

// +----------------------------------------------------------------------
// | Create and validate the current session
// +----------------------------------------------------------------------
    $Session = new fmSession;
    $Timer->start('session');
    $Session->init('FMC_FlashCard');
    $Timer->stop('session');

// +----------------------------------------------------------------------
// | Initiate the Account Object
// +----------------------------------------------------------------------
    $Account = new fmAccount;
    $Account->init($Session->getID());

// +----------------------------------------------------------------------
// | Verify Access Rights
// +----------------------------------------------------------------------
    if ($Account->req_access(35) != "G") {
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><title>Redirect</title></head><body bgcolor="White"><script language=javascript>location.replace("index.php");</script></body></html>';
    }

// +----------------------------------------------------------------------
// | Process requests
// +----------------------------------------------------------------------
    if (isset($_REQUEST['changeItem'])) {
        $Session->post('/webpage/edit/item',$_REQUEST['changeItem']);
    }
    if (isset($_REQUEST['changePage'])) {
        $Session->post('/webpage/edit/page',$_REQUEST['changePage']);
    }
    if (isset($_REQUEST['changeType'])) {
        $Session->post('/webpage/edit/type',$_REQUEST['changeType']);
    }
    $wItem = $Session->get('/webpage/edit/item');
    $wPage = $Session->get('/webpage/edit/page');
    $wType = $Session->get('/webpage/edit/type');
    if (isset($_REQUEST['add'])) {
        // Adding one to the database!
        $mItem = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['itemName']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $mPage = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['pageName']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $mContent = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], stripslashes($_REQUEST['content'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        mysqli_query($db_link, "INSERT INTO WebContent SET wItem='$mItem', wPage='$mPage', wContent='$mContent', wType='$wType'");
        $wItem = $_REQUEST['itemName'];
        $wPage = $_REQUEST['pageName'];
    }
    if (isset($_REQUEST['update'])) {
        // Adding one to the database!
        $mItem = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['itemName']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $mPage = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['pageName']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $mContent = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], stripslashes($_REQUEST['content'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $sql = "UPDATE WebContent SET wContent='$mContent' WHERE wItem='$mItem' AND wPage='$mPage' AND wType='$wType' ";
        mysqli_query($db_link, $sql);
        $wItem = $_REQUEST['itemName'];
        $wPage = $_REQUEST['pageName'];
    }
    if (isset($_REQUEST['delete'])) {
        // Delete it!
        $mItem = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['itemName']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $mPage = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['pageName']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $sql = "DELETE FROM WebContent WHERE wItem='$mItem' AND wPage='$mPage'";
        mysqli_query($db_link, $sql);
        $wItem = '';
        $wPage = '';
    }
    $tc = ""; if ($wType == 'text/css') { $tc = ' SELECTED'; }
    $th = ""; if ($wType == 'text/html') { $th = ' SELECTED'; }
    $tj = ""; if ($wType == 'text/javascript') { $tj = ' SELECTED'; }
    $tv = ""; if ($wType == 'text/variables') { $tv = ' SELECTED'; }
    $full_pagename = $_SERVER['SCRIPT_FILENAME'];
    $ar = preg_split('/\//',$full_pagename);
    $current_page = $ar[count($ar)-1];

    $Session->save();
// +----------------------------------------------------------------------
// | Output title and header
// +----------------------------------------------------------------------
    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
    echo '<HTML>';
    echo '<HEAD><TITLE>Web Content Editor - 5 Muses L.L.C.</TITLE></HEAD>';
    echo '<BODY>';

// +----------------------------------------------------------------------
// | Output the Inline CSS
// +----------------------------------------------------------------------
    $css = '<STYLE TYPE="text/css">'."\n";
    // Pull CSS From Here...Output...
    $result = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/css'");
    while ($row = mysqli_fetch_array($result)) {
        $output = strtr($row['wContent'],"\x09\x0A\x0D",'!!!');
        $output = str_replace('        ','',$output);
        $output = str_replace('!','',$output);
        $css .= $output."\n";
    }
    $css .= '</STYLE>'."\n";

// +----------------------------------------------------------------------
// | Setup the Javascript Stuff, if any
// +----------------------------------------------------------------------
    $html .= '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">';
    // Pull Javascript From Here...Output...
    $html .= "function godaddy() { DOMViewerObj=null;window.open('domviewer.html');return false; } ";
    //$html .= "return false;";
    $html .= '</SCRIPT>';


// +----------------------------------------------------------------------
// | Setup Main HTML Body
// +----------------------------------------------------------------------
    $html .= '';
    $html .= '<TABLE BORDER=0 CELLPADDING=5 CELLSPACING=0>';
    $html .= '<TR>';
    $html .= '<TD VALIGN=TOP CLASS=nav><BR>';
    $html .= '<FORM ACTION=edit.php METHOD=POST NAME=edittype>';
    $html .= '<SELECT NAME=changeType onChange="edittype.submit()">'; // onClick="edittype.submit()">';
    $html .= "<OPTION$tc>text/css</OPTION>";
    $html .= "<OPTION$th>text/html</OPTION>";
    $html .= "<OPTION$tj>text/javascript</OPTION>";
    $html .= "<OPTION$tv>text/variables</OPTION>";
    $html .= '</SELECT>';
    $html .= '</FORM>';
    $result = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wType='$wType' GROUP BY wPage");
    while ($row = mysqli_fetch_array($result)) {
        $html .= '<DIV CLASS=maindiv></DIV>'."\n";
        $html .= '<DIV CLASS=nav>[ <A HREF=edit.php?changePage='.urlencode($row['wPage']).'>'.$row['wPage'].'</A> ]</DIV>';
        // if page then show items
        if ($row['wPage'] == $wPage) {
            $iresult = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wPage='$wPage' AND wType='$wType' ORDER BY wItem");
            while ($row = mysqli_fetch_array($iresult)) {
                $html .= '<DIV CLASS=subdiv></DIV>';
                $del = '<A HREF=edit.php?itemName='.urlencode($row['wItem']).'pageName'.urlencode($row['wPage']).'>';
                $html .= '<DIV CLASS=item>['.$del.'X</A>] <A HREF=edit.php?changeItem='.urlencode($row['wItem']).'>'.$row['wItem'].'</A></DIV>';
                if ($row['wItem'] == $wItem) {
                    $pageName = $wPage;
                    $itemName = $wItem;
                    $content = htmlentities($row['wContent']);
                }
            }
        }
    }
    $html .= '<DIV CLASS=maindiv></DIV>';
    $html .= '</TD>';
    $html .= '<TD CLASS=edit VALIGN=TOP>';
    $html .= '<FORM ACTION=edit.php METHOD=POST NAME=Editor>';
    $html .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>';
    $html .= '<TR>';
    $html .= '<TD ALIGN=RIGHT onclick="godaddy();">';
    $html .= '<FONT COLOR=white>Page: </FONT>';
    $html .= '</TD>';
    $html .= '<TD ALIGN=LEFT>';
    $html .= "<INPUT NAME=pageName VALUE=\"$pageName\" SIZE=30>";
    $html .= '</TD>';
    $html .= '</TR>';
    $html .= '<TR>';
    $html .= '<TD ALIGN=RIGHT>';
    $html .= '<FONT COLOR=white>Item: </FONT>';
    $html .= '</TD>';
    $html .= '<TD ALIGN=LEFT>';
    $html .= "<INPUT NAME=itemName VALUE=\"$itemName\" SIZE=30>";
    $html .= '</TD>';
    $html .= '</TR>';
    $html .= '<TR>';
    $html .= '<TD COLSPAN=2>';
    if ($wType == 'text/variables') {
        $html .= "<INPUT SIZE=40 NAME=content VALUE=\"$content\">";
    } else {
        $html .= "<TEXTAREA NAME=content COLS=80 ROWS=28>$content</TEXTAREA>";
    }
    $html .= '</TD>';
    $html .= '</TR>';
    $html .= '<TR>';
    $html .= '<TD COLSPAN=2>';
    $html .= "<INPUT TYPE=SUBMIT NAME=add VALUE=Add>";
    $html .= "<INPUT TYPE=SUBMIT NAME=update VALUE=Update>";
    $html .= '</TD>';
    $html .= '</TR>';
    $html .= '</TABLE>';
    $html .= '</TD></TR></TABLE>';
    $html .= '</FORM>';

// +----------------------------------------------------------------------
// | Make Variable Replacements
// +----------------------------------------------------------------------
    $var = array();
    $sql = "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/variables'";
//     echo "<PRE>$sql</PRE>";
    $vresult = mysqli_query($db_link, $sql);
    if (is_resource($vresult)) {
        while ($row = mysqli_fetch_array($vresult)) {
            $var[$row['wItem']] = $row['wContent'];
        }
        foreach ($var as $key => $item) {
            $css = str_replace("%$key%",$item,$css);
            $js = str_replace("%$key%",$item,$js);
            //$html = str_replace("%$key%",$item,$html);
        }
    }

// +----------------------------------------------------------------------
// | Output the Main HTML
// +----------------------------------------------------------------------
    echo $css;
    echo $js;
    echo $html;


?>
