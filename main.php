<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | General Includes
// +----------------------------------------------------------------------

    include_once("functions.php");
        $Timer->start('init');
    include_once("db.php");
    include_once("session.php");
    include_once("prg_account.php");
    include_once("prg_lesson.php");
    include_once("prg_vocabulary.php");
    include_once("main_lib.php");
    include_once("prg_flash.php");

// +----------------------------------------------------------------------
// | Cache Control
// +----------------------------------------------------------------------
    header("Content-type: text/html; charset=utf-8");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

// +----------------------------------------------------------------------
// | Create and validate the current session
// +----------------------------------------------------------------------
    $Session = new fmSession;
    $Timer->start('session');
    $RecentlyExpired = $Session->init('FMC_FlashCard');
    $Timer->stop('session');

// +----------------------------------------------------------------------
// | Initiate the Account Object
// +----------------------------------------------------------------------
    $Account = new fmAccount;
    $Account->init($Session->getID());

// +----------------------------------------------------------------------
// | Initiate the Dynamic Flash Card Object
// +----------------------------------------------------------------------
    $FlashCard = new fmFlashCard;
    $FlashCard->restore($Session->get("/w2/f"));
    
// +----------------------------------------------------------------------
// | Misc Functions
// +----------------------------------------------------------------------
    function make_seed() {
        list($usec, $sec) = explode(' ', microtime());
        return (float) $sec + ((float) $usec * 100000);
    }

// +----------------------------------------------------------------------
// | Account Adjustments
// +----------------------------------------------------------------------
    // Account Login
        if ($_REQUEST["login"] == "true") {
            $lg_email = $_REQUEST["log_email"];
            $lg_pass = $_REQUEST["log_pass"];
            $login = $Account->login($lg_email,$lg_pass,$Session->getID());
            // Transfer Account settings to Session for holding.
            $set = $Account->get_audio_settings();
            $volume = ($set >> 16) & 255;
            $delay = ($set >> 8) & 15;
            $repeat = ($set >> 4) & 15;
            $size = $set & 15;
            $Session->post("/program/settings/volume=$volume");
            $Session->post("/program/settings/delay=$delay");
            $Session->post("/program/settings/repeat=$repeat");
            $Session->post("/program/settings/size=$size");
            $Session->post('/program/dview/signup',700);
            $Session->post('/program/lesson/language',2);
        }

    // Account Logout
        if ($_REQUEST["login"] == "false") {
            $Account->logout($Session->getID());
            $Session->destroy('FMC_FlashCard');
            // Redirect to a different page...
        }
    // The redir section for the entry
        if (isset($_REQUEST['redir'])) {
            $redir = $_REQUEST['redir'];
            // Initialize the vocab and lesson?
            
            $Vocabulary = new fmPrgVocabulary;
            if (strlen($Session->get("/w2/v")) > 1)
            $Vocabulary->restore($Session->get("/w2/v"));
            
            $Lesson = new fmPrgLesson;
            $aid = $Account->getID();
            $sid = $Session->getID();
            $Lesson->init($aid,$sid);
            $Lesson->restore($Session->get("/w2/l"));
            
            // Handle the setup of the jsrpc
            if ($redir == 'sp_ad') {
                // Came from the spanish ad...
                $Vocabulary->vLanguage = 2;
                $Lesson->_language = 2;
            }
            if ($redir == 'fr_ad') {
                // Came from the spanish ad...
                $Vocabulary->vLanguage = 4;
                $Lesson->_language = 4;
            }
            if ($redir == 'ge_ad') {
                // Came from the spanish ad...
                $Vocabulary->vLanguage = 3;
                $Lesson->_language = 3;
            }
            if ($redir == 'sp400demo') {
                // Load a random free Spanish lesson
                // Make it all start
                $Vocabulary->vLanguage = 2;
                $Lesson->_language = 2;
                $summary = $Lesson->lesson_summary(true);
                $freeLessons = array();
                if ($summary !== false) foreach ($summary['litems'] as $entry) {
                    if (($entry['name'] != "[Current]") and ($entry['lang'] == $Lesson->_language)) {
                        // Use a field in the Lesson Table for designating if this is free
                        if (($entry['owner'] == 'PreMade-Free') or ($Account->has_access(0,$Lesson->_language)) ) {
                            // Add to an array of free lessons...
                            $freeLessons[] = $entry['ID'];
                        }
                    }
                }
                shuffle($freeLessons);
                $freeLessonID = array_shift($freeLessons);
                $Session->post("/w2/n/adfc","redir:400free.php");
            }
            if ($redir == 'fr400demo') {
                // Load a random free Spanish lesson
                // Make it all start
                $Vocabulary->vLanguage = 4;
                $Lesson->_language = 4;
                $summary = $Lesson->lesson_summary(true);
                $freeLessons = array();
                if ($summary !== false) foreach ($summary['litems'] as $entry) {
                    if (($entry['name'] != "[Current]") and ($entry['lang'] == $Lesson->_language)) {
                        // Use a field in the Lesson Table for designating if this is free
                        if (($entry['owner'] == 'PreMade-Free') or ($Account->has_access(0,$Lesson->_language)) ) {
                            // Add to an array of free lessons...
                            $freeLessons[] = $entry['ID'];
                        }
                    }
                }
                shuffle($freeLessons);
                $freeLessonID = array_shift($freeLessons);
                $Session->post("/w2/n/adfc","redir:400free.php");
            }
            if ($redir == 'ge400demo') {
                // Load a random free Spanish lesson
                // Make it all start
                $Vocabulary->vLanguage = 3;
                $Lesson->_language = 3;
                $summary = $Lesson->lesson_summary(true);
                $freeLessons = array();
                if ($summary !== false) foreach ($summary['litems'] as $entry) {
                    if (($entry['name'] != "[Current]") and ($entry['lang'] == $Lesson->_language)) {
                        // Use a field in the Lesson Table for designating if this is free
                        if (($entry['owner'] == 'PreMade-Free') or ($Account->has_access(0,$Lesson->_language)) ) {
                            // Add to an array of free lessons...
                            $freeLessons[] = $entry['ID'];
                        }
                    }
                }
                shuffle($freeLessons);
                $freeLessonID = array_shift($freeLessons);
                $Session->post("/w2/n/adfc","redir:400free.php");
            }
            $Lesson->load("[Current]");
            
            // Store the values for the jsrpc
            $Session->post("/w2/v=".$Vocabulary->preserve());
            $Session->post("/w2/l=".$Lesson->preserve());
        }
// +----------------------------------------------------------------------
// | Process requests
// +----------------------------------------------------------------------
    $run = $_REQUEST['run'];
    $goto = $_REQUEST['goto'];
    $gosub = $_REQUEST['gosub'];
    $full_pagename = $_SERVER['SCRIPT_FILENAME'];
    $ar = preg_split('/\//',$full_pagename);
    $current_page = $ar[count($ar)-1];
    if (isset($_REQUEST['goto'])) {
        $Session->post('/webpage/nav/page',$_REQUEST['goto']);
    }

    $Page = $Session->get('/webpage/nav/page');
    $Session->save();

// +----------------------------------------------------------------------
// | Setup the Inline CSS
// +----------------------------------------------------------------------
    $css = '<STYLE TYPE="text/css">'."\n";
    // Pull CSS From Here...Output...
    $result = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/css'");
    while ($row = mysqli_fetch_array($result)) {
        $output = strtr($row['wContent'],"\x09\x0A\x0D",'!!!');
        $output = str_replace('        ','',$output);
        $output = str_replace('!','',$output);
        $css .= $output."\n";
    }
    $css .= '</STYLE>'."\n";

// +----------------------------------------------------------------------
// | Setup the Javascript Stuff, if any
// +----------------------------------------------------------------------
    //echo '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">';
    // Pull Javascript From Here...Output...
    //echo '</SCRIPT>';

    $pl_images[] = "images/bback1.jpg";
    $pl_images[] = "images/back1.jpg";
    $pl_images[] = "images/sec_banner_02.jpg";
    $pl_images[] = "images/sc_nav_text-1.gif";
    $pl_images[] = "images/sc_nav_text-2.gif";
    $pl_images[] = "images/sc_nav_text-3.gif";
    $pl_images[] = "images/sc_nav_text-4.gif";
    $pl_images[] = "images/sc_nav_text-h1.gif";
    $pl_images[] = "images/sc_nav_text-h2.gif";
    $pl_images[] = "images/sc_nav_text-h3.gif";
    $pl_images[] = "images/sc_nav_text-h4.gif";
    $pl_images[] = "images/sc_nav_text-s1.gif";
    $pl_images[] = "images/sc_nav_text-s2.gif";
    $pl_images[] = "images/sc_nav_text-s3.gif";
    $pl_images[] = "images/sc_nav_text-s4.gif";
    $pl_images[] = "images/sc_nav_b3.gif";
    $pl_images[] = "images/sc_nav_01.jpg"; 
    $pl_images[] = "images/button4.gif";
    $pl_images[] = "images/button3.gif";
    //$pl_images[] = "images/";
    $pl_images[] = "images/2100-1.jpg";
    $pl_images[] = "images/2100-2.jpg";
    $pl_images[] = "images/2100-3.jpg";
    $pl_images[] = "images/2130-1.jpg";
    $pl_images[] = "images/2130-2.gif";
    $pl_images[] = "images/2130-2.jpg";
    $pl_images[] = "images/2130-3.gif";
    $pl_images[] = "images/2130-3.jpg";
    $pl_images[] = "images/2130-4.jpg";
    $pl_images[] = "images/2130-5.jpg";
    $pl_images[] = "images/2130-6.jpg";
    $pl_images[] = "images/fr_bvocab1.jpg";
    $pl_images[] = "images/sp_bvocab1.jpg";
    $pl_images[] = "images/ge_bvocab1.jpg";
    for ($i=0;$i<20;$i++) {
        $pl_images[$i] = "images/sc_nav_$i.jpg";
        $pl_images[$i+20] = "images/sc_nav_s$i.jpg";
        $pl_images[$i+40] = "images/sc_nav_h$i.jpg";
    }
    $pl_count = count($pl_images);
    
// +----------------------------------------------------------------------
// | Setup Main HTML Body
// +----------------------------------------------------------------------

    // Output title and header
    // FIXME!! Meta tags
    $head .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    $head .= '<HTML>';
    $head .= '<HEAD ID="fmHead"><TITLE>5 Muses Software L.L.C.</TITLE>';
    //$html .= '<BODY>';
    $html .= '</HEAD><BODY onload="fmInitialize()" onunload="fmFinalize()">';
    
    // Main Javascript Code for the RPC
    $html .= '<SCRIPT LANGUAGE="JavaScript" SRC="fm_main_0009.js" TYPE="text/javascript"></SCRIPT>';
    
    // Javascript Code for the ID'ing of the Browser and Res
    $html .= '<SCRIPT LANGUAGE="JavaScript" SRC="id.js" TYPE="text/javascript"></SCRIPT>';
    
    $html .= '<DIV ID="fmPostLoad" STYLE="margin: 8px 0px 8px 3px; width:985; display:none;">';  // NO CENTER!
    
    // =====================================================================
    // ===== Login Pane
    // =====================================================================
    
    $html .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=980>';
    
    // =====================================================================
    // ===== Top 5m Banner
    // =====================================================================
    $html .= '<TR>';
    $html .= '<TD COLSPAN=2>';
    $html .= fmlibBanner("default");
    $html .= '</TD>';
    $html .= '</TR><TR>';
    
    // =====================================================================
    // ===== Left 5m Navigation Section
    // =====================================================================
    
    //5Muses Software
    $headings = array();
//     $subs = array();
//     $subs[] = fmlibNavSubHeading("Latest News",1100,"#000000","rpcFetch('act=html&ibTag=1100');");
//     $subs[] = fmlibNavSubHeading("About The Company",1110,"#000000","rpcFetch('act=html&ibTag=1110');");
//     $subs[] = fmlibNavSubHeading("5M History",1120,"#000000","rpcFetch('act=html&ibTag=1120');");
//     $subs[] = fmlibNavSubHeading("Contact Us",1130,"#000000","rpcFetch('act=html&ibTag=1130');");
//     $nav1count = count($subs);
//     $block = fmlibNavSubHeadingBlock("iFMmainSub",$subs);
//     $headings[] = fmlibNavHeading("iFMmain","5 Muses Software",1000,$block);
    
     $nav1count = 0;
    // Dynamic Fast Cards
    $subs = array();
    $subs[] = fmlibNavSubHeading("An Introduction",2100,"#000000","rpcFetch('act=html&ibTag=2100');");
    $subs[] = fmlibNavSubHeading("Tutorial",2105,"#000000","rpcFetch('act=html&ibTag=2105');");
    $subs[] = fmlibNavSubHeading("Purchasing",2120,"#000000","rpcFetch('act=dfc&ibTag=2120');");
    $subs[] = fmlibNavSubHeading("Word Database",2130,"#000000","rpcFetch('act=dfc&ibTag=2130');");
    $subs[] = fmlibNavSubHeading("Lesson Builder",2140,"#000000","rpcFetch('act=dfc&ibTag=2140');");
    $subs[] = fmlibNavSubHeading("Pre-Made Lessons",2110,"#000000","rpcFetch('act=dfc&ibTag=2110');");
    $nav1count = count($subs);
    $block = fmlibNavSubHeadingBlock("iFMdfcSub",$subs);
    $headings[] = fmlibNavHeading("iFMdfc","Dynamic Fast Cards",2000,$block);
    
    // Your Account    
    $subs = array();
    $subs[] = fmlibNavSubHeading("Overview",3100,"#000000","rpcFetch('act=acct&ibTag=3100');");
    $subs[] = fmlibNavSubHeading("Billing History",3110,"#000000","rpcFetch('act=acct&ibTag=3110');");
    $subs[] = fmlibNavSubHeading("Edit Information",3120,"#000000","rpcFetch('act=acct&ibTag=3120');");
    $nav2count = count($subs);
    $block = fmlibNavSubHeadingBlock("iFMaccountSub",$subs);
    $headings[] = fmlibNavHeading("iFMaccount","Your Account",3000,$block);
        
    // Support    
    $subs = array();
    $subs[] = fmlibNavSubHeading("Checklist",4100,"#000000","rpcFetch('act=support&ibTag=4100');");
    $subs[] = fmlibNavSubHeading("Common Questions",4110,"#000000","rpcFetch('act=support&ibTag=4110');");
    $subs[] = fmlibNavSubHeading("Submit a Ticket",4130,"#000000","rpcFetch('act=support&ibTag=4130');");
    $subs[] = fmlibNavSubHeading("Send Us Feedback",4120,"#000000","rpcFetch('act=support&ibTag=4120');");
    $nav3count = count($subs);
    $block = fmlibNavSubHeadingBlock("iFMsupportSub",$subs);
    $headings[] = fmlibNavHeading("iFMsupport","Support",4000,$block);
    
    
    // If logged in and an administrator.... Then show this...
    // Admin    
//     $subs = array();
//     $subs[] = fmlibNavSubHeading("Browser Info",5100,"#000000","rpcFetch('act=admin&ibTag=5100');");
//     $subs[] = fmlibNavSubHeading("Web Content Editor",5110,"#000000","rpcFetch('act=admin&ibTag=5110');");
//     $nav5count = count($subs);
//     $block = fmlibNavSubHeadingBlock("iFMadminSub",$subs);
//     $headings[] = fmlibNavHeading("iFMadmin","Admin",5000,$block,"margin:0px 0px 5px 0px;");
    
    $html .= '<TD>';
    $html .= fmlibNavGuide($headings);
    
    // This is where we will put the IFRAME
    
    $html .= '</TD>';
    
    // =====================================================================
    // ===== Main 5m Section
    // =====================================================================
    //determine background
    //case 
    $html .= '<TD>';
    $html .= '<DIV ID="fmMainContent" CLASS="mainct">';
    
//     $html .= '<DIV ID="iFMct" STYLE="font-size:12;font-family:bitstream vera sans,arial,verdana,helvetica,sans-serif;width:100%;height:100%;background-color: transparent;display:block;padding:0px 0px 0px 0px;border:none;">';
//     $html .= 'Yes?';
//     $html .= '</DIV>';

    //$html .= '<P ID="fmMainContent" CLASS="mcspan">...<BR>...<BR>...<BR>...</P>';
    
    $html .= '</DIV>';
    $html .= '</TD>';
    $html .= '</TR>';
    $html .= '</TABLE>';
    
    // End of page
    
    // Login Pane ---------------------------------------------
    $html .= '<DIV ID="fmLogin" CLASS="loginp">';
    if ($Account->is_guest()) {
        $html .= fmlibLogin("default");
    } else {
        $html .= fmlibLogin("reload0");
    }
    $html .= '</DIV>';
    // Login Pane ---------------------------------------------
    
    $html .= '</DIV>';
    
    // Copyright Tag...
    
    // Loading Part...
    $html .= '<DIV ID="fmPreLoad" STYLE="text-align:center;height:300;width:100%;">';
    $html .= '<TABLE CELLPADDING=0 CELLSPACING=0 STYLE="align:center;width:100%;height:100%;">';
    $html .= '<TR>';
    $html .= '<TD ROWSPAN=2 STYLE="text-align:center;">&nbsp;</TD>';
    $html .= '<TD COLSPAN='.$pl_count.' STYLE="text-align:center;vertical-align:bottom;">Loading...</TD>';
    $html .= '<TD ROWSPAN=2 STYLE="text-align:center;">&nbsp;</TD>';
    $html .= '</TR>';
    $html .= '<TR>';
    for ($i=0;$i<$pl_count;$i++) {
        $html .= "<TD ID=\"pl_bar$i\" STYLE=\"background-color:#C0C0C0;width:2;height:5;\"\n></TD>";
    }
    $html .= '</TR>';
    $html .= '</TABLE><BR>';
    //for ($i=0;$i<$pl_count;$i++) {
    //    $html .= "<IMG ID=\"pl_img$i\" SRC=\"images/nav2.gif\" ALT=\".\" TITLE=\"\" STYLE=\"background-color:#C0C0C0;width:0;height:0;\" onload=\"fm_preload(this)\">\n";
    //}
    $html .= '</DIV>';    
    
    // =====================================================================
    // ===== DFC Running Program
    // =====================================================================
    
    // Calculate the volume for the mp3/wm/flash object
    $m_volume = intval((100 - $FlashCard->Volume) * 40.95);
        switch ($FlashCard->Size) {
            case 0: 
                $size = 7;  // PDA?
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                $size = 16 * $FlashCard->Size; 
                break;
        }
        $size = "{$size}pt";
            
    $html .= '<DIV ID="fmVScreen" STYLE="display:none;position:absolute;top:0px;left:0px;text-align:center;height:100%;width:100%;">';
        $html .= '<DIV ID="fmVScreenContent">';
        // The Embedded Audio Piece
        //$html .= '<OBJECT ID=\"dfcaudio\" TYPE=\"application/x-mplayer2\" WIDTH=\"280\" HEIGHT=\"50\">'; //CLASSID=\"clsid:22d6f312-b0f6-11d0-94ab-0080c74c7e95\"
        //$html .= "<param name=\\\"fileName\\\" value=\\\"audio.php?af=$audio\\\">";
        //$html .= '<param name=\"animationatStart\" value=\"false\">';
        //$html .= '<param name=\"transparentatStart\" value=\"true\">';
        //$html .= '<param name=\"autoStart\" value=\"true\">';
        //$html .= '<param name=\"showControls\" value=\"true\">';
        //$html .= "<param name=\\\"volume\\\" value=\\\"-$m_volume\\\">";
        //$html .= "</OBJECT>";
        
        $html .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100% HEIGHT=100% BGCOLOR=#040673 ALIGN=LEFT>";
        $html .= "<TR><TD VALIGN=BOTTOM ALIGN=CENTER HEIGHT=50%>";
        // The Foriegn Word
        $html .= '<SPAN ID="dfcFlN" STYLE="font-size:'.$size.';color:white;">&nbsp;</SPAN>';
        $html .= "</TD></TR>";
        $html .= "<TR><TD VALIGN=TOP ALIGN=CENTER HEIGHT=50% BGCOLOR=#0691CA>";
        // The Native Word and toolbar
            $html .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100% HEIGHT=100%>';
            $html .= '<TR><TD VALIGN=TOP ALIGN=CENTER BGCOLOR=#0691CA>';
            $html .= '<SPAN ID="dfcFlF" STYLE="font-size:'.$size.';color:#040673;">&nbsp;</SPAN>';
            $html .= '</TD></TR>';
            // Toolbar
            $html .= "<TR><TD VALIGN=BOTTOM ALIGN=CENTER HEIGHT=40>";
            $html .= stripslashes($FlashCard->makeToolbar(2));
            $html .= '<OBJECT NAME="dfcAudio2" CLASSID="CLSID:22D6f312-B0F6-11D0-94AB-0080C74C7E95" STANDBY="..." TYPE="application/x-oleobject" WIDTH=0 HEIGHT=0 style="display:none;">';
            //$html .= '<param name="ShowDisplay" value="0">';
            //$html .= '<param name="AutoStart" value="False">';
            //$html .= '<param name="hidden" value="true">';
            $html .= "<param name=\"volume\" value=\"-$m_volume\">";
            
            $html .= '<EMBED SRC="" NAME="dfcAudio2" TYPE="application/x-mplayer2" autostart="false" hidden="true" volume="-'.$m_volume.'" loop="false" width="0" height="0">';
            $html .= '</OBJECT>';
            $html .= "</TD></TR></TABLE>";
        $html .= "</TD></TR></TABLE>";
        
        $html .= '</DIV>';
    $html .= '</DIV>';    
    
    // Things to pass to the javascript...
    // FIXME!!  Turn me into an RPC
    $html .= '<FORM NAME="screenSetup" ACTION="">';
    $html .= "<INPUT TYPE=HIDDEN NAME=\"navACount\" VALUE=\"$nav1count\">";
    $html .= "<INPUT TYPE=HIDDEN NAME=\"navBCount\" VALUE=\"$nav2count\">";
    $html .= "<INPUT TYPE=HIDDEN NAME=\"navCCount\" VALUE=\"$nav3count\">";
    $html .= "<INPUT TYPE=HIDDEN NAME=\"navDCount\" VALUE=\"$nav4count\">";
    $html .= "<INPUT TYPE=HIDDEN NAME=\"navECount\" VALUE=\"$nav5count\">";
    $html .= "<INPUT TYPE=HIDDEN NAME=pl_max VALUE=\"$pl_count\">";
    $html .= '</FORM>';
    
    // the "Loading screen" javascript to preload all the images.
    $html .= '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">'."\n";
    $html .= 'function do_preload() {';
    $html .= "introLoading = true;\n";
    //$html .= 'document.getElementById("pl_bar3").style.backgroundColor = "#4040FF";';
    for ($i=0;$i<$pl_count;$i++) {
        //$html .= "document.getElementById(\"pl_bar$i\").onload = fm_preload;\n";
        $html .= "ipl$i = new Image(200,20);\n";
        $html .= "ipl$i.onload = fm_preload;\n";
        $html .= "ipl$i.src = \"{$pl_images[$i]}\";\n";
        
        
        //$html .= "document.getElementById(\"pl_bar$i\").src = \"{$pl_images[$i]}\";\n";
    }
    if (($redir == "sp400demo") or ($redir == "fr400demo") or ($redir == "ge400demo")){
        $html .= "entryTag = \"act=dfc&ibTag=2150\";\n";
        $html .= "rpcFetch('act=set&ll=$freeLessonID');\n";
        //$html .= '<SCRIPT ID="fmjsrpc" LANGUAGE="JavaScript" SRC="jsrpc.php?act=dfc&ibTag=2150" TYPE="text/javascript"></SCRIPT>';
    }
    if ($redir == "quickstart") {
        $Session->post("/w2/n/adfc","none:");
        $html .= "entryTag = \"act=dfc&ibTag=2140\";\n";
        //$html .= "rpcFetch('act=set&ll=$freeLessonID');\n";
        //$html .= '<SCRIPT ID="fmjsrpc" LANGUAGE="JavaScript" SRC="jsrpc.php?act=dfc&ibTag=2150" TYPE="text/javascript"></SCRIPT>';
    }
    //$html .= 'document.getElementById("pl_bar6").style.backgroundColor = "#4040FF";';
    $html .= '}';
    $html .= '</SCRIPT>';
    $foot = '</BODY></HTML>';


// +----------------------------------------------------------------------
// | Make Variable Replacements
// +----------------------------------------------------------------------
//     $var = array();
//     $sql = "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/variables'";
//     $vresult = mysql_query($sql,$db_link);
//     if (is_resource($vresult)) {
//         while ($row = mysql_fetch_array($vresult)) {
//             $var[$row['wItem']] = $row['wContent'];
//         }
//         foreach ($var as $key => $item) {
//             $css = str_replace("%$key%",$item,$css);
//             $js = str_replace("%$key%",$item,$js);
//             $html = str_replace("%$key%",$item,$html);
//             $head = str_replace("%$key%",$item,$head);
//             $foot = str_replace("%$key%",$item,$foot);
//         }
//     }

// +----------------------------------------------------------------------
// | Output the Main HTML
// +----------------------------------------------------------------------
    echo $head;
    echo $css;
    echo $js;
    echo $html;
    echo $foot;


// +----------------------------------------------------------------------
// | End of file
// +----------------------------------------------------------------------
?>
