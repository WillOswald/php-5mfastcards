<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//


//   _____________________________________________________
//   ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
//   5Muses.com Handheld Stats Page
//   _____________________________________________________
//   ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
    

#   +----------------------------------------------------
#   | Initialize Session Information
#   +----------------------------------------------------
    include_once("functions.php");
        //$Timer->start('init');
    include_once("db.php");
    //include_once("session.php");
    //include_once("prg_account.php");
    include_once("main_lib.php");
    include_once("5mc_reports.php");
    
    //$Session = new fmSession;
    //$Session->init('FMC_AdminPanel');
    $Report = new fmcReports;
#   +----------------------------------------------------
#   | Show Some Stats
#   +----------------------------------------------------
    $ip = $_SERVER['REMOTE_ADDR'];
    if (strpos("70.57.234.155,172.30.70.77,64.32.159.97,71.32.36.107",$ip) !== false) {
        // Output title and header
        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
        $html .= '<HTML>';
        $html .= '<HEAD>';
        $html .= "\n<TITLE>Stats</TITLE>\n";
        $html .= '</HEAD>';
        $html .= '<BODY STYLE="margin: 0px; padding: 0px; background-color: #FFF; ">';
        $html .= '<meta http-equiv="refresh" content="300">';
        // +----------------------------------------------------------------------
        // | Cascading Style Sheets
        // +----------------------------------------------------------------------
        $html .= '<LINK HREF="css.php?sheet=global" REL=stylesheet TYPE="text/css">';
        $html .= '<LINK HREF="css.php?sheet=html" REL=stylesheet TYPE="text/css">';
        // This is the main div enclosure for the margins
        // +----------------------------------------------------------------------
        // | Content
        // +----------------------------------------------------------------------
        // Users online...
        $online = $Report->usersOnline() ;
        $guestCount = $online['guests'];
        $memberCount = $online['members'];
        $today = date('D',strtotime('today'));
        $yesterday = date('D',strtotime('today -1 day'));
        $daybefore = date('D',strtotime('today -2 day'));
        // Get Report Data for last 3 days
        $html .= '<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=240>';
        $html .= '<TR>';
        $html .= '<TD STYLE="border-bottom:1px solid #888;border-right:1px solid #888;">';
        $html .= "Members: $memberCount<BR>";
        $html .= "Guests: $guestCount<BR>";
        $html .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>';
        $html .= '<TR><TD COLSPAN=7>New Accounts:</TD></TR><TR>';
        for ($i=6;$i>=0;$i--) {
            $cna = strtotime("today -$i day");
            $iyear = date('Y',$cna);
            $imonth = date('m',$cna);
            $iday = date('d',$cna);
            $accountCount = $Report->countNewAccounts($iyear,$imonth,$iday);
            $html .= "<TD STYLE=\"border:1px solid #888;\" ALIGN=CENTER>$accountCount</TD>";
        }
        
        $html .= "</TR></TABLE>";
        $html .= '</TD>';
        $html .= '<TD STYLE="border-bottom:1px solid #888;border-right:1px solid #888;">';
        $month = date('M');
        $numbers = $Report->genMonth($month,2005);
        
        // Funky Graph
        $max = 0;
        $height = 50;
        //$html .= "<SPAN CLASS=TITLE STYLE=\"border-bottom:1px solid #888;\">Visitors Report for $month</SPAN><BR><BR>";
        $html .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 HEIGHT='.$height.' STYLE="font-size: 100%;">';
        //$html .= '<TR>';
        foreach($numbers as $day => $values) {
            $max = ($values[0] > $max) ? $values[0] : $max;
            $max = ($values[1] > $max) ? $values[1] : $max;
            $days = $day;
        }
        //for ($i=1;$i<=$days;$i++) {
        //    $html .= '<TD COLSPAN=2 CLASS=auth>'.$i.'</TD>';
        //    $html .= '<TD VALIGN=BOTTOM ALIGN=CENTER WIDTH=5><IMG SRC="images/1px.gif" WIDTH=1 HEIGHT=20 STYLE="background-color:#444;"></TD>';
        //}
        //$html .= '</TR>';
        $html .= '<TR>';
        for ($i=1;$i<=$days;$i++) {
            $values = $numbers[$i];
            $mHeight = intval(($values[1] / $max) * $height);
            $gHeight = intval(($values[0] / $max) * $height);
            $html .= '<TD ROWSPAN=2 VALIGN=BOTTOM><IMG SRC="images/graph1.png" WIDTH=3 HEIGHT="'.$gHeight.'"></TD>';
            //$html .= '<TD ROWSPAN=2 VALIGN=BOTTOM><IMG SRC="images/graph1.png" WIDTH=3 HEIGHT="'.$gHeight - $mHeight.'"><IMG SRC="images/graph2.png" WIDTH=7 HEIGHT='.$mHeight.'></TD>';
            //$html .= '<TD ROWSPAN=2 VALIGN=BOTTOM><IMG SRC="images/graph2.png" WIDTH=7 HEIGHT='.$mHeight.'></TD>';
            //$html .= '<TD ROWSPAN=2 VALIGN=BOTTOM ALIGN=CENTER WIDTH=5><IMG SRC="images/1px.gif" WIDTH=1 HEIGHT='.$height.' STYLE="background-color:#444;"></TD>';
        }
        $html .= '<TD STYLE="border-top:1px solid #444;font-size: 40%;" VALIGN=TOP>'.$max.'</TD>';
        $html .= '</TR><TR>';
        $max = intval($max / 2);
        $html .= '<TD STYLE="border-top:1px solid #444;font-size: 40%;" VALIGN=TOP>'.$max.'</TD>';
        $html .= '</TR><TR>';
        $html .= '</TD>';
        $html .= '</TR></TABLE>';
        /*
        $html .= '<TR>';
        $html .= '<TD COLSPAN=2>';
            $html .= '<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=100% STYLE="margin:4px 0px 0px 0px;border-top:1px solid #F44;">';
            foreach ($online['guests'] as $guest) {
            $html .= "<TR>";
                $html .= "<TD STYLE=\"overflow:hidden;border-bottom:1px solid #CCC;border-right:1px solid #CCC;color:#888;\">{$guest['hostname']}</TD>";
                $html .= "<TD ALIGN=RIGHT STYLE=\"border-bottom:1px solid #CCC;border-right:1px solid #CCC;color:#888;\">{$guest['clicks']}</TD>";
            $html .= "</TR>";
            }
            $html .= '</TABLE>';
        $html .= '</TD>';
        $html .= '</TR>';
        */
        $html .= '</TABLE>';
    } else {
        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><title>Redirect</title></head><body bgcolor="White"><script language=javascript>location.replace("/");</script></body></html>';
    }
    
    echo $html;

?>
