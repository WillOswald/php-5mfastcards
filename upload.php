<?php
/***************************************************************************
 *   Copyright (C) 2004 by 5Muses Software LLC                                   *
 *   info@5muses.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
    include_once("functions.php");
    include_once("db.php");
    include_once("session.php");
    include_once("prg_account.php");

    function create_archive () {
        @unlink("WebContent.mysql.gz");
        @unlink("upload.tgz");
        //$ret1 = syscall("mysqldump -v --no-create-info --user=admin --password=pavilion77 www_5muses_com WebContent p_itemcodes | gzip > WebContent.mysql.gz");
        $ret1 = syscall("mysqldump -v --no-create-info --user=mysql5mc --password=Vgy741MX 5mc_dfc WebContent p_itemcodes | gzip > WebContent.mysql.gz");
        $ret2 = syscall("tar -czvf upload.tgz *.js *.php *.css *.html images/*.jpg images/*.gif images/*.png WebContent.mysql.gz");
        return "$ret1\n$ret2";
    }

    function install_archive () {
        global $db_link;
        $ret1 = syscall("tar -xvzf upload.tgz --owner=support --group=muses");
        @mysqli_query($db_link, "DELETE FROM WebContent");
        @mysqli_query($db_link, "DELETE FROM p_itemcodes");
        $ret2 = syscall("cat WebContent.mysql.gz | gzip -d | mysql --user=mysql5mc --password=Vgy741MX -f 5mc_dfc");
        @unlink("WebContent.mysql.gz");
        @unlink("upload.tgz");
        return "$ret1\n$ret2";
    }

// +----------------------------------------------------------------------
// | Create and validate the current session
// +----------------------------------------------------------------------
    $Session = new fmSession;
    $Session->init('FMC_FlashCard');

// +----------------------------------------------------------------------
// | Initiate the Account Object
// +----------------------------------------------------------------------
    $Account = new fmAccount;
    $Account->init($Session->getID());

if ($Account->has_access(39)) {
    echo '<pre>';
    if ($_REQUEST['uploading'] == 'yeppers') {
        $uploaddir = '/var/www/htdocs/';
        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
        
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            echo "File is valid, and was successfully uploaded.\n";
        } else {
            echo "Possible file upload attack!\n";
            echo "Here is some more debugging info:\n";
            print_r($_FILES);
        }
    }
    echo '<form enctype="multipart/form-data" action="upload.php" method="POST" name=frmupload>
        <input type="hidden" name="uploading" value="yeppers" />
        <input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
        Send this file: <input name="userfile" type="file" />
        <input type="submit" value="Send File" />
        </form>';
    echo '<HR><form enctype="multipart/form-data" action="upload.php" method="POST" name=frminstall>
        <input type="hidden" name="uploading" value="install" />
        <input type="submit" value="Install Archive" />
        </form>';
    echo '<HR><form enctype="multipart/form-data" action="upload.php" method="POST" name=frmcreate>
        <input type="hidden" name="uploading" value="create" />
        <input type="submit" value="Create Archive" />
        </form>';
    echo "<HR>";
    if ($_REQUEST['uploading'] == 'create') {
        $html = create_archive();
    }
    if ($_REQUEST['uploading'] == 'install') {
        $html = install_archive();
    }
    echo "$html</pre>";
}

?>