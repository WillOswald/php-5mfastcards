<?PHP
//Version:0.71

/***************************************************************************
 *   Copyright (C) 2004 by 5Muses Software LLC                                   *
 *   info@5muses.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//
// PayPal IPN Interface
// +----------------------------------------------------------------------
// | General Includes
// +----------------------------------------------------------------------
    include_once('db.php');
    include_once('prg_records.php');
// +----------------------------------------------------------------------
// | Initiate the Accounting and Records Object
// +----------------------------------------------------------------------
    $pipn_Records = new fmRecords;
    //$Records->db_init('www_5muses_com');

// +----------------------------------------------------------------------
// | Debug Stuff
// +----------------------------------------------------------------------
/*    $pfh = fopen('pipn.log','ab');
    fputs($pfh,"------------\n");
    
    $line = '';
    foreach ($_REQUEST as $key => $value) {
        $line .= "$key=\"$value\",";
    }
    fputs($pfh,"$line\n");
    $line = '';
    $line .= "SERVER_NAME=\"{$_SERVER['SERVER_NAME']}\",";
    $line .= "REQUEST_METHOD=\"{$_SERVER['REQUEST_METHOD']}\",";
    $line .= "HTTP_USER_AGENT=\"{$_SERVER['HTTP_USER_AGENT']}\",";
    $line .= "REMOTE_HOST=\"{$_SERVER['REMOTE_HOST']}\",";
    $line .= "REMOTE_ADDR=\"{$_SERVER['REMOTE_ADDR']}\",";
    $line .= "REMOTE_PORT=\"{$_SERVER['REMOTE_PORT']}\",";
    $line .= "SERVER_PORT=\"{$_SERVER['SERVER_PORT']}\",";
    fputs($pfh,"$line\n");*/
// +----------------------------------------------------------------------
// | Generate Validation Request and copy Data to pipn_data
// +----------------------------------------------------------------------
    // read the post from PayPal system and add 'cmd'
    $req = 'cmd=_notify-validate';
    $pipn_data = array();
    foreach ($_POST as $key => $value) {
        $cvalue = urlencode(stripslashes($value));
        $req .= "&$key=$cvalue";
        $pipn_data["$key"] = $value;
    }
    $pipn_data["REQUEST_METHOD"] = $_SERVER['REQUEST_METHOD'];
    $pipn_data["HTTP_USER_AGENT"] = $_SERVER['HTTP_USER_AGENT'];
    $pipn_data["REMOTE_HOST"] = $_SERVER['REMOTE_HOST'];
    $pipn_data["REMOTE_ADDR"] = $_SERVER['REMOTE_ADDR'];
    $pipn_data["REMOTE_PORT"] = $_SERVER['REMOTE_PORT'];
    $pipn_data["SERVER_PORT"] = $_SERVER['SERVER_PORT'];
    $pipn_data["VALIDATED"] = '0';
    $pipn_data["CLEARED"] = '0';

// +----------------------------------------------------------------------
// | Validate Request 
// +----------------------------------------------------------------------
    // post back to PayPal system to validate
    $validate_status = 'INVALID';
    $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
    $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
    //$fp = fsockopen ('www.sandbox.paypal.com', 80, $errno, $errstr, 30);
    $fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
    if (!$fp) {
        $validate_status = 'Comm. Error';
    } else {
        fputs ($fp, $header . $req);
        $res = '';
        while (!feof($fp)) {
            // Might want a timeout function here and a sleep function...
            // Don't want a runaway script... Check NZProxy...
            $res .= fgets ($fp, 1024);
        }
        if (strpos($res, "VERIFIED") !== false) {
            $validate_status = 'VERIFIED';
        }
        if (strpos($res, "INVALID") !== false) {
            $validate_status = 'INVALID';
        }
        fclose ($fp);
    }
// +----------------------------------------------------------------------
// | Request Invalid, Take action and log...
// +----------------------------------------------------------------------
    if ($validate_status == 'INVALID') {
        // Invalid Request... Log and send e-mail
        $pipn_data["VALIDATED"] = '-1';
        $pipn_Records->post_transaction($pipn_data);
/*        $line = "Paypal Verification: Invalid\n";
        fputs($pfh,"$line\n");
        $line = "Paypal POST Data: $req\n";
        fputs($pfh,"$line\n");
        $line = "Paypal Response Data: $res\n";
        fputs($pfh,"$line\n");*/
    }
// +----------------------------------------------------------------------
// | Request Error... Take action and log... 
// +----------------------------------------------------------------------
    if ($validate_status == 'Comm. Error') {
        // Communications Error... Log and send e-mail... Verify again?
        $pipn_data["VALIDATED"] = '-2'; 
        $pipn_Records->post_transaction($pipn_data);
/*        $line = "Paypal Verification: Communications Error - ($errno) $errstr\n";
        fputs($pfh,"$line\n");*/
    }
// +----------------------------------------------------------------------
// | Request Valid, authenticate
// +----------------------------------------------------------------------
    if ($validate_status == 'VERIFIED') {
        // Authenticate request, check values etc...
        
/*        $line = "Paypal Verification: Verified\n";
        fputs($pfh,"$line\n");*/
        
        $auth = true;
        // check that txn_id has not been previously processed
        if ($pipn_Records->find_transaction($pipn_data['txn_id'])) { 
            $auth = false; 
/*            $line = "Paypal Authentification: txn_id exists.";
            fputs($pfh,"$line\n");*/
        }
        
        // check that receiver_email is your Primary PayPal email
        if ($pipn_data['receiver_email'] != 'paypal@5muses.com') { 
            $auth = false; 
/*            $line = "Paypal Authentification: receiver_email ({$pipn_data['receiver_email']}) != (testpaypal@5muses.org).";
            fputs($pfh,"$line\n");*/
        }
        
        // check that payment_amount/payment_currency are correct
        $item = $pipn_Records->find_item($pipn_data['item_number']);
        if ($item !== false) {
            $quantity = intval($pipn_data['quantity']);
            $gross = floatval($pipn_data['payment_gross']);
            $item_amount = floatval($item['item_amount']);
            
            if ( ($item_amount * $quantity) != $gross) {
                // Discrepancy in amount... log and flag...
                $auth = false;
/*                $line = "Paypal Authentification: item_amount($item_amount) * quantity($quantity) != payment_gross($gross)";
                fputs($pfh,"$line\n");*/
            }
        }
        
        // Check that the account is valid.
        $eaccount = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $pipn_data['custom']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $result = mysqli_query($db_link, "SELECT * from Account WHERE uSHA1='$eaccount'");
        if (mysqli_num_rows($result) <= 0) {
            // No account? Something's up...
            $auth = false;
/*            $line = "Paypal Authentification: Account not found... ($eaccount)";
            fputs($pfh,"$line\n");*/
        }
        @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        
/*        $line = "Paypal Authentification: Checking \$auth.";
        fputs($pfh,"$line\n");*/
        if ($auth) {
            // If we made it this far... then the IPN came from paypal
            // and has been authenticated.
/*            $line = "Paypal Authentification: Success.";
            fputs($pfh,"$line\n");
            $line = ">>>>> pipn_data Dump >>>>>".print_r($pipn_data,true)."<<<<< pipn_data Dump <<<<<";
            fputs($pfh,"$line\n");*/
            $pipn_data["VALIDATED"] = '1'; 
            $err = $pipn_Records->post_transaction($pipn_data);
            $logfile = fopen("pipn.log","ab");
            foreach($pipn_Records as $key => $item) {
                fputs($logfile,"$key|\"$item\",");
            }
            fputs($logfile,"\n");
            fclose($logfile);
/*            if ($err !== true) {
                $line = "post_transaction: $err.";
                fputs($pfh,"$line\n");
            }*/
            if ($pipn_data['payment_status'] == 'Completed') {
                // Transaction completed... add to purchases
/*                $line = "Status: Completed[Begin]";
                fputs($pfh,"$line\n");*/
                $account = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $pipn_data['custom']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $item_number = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $pipn_data['item_number']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $sql = "INSERT INTO p_purchases SET f_account='$account', f_item_number='$item_number', f_purchased=NOW(), ";
                $sql .= "f_received=NOW(), f_status='Completed', f_valid='1'";
                // Add to the purchases for this account.
                @mysqli_query($db_link, $sql);
/*                if (mysql_errno($db_link) != 0) {
                    $line .= "MySQL Error: (".mysql_errno($db_link)."): ".mysql_error($db_link)."\n";
                    fputs($pfh,"$line\n");
                }*/
            } 
            if ($pipn_data['payment_status'] == 'Denied') {
                // Transaction Denied! We have declined the transaction.
                // Add the record with a reason as to why we did this.
                //$account = mysql_escape_string($pipn_data['custom']);
                //$item_number = mysql_escape_string($pipn_data['item_number']);
                //$sql = "INSERT INTO p_purchases SET f_account='$account', f_item_number='$item_number', f_purchased=NOW(), ";
                //$sql .= "f_received=NOW(), f_status='Completed', f_valid='1'";
/*                $line = "Status: Denied[Begin]";
                fputs($pfh,"$line\n");*/
            } 
            if ($pipn_data['payment_status'] == 'Failed') {
                // Transaction Failed! Our customer has no money
                // Whoops! Add the record showing that the transaction failed. 
/*                $line = "Status: Failed[Begin]";
                fputs($pfh,"$line\n");*/
            } 
            if ($pipn_data['payment_status'] == 'Pending') {
                // Transaction Pending... eCheck or Foreign Currency
                // Add the record as pending so they know what's going on.
                // Set it pending...?
                //$account = mysql_escape_string($pipn_data['custom']);
                //$item_number = mysql_escape_string($pipn_data['item_number']);
                //$sql = "INSERT INTO p_purchases SET f_account='$account', f_item_number='$item_number', f_purchased=NOW(), ";
                //$sql .= "f_status='Pending', f_valid='0'";
/*                $line = "Status: Pending[Begin]";
                fputs($pfh,"$line\n");*/
            } 
            if ($pipn_data['payment_status'] == 'Refunded') {
                // Transaction Refunded... We gave them back their monies
                // Mark the record as refunded and close record.
/*                $line = "Status: Refunded[Begin]";
                fputs($pfh,"$line\n");*/
            } 
            if ($pipn_data['payment_status'] == 'Reversed') {
                // Transaction Reversed... DUnno what this means....
/*                $line = "Status: Reversed[Begin]";
                fputs($pfh,"$line\n");*/
            } 
        } else {
            // Something's wrong... notify admin.
            $pipn_data["VALIDATED"] = '-3'; 
            $pipn_Records->post_transaction($pipn_data);
/*            $line = "Paypal Authentification: Failure.";
            fputs($pfh,"$line\n");*/
        }
    } 
/*    $line = "--End Of Line--\n";
    fputs($pfh,"$line\n");
    fclose($pfh);*/
            // check the payment_status is Completed
            
            
            // process payment
/*
    // assign posted variables to local variables
    $item_name = $_REQUEST['item_name'];
    $item_number = $_REQUEST['item_number'];
    $payment_status = $_REQUEST['payment_status'];
    $payment_amount = $_REQUEST['mc_gross'];
    $payment_currency = $_REQUEST['mc_currency'];
    $txn_id = $_REQUEST['txn_id'];
    $receiver_email = $_REQUEST['receiver_email'];
    $payer_email = $_REQUEST['payer_email'];
    
txn_type="web_accept",
payment_date="16:09:26 Feb 28, 2005 PST",
last_name="Holland",
item_name="Spanish Dynamic Fast Cards - 1 Month Access",
payment_gross="9.95",
mc_currency="USD",
business="testpaypal@5muses.org",
payment_type="instant",
payer_status="unverified",
verify_sign="AGcS7BAAfh3hgM8zLQ6ic4smZQ3cA.uhLL3tOxXlXezSsrBhuGabWuc1",
test_ipn="1",
payer_email="anheuser50@yahoo.com",
tax="0.00",
txn_id="6YE719769R2787546",
receiver_email="testpaypal@5muses.org",
quantity="1",
first_name="Chris",
payer_id="LS9Z5C5M7VAH2",
receiver_id="HFDEQ63TU88F6",
item_number="402001",
payment_status="Completed",
payment_fee="0.59",
mc_fee="0.59",
mc_gross="9.95",
custom="629d7d6284e66e37d5599a5281b8069d6b935123",
notify_version="1.6",
    }
*/
?>
