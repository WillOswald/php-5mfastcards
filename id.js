// =====================================================================
// ===== Resolution Detection
// =====================================================================

    // +----------------------------------------------------------------------
    // | rpcSend Funtion
    // +----------------------------------------------------------------------
    function rpcSend(rpcData) {
        oldImage = document.getElementById("rpcSendImage");
        if (oldImage) {
            oldParent = oldImage.parentNode;
            oldParent.removeChild(oldImage);
            oldImage = null;
        }
        image = document.createElement('img');
        image.setAttribute('src','jsrpc.php?'+rpcData);
        image.setAttribute('id','rpcSendImage');
        image.setAttribute('width','0');
        image.setAttribute('height','0');
        //image.style.display = "none";
        document.getElementsByTagName('body').item(0).appendChild(image);
    }

    // +----------------------------------------------------------------------
    // | Identify Browser and Screen Resolution
    // +----------------------------------------------------------------------
    function idBrowser() {
        sres = screen.width+'x'+screen.height+'x'+screen.colorDepth;
        cres = window.innerWidth+'x'+window.innerHeight;
        bres = window.outerWidth+'x'+window.outerHeight;
        agent = navigator.userAgent;
        udata = sres+'|'+bres+'|'+cres+'|'+agent;
        rpcSend("act=set&rpcSend=true&element=lgData&value="+udata);
    }
    
