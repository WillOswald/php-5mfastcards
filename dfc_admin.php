<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//


//   _____________________________________________________
//   ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
//   DFC Admin Page
//   _____________________________________________________
//   ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
    

#   +----------------------------------------------------
#   | Initialize Session Information
#   +----------------------------------------------------
    include_once("db.php");
    include_once("session.php");
    include_once("prg_account.php");
    include_once("5mc_reports.php");
    include_once("5mc_todo.php");
    include_once("commboard.php");
    //include_once("prg_lesson.php");
    //include_once("prg_vocabulary.php");
    //include_once("prg_records.php");
    //include_once("prg_flash.php");
    //include_once("main_lib.php");

    db_sanity();
    
    
    // Major Debug here

    $AdminSession = new fmSession;
    $Timer->start('session');
    $AdminSession->init('FMC_AdminPanel');
    $Timer->stop('session');
        
    $AdminAccount = new fmAccount;
    $AdminAccount->init($AdminSession->getID());
                
    $Reports = new fmcReports;
    
    $nav1dec = array(
            '1000' => 'Web Page / Main Home Page',
            '1100' => 'Web Page / Latest News',
            '1110' => 'Web Page / About The Company',
            '1120' => 'Web Page / 5m History',
            '1130' => 'Web Page / Contact Us',
            '1140' => 'Web Page / Learn Spanish',
            '1150' => 'Web Page / Learn French',
            '1160' => 'Web Page / Learn German',
            '1170' => 'Web Page / Support',
            '1180' => 'Web Page / Vocabulary',
            '1190' => 'Web Page / Tutorial',
            '1200' => 'Web Page / 400 Free',
            '1210' => 'Web Page / Purchasing',
            '1999' => 'Web Page / Site Map',
            '2000' => 'Fast Cards',
            '2100' => 'Fast Cards / Introduction',
            '2105' => 'Fast Cards / Tutorial',
            '2110' => 'Fast Cards / Pre-Made Lessons',
            '2120' => 'Fast Cards / Purchasing',
            '2130' => 'Fast Cards / Word Database',
            '2140' => 'Fast Cards / Lesson Builder',
            '2150' => 'Fast Cards / Show Flash Cards',
            '2151' => 'Fast Cards / Show Flash Cards (running)',
            '3000' => 'Account',
            '3100' => 'Account / Overview',
            '3110' => 'Account / Billing History',
            '3120' => 'Account / Edit Information',
            '4000' => 'Support',
            '4100' => 'Support / Checklist',
            '4110' => 'Support / Common Questions',
            '4120' => 'Support / Send Feedback',
            '5000' => 'Admin Panel - Authorizing',
            '5100' => 'Admin Panel',
            '5110' => 'Admin Panel - Users Online',
            '5400' => 'Admin Panel - Reports',
            '5401' => 'Admin Panel - Reports - Monthly Visits',
            '5120' => 'Admin Panel - View Logs',
            '5210' => 'Admin Panel - [DB] Session',
            '5220' => 'Admin Panel - [DB] Account',
            '5230' => 'Admin Panel - [DB] Records',
            '5240' => 'Admin Panel - [DB] Transactions',
            '5300' => 'Admin Panel - Monitoring Session',
            );

    $langDec = array(
        '2' => 'Spanish',
        '3' => 'German',
        '4' => 'French');
        
    $todoDec = array(
        '10' => 'Critical,#F00',
        '20' => 'Important,#F80',
        '30' => 'High,#FF0',
        '40' => 'Medium,#0F0',
        '50' => 'Normal,#CCC',
        '60' => 'Low,#888',
        '100' => 'None,#444');    

    $todoDec2 = array(
        '1' => '<SPAN STYLE="color:#F00">1</SPAN> 2 3 4 5',
        '2' => '1 <SPAN STYLE="color:#F80">2</SPAN> 3 4 5',
        '3' => '1 2 <SPAN STYLE="color:#FF0">3</SPAN> 4 5',
        '4' => '1 2 3 <SPAN STYLE="color:#CCC">4</SPAN> 5',
        '5' => '1 2 3 4 <SPAN STYLE="color:#888">5</SPAN>');
        
#   +----------------------------------------------------
#   | Top HTML / Header
#   +----------------------------------------------------
?>
        <HEAD><TITLE>5Muses.com Admin Panel</TITLE></HEAD>
<?PHP        
    if ($AdminSession->get('/log/logstate') != 'complete') {
        echo '<BODY STYLE="background-color:black; color:white;padding:10px 10px 10px 10px;" onLoad="idBrowser();">';
    } else {
        echo '<BODY STYLE="background-color:black; color:white;padding:10px 10px 10px 10px;">';
    }
?>
        <STYLE TYPE="text/css">
            a {
                text-decoration:none;
                color:#ccc;
            }
            a:hover {
                text-decoration:none;
                color:#F84;
            }
            img {
                border: none;
            }
            div.header {
                border-bottom:1px solid #888;
                border-top:1px solid #888;
                margin:0px 0px 20px 0px;
                padding:5px 5px 5px 5px;
            }
            div.headeri {
                border-bottom:1px solid #444;
                border-top:1px solid #444;
                padding:10px 10px 10px 10px;
                font-weight:normal;
                color:#ccc;
                font-size:150%;
                text-transform:uppercase;
                letter-spacing:.2em;
                line-height:1.2em;
                text-align:center;
            }
            div.header:hover {
                border-bottom:1px solid #88F;
                border-top:1px solid #88F;
            }
            div.headeri:hover {
                border-bottom:1px solid #448;
                border-top:1px solid #448;
                color:#ccf;
            }
            div.nav {
                border-bottom:1px solid #888;
                border-top:1px solid #888;
                margin:0px 0px 3px 0px;
                padding:3px 3px 3px 3px;
                width:200px;
            }
            div.nav:hover {
                border-bottom:1px solid #88F;
                border-top:1px solid #88F;
            }
            div.navi {
                border-bottom:1px solid #444;
                border-top:1px solid #444;
                padding:3px 3px 3px 3px;
                font-weight:normal;
                color:#ccc;
                font-size:80%;
                text-transform:uppercase;
                letter-spacing:.2em;
                line-height:1.2em;
                text-align:center;
            }
            div.navi:hover {
                border-bottom:1px solid #448;
                border-top:1px solid #448;
                color:#CCF;
            }
            div.navit {
                border-bottom:1px solid #444;
                border-top:1px solid #444;
                padding:3px 3px 3px 3px;
                font-weight:normal;
                color:#ccc;
                font-size:60%;
                text-transform:uppercase;
                letter-spacing:.2em;
                line-height:1.8em;
            }
            div.navit:hover {
                border-bottom:1px solid #448;
                border-top:1px solid #448;
                color:#CCF;
            }
            div.navc {
                border-bottom:1px solid #888;
                border-top:1px solid #888;
                padding:3px 3px 3px 3px;
                margin:0px 0px 0px 13px;
                font-weight:normal;
                color:#ccc;
                letter-spacing:.2em;
                line-height:1.2em;
                height:600px;
            }
            div.navc:hover {
                border-bottom:1px solid #88F;
                border-top:1px solid #88F;
                color:#CCF;
            }
            div.navci {
                border-bottom:1px solid #444;
                border-top:1px solid #444;
                padding:6px 6px 6px 6px;
                font-weight:normal;
                color:#ccc;
                font-size:60%;
                letter-spacing:.2em;
                line-height:1.8em;
                overflow: auto;
                height:586px;
            }
            div.uinfo {
                border:1px solid #88F;
                margin:6px 6px 6px 6px;
                font-weight:normal;
                font-size:100%;
                color:#ccc;
                letter-spacing:.1em;
                line-height:1.8em;
                float:left;
            }
            tr.uinfot {
                background-color:#224;
                border-bottom:2px solid #448;
                font-weight:normal;
                color:#ccf;
                font-size:80%;
                letter-spacing:.25em;
                line-height:1.4em;
            }
            td.uinfoc {
                border-bottom:1px dashed #448;
                padding:0px 4px 0px 4px;
                font-weight:normal;
                font-size:70%;
                color:#aaf;
                letter-spacing:0em;
                line-height:1.3em;
                text-align:right;
            }
            td.uinfod {
                border-bottom:1px dashed #448;
                padding:0px 4px 0px 4px;
                font-weight:normal;
                font-size:70%;
                color:#ccf;
                letter-spacing:.1em;
                line-height:1.3em;
            }
            span.title {
                font-size:160%;
                letter-spacing:.2em;
                line-height:1.8em;
            }
            span.dark {
                color:#448;
            }
            div.navci:hover {
                border-bottom:1px solid #448;
                border-top:1px solid #448;
                /*color:#CCF;*/
            }
            td {
                font-size:100%;
            }
            td.auth {
                color:#CCC;
                font-size:60%;
            }
            td.resc {
                padding:1px 5px 1px 5px;
                border-top:1px solid #AAA;
                border-left:1px solid #AAA;
                border-right:1px solid #444;
                border-bottom:1px solid #444;
                background-color:#777;
                color:#EEE;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.link {
                color:#EEE;
                cursor: pointer;
            }
            td.link:hover {
                background-color:#77D;
                color:#FFF;
                cursor: hand;
            }
            td.resd {
                padding:1px 5px 1px 5px;
                border-right:1px dashed #888;
                border-bottom:1px dashed #888;
                color:#CCC;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.resdh {
                padding:3px 5px 3px 5px;
                border-right:1px dashed #888;
                border-left:1px dashed #888;
                border-bottom:1px dashed #888;
                color:#CCC;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.task1 {
                background-color:#223;
                padding:3px 5px 3px 5px;
                border-top:1px solid #888;
                color:#CCC;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.task2 {
                background-color:#223;
                padding:3px 5px 3px 5px;
                border-top:1px dashed #444;
                color:#555;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.task3 {
                padding:3px 5px 3px 5px;
                border-top:1px solid #888;
                font-size:60%;
            }
            .taskd {
                color:#CCC;
                font-size:10pt;
                letter-spacing:.15em;
            }
            td.ctask1 {
                background-color:#232;
                padding:3px 5px 3px 5px;
                border-top:1px solid #888;
                color:#CCC;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.ctask2 {
                background-color:#232;
                padding:3px 5px 3px 5px;
                border-top:1px dashed #444;
                color:#555;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.message1 {
                background-color:#222;
                padding:3px 5px 3px 5px;
                color:#888;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.message2 {
                background-color:#222;
                padding:3px 5px 3px 5px;
                color:#888;
                font-size:60%;
                letter-spacing:.2em;
            }
            td.message3 {
                background-color:#444;
                padding:3px 5px 3px 5px;
                color:#CCC;
                border-top:1px solid #888;
                border-bottom:1px solid #888;
                font-size:60%;
                letter-spacing:.2em;
            }
            .code {
                background-color:#222;
                padding:5px 5px 5px 5px;
                margin:15px 15px 15px 15px;
                color:#888;
                font-family: fixed, terminal, courier new;
                border:1px solid #888;
                font-size:10pt;
                letter-spacing:0em;
                line-height:1.3em;
            }
            .resex {
                display:none;
                padding:1px 5px 1px 5px;
                border-right:1px dashed #888;
                border-bottom:1px dashed #888;
                color:#CCC;
                font-size:60%;
                letter-spacing:.2em;
            }
            input.t1 {
                border:1px solid #444;
                background-color:black;
                font-size:80%;
                color:#888
            }
            option.t1 {
                border:1px solid #444;
                background-color:black;
                font-size:80%;
                color:#888
            }
            textarea.t1 {
                border:1px solid #444;
                background-color:black;
                font-size:10pt;
                font-family: sans-serif;
                color:#888;
            }
            input.s1 {
                border-top:1px solid #AAA;
                border-left:1px solid #AAA;
                border-right:1px solid #444;
                border-bottom:1px solid #444;
                background-color:#777;
                color:#888
                letter-spacing:.2em;
                font-size:100%;
            }
            ul {
                margin: 0px 0px 0px 0px;
            }
        </STYLE>
        <SCRIPT TYPE="text/javascript">
            function toggle(elem) {
                obj = document.getElementById(elem);
                if (obj.style.display == "table-row") { 
                    obj.style.display = "none";
                } else {
                    obj.style.display = "table-row"; 
                }
            }
            // +----------------------------------------------------------------------
            // | rpcSend Funtion
            // +----------------------------------------------------------------------
            function rpcSend(rpcData) {
                oldImage = document.getElementById("rpcSendImage");
                if (oldImage) {
                    oldParent = oldImage.parentNode;
                    oldParent.removeChild(oldImage);
                    oldImage = null;
                }
                image = document.createElement('img');
                image.setAttribute('src',rpcData);
                image.setAttribute('id','rpcSendImage');
                image.setAttribute('width','0');
                image.setAttribute('height','0');
                //image.style.display = "none";
                document.getElementsByTagName('body').item(0).appendChild(image);
            }
        
            // +----------------------------------------------------------------------
            // | Grab the information
            // +----------------------------------------------------------------------
            function idBrowser() {
                sres = screen.width+'x'+screen.height+'x'+screen.colorDepth;
                cres = window.innerWidth+'x'+window.innerHeight;
                bres = window.outerWidth+'x'+window.outerHeight;
                agent = navigator.userAgent;
                udata = sres+'|'+bres+'|'+cres+'|'+agent;
                rpcSend("dfc_log.php?act=set&rpcSend=true&element=lgData&value="+udata);
            }
        </SCRIPT>
<?PHP
    echo "<DIV CLASS=\"header\">";
    echo "<DIV CLASS=\"headeri\">";
    echo "5Muses.com Admin Panel";
    echo "</DIV>";
    echo "</DIV>";
    
#   +----------------------------------------------------
#   | Process Requests
#   +----------------------------------------------------
    if (isset($_REQUEST['act'])) {
        // Got an action....
        $act = $_REQUEST['act'];
        if ($act == 'login') {
            $email = $_REQUEST['alogin'];
            $pass = $_REQUEST['apass'];
            $AdminAccount->login($email,$pass,$AdminSession->getID());
            $AdminSession->post("/w2/n/np",5100);
        }
    }
    if ($AdminAccount->has_access(39)) {
        if (isset($_REQUEST['act'])) {
            // Got an action....
            $act = $_REQUEST['act'];
            if ($act == 'uo') {
                $AdminSession->post("/w2/n/np",5110);
                $AdminSessionSort = "sCreated";
                $uo_sort = $_REQUEST['uoSort'];
                if ($uo_sort == 'id') $AdminSessionSort = 'sID';
                if ($uo_sort == 'fn') $AdminSessionSort = 'sFName';
                if ($uo_sort == 'ln') $AdminSessionSort = 'sLName';
                if ($uo_sort == 'em') $AdminSessionSort = 'sEmail';
                if ($uo_sort == 'on') $AdminSessionSort = 'sCreated';
                // Calculate the Users that are online...
                $pl = '';
                $AdminSession->post("/nav/t1=uo");
                $result = mysqli_query($db_link, "SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE NOW() <= sExpires ORDER BY $AdminSessionSort DESC");
                if ($result !== false) {
                    // Get all the Sessions...
                    $OnlineSessions = array();
                    while ($row = mysqli_fetch_assoc($result)) {
                        $data = array();
                        //$pl .= $row['sSessionID']."<BR>";
                        foreach($row as $field => $value) { $data[$field] = $value; }
                        $OnlineSessions[] = $data;
                    }
                    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                    // Get Account Info For Each Session...
                    reset($OnlineSessions);
                    for($i=0;$i<count($OnlineSessions);$i++) {
                        $conn = $OnlineSessions[$i];
                        $sid = $conn['sSessionID'];
                        $result = mysqli_query($db_link, "SELECT * FROM Account WHERE uSessionID='$sid'");
                        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                            $conn['acct'] = 1;
                            $row = mysqli_fetch_assoc($result);
                            $conn['FName'] = $row['uFName'];
                            $conn['LName'] = $row['uLName'];
                            $conn['Addr'] = $row['uAddress1'];
                            $conn['City'] = $row['uCity'];
                            $conn['State'] = $row['uState'];
                            $conn['Zip'] = $row['uZip'];
                            $conn['Visits'] = $row['uVisits'];
                            $conn['Country'] = $row['uCountry'];
                            $conn['Province'] = $row['uProvince'];
                            $conn['AcctCreated'] = $row['uCreated'];
                            $conn['EMail'] = $row['uEmail'];
                            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                            $OnlineSessions[$i] = $conn;
                        } else {
                            $conn = $OnlineSessions[$i];
                            $conn['acct'] = 0;
                            $conn['FName'] = '---';
                            $conn['LName'] = '---';
                            $conn['EMail'] = '---';
                            $OnlineSessions[$i] = $conn;
                        }
                    }
                    // Re-Sort the $OnlineSessions...
                    // Create the Table?
                    $pl .= "<SPAN CLASS=TITLE>Members Logged In</SPAN><BR>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    reset($OnlineSessions);
                    // Headers
                    $pl .= "<TR>";
                    $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=uo&uoSort=id'\">ID</TD>";
                    $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=uo&uoSort=fn'\">Clicks</TD>";
                    $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=uo&uoSort=fn'\">First Name</TD>";
                    $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=uo&uoSort=ln'\">Last Name</TD>";
                    $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=uo&uoSort=em'\">E-Mail Address</TD>";
                    $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=uo&uoSort=em'\">Visits</TD>";
                    $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=uo&uoSort=on'\">Online</TD>";
                    $pl .= "</TR>";
                    foreach ($OnlineSessions as $conn) {
                        if ($conn['acct'] == 1) {
                            // Show the Logged In Users First...
                            $age = $conn['Age'];
                            $pl .= "<TR onMouseOver=\"this.style.backgroundColor='#224';\" onMouseOut=\"this.style.backgroundColor='transparent';\" onClick=\"toggle('rx{$conn['sID']}');\">";
                            $userData = unserialize(base64_decode($conn['sSessionData']));
                            $logID = $userData['log']['logid'];
                            $logState = $userData['log']['logstate'];
                            // Log Information
                            $result = mysqli_query($db_link, "SELECT *,INET_NTOA(ip_address) as ip_addr FROM log_enter WHERE logID='$logID'");
                            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                $row = mysqli_fetch_assoc($result);
                                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                                $ip = $row['ip_addr'];
                                $port = $row['ip_port'];
                                $sres = $row['s_res_x'].'x'.$row['s_res_y'].' '.$row['s_res_d'].'bit';
                                $cres = $row['c_res_x'].'x'.$row['c_res_y'];
                                $bid = $row['browser_id'];
                                $oid = $row['os_id'];
                                $mid = $row['moz_id'];
                                $rid = $row['refered'];
                                $clicks = $row['clicks'];
                                if ((intval($row['s_res_x']) < 1280) and (intval($row['s_res_y']) < 1024) and (intval($row['s_res_d']) < 15)) {
                                    $sres_warn = '<IMG BORDER=0 SRC=images/warning.gif>';
                                } else {
                                    $sres_warn = '';
                                }
                            }
                            //$ipName = @gethostbyaddr($ip);
                            //if ($ipName == $ip) $ipName = $ip;
                            $ipName = $Reports->dnsHostname($ip);
                            $result = mysqli_query($db_link, "SELECT * FROM log_browser WHERE ID='$bid'");
                            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                $row = mysqli_fetch_assoc($result);
                                $browser = $row['browser']." ".$row['ver'];
                                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                            }
                            $result = mysqli_query($db_link, "SELECT * FROM log_os WHERE ID='$oid'");
                            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                $row = mysqli_fetch_assoc($result);
                                $os = $row['os'];
                                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                            }
                            $result = mysqli_query($db_link, "SELECT * FROM log_refer WHERE ID='$rid'");
                            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                $row = mysqli_fetch_assoc($result);
                                $rDomain = $row['domain'];
                                $rArgv = $row['argv'];
                                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                            }
    
                            if ($sres == '0x0 0bit') $sres = '<SPAN CLASS=dark>Unavailable</SPAN>';
                            if ($cres == '0x0') $cres = '<SPAN CLASS=dark>Unavailable</SPAN>';
                            if ($rid == 0) {
                                $referer = '<SPAN CLASS=dark>Unavailable</SPAN>';
                            } else {
                                $referer = "<A HREF=\"http://$rDomain/$rArgv\">$rDomain</A>";
                            }
                            $nav = strval($userData['w2']['n']['np']);
                            $navPlace = $nav1dec[$nav];
                            list ($vSearch,$vBrowse,$vSearchMode,$vFilter,$vLang,$vDBPage,$vDBCount,$vDBTotal,$vDBPages) = explode('|',$userData['w2']['v']);
                            list ($lName,$lSavedName,$lPos,$lRepeat,$lLang,$lWords) = explode('|',$userData['w2']['l']);
                            list ($fVolume,$fSize,$fTheme,$fCycle,$fRepeat,$fDelay,$fFormat,$flPos,$flWordCount,$flRepeat,$flCycle,$flDelay,$flAudioLength) = explode('|',$userData['w2']['f']);
                            
                            if ($AdminSession->getID() == $conn['sSessionID']) {
                                $pl .= "<TD CLASS=resc STYLE=\"color:yellow;\" WIDTH=80>{$conn['sID']}*&nbsp;</TD>";
                            } else {
                                $pl .= "<TD CLASS=resc WIDTH=80>{$conn['sID']}&nbsp;</TD>";
                            }
                            $pl .= "<TD CLASS=resd>$clicks&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd>{$conn['FName']}&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd>{$conn['LName']}&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd>{$conn['EMail']}&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd>{$conn['Visits']}&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd WIDTH=100>$age&nbsp;</TD>";
                            $pl .= "</TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}>";
                            $pl .= "<TD CLASS=resdh COLSPAN=7>";
    
                            //$pl .= "<DIV CLASS=uinfo><DIV CLASS=uinfot>CPU Indetification</DIV>";
                            $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}cp');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Computer Indetification</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}cp><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>IP Address:</TD><TD CLASS=uinfod>$ip:$port</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Hostname:</TD><TD CLASS=uinfod>$ipName</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Referer:</TD><TD CLASS=uinfod>$refer</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Browser:</TD><TD CLASS=uinfod>$browser</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>OS:</TD><TD CLASS=uinfod>$os</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Screen Res:</TD><TD CLASS=uinfod>$sres$sres_warn</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Browser Res:</TD><TD CLASS=uinfod>$cres</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Logging:</TD><TD CLASS=uinfod>$logState ($logID)</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Cookie Name:</TD><TD CLASS=uinfod>{$userData['log']['cookie']}&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>SessionID:</TD><TD CLASS=uinfod>{$conn['sSessionID']}&nbsp;<A HREF=?act=mn&mnss={$conn['sSessionID']}>[MONITOR]</A></TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // Contact Info
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}ci');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Contact Information</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}ci><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Name:</TD><TD CLASS=uinfod>{$conn['FName']} {$conn['LName']}</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Address:</TD><TD CLASS=uinfod>{$conn['Addr']}</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>&nbsp;</TD><TD CLASS=uinfod>{$conn['City']},&nbsp;{$conn['State']}&nbsp;&nbsp;{$conn['Zip']}</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Country:</TD><TD CLASS=uinfod>{$conn['Country']}&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Province:</TD><TD CLASS=uinfod>{$conn['Province']}&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Visits:</TD><TD CLASS=uinfod>{$conn['Visits']}</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // Nav Location
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}na');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Navigation</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}na><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Web Page Place:</TD><TD CLASS=uinfod>$navPlace&nbsp;</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // Vocabulary Info...
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}vo');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Vocabulary</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}vo><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                if ($vFilter == 'true') {
                                    if ($vSearchMode == 'true') {
                                        $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Searching for '$vSearch'</TD</TR>";
                                    } else {
                                        $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Browsing by '$vBrowse'</TD</TR>";
                                    }
                                } else {
                                    $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Browse All</TD</TR>";
                                }
                                $lang = $langDec["$vLang"];
                                $pl .= "<TR><TD CLASS=uinfoc>Language:</TD><TD CLASS=uinfod>$lang</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>SQL Page:</TD><TD CLASS=uinfod>$vDBPage of $vDBPages</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>SQL Count:</TD><TD CLASS=uinfod>$vDBCount of $vDBTotal</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // Lesson Info...
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}le');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Lesson</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}le><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $lang = $langDec["$lLang"];
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Language:</TD><TD CLASS=uinfod>$lang</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Name:</TD><TD CLASS=uinfod>$lName&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Saved Name:</TD><TD CLASS=uinfod>$lSavedName&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Pos / Repeat:</TD><TD CLASS=uinfod>$lPos / $lRepeat</TD</TR>";
                                $words = unserialize(base64_decode($lWords));
                                $wordCount = count($words);
                                $pl .= "<TR><TD CLASS=uinfoc>Words:</TD><TD CLASS=uinfod>$wordCount</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // FastCard Info...
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}fc');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Dynamic Fast Card Program</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}fc><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $lang = $langDec["$lLang"];
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Volume:</TD><TD CLASS=uinfod>$fVolume%</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Text Size:</TD><TD CLASS=uinfod>$fSize&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Theme:</TD><TD CLASS=uinfod>$fTheme&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Lesson Cycle:</TD><TD CLASS=uinfod>$fCycle&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Word Repeat:</TD><TD CLASS=uinfod>$fRepeat&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Speed (0-5):</TD><TD CLASS=uinfod>".sprintf("%01.1f",(5000-$fDelay)/1000)."&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Audio Format:</TD><TD CLASS=uinfod>$fFormat&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Pos (Lesson):</TD><TD CLASS=uinfod>$flPos&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Words (Lesson):</TD><TD CLASS=uinfod>$flWordCount&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Word Repeat (Lesson):</TD><TD CLASS=uinfod>$flRepeat&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Lesson Cycle (Lesson):</TD><TD CLASS=uinfod>$flCycle&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Speed (0-5) (Lesson):</TD><TD CLASS=uinfod>".sprintf("%01.1f",(5000 - floatval($flDelay))/1000)."&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Audio Length (Lesson):</TD><TD CLASS=uinfod>".sprintf("%01.1f",floatval($flAudioLength)/1000)."&nbsp;</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            $pl .= "</TABLE>";
                            
                            $pl .= "</TD>";
                            $pl .= "</TR>";
                        }
                    }
                    $pl .= "</TABLE>";
                    $pl .= "<BR><SPAN CLASS=TITLE>Guest Visitors</SPAN><BR>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    reset($OnlineSessions);
                    // Headers
                    $pl .= "<TR>";
                    $pl .= "<TD CLASS=resc>ID</TD>";
                    $pl .= "<TD CLASS=resc>Clicks</TD>";
                    $pl .= "<TD CLASS=resc>Hostname</TD>";
                    $pl .= "<TD CLASS=resc>Location</TD>";
                    $pl .= "<TD CLASS=resc>Online</TD>";
                    $pl .= "</TR>";
                    foreach ($OnlineSessions as $conn) {
                        if ($conn['acct'] == 0) {
                            // Show the Visiting Guests
                            $age = $conn['Age'];
                            $otime = sprintf("%dd %02d:%02d:%02d",$day,$hour,$min,$sec);
                            $userData = unserialize(base64_decode($conn['sSessionData']));
                            $logID = $userData['log']['logid'];
                            $logState = $userData['log']['logstate'];
                            $result = mysqli_query($db_link, "SELECT *,INET_NTOA(ip_address) as ip_addr FROM log_enter WHERE logID='$logID'");
                            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                $row = mysqli_fetch_assoc($result);
                                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                                $ip = $row['ip_addr'];
                                $port = $row['ip_port'];
                                $sres = $row['s_res_x'].'x'.$row['s_res_y'].' '.$row['s_res_d'].'bit';
                                $cres = $row['c_res_x'].'x'.$row['c_res_y'];
                                $bid = $row['browser_id'];
                                $oid = $row['os_id'];
                                $mid = $row['moz_id'];
                                $rid = $row['refered'];
                                $clicks = $row['clicks'];
                                if ((intval($row['s_res_x']) < 1280) and (intval($row['s_res_y']) < 1024) and (intval($row['s_res_d']) < 15)) {
                                    $sres_warn = '<IMG BORDER=0 SRC=images/warning.gif>';
                                } else {
                                    $sres_warn = '';
                                }
                            }
                            $result = mysqli_query($db_link, "SELECT * FROM log_browser WHERE ID='$bid'");
                            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                $row = mysqli_fetch_assoc($result);
                                $browser = $row['browser']." ".$row['ver'];
                                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                            }
                            $result = mysqli_query($db_link, "SELECT * FROM log_os WHERE ID='$oid'");
                            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                $row = mysqli_fetch_assoc($result);
                                $os = $row['os'];
                                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                            }
                            $result = mysqli_query($db_link, "SELECT * FROM log_refer WHERE ID='$rid'");
                            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                $row = mysqli_fetch_assoc($result);
                                $rDomain = $row['domain'];
                                $rArgv = $row['argv'];
                                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                            }
    
                            $ipName = $Reports->dnsHostname($ip);
                            //$ipName = @gethostbyaddr($ip);
                            //if ($ipName == $ip) $ipName = $ip;
                            if ($sres == '0x0 0bit') $sres = '<SPAN CLASS=dark>Unavailable</SPAN>';
                            if ($cres == '0x0') $cres = '<SPAN CLASS=dark>Unavailable</SPAN>';
                            if ($rid == 0) {
                                $referer = '<SPAN CLASS=dark>Unavailable</SPAN>';
                            } else {
                                $referer = "<A HREF=\"http://$rDomain/$rArgv\">$rDomain</A>";
                            }
                            if ($ipName == $ip) $ipName = '<SPAN CLASS=dark>Unavailable</SPAN>';
                            $nav = strval($userData['w2']['n']['np']);
                            $navPlace = $nav1dec[$nav];
                            list ($vSearch,$vBrowse,$vSearchMode,$vFilter,$vLang,$vDBPage,$vDBCount,$vDBTotal,$vDBPages) = explode('|',$userData['w2']['v']);
                            list ($lName,$lSavedName,$lPos,$lRepeat,$lLang,$lWords) = explode('|',$userData['w2']['l']);
                            list ($fVolume,$fSize,$fTheme,$fCycle,$fRepeat,$fDelay,$fFormat,$flPos,$flWordCount,$flRepeat,$flCycle,$flDelay,$flAudioLength) = explode('|',$userData['w2']['f']);
                            // Output the Collected Data
                            $pl .= "<TR onMouseOver=\"this.style.backgroundColor='#224';\" onMouseOut=\"this.style.backgroundColor='transparent';\" onClick=\"toggle('rx{$conn['sID']}');\">";
                            if ($AdminSession->getID() == $conn['sSessionID']) {
                                $pl .= "<TD CLASS=resc STYLE=\"color:yellow;\" WIDTH=80>{$conn['sID']}*&nbsp;</TD>";
                            } else {
                                $pl .= "<TD CLASS=resc WIDTH=80>{$conn['sID']}&nbsp;</TD>";
                            }
                            $pl .= "<TD CLASS=resd>$clicks&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd>$ipName&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd NOWRAP>$navPlace&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd WIDTH=100>$age&nbsp;</TD>";
                            $pl .= "</TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}>";
                            $pl .= "<TD CLASS=resdh COLSPAN=5>";
                            $userData = unserialize(base64_decode($conn['sSessionData']));
                            $logID = $userData['log']['logid'];
                            $logState = $userData['log']['logstate'];
                            // Log Information
    
                            //$pl .= "<DIV CLASS=uinfo><DIV CLASS=uinfot>CPU Indetification</DIV>";
                            $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}cp');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Computer Indetification</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}cp><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>IP Address:</TD><TD CLASS=uinfod>$ip:$port</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Hostname:</TD><TD CLASS=uinfod>$ipName</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Referer:</TD><TD CLASS=uinfod>$referer</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Browser:</TD><TD CLASS=uinfod>$browser</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>OS:</TD><TD CLASS=uinfod>$os</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Screen Res:</TD><TD CLASS=uinfod>$sres$sres_warn</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Browser Res:</TD><TD CLASS=uinfod>$cres</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Logging:</TD><TD CLASS=uinfod>$logState ($logID)</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Cookie Name:</TD><TD CLASS=uinfod>{$userData['log']['cookie']}&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>SessionID:</TD><TD CLASS=uinfod>{$conn['sSessionID']}&nbsp;<A HREF=?act=mn&mnss={$conn['sSessionID']}>[MONITOR]</A></TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // Nav Location
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}na');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Navigation</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}na><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Web Page Place:</TD><TD CLASS=uinfod>$navPlace&nbsp;</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // Vocabulary Info...
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}vo');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Vocabulary</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}vo><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                if ($vFilter == 'true') {
                                    if ($vSearchMode == 'true') {
                                        $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Searching for '$vSearch'</TD</TR>";
                                    } else {
                                        $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Browsing by '$vBrowse'</TD</TR>";
                                    }
                                } else {
                                    $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Browse All</TD</TR>";
                                }
                                $lang = $langDec["$vLang"];
                                $pl .= "<TR><TD CLASS=uinfoc>Language:</TD><TD CLASS=uinfod>$lang</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>SQL Page:</TD><TD CLASS=uinfod>$vDBPage of $vDBPages</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>SQL Count:</TD><TD CLASS=uinfod>$vDBCount of $vDBTotal</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // Lesson Info...
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}le');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Lesson</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}le><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $lang = $langDec["$lLang"];
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Language:</TD><TD CLASS=uinfod>$lang</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Name:</TD><TD CLASS=uinfod>$lName&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Saved Name:</TD><TD CLASS=uinfod>$lSavedName&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Pos / Repeat:</TD><TD CLASS=uinfod>$lPos / $lRepeat</TD</TR>";
                                $words = unserialize(base64_decode($lWords));
                                $wordCount = count($words);
                                $pl .= "<TR><TD CLASS=uinfoc>Words:</TD><TD CLASS=uinfod>$wordCount</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            // FastCard Info...
                            $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}fc');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Dynamic Fast Card Program</TD></TR>";
                            $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}fc><TD>";
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $lang = $langDec["$lLang"];
                                $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Volume:</TD><TD CLASS=uinfod>$fVolume%</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Text Size:</TD><TD CLASS=uinfod>$fSize&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Theme:</TD><TD CLASS=uinfod>$fTheme&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Lesson Cycle:</TD><TD CLASS=uinfod>$fCycle&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Word Repeat:</TD><TD CLASS=uinfod>$fRepeat&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Speed (0-5):</TD><TD CLASS=uinfod>".sprintf("%01.1f",(5000-$fDelay)/1000)."&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Audio Format:</TD><TD CLASS=uinfod>$fFormat&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Pos (Lesson):</TD><TD CLASS=uinfod>$flPos&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Words (Lesson):</TD><TD CLASS=uinfod>$flWordCount&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Word Repeat (Lesson):</TD><TD CLASS=uinfod>$flRepeat&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Lesson Cycle (Lesson):</TD><TD CLASS=uinfod>$flCycle&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Speed (0-5) (Lesson):</TD><TD CLASS=uinfod>".sprintf("%01.1f",(5000 - floatval($flDelay))/1000)."&nbsp;</TD</TR>";
                                $pl .= "<TR><TD CLASS=uinfoc>Audio Length (Lesson):</TD><TD CLASS=uinfod>".sprintf("%01.1f",floatval($flAudioLength)/1000)."&nbsp;</TD</TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                            $pl .= "</TABLE>";
                            
                            $pl .= "</TD>";
                            $pl .= "</TR>";
                        }
                    }
                    $pl .= "</TABLE>";
                }
            }
            /*
            if ($act == 'vl') {
                // View Logs?
                $AdminSession->post("/w2/n/np",5120);
                // Grab Browsers, OS's, and Moz's
                $result = mysql_query("SELECT * FROM ",$db_link);
                // Start with a summary... Neato Bar Graphs?
            }
            */
            if ($act == 'db') {
                // Database Viewing
                if (isset($_REQUEST['table'])) {
                    // Table Detail
                    $dbTable = $_REQUEST['table'];
                    if ($dbTable == 'session') {
                        // Database Session Information
                        $AdminSession->post("/w2/n/np",5210);
                        if (isset($_REQUEST['time'])) {
                            $time = intval($_REQUEST['time']);
                            $secs = $time * 60;
                                $result = mysqli_query($db_link, "SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age, SEC_TO_TIME(UNIX_TIMESTAMP(sExpires) - UNIX_TIMESTAMP(NOW())) as Exp FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - $secs) ORDER BY sID");
                        } else {
                                $result = mysqli_query($db_link, "SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age, SEC_TO_TIME(UNIX_TIMESTAMP(sExpires) - UNIX_TIMESTAMP(NOW())) as Exp FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - 3600) ORDER BY sID");
                        }
                        $ds_sort = 'sID';
                        $scount = @mysqli_num_rows($result);
                        $pl .= "<SPAN CLASS=TITLE>Database Viewer - [Session]</SPAN><BR>";
                        $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                        // Headers
                        $pl .= "<TR>";
                        $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=ds&Sort=sID'\">ID</TD>";
                        $pl .= "<TD CLASS=\"resc\">Browser</TD>";
                        $pl .= "<TD CLASS=\"resc\">Screen</TD>";
                        $pl .= "<TD CLASS=\"resc\">Status</TD>";
                        $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=ds&Sort=sExpires'\">Expires</TD>";
                        $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=ds&Sort=sCreated'\">Created</TD>";
                        $pl .= "</TR>";
                        while ($row = mysqli_fetch_assoc($result)) {
                            //$pl .= "<TD CLASS=resd>{$row['sSessionID']}</TD>";
                            $userData = unserialize(base64_decode($row['sSessionData']));
                            $userDataCount = strlen($row['sSessionData']);
                            $logID = intval($userData['log']['logid']);
                            $bresult = mysqli_query($db_link, "SELECT *,INET_NTOA(ip_address) as ip_addr FROM log_enter WHERE logID='$logID'");
                            if (($bresult !== false) && (mysqli_num_rows($bresult) > 0)) {
                                $brow = mysqli_fetch_assoc($bresult);
                                ((mysqli_free_result($bresult) || (is_object($bresult) && (get_class($bresult) == "mysqli_result"))) ? true : false);
                                $ip = $brow['ip_addr'];
                                //$ipName = @gethostbyaddr($ip);
                                $port = $brow['ip_port'];
                                $sx = intval($brow['s_res_x']); $sy = intval($brow['s_res_y']);
                                $sres = $brow['s_res_x'].'x'.$brow['s_res_y'].' '.$brow['s_res_d'].'bit';
                                $cres = $brow['c_res_x'].'x'.$brow['c_res_y'];
                                $bid = $brow['browser_id'];
                                $oid = $brow['os_id'];
                                $mid = $brow['moz_id'];
                                if (($sx < 1280) and ($sy < 1024) and (intval($brow['s_res_d']) < 15)) {
                                    $sres_warn = '<IMG BORDER=0 SRC=images/warning.gif>';
                                } else {
                                    $sres_warn = '';
                                }
                            }
                            $bresult = mysqli_query($db_link, "SELECT * FROM log_moz WHERE ID='$mid'");
                            if (($bresult !== false) && (mysqli_num_rows($bresult) > 0)) {
                                $brow = mysqli_fetch_assoc($bresult);
                                $mozilla = $brow['name']." ".$brow['ver'];
                                ((mysqli_free_result($bresult) || (is_object($bresult) && (get_class($bresult) == "mysqli_result"))) ? true : false);
                            }
                            $bresult = mysqli_query($db_link, "SELECT * FROM log_browser WHERE ID='$bid'");
                            if (($bresult !== false) && (mysqli_num_rows($bresult) > 0)) {
                                $brow = mysqli_fetch_assoc($bresult);
                                $browser = $brow['browser'];
                                $browserVer = $brow['ver'];
                                ((mysqli_free_result($bresult) || (is_object($bresult) && (get_class($bresult) == "mysqli_result"))) ? true : false);
                            }
                            $bresult = mysqli_query($db_link, "SELECT * FROM log_os WHERE ID='$oid'");
                            if (($bresult !== false) && (mysqli_num_rows($bresult) > 0)) {
                                $brow = mysqli_fetch_assoc($bresult);
                                $os = $brow['os'];
                                ((mysqli_free_result($bresult) || (is_object($bresult) && (get_class($bresult) == "mysqli_result"))) ? true : false);
                            }
                            // Browser
                            // Screen
                            // Address
                            // Has words in Lesson?
                            $svLesson = $userData['w2']['l'];
                            list($a,$b,$c,$d,$e,$svLessonWords) = explode('|',$svLesson);
                            $svWords = unserialize(base64_decode($svLessonWords));
                            list($altered,$index,$paging,$pagelen,$page,$svLWords) = explode('|',$svWords);
                            $svWordArray = unserialize(base64_decode($svWords));
                            $stL = (count($svWordArray) > 0) ? 'L' : '';
                            // Has Account and is Logged In?
                            $aresult = mysqli_query($db_link, "SELECT * FROM Account WHERE uSessionID='{$row['sSessionID']}'");
                            if (($aresult !== false) && (mysqli_num_rows($aresult) > 0)) {
                                $ac = mysqli_fetch_assoc($aresult);
                                $stA = 'A';
                                $stAcc = $ac['uAccess'];
                                ((mysqli_free_result($aresult) || (is_object($aresult) && (get_class($aresult) == "mysqli_result"))) ? true : false);
                            } else {
                                $stA = '';
                                $stAcc = 'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNGGNNNNNNNNN';
                            }
                            
                            // Has Purchased Language?
                            // Request access for a specified level.
                            if ($level == 0) {
                                if ($lang == 2) {$clevel = 1;} // Spanish
                                if ($lang == 3) {$clevel = 3;} // German
                                if ($lang == 4) {$clevel = 2;} // French
                            } else {
                                $clevel = $level;
                            }
                            $stAccSp = (($stAcc[0] == "P") or ($stAcc[0] == "C"));
                            $stAccFr = (($stAcc[1] == "P") or ($stAcc[1] == "C"));
                            $stAccGe = (($stAcc[2] == "P") or ($stAcc[2] == "C"));
                            $stAccSpG = ($stAcc[0] == "G");
                            $stAccFrG = ($stAcc[1] == "G");
                            $stAccGeG = ($stAcc[2] == "G");
                            $stAccAdmin = ($stAcc[38] == "G");
                            $stP = ($stAccSp or $stAccFr or $stAccGe) ? 'P' : (($stAccSpG or $stAccFrG or $stAccGeG) ? 'P' : '' ) ;
                            $admin = ($stAccAdmin) ? ' STYLE="color:red;"' : (($stA=='A') ? ' STYLE="color:#8F8;"' : '')  ;
                            $pl .= "<TR onClick=\"toggle('ds{$row['sID']}');\">";
                            if ($AdminSession->getID() == $row['sSessionID']) {
                                $pl .= "<TD CLASS=resc STYLE=\"color:yellow;\" WIDTH=80>{$row['sID']}*&nbsp;</TD>";
                            } else {
                                $pl .= "<TD CLASS=resc WIDTH=80>{$row['sID']}&nbsp;</TD>";
                            }
                            $browserName = ($browser != 'Unknown') ? "<FONT COLOR=#DDF>$mozilla</FONT>" : $browserName . ' ' . $browserVer;
                            $pl .= "<TD$admin CLASS=resd>$browser</TD>";
                            $pl .= "<TD$admin CLASS=resd>$sx x $sy</TD>";
                            $pl .= "<TD$admin CLASS=resd>";
                            $pl .= "<DIV STYLE=\"width:10px;float:left;\">$stA&nbsp;</DIV>";
                            $pl .= "<DIV STYLE=\"width:10px;float:left;\">$stP&nbsp;</DIV>";
                            $pl .= "<DIV STYLE=\"width:10px;float:left;\">$stL&nbsp;</DIV>";
                            $pl .= "</TD>";
                            $exp = ($row['Exp'][0] == '-') ? '---' : $row['Exp'];
                            $pl .= "<TD$admin CLASS=resd>$exp</TD>";
                            $pl .= "<TD$admin CLASS=resd>{$row['Age']}</TD>";
                            $pl .= "</TR>";
                            $pl .= "<TR CLASS=resex ID=ds{$row['sID']}><TD CLASS=resdh COLSPAN=6>";
                                // Hidden Data
                                // Grab the Log Line, and the Account line if any...
                                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                                $aresult = mysqli_query($db_link, "SELECT * FROM Account WHERE uSessionID='{$row['sSessionID']}'");
                                //$pl .= print_r($aresult,true) . "<BR>".mysql_num_rows($aresult) . "<BR>";
                                if (($aresult !== false) && (mysqli_num_rows($aresult) > 0)) {
                                    while ($arow = mysqli_fetch_assoc($aresult)) {
                                        $uAccount = "{$arow['FName']} {$arow['LName']} ( {$arow['uEmail']} )";
                                        $pl .= "<TR><TD WIDTH=100 CLASS=uinfoc>Account:</TD><TD CLASS=uinfod>$uAccount</TD</TR>";
                                    }
                                    ((mysqli_free_result($aresult) || (is_object($aresult) && (get_class($aresult) == "mysqli_result"))) ? true : false);
                                } else {
                                    $pl .= "<TR><TD WIDTH=100 CLASS=uinfoc>Account:</TD><TD CLASS=uinfod><SPAN CLASS=dark>Unavailable</SPAN></TD</TR>";
                                }
                                $logID = $userData['log']['logid'];
                                $logState = $userData['log']['logstate'];
                                $lresult = mysqli_query($db_link, "SELECT *,INET_NTOA(ip_address) as ip_addr FROM log_enter WHERE logID='$logID'");
                                if (($lresult !== false) && (mysqli_num_rows($lresult) > 0)) {
                                    $lrow = mysqli_fetch_assoc($lresult);
                                    ((mysqli_free_result($lresult) || (is_object($lresult) && (get_class($lresult) == "mysqli_result"))) ? true : false);
                                    $ip = $lrow['ip_addr'];
                                    $port = $lrow['ip_port'];
                                    $sres = $lrow['s_res_x'].'x'.$lrow['s_res_y'].' '.$lrow['s_res_d'].'bit';
                                    $cres = $lrow['c_res_x'].'x'.$lrow['c_res_y'];
                                    $bid = $lrow['browser_id'];
                                    $oid = $lrow['os_id'];
                                    $mid = $lrow['moz_id'];
                                    if ((intval($lrow['s_res_x']) < 1280) and (intval($lrow['s_res_y']) < 1024) and (intval($lrow['s_res_d']) < 15)) {
                                        $sres_warn = '<IMG BORDER=0 SRC=images/warning.gif>';
                                    } else {
                                        $sres_warn = '';
                                    }
                                }
                                $bresult = mysqli_query($db_link, "SELECT * FROM log_browser WHERE ID='$bid'");
                                if (($bresult !== false) && (mysqli_num_rows($bresult) > 0)) {
                                    $brow = mysqli_fetch_assoc($bresult);
                                    $browser = $brow['browser']." ".$brow['ver'];
                                    ((mysqli_free_result($bresult) || (is_object($bresult) && (get_class($bresult) == "mysqli_result"))) ? true : false);
                                }
                                $oresult = mysqli_query($db_link, "SELECT * FROM log_os WHERE ID='$oid'");
                                if (($oresult !== false) && (mysqli_num_rows($oresult) > 0)) {
                                    $orow = mysqli_fetch_assoc($oresult);
                                    $os = $orow['os'];
                                    ((mysqli_free_result($oresult) || (is_object($oresult) && (get_class($oresult) == "mysqli_result"))) ? true : false);
                                }
                                $pl .= "<TR><TD CLASS=uinfoc>Logged:</TD><TD CLASS=uinfod>[$logID] $ip:$port - $os $browser ($cres)</TD></TR>";
                                $lDebug = print_r($userData,true);
                                $pl .= "<TR><TD VALIGN=TOP CLASS=uinfoc>Debug:</TD><TD VALIGN=TOP CLASS=uinfod STYLE=\"white-space:pre;\">$lDebug</TD></TR>";
                                $pl .= "</TABLE>";
                            $pl .= "</TD></TR>";
                        }
                        $pl .= "</TABLE>";
                        $pl .= "<BR>Total of $scount active sessions in the past ".($time/60)." hours.<BR>";
                    }
    //--------------------
                    if ($dbTable == 'account') {
                        // Database Session Information
                        $AdminSession->post("/w2/n/np",5210);
                        if (isset($_REQUEST['time'])) {
                            $time = intval($_REQUEST['time']);
                            $secs = $time * 60;
                                $result = mysqli_query($db_link, "SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - $secs) ORDER BY sID");
                        } else {
                                $result = mysqli_query($db_link, "SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - 3600) ORDER BY sID");
                        }
                    }
    //                 if ($dbTable == 'records') {
    //                     // Database Session Information
    //                     $AdminSession->post("/w2/n/np",5210);
    //                     if (isset($_REQUEST['time'])) {
    //                         $time = intval($_REQUEST['time']);
    //                         $secs = $time * 60;
    //                             $result = mysql_query("SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - $secs) ORDER BY sID",$db_link);
    //                     } else {
    //                             $result = mysql_query("SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - 3600) ORDER BY sID",$db_link);
    //                     }
    //                 }
                    if ($dbTable == 'transactions') {
                        // Database Session Information
                        $AdminSession->post("/w2/n/np",5210);
                        if (isset($_REQUEST['time'])) {
                            $time = intval($_REQUEST['time']);
                            $secs = $time * 60;
                                $result = mysqli_query($db_link, "SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - $secs) ORDER BY sID");
                        } else {
                                $result = mysqli_query($db_link, "SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - 3600) ORDER BY sID");
                        }
                    }
                    if ($dbTable == 'records') {
                        // Database Session Information
                        $AdminSession->post("/w2/n/np",5210);
                        //span=cyear
                        if (isset($_REQUEST['span'])) {
                            $timea = array('lday' => 86400, 'lmonth' => 2592000, 'lyear' => 31536000);
                            $span = $_REQUEST['span'];
                            if ($span == 'cday') {
                                $secs = 86400;
                                $result = mysqli_query($db_link, "SELECT * FROM p_purchases WHERE UNIX_TIMESTAMP(f_purchased) > (UNIX_TIMESTAMP() - $secs) ORDER BY fID");
                            }
                            if ($span == 'cmonth') {
                                $secs = 2592000;
                                $result = mysqli_query($db_link, "SELECT * FROM p_purchases WHERE UNIX_TIMESTAMP(f_purchased) > (UNIX_TIMESTAMP() - $secs) ORDER BY fID");
                            }
                            if ($span == 'cyear') {
                                $secs = 31536000;
                                $result = mysqli_query($db_link, "SELECT * FROM p_purchases WHERE UNIX_TIMESTAMP(f_purchased) > (UNIX_TIMESTAMP() - $secs) ORDER BY fID");
                            }
                            if ($span == 'lday') {
                                $secs = $timea['lday'];
                                $result = mysqli_query($db_link, "SELECT * FROM p_purchases WHERE UNIX_TIMESTAMP(f_purchased) > (UNIX_TIMESTAMP() - $secs) ORDER BY fID");
                            }
                            if ($span == 'lmonth') {
                                $secs = $timea['lmonth'];
                                $result = mysqli_query($db_link, "SELECT * FROM p_purchases WHERE UNIX_TIMESTAMP(f_purchased) > (UNIX_TIMESTAMP() - $secs) ORDER BY fID");
                            }
                            if ($span == 'lyear') {
                                $secs = $timea['lyear'];
                                $result = mysqli_query($db_link, "SELECT * FROM p_purchases WHERE UNIX_TIMESTAMP(f_purchased) > (UNIX_TIMESTAMP() - $secs) ORDER BY fID");
                            }
                        } else {
                            $secs = 86400;
                            $result = mysqli_query($db_link, "SELECT * FROM p_purchases WHERE UNIX_TIMESTAMP(f_purchased) > (UNIX_TIMESTAMP() - 86400) ORDER BY fID");
                        }
                        $scount = @mysqli_num_rows($result);
                        $days = $secs / 86400;
                        // Output the table of records...
                        $pl .= "<SPAN CLASS=TITLE>Database Viewer - [Records]</SPAN><BR>";
                        //$pl .= mysql_error();
                        $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                        // Headers
                        /* Database Table Layout
                            +---------------+---------------------+------+-----+---------+----------------+
                            | Field         | Type                | Null | Key | Default | Extra          |
                            +---------------+---------------------+------+-----+---------+----------------+
                            | fID           | bigint(20) unsigned |      | PRI | NULL    | auto_increment |
                            | f_account     | varchar(40)         | YES  | MUL | NULL    |                |
                            | f_item_number | varchar(64)         | YES  |     | NULL    |                |
                            | f_purchased   | datetime            | YES  | MUL | NULL    |                |
                            | f_received    | datetime            | YES  | MUL | NULL    |                |
                            | f_activated   | datetime            | YES  |     | NULL    |                |
                            | f_expires     | datetime            | YES  | MUL | NULL    |                |
                            | f_status      | varchar(20)         | YES  |     | Pending |                |
                            | f_valid       | tinyint(4)          | YES  |     | 0       |                |
                            | f_vacation    | tinyint(4)          | YES  |     | 0       |                |
                            +---------------+---------------------+------+-----+---------+----------------+
                            mysql> explain p_itemcodes;
                            +---------------+---------------------+------+-----+---------+----------------+
                            | Field         | Type                | Null | Key | Default | Extra          |
                            +---------------+---------------------+------+-----+---------+----------------+
                            | fID           | bigint(20) unsigned |      | PRI | NULL    | auto_increment |
                            | item_number   | varchar(64)         | YES  | MUL | NULL    |                |
                            | item_name     | varchar(255)        | YES  |     | NULL    |                |
                            | item_amount   | double              | YES  |     | NULL    |                |
                            | item_duration | varchar(64)         | YES  | MUL | NULL    |                |
                            | item_note     | blob                | YES  |     | NULL    |                |
                            +---------------+---------------------+------+-----+---------+----------------+
                            
                        */
                        $pl .= "<TR>";
                        $pl .= "<TD CLASS=\"resc\">ID</TD>";
                        $pl .= "<TD CLASS=\"resc\">Account</TD>";
                        $pl .= "<TD CLASS=\"resc\">Item</TD>";
                        $pl .= "<TD CLASS=\"resc\">Activated</TD>";
                        $pl .= "<TD CLASS=\"resc\">Valid</TD>";
                        $pl .= "<TD CLASS=\"resc\">Vacation</TD>";
                        $pl .= "</TR>";
                        // Read in the Item codes
                        $bresult = mysqli_query($db_link, "SELECT * FROM p_itemcodes");
                        $items = array();
                        while ($brow = mysqli_fetch_assoc($bresult)) {
                            $item = array();
                            $item['item_number'] = $brow['item_number'];
                            $item['item_name'] = $brow['item_name'];
                            $item['item_amount'] = $brow['item_amount'];
                            $item['item_duration'] = $brow['item_duration'];
                            $item['item_note'] = $brow['item_note'];
                            $items["{$item['item_number']}"] = $item;
                        }
                        ((mysqli_free_result($bresult) || (is_object($bresult) && (get_class($bresult) == "mysqli_result"))) ? true : false);
                        while ($row = mysqli_fetch_assoc($result)) {
                            //$pl .= "<TD CLASS=resd>{$row['sSessionID']}</TD>";
                            
                        
                            // Has Account and is Logged In?
                            $aresult = mysqli_query($db_link, "SELECT * FROM Account WHERE uSHA1='{$row['f_account']}'");
                            if (($aresult !== false) && (mysqli_num_rows($aresult) > 0)) {
                                $ac = mysqli_fetch_assoc($aresult);
                                $pAccountName = "{$ac['uLName']}, {$ac['uFName']}";
                                $pAccountEmail = $ac['uEmail'];
                                ((mysqli_free_result($aresult) || (is_object($aresult) && (get_class($aresult) == "mysqli_result"))) ? true : false);
                            } else {
                                $pAccountName = "<FONT COLOR=RED>No Account</FONT>";
                                $pAccountEmail = "<FONT COLOR=RED>No E-Mail</FONT>";
                            }
                            $itemDesc = $items[$row['f_item_number']]['item_name'];
                            $itemActivated = ($row['f_activated'] != '') ? '<FONT COLOR=GREEN>Yes</FONT>' : "<FONT COLOR=silver>No</FONT>";
                            $itemValid = ($row['f_valid'] == 1) ? '<FONT COLOR=GREEN>Yes</FONT>' : '<FONT COLOR=silver>No</FONT>';
                            $itemVacation = ($row['f_vacation'] == 1) ? '<FONT COLOR=GREEN>Yes</FONT>' : '<FONT COLOR=silver>No</FONT>';
                            $pl .= "<TR>";
                            $pl .= "<TD CLASS=resc WIDTH=80>{$row['fID']}&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd NOWRAP>$pAccountName</TD>";
                            $pl .= "<TD CLASS=resd NOWRAP>$itemDesc</TD>";
                            $pl .= "<TD CLASS=resd NOWRAP>$itemActivated</TD>";
                            $pl .= "<TD CLASS=resd NOWRAP>$itemValid</TD>";
                            $pl .= "<TD CLASS=resd NOWRAP>$itemVacation</TD>";
                            $pl .= "</TR>";
                        }
                        $pl .= "</TABLE>";
                        $pl .= "<BR>Total of $scount purchases in the past $days days.<BR>";
                    }
                    if ($dbTable == 'lessons') {
                        $pl .= "<SPAN CLASS=TITLE>Database Viewer - [Lessons]</SPAN><BR>";
                        if (isset($_REQUEST['t'])) {
                            $t = $_REQUEST['t'];
                            if ($t == 'modify') {
                                // Modify the lesson...
                                $lID = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['id']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                                $lsNewOwner = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['ls_reassign']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                                mysqli_query($db_link, "UPDATE Lesson SET lOwner='$lsNewOwner' WHERE lID='$lID'");
                                $t = 'search';
                            }
                            if ($t == 'search') {
                                // Search Fields
                                $lsOwner = $_REQUEST['ls_owner'];
                                $pl .= '<FORM METHOD=POST NAME=lessonSearch>';
                                $pl .= '<INPUT TYPE=HIDDEN NAME=act VALUE="db">';
                                $pl .= '<INPUT TYPE=HIDDEN NAME=table VALUE="lessons">';
                                $pl .= '<INPUT TYPE=HIDDEN NAME=t VALUE="search">';
                                $pl .= '<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=100% STYLE="background-color:#202840;">';
                                $pl .= '<TR><TD CLASS=auth ALIGN=RIGHT>OwnerID:</TD><TD><INPUT CLASS="t1" SIZE=30 NAME=ls_owner VALUE="'.$lsOwner.'"></TD></TR>';
                                $pl .= '<TR><TD CLASS=auth COLSPAN=2 ALIGN=RIGHT><INPUT TYPE=SUBMIT CLASS="s1" VALUE=Search></TD></TR>';
                                $pl .= '</TABLE></FORM><BR>';
                                if ($lsOwner != '') {
                                    $elsOwner = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $lsOwner) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                                    $result = mysqli_query($db_link, "SELECT * FROM Lesson WHERE lOwner='$elsOwner' ORDER BY lLanguage");
                                    if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                                        $count = 1;
                                        $pl .= '<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=100%>';
                                        while ($row = mysqli_fetch_assoc($result)) {
                                            $pl .= '<TR>';
                                            $pl .= "<TD CLASS=resc>{$row['lName']}</TD>";
                                            $lang = $langDec[$row['lLanguage']];
                                            if ($lang == 'German') { $lang = "<SPAN STYLE=\"color:#F44;\">$lang</SPAN>"; }
                                            if ($lang == 'French') { $lang = "<SPAN STYLE=\"color:#44F;\">$lang</SPAN>"; }
                                            if ($lang == 'Spanish') { $lang = "<SPAN STYLE=\"color:#4F4;\">$lang</SPAN>"; }
                                            $pl .= "<TD CLASS=resd>$lang</TD>";
                                            // Grab info from the Lesson Itself
                                            $pl .= "<FORM METHOD=POST NAME=modify_$count>";
                                            $pl .= '<INPUT TYPE=HIDDEN NAME=act VALUE="db">';
                                            $pl .= '<INPUT TYPE=HIDDEN NAME=table VALUE="lessons">';
                                            $pl .= '<INPUT TYPE=HIDDEN NAME=t VALUE="modify">';
                                            $pl .= '<INPUT TYPE=HIDDEN NAME=id VALUE="'.$row['lID'].'">';
                                            $pl .= '<INPUT TYPE=HIDDEN NAME=ls_owner VALUE="'.$lsOwner.'">';
                                            $pl .= "<TD CLASS=resd><INPUT NAME=ls_reassign SIZE=44 CLASS=t1 VALUE=\"{$row['lOwner']}\">&nbsp;<INPUT TYPE=SUBMIT CLASS=\"s1\" VALUE=modify></TD>";
                                            $pl .= '</TR>';
                                            $pl .= '</FORM>';
                                            $count++;
                                        }
                                        $pl .= '</TABLE>';
                                    }
                                    
                                }
                            
                            }
                        } else {
                            // Just come sounts?
                        }
                    }
                    if ($dbTable == 'account') {
                        $pl .= "<SPAN CLASS=TITLE>Database Viewer - [Account]</SPAN><BR>";
                    
                        $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    }
                } else {
                    // Database Overview
                    // SHOW TABLE STATUS
                    // Name,Type,Row_format,Rows,Avg_row_length,Data_length,Max_data_length,Index_length,Data_free,Auto_increment,Create_time,Update_time,Check_time,Create_options,Comment
                    $tsResult = mysqli_query($db_link, 'SHOW TABLE STATUS');
                    $tStatus = array();
                    while ($row = mysqli_fetch_assoc($tsResult)) {
                        $tsRow = array();
                        $tsRow[] = $row['Name'];
                        $tsRow[] = $row['Rows'];
                        $tsRow[] = number_format($row['Data_length']);
                        $tsRow[] = number_format($row['Index_length']);
                        $tsRow[] = $row['Auto_increment'];
                        $tStatus[] = $tsRow;
                    }
                    $pl .= "<SPAN CLASS=TITLE>Database Status</SPAN><BR>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    // Headers
                    $pl .= "<TR>";
                    $pl .= "<TD CLASS=\"resc link\">Table</TD>";
                    $pl .= "<TD CLASS=\"resc link\">Rows</TD>";
                    $pl .= "<TD CLASS=\"resc link\">Data Size</TD>";
                    $pl .= "<TD CLASS=\"resc link\">Index Size</TD>";
                    $pl .= "<TD CLASS=\"resc link\">Auto Increment</TD>";
                    $pl .= "</TR>";
                    foreach($tStatus as $tsRow) {
                        $pl .= '<TR>';
                        foreach($tsRow as $value) {
                            $pl .= "<TD CLASS=uinfoc>$value</TD>";
                        }
                        $pl .= '</TR>';
                    }
                    
                }
            }
            /*
            if ($act == 'dr') {
                // Database Record Payment Information
                $AdminSession->post("/w2/n/np",5230);
                $span = intval($_REQUEST['span']);
                if ($span == 'cday') {
                    $sql_date = 'DATE(NOW())';
                }
                if ($span == 'cmonth') {
                    $sql_date = 'SUBDATE(DATE(NOW()),DAYOFMONTH(NOW())-1)';
                }            
                if ($span == 'lmonth') {
                    $sql_date = 'SUBDATE(DATE(NOW()),DAYOFMONTH(NOW())-1) - INTERVAL 1 MONTH';
                }
                $secs = $time * 60;
                $ds_sort = 'sID';
                if ($time == 60) {
                    $result = mysql_query("SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - 3600) ORDER BY sID",$db_link);
                }
                if ($time == 1440) {
                    $result = mysql_query("SELECT *,SEC_TO_TIME(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(sCreated)) as Age FROM Session WHERE UNIX_TIMESTAMP(sCreated) > (UNIX_TIMESTAMP() - 86400) ORDER BY sID",$db_link);
                }
                $scount = @mysql_num_rows($result);
                $pl .= "<SPAN CLASS=TITLE>Database Viewer - [Session]</SPAN><BR>";
                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                // Headers
                $pl .= "<TR>";
                $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=ds&Sort=sID'\">ID</TD>";
                $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=ds&Sort=sSessionID'\">SessionID</TD>";
                $pl .= "<TD CLASS=\"resc\">Data</TD>";
                $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=ds&Sort=sExpires'\">Expires</TD>";
                $pl .= "<TD CLASS=\"resc link\" onClick=\"document.location='dfc_admin.php?act=ds&Sort=sCreated'\">Created</TD>";
                $pl .= "</TR>";
                while ($row = mysql_fetch_assoc($result)) {
                    $pl .= "<TR onClick=\"toggle('ds{$row['sID']}');\">";
                    if ($AdminSession->getID() == $row['sSessionID']) {
                        $pl .= "<TD CLASS=resc STYLE=\"color:yellow;\" WIDTH=80>{$row['sID']}*&nbsp;</TD>";
                    } else {
                        $pl .= "<TD CLASS=resc WIDTH=80>{$row['sID']}&nbsp;</TD>";
                    }
                    $pl .= "<TD CLASS=resd>{$row['sSessionID']}</TD>";
                    $userData = unserialize(base64_decode($row['sSessionData']));
                    $userDataCount = strlen($row['sSessionData']);
                    $pl .= "<TD CLASS=resd>($userDataCount)</TD>";
                    $pl .= "<TD CLASS=resd>{$row['sExpires']}</TD>";
                    $pl .= "<TD CLASS=resd>{$row['Age']}</TD>";
    //                 $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
    //                 $pl .= "<TR CLASS=uinfot onClick=\"toggle('rx{$conn['sID']}cp');\"><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Computer Indetification</TD></TR>";
    //                 $pl .= "<TR CLASS=resex ID=rx{$conn['sID']}cp><TD>";
    //                     $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
    //                     $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>IP Address:</TD><TD CLASS=uinfod>$ip:$port</TD</TR>";
    //                     $pl .= "<TR><TD CLASS=uinfoc>Hostname:</TD><TD CLASS=uinfod>$ipName</TD</TR>";
    //                     $pl .= "<TR><TD CLASS=uinfoc>Browser:</TD><TD CLASS=uinfod>$browser</TD</TR>";
    //                     $pl .= "<TR><TD CLASS=uinfoc>OS:</TD><TD CLASS=uinfod>$os</TD</TR>";
    //                     $pl .= "<TR><TD CLASS=uinfoc>Screen Res:</TD><TD CLASS=uinfod>$sres$sres_warn</TD</TR>";
    //                     $pl .= "<TR><TD CLASS=uinfoc>Browser Res:</TD><TD CLASS=uinfod>$cres</TD</TR>";
    //                     $pl .= "<TR><TD CLASS=uinfoc>Logging:</TD><TD CLASS=uinfod>$logState ($logID)</TD</TR>";
    //                     $pl .= "<TR><TD CLASS=uinfoc>Cookie Name:</TD><TD CLASS=uinfod>{$userData['log']['cookie']}&nbsp;</TD</TR>";
    //                     $pl .= "<TR><TD CLASS=uinfoc>SessionID:</TD><TD CLASS=uinfod>{$conn['sSessionID']}&nbsp;</TD</TR>";
    //                     $pl .= "</TABLE>";
    //                 $pl .= "</TD></TR>";
                    $pl .= "</TR>";
                    $pl .= "<TR CLASS=resex ID=ds{$row['sID']}><TD CLASS=resdh COLSPAN=5>";
                        // Hidden Data
                        // Grab the Log Line, and the Account line if any...
                        $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                        $aresult = mysql_query("SELECT * FROM Account WHERE uSessionID='{$row['sSessionID']}'",$db_link);
                        //$pl .= print_r($aresult,true) . "<BR>".mysql_num_rows($aresult) . "<BR>";
                        if (($aresult !== false) && (mysql_num_rows($aresult) > 0)) {
                            while ($arow = mysql_fetch_assoc($aresult)) {
                                $uAccount = "{$arow['FName']} {$arow['LName']} ( {$arow['uEmail']} )";
                                $pl .= "<TR><TD WIDTH=100 CLASS=uinfoc>Account:</TD><TD CLASS=uinfod>$uAccount</TD</TR>";
                            }
                            mysql_free_result($aresult);
                        } else {
                            $pl .= "<TR><TD WIDTH=100 CLASS=uinfoc>Account:</TD><TD CLASS=uinfod><SPAN CLASS=dark>Unavailable</SPAN></TD</TR>";
                        }
                        $logID = $userData['log']['logid'];
                        $logState = $userData['log']['logstate'];
                        $lresult = mysql_query("SELECT *,INET_NTOA(ip_address) as ip_addr FROM log_enter WHERE logID='$logID'",$db_link);
                        if (($lresult !== false) && (mysql_num_rows($lresult) > 0)) {
                            $lrow = mysql_fetch_assoc($lresult);
                            mysql_free_result($lresult);
                            $ip = $lrow['ip_addr'];
                            $port = $lrow['ip_port'];
                            $sres = $lrow['s_res_x'].'x'.$lrow['s_res_y'].' '.$lrow['s_res_d'].'bit';
                            $cres = $lrow['c_res_x'].'x'.$lrow['c_res_y'];
                            $bid = $lrow['browser_id'];
                            $oid = $lrow['os_id'];
                            $mid = $lrow['moz_id'];
                            if ((intval($lrow['s_res_x']) < 1280) and (intval($lrow['s_res_y']) < 1024) and (intval($lrow['s_res_d']) < 15)) {
                                $sres_warn = '<IMG BORDER=0 SRC=images/warning.gif>';
                            } else {
                                $sres_warn = '';
                            }
                        }
                        $bresult = mysql_query("SELECT * FROM log_browser WHERE ID='$bid'",$db_link);
                        if (($bresult !== false) && (mysql_num_rows($bresult) > 0)) {
                            $brow = mysql_fetch_assoc($bresult);
                            $browser = $brow['browser']." ".$brow['ver'];
                            mysql_free_result($bresult);
                        }
                        $oresult = mysql_query("SELECT * FROM log_os WHERE ID='$oid'",$db_link);
                        if (($oresult !== false) && (mysql_num_rows($oresult) > 0)) {
                            $orow = mysql_fetch_assoc($oresult);
                            $os = $orow['os'];
                            mysql_free_result($oresult);
                        }
                        $pl .= "<TR><TD CLASS=uinfoc>Logged:</TD><TD CLASS=uinfod>[$logID] $ip:$port - $os $browser ($cres)</TD></TR>";
                        $pl .= "</TABLE>";
                        //$pl .= "I'm hidden";
                    $pl .= "</TD></TR>";
                }
                $pl .= "</TABLE>";
                $pl .= "<BR>Total of $scount active sessions in the past ".($time/60)." hours.<BR>";
                    // Past Day
                    // Today
            }
            */
            if ($act == 'todo') {
                // Todo list
                $Todo = new fmcTodo();
                if (isset($_REQUEST['tview'])) {
                    $tview = $_REQUEST['tview'];
                    $taskID = $_REQUEST['taskID'];
                    $userID = $_REQUEST['userID'];
                    if ($tview == 'overview') {
                        $owners = $Todo->listTaskOwners();
                        $pl .= '<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>';
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=4 STYLE="font-size:100%;border: none;">Task List Overview</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=4>&nbsp;</TD>';
                        $pl .= '</TR>';
                        $pl .= "<TR>";
                        $pl .= "<TD CLASS=resc>Administrator</TD>";
                        $pl .= "<TD CLASS=resc>Completed</TD>";
                        $pl .= "<TD CLASS=resc>Pending</TD>";
                        $pl .= "<TD WIDTH=400>&nbsp;</TD>";
                        $pl .= "</TR>";
                        foreach($owners as $owner) {
                            $tasks = $Todo->listTasks($owner['id']);
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=resd><A HREF="?act=todo&tview=viewfor&userID='.$owner['id'].'">'.$owner['name'].'</A></TD>';
                            $pl .= '<TD CLASS=resd>'.count($tasks['completed']).'</TD>';
                            $pl .= '<TD CLASS=resd>'.count($tasks['incomplete']).'</TD>';
                            $pl .= '</TR>';
                        }
                        $pl .= '</TABLE>';
                    }
                    if ($tview == 'post') {
                        // Post the new note!
                        $owner = $_REQUEST['tOwner'];
                        $subject = stripslashes($_REQUEST['tSubject']);
                        $priA = $_REQUEST['tPriA'];
                        $priB = $_REQUEST['tPriB'];
                        $notes = stripslashes($_REQUEST['tNotes']);
                        $Todo->newTask($owner,$subject,$priA,$priB,$notes);
                        $tview = 'view';
                    }
                    if ($tview == 'postedit') {
                        // Post the new note!
                        $owner = $_REQUEST['tOwner'];
                        $subject = stripslashes($_REQUEST['tSubject']);
                        $priA = $_REQUEST['tPriA'];
                        $priB = $_REQUEST['tPriB'];
                        $sep =  "<UPDATE TIME=\"".time()."\" MEMBER=\"".$AdminAccount->getID()."\">";
                        $notes = $sep.stripslashes($_REQUEST['tNotes']);
                        $taskID = $_REQUEST['taskID'];
                        $Todo->alterTask($taskID,$owner,$subject,$priA,$priB,$notes);
                        $tview = 'view';
                    }
                    if ($tview == 'complete') {
                        // Post the new note!
                        $taskID = $_REQUEST['taskID'];
                        $Todo->finishTask($taskID);
                        $tview = 'view';
                    }
                    if ($tview == 'incomplete') {
                        // Post the new note!
                        $taskID = $_REQUEST['taskID'];
                        $Todo->unfinishTask($taskID);
                        $tview = 'view';
                    }
                    if ($tview == 'new') {
                        // Form for new task
                        $pl .= '<FORM METHOD=POST NAME=newTask>';
                        $pl .= '<INPUT TYPE=HIDDEN NAME=act VALUE=todo>';
                        $pl .= '<INPUT TYPE=HIDDEN NAME=tview VALUE=post>';
                        $pl .= '<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>';
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">New Task</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth>OWNER:</TD>';
                        $pl .= '<TD ALIGN=LEFT COLSPAN=3><SELECT CLASS=t1 NAME=tOwner>';
                        $owners = $AdminAccount->listAdmins();
                        foreach($owners as $owner) {
                            if ($owner['id'] == $AdminAccount->getID()) {
                                $pl .= '<OPTION STYLE="color:#F80;background-color:#444;" VALUE='.$owner['id'].' SELECTED>'.$owner['name'].'</OPTION>';
                            } else {
                                $pl .= '<OPTION STYLE="color:#888;background-color:#444;" VALUE='.$owner['id'].'>'.$owner['name'].'</OPTION>';
                            }
                        }
                        $pl .= '</TD>';
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth>SUBJECT:</TD>';
                        $pl .= '<TD ALIGN=LEFT COLSPAN=3><INPUT CLASS=t1 SIZE=60 NAME=tSubject VALUE=""></TD>';
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth>PRIORITY 1:</TD>';
                        $pl .= '<TD ALIGN=LEFT><SELECT CLASS=t1 NAME=tPriA>';
                        $pl .= '<OPTION STYLE="color:#F00;background-color:#444;" VALUE=10>Critical</OPTION>';
                        $pl .= '<OPTION STYLE="color:#F80;background-color:#444;" VALUE=20>Important</OPTION>';
                        $pl .= '<OPTION STYLE="color:#FF0;background-color:#444;" VALUE=30>High</OPTION>';
                        $pl .= '<OPTION STYLE="color:#0F0;background-color:#444;" VALUE=40>Medium</OPTION>';
                        $pl .= '<OPTION STYLE="color:#CCC;background-color:#444;" VALUE=50 SELECTED>Normal</OPTION>';
                        $pl .= '<OPTION STYLE="color:#888;background-color:#444;" VALUE=60>Low</OPTION>';
                        $pl .= '<OPTION STYLE="color:#000;background-color:#444;" VALUE=100>None</OPTION>';
                        $pl .= '</SELECT></TD>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth>PRIORITY 2:</TD>';
                        $pl .= '<TD ALIGN=LEFT><SELECT CLASS=t1 NAME=tPriB>';
                        $pl .= '<OPTION VALUE=1>Highest</OPTION>';
                        $pl .= '<OPTION VALUE=2>High</OPTION>';
                        $pl .= '<OPTION VALUE=3 SELECTED>Medium</OPTION>';
                        $pl .= '<OPTION VALUE=4>Low</OPTION>';
                        $pl .= '<OPTION VALUE=5>Lowest</OPTION>';
                        $pl .= '</SELECT></TD>';
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth VALIGN=TOP>NOTES:</TD>';
                        $pl .= '<TD ALIGN=LEFT CLASS=auth COLSPAN=3><TEXTAREA NAME=tNotes CLASS=t1 ROWS=20 COLS=80></TEXTAREA></TD>';
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth COLSPAN=4><INPUT TYPE=SUBMIT CLASS=s1 VALUE=POST></TD>';
                        $pl .= '</TR>';
                        $pl .= '</TABLE>';
                        $pl .= '</FORM>';
                    }
                    if ($tview == 'viewfor') {
                        //Show your tasks - split complete
                        $tasks = $Todo->listTasks($userID);
                        $user = $AdminAccount->finger($userID);
                        $username = "{$user['uFName']} {$user['uLName']}";
                        $pl .= '<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=700>';
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">Viewing All Tasks for '.$username.'</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        foreach ($tasks['incomplete'] as $task) {
                            list($name,$color) = explode(',',$todoDec[$task['priorityA']]);
                            $created = date("M j 'y",strtotime($task['started']));
                            $priBcolor = $todoDec2[$task['priorityB']];
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=task1 WIDTH=60%><A HREF=?act=todo&tview=viewtask&taskID='.$task['taskID'].'>'.$task['subject'].'</A></TD>';
                            $pl .= '<TD CLASS=task1 NOWRAP>'.$created.'&nbsp;</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:'.$color.';" NOWRAP>'.$name.'</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:#444;" NOWRAP>'.$todoDec2[$task['priorityB']].'</TD>';
                            if (($task['owner'] == $AdminAccount->getID()) or ($AdminAccount->getID() == '629d7d6284e66e37d5599a5281b8069d6b935123')){
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=complete&taskID='.$task['taskID'].'><IMG SRC=images/checkbox.png></A></TD>';
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            } else {
                                $pl .= '<TD CLASS=task1>&nbsp;</TD>';
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            }
                            $pl .= '</TR><TR>';
                            $taskBody = htmlentities(preg_replace('/<UPDATE TIME="[0-9]+" MEMBER="[0-9a-f]+">/','`',$task['tnotes']));
                            $taskBody = str_replace('`',' <FONT COLOR=#8888FF>/</FONT> ',substr($taskBody,0,160));
                            if (strlen($taskBody) >= 158) {
                                $pl .= '<TD CLASS=task2 COLSPAN=6>'.$taskBody.'...</TD>';
                            } else {
                                $pl .= '<TD CLASS=task2 COLSPAN=6>'.$taskBody.'</TD>';
                            }
                            $pl .= '</TR><TR>';
                            $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                            $pl .= '</TR>';
                        }
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6><H1>Completed Tasks</H1></TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        foreach ($tasks['completed'] as $task) {
                            list($name,$color) = explode(',',$todoDec[$task['priorityA']]);
                            $created = date("M j 'y",strtotime($task['started']));
                            $priBcolor = $todoDec2[$task['priorityB']];
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=ctask1 WIDTH=60%><A HREF=?act=todo&tview=viewtask&taskID='.$task['taskID'].'>'.$task['subject'].'</A></TD>';
                            $pl .= '<TD CLASS=ctask1 NOWRAP>'.$created.'&nbsp;</TD>';
                            $pl .= '<TD CLASS=ctask1 ALIGN=RIGHT STYLE="color:'.$color.';" NOWRAP>'.$name.'</TD>';
                            $pl .= '<TD CLASS=ctask1 ALIGN=RIGHT STYLE="color:#444;" NOWRAP>'.$todoDec2[$task['priorityB']].'</TD>';
                            if (($task['owner'] == $AdminAccount->getID()) or ($AdminAccount->getID() == '629d7d6284e66e37d5599a5281b8069d6b935123')){
                                $pl .= '<TD CLASS=ctask1><A HREF=?act=todo&tview=incomplete&taskID='.$task['taskID'].' TITLE="Mark As Incomplete"><IMG SRC=images/checkedbox.png></A></TD>';
                                $pl .= '<TD CLASS=ctask1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            } else {
                                $pl .= '<TD CLASS=ctask1>&nbsp;</TD>';
                                $pl .= '<TD CLASS=ctask1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            }
                            $pl .= '</TR><TR>';
                            $taskBody = htmlentities(preg_replace('/<UPDATE TIME="[0-9]+" MEMBER="[0-9a-f]+">/','`',$task['tnotes']));
                            $taskBody = str_replace('`',' <FONT COLOR=#8888FF>/</FONT> ',substr($taskBody,0,160));
                            if (strlen($taskBody) >= 158) {
                                $pl .= '<TD CLASS=ctask2 COLSPAN=6>'.$taskBody.'...</TD>';
                            } else {
                                $pl .= '<TD CLASS=ctask2 COLSPAN=6>'.$taskBody.'</TD>';
                            }
                            $pl .= '</TR><TR>';
                            $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                            $pl .= '</TR>';
                        }
                        $pl .= '</TABLE>';
                    }
                    if ($tview == 'view') {
                        //Show your tasks - split complete
                        $tasks = $Todo->listTasks($AdminAccount->getID());
                        $pl .= '<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=700>';
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">Viewing Active Tasks</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        foreach ($tasks['incomplete'] as $task) {
                            list($name,$color) = explode(',',$todoDec[$task['priorityA']]);
                            $created = date("M j 'y",strtotime($task['started']));
                            $priBcolor = $todoDec2[$task['priorityB']];
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=task1 WIDTH=60%><A HREF=?act=todo&tview=viewtask&taskID='.$task['taskID'].'>'.$task['subject'].'</A></TD>';
                            $pl .= '<TD CLASS=task1 NOWRAP>'.$created.'&nbsp;</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:'.$color.';" NOWRAP>'.$name.'</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:#444;" NOWRAP>'.$todoDec2[$task['priorityB']].'</TD>';
                            $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=complete&taskID='.$task['taskID'].' TITLE="Mark As Complete"><IMG SRC=images/checkbox.png></A></TD>';
                            $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            $pl .= '</TR><TR>';
                            $taskBody = htmlentities(preg_replace('/<UPDATE TIME="[0-9]+" MEMBER="[0-9a-f]+">/','`',$task['tnotes']));
                            $taskBody = str_replace('`',' <FONT COLOR=#8888FF>/</FONT> ',substr($taskBody,0,160));
                            if (strlen($taskBody) >= 158) {
                                $pl .= '<TD CLASS=task2 COLSPAN=6>'.$taskBody.'...</TD>';
                            } else {
                                $pl .= '<TD CLASS=task2 COLSPAN=6>'.$taskBody.'</TD>';
                            }
                            $pl .= '</TR><TR>';
                            $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                            $pl .= '</TR>';
                        }
                        $pl .= '</TABLE>';
                    }
                    if ($tview == 'viewc') {
                        //Show your tasks - split complete
                        $tasks = $Todo->listTasks($AdminAccount->getID());
                        $pl .= '<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=700>';
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">Viewing Completed Tasks</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        foreach ($tasks['completed'] as $task) {
                            list($name,$color) = explode(',',$todoDec[$task['priorityA']]);
                            $created = date("M j 'y",strtotime($task['completed']));
                            $priBcolor = $todoDec2[$task['priorityB']];
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=task1 WIDTH=60%><A HREF=?act=todo&tview=viewtask&taskID='.$task['taskID'].'>'.$task['subject'].'</A></TD>';
                            $pl .= '<TD CLASS=task1 NOWRAP>'.$created.'&nbsp;</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:'.$color.';" NOWRAP>'.$name.'</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:#444;" NOWRAP>'.$todoDec2[$task['priorityB']].'</TD>';
                            $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=incomplete&taskID='.$task['taskID'].' TITLE="Mark As Incomplete"><IMG SRC=images/checkedbox.png></A></TD>';
                            $pl .= '<TD CLASS=task1>&nbsp;</TD>';
                            $pl .= '</TR><TR>';
                            $taskBody = htmlentities(preg_replace('/<UPDATE TIME="[0-9]+" MEMBER="[0-9a-f]+">/','`',$task['tnotes']));
                            $taskBody = str_replace('`',' <FONT COLOR=#8888FF>/</FONT> ',substr($taskBody,0,160));
                            if (strlen($taskBody) == 160) {
                                $pl .= '<TD CLASS=task2 COLSPAN=6>'.$taskBody.'...</TD>';
                            } else {
                                $pl .= '<TD CLASS=task2 COLSPAN=6>'.$taskBody.'</TD>';
                            }
                            $pl .= '</TR><TR>';
                            $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                            $pl .= '</TR>';
                        }
                        $pl .= '</TABLE>';
                    }
                    if ($tview == 'viewtask') {
                        //Show Detail for a Task
                        $taskID = $_REQUEST['taskID'];
                        $task = $Todo->getTask($taskID);
                        $pl .= '<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=700>';
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">Viewing Task Detail</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        
                            list($name,$color) = explode(',',$todoDec[$task['priorityA']]);
                            $created = date("M j, Y",strtotime($task['started']));
                            $priBcolor = $todoDec2[$task['priorityB']];
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=task1 WIDTH=60%><A HREF=?act=todo&tview=viewtask&taskID='.$task['taskID'].'>'.$task['subject'].'</A></TD>';
                            $pl .= '<TD CLASS=task1 NOWRAP>'.$created.'&nbsp;</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:'.$color.';" NOWRAP>'.$name.'</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:#444;" NOWRAP>'.$todoDec2[$task['priorityB']].'</TD>';
                            if (($task['owner'] == $AdminAccount->getID()) or ($AdminAccount->getID() == '629d7d6284e66e37d5599a5281b8069d6b935123')){
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=complete&taskID='.$task['taskID'].'><IMG SRC=images/checkbox.png></A></TD>';
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            } else {
                                $pl .= '<TD CLASS=task1>&nbsp;</TD>';
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            }
                            $pl .= '</TR>';
                            //preg_replace(
                            $taskBody = preg_split('/<UPDATE TIME="[0-9]+" MEMBER="[0-9a-f]+">/',$task['notes']);
                            preg_match_all('/<UPDATE TIME="([0-9]+)" MEMBER="([0-9a-f]+)">/',$task['notes'],$taskTime);
                            $note = str_replace("\n",'<BR>',$taskBody[0]);
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=task3 COLSPAN=6><SPAN CLASS=taskd>'.$note.'</SPAN>';
                            for($i=1;$i<count($taskBody);$i++) {
                                $note = str_replace("\n",'<BR>',$taskBody[$i]);
                                $time = date("g:ia",$taskTime[1][$i-1]);
                                $date = date("M j, Y",$taskTime[1][$i-1]);
                                $member = $taskTime[2][$i-1];
                                $memberData = $AdminAccount->finger($member);
                                $memberName = $memberData['uFName'] . ' ' . $memberData['uLName'];
                                $pl .= "<BR><BR><IMG ALIGN=TOP SRC=images/pencil16.png><SPAN STYLE=\"color:#555;border-bottom:1px dashed #444;\"> Updated on $date at $time by $memberName</SPAN><BR><SPAN CLASS=taskd>$note</SPAN>";
                            }
                            $pl .= '</TD>';
                            $pl .= '</TR><TR>';
                            $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                            $pl .= '</TR>';
                        $pl .= '</TABLE>';
                        //$pl .= "<PRE>".print_r($taskTime,true)."<PRE>";
                    }
                    if ($tview == 'edit') {
                        // Form for new task
                        $task = $Todo->getTask($taskID);
                        $pl .= '<FORM METHOD=POST NAME=editTask>';
                        $pl .= '<INPUT TYPE=HIDDEN NAME=act VALUE=todo>';
                        $pl .= '<INPUT TYPE=HIDDEN NAME=tview VALUE=postedit>';
                        $pl .= '<INPUT TYPE=HIDDEN NAME=taskID VALUE='.$taskID.'>';
                        $pl .= '<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>';
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">Edit Task</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth>OWNER:</TD>';
                        $pl .= '<TD ALIGN=LEFT COLSPAN=3><SELECT CLASS=t1 NAME=tOwner>';
                        $owners = $AdminAccount->listAdmins();
                        foreach($owners as $owner) {
                            if ($owner['id'] == $task['owner']) {
                                $pl .= '<OPTION STYLE="color:#F80;background-color:#444;" VALUE='.$owner['id'].' SELECTED>'.$owner['name'].'</OPTION>';
                            } else {
                                $pl .= '<OPTION STYLE="color:#888;background-color:#444;" VALUE='.$owner['id'].'>'.$owner['name'].'</OPTION>';
                            }
                        }
                        $pl .= '</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth>SUBJECT:</TD>';
                        $pl .= '<TD ALIGN=LEFT COLSPAN=3><INPUT CLASS=t1 SIZE=60 NAME=tSubject VALUE="'.$task['subject'].'"></TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth>PRIORITY 1:</TD>';
                        $pl .= '<TD ALIGN=LEFT><SELECT CLASS=t1 NAME=tPriA>';
                        $sa1 = (intval($task['priorityA']) == 10) ? ' SELECTED' : '';
                        $sa2 = (intval($task['priorityA']) == 20) ? ' SELECTED' : '';
                        $sa3 = (intval($task['priorityA']) == 30) ? ' SELECTED' : '';
                        $sa4 = (intval($task['priorityA']) == 40) ? ' SELECTED' : '';
                        $sa5 = (intval($task['priorityA']) == 50) ? ' SELECTED' : '';
                        $sa6 = (intval($task['priorityA']) == 60) ? ' SELECTED' : '';
                        $sa7 = (intval($task['priorityA']) == 70) ? ' SELECTED' : '';
                        $pl .= '<OPTION STYLE="color:#F00;background-color:#444;" VALUE=10'.$sa1.'>Critical</OPTION>';
                        $pl .= '<OPTION STYLE="color:#F80;background-color:#444;" VALUE=20'.$sa2.'>Important</OPTION>';
                        $pl .= '<OPTION STYLE="color:#FF0;background-color:#444;" VALUE=30'.$sa3.'>High</OPTION>';
                        $pl .= '<OPTION STYLE="color:#0F0;background-color:#444;" VALUE=40'.$sa4.'>Medium</OPTION>';
                        $pl .= '<OPTION STYLE="color:#CCC;background-color:#444;" VALUE=50'.$sa5.'>Normal</OPTION>';
                        $pl .= '<OPTION STYLE="color:#888;background-color:#444;" VALUE=60'.$sa6.'>Low</OPTION>';
                        $pl .= '<OPTION STYLE="color:#000;background-color:#444;" VALUE=100'.$sa7.'>None</OPTION>';
                        $pl .= '</SELECT></TD>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth>PRIORITY 2:</TD>';
                        $pl .= '<TD ALIGN=LEFT><SELECT CLASS=t1 NAME=tPriB>';
                        $sb1 = (intval($task['priorityB']) == 1) ? ' SELECTED' : '';
                        $sb2 = (intval($task['priorityB']) == 2) ? ' SELECTED' : '';
                        $sb3 = (intval($task['priorityB']) == 3) ? ' SELECTED' : '';
                        $sb4 = (intval($task['priorityB']) == 4) ? ' SELECTED' : '';
                        $sb5 = (intval($task['priorityB']) == 5) ? ' SELECTED' : '';
                        $pl .= '<OPTION VALUE=1'.$sb1.'>Highest</OPTION>';
                        $pl .= '<OPTION VALUE=2'.$sb2.'>High</OPTION>';
                        $pl .= '<OPTION VALUE=3'.$sb3.'>Medium</OPTION>';
                        $pl .= '<OPTION VALUE=4'.$sb4.'>Low</OPTION>';
                        $pl .= '<OPTION VALUE=5'.$sb5.'>Lowest</OPTION>';
                        $pl .= '</SELECT></TD>';
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth VALIGN=TOP>NOTES:</TD>';
                        $date = time();
                        $pl .= '<TD ALIGN=LEFT CLASS=auth COLSPAN=3><TEXTAREA NAME=tNotes CLASS=t1 ROWS=20 COLS=80></TEXTAREA></TD>';
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= '<TD ALIGN=RIGHT CLASS=auth COLSPAN=4><INPUT TYPE=SUBMIT CLASS=s1 VALUE=POST></TD>';
                        $pl .= '</TR>';
                        $pl .= '</TABLE>';
                        $pl .= '</FORM>';
                        //$pl .= '<PRE>'.print_r($task,true).'</PRE>';
                        //$pl .= "<PRE>1:$sa1 2:$sa2 3:$sa3 4:$sa4 5:$sa5 6:$sa6 7:$sa7 </PRE>";
                        //$pl .= "<PRE>1:$sb1 2:$sb2 3:$sb3 4:$sb4 5:$sb5</PRE>";
                    }
                    if ($tview == 'viewall') {
                        //Show all tasks - split complete
                        $tasks = $Todo->listTasks(false);
                        $pl .= '<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=700>';
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">Viewing all Tasks</TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        foreach ($tasks['incomplete'] as $task) {
                            list($name,$color) = explode(',',$todoDec[$task['priorityA']]);
                            $created = date("M j 'y",strtotime($task['started']));
                            $priBcolor = $todoDec2[$task['priorityB']];
                            if ($task['owner'] != $oldowner) {
                                $fData = $AdminAccount->finger($task['owner']);
                                $pl .= '<TR>';
                                $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">'.$fData['uFName'] . ' ' . $fData['uLName'].'</TD>';
                                $pl .= '</TR><TR>';
                                $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                                $pl .= '</TR>';
                                $oldowner = $task['owner'];
                            }
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=task1 WIDTH=60%><A HREF=?act=todo&tview=viewtask&taskID='.$task['taskID'].'>'.$task['subject'].'</A></TD>';
                            $pl .= '<TD CLASS=task1 NOWRAP>'.$created.'&nbsp;</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:'.$color.';" NOWRAP>'.$name.'</TD>';
                            $pl .= '<TD CLASS=task1 ALIGN=RIGHT STYLE="color:#444;" NOWRAP>'.$todoDec2[$task['priorityB']].'</TD>';
                            if (($task['owner'] == $AdminAccount->getID()) or ($AdminAccount->getID() == '629d7d6284e66e37d5599a5281b8069d6b935123')){
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=complete&taskID='.$task['taskID'].'><IMG SRC=images/checkbox.png></A></TD>';
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            } else {
                                $pl .= '<TD CLASS=task1>&nbsp;</TD>';
                                $pl .= '<TD CLASS=task1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            }
                            $pl .= '</TR><TR>';
                            $taskBody = htmlentities(preg_replace('/<UPDATE TIME="[0-9]+" MEMBER="[0-9a-f]+">/','`',$task['tnotes']));
                            $taskBody = str_replace('`',' <FONT COLOR=#8888FF>/</FONT> ',substr($taskBody,0,160));
                            if (strlen($taskBody) == 160) {
                                $pl .= '<TD CLASS=task2 COLSPAN=6>'.$taskBody.'...</TD>';
                            } else {
                                $pl .= '<TD CLASS=task2 COLSPAN=6>'.$taskBody.'</TD>';
                            }
                            $pl .= '</TR><TR>';
                            $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                            $pl .= '</TR>';
                        }
                        $pl .= '<TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6><H1>Completed Tasks</H1></TD>';
                        $pl .= '</TR><TR>';
                        $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                        $pl .= '</TR>';
                        foreach ($tasks['completed'] as $task) {
                            list($name,$color) = explode(',',$todoDec[$task['priorityA']]);
                            $created = date("M j 'y",strtotime($task['started']));
                            $priBcolor = $todoDec2[$task['priorityB']];
                            if ($task['owner'] != $oldowner) {
                                $fData = $AdminAccount->finger($task['owner']);
                                $pl .= '<TR>';
                                $pl .= '<TD CLASS=task3 COLSPAN=6 STYLE="font-size:100%;border: none;">'.$fData['uFName'] . ' ' . $fData['uLName'].'</TD>';
                                $pl .= '</TR><TR>';
                                $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                                $pl .= '</TR>';
                                $oldowner = $task['owner'];
                            }
                            $pl .= '<TR>';
                            $pl .= '<TD CLASS=ctask1 WIDTH=60%><A HREF=?act=todo&tview=viewtask&taskID='.$task['taskID'].'>'.$task['subject'].'</A></TD>';
                            $pl .= '<TD CLASS=ctask1 NOWRAP>'.$created.'&nbsp;</TD>';
                            $pl .= '<TD CLASS=ctask1 ALIGN=RIGHT STYLE="color:'.$color.';" NOWRAP>'.$name.'</TD>';
                            $pl .= '<TD CLASS=ctask1 ALIGN=RIGHT STYLE="color:#444;" NOWRAP>'.$todoDec2[$task['priorityB']].'</TD>';
                            if (($task['owner'] == $AdminAccount->getID()) or ($AdminAccount->getID() == '629d7d6284e66e37d5599a5281b8069d6b935123')){
                                $pl .= '<TD CLASS=ctask1><A HREF=?act=todo&tview=incomplete&taskID='.$task['taskID'].' TITLE="Mark As Incomplete"><IMG SRC=images/checkedbox.png></A></TD>';
                                $pl .= '<TD CLASS=ctask1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            } else {
                                $pl .= '<TD CLASS=ctask1>&nbsp;</TD>';
                                $pl .= '<TD CLASS=ctask1><A HREF=?act=todo&tview=edit&taskID='.$task['taskID'].'><IMG SRC=images/pencil16.png></A></TD>';
                            }
                            $pl .= '</TR><TR>';
                            $taskBody = htmlentities(preg_replace('/<UPDATE TIME="[0-9]+" MEMBER="[0-9a-f]+">/','`',$task['tnotes']));
                            $taskBody = str_replace('`',' <FONT COLOR=#8888FF>/</FONT> ',substr($taskBody,0,160));
                            if (strlen($taskBody) == 160) {
                                $pl .= '<TD CLASS=ctask2 COLSPAN=6>'.$taskBody.'...</TD>';
                            } else {
                                $pl .= '<TD CLASS=ctask2 COLSPAN=6>'.$taskBody.'</TD>';
                            }
                            $pl .= '</TR><TR>';
                            $pl .= '<TD CLASS=task3 COLSPAN=6>&nbsp;</TD>';
                            $pl .= '</TR>';
                        }
                        $pl .= '</TABLE>';
                    }
                }
            }
            if ($act == 'vl') {
                // Visitor Logs - Go back 7 days
                
                $span = intval($_REQUEST['span']);
                $pl .= "<SPAN CLASS=TITLE STYLE=\"border-bottom:1px solid #888;\">Visitor Logs</SPAN><BR>";
                $browser = array();
                $mozilla = array();
                $referer = array();
                $os = array();
                $result = mysqli_query($db_link, 'SELECT * FROM log_browser');
                while ($row = mysqli_fetch_assoc($result)) {
                    $b = $row['browser'] . ' ' . $row['ver'];
                    $browser[$row['ID']] = $b;
                }
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                $result = mysqli_query($db_link, 'SELECT * FROM log_moz');
                while ($row = mysqli_fetch_assoc($result)) {
                    $m = $row['name'] . ' ' . $row['ver'];
                    $mozilla[$row['ID']] = $m;
                }
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                $result = mysqli_query($db_link, 'SELECT * FROM log_os');
                while ($row = mysqli_fetch_assoc($result)) {
                    $os[$row['ID']] = $row['os'];
                }
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                $result = mysqli_query($db_link, 'SELECT * FROM log_refer');
                while ($row = mysqli_fetch_assoc($result)) {
                    $r = 'http://' . $row['domain'] . '/' . $row['argv'];
                    $referer[$row['ID']] = $r;
                }
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                
                $time = time() - $span;
                $day = 0;
                
                $pl .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=700>';
                $people = 0; $emp = 0;$bots = 0; $msn = 0; $google = 0; $other = 0;
                $result = mysqli_query($db_link, "SELECT * FROM log_enter WHERE entry_time > '$time' ORDER BY entry_time DESC");
                while ($row = mysqli_fetch_assoc($result)) {
                    $entry_day = intval(date('d',$row['entry_time']));
                    if ($day != $entry_day) {
                        // Post a row for the day
                        $date = date('l, M jS',$row['entry_time']);
                        if (($people + $bots) > 0) {
                            // Bot Summary Line
                            $pl .= '<TR>';
                            $guest = $people - $emp;
                            $summary = "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=700><TR><TD CLASS=auth>People: $guest (+$emp)</TD><TD CLASS=auth>Bots: $bots</TD><TD CLASS=auth>MSN: $msn</TD><TD CLASS=auth>GOOGLE: $google</TD><TD CLASS=auth>Other: $other</TD></TR></TABLE>";
                            $pl .= "<TD NOWRAP COLSPAN=6 CLASS=\"auth\" STYLE=\"font-weight:bold;color:#4AF;padding:1px 4px 1px 4px;border-bottom:1px solid #08F;background-color:#024;\">$summary</TD>";
                            $pl .= '</TR>';
                            $people = 0; $bots = 0; $msn = 0; $google = 0; $other = 0; $emp = 0;
                        }
                        $pl .= '<TR>';
                        $pl .= "<TD NOWRAP COLSPAN=6 CLASS=\"auth\" HEIGHT=30>&nbsp;</TD>";
                        $pl .= '</TR>';
                        $pl .= '<TR>';
                        $pl .= "<TD NOWRAP COLSPAN=6 CLASS=\"auth\" STYLE=\"font-weight:bold;color:#4AF;padding:1px 4px 1px 4px;border-top:1px solid #08F;border-bottom:1px solid #08F;background-color:#024;\">$date</TD>";
                        $pl .= '</TR>';
                        $day = intval(date('d',$row['entry_time']));
                    }
                    $ipA = long2ip($row['ip_address']);
                    //$ipN = @gethostbyaddr($ipA);
                    $ipN = $Reports->dnsHostname($ipA);
                    $color = '#AAB'; $color2='#CCC';
                    if ($ipA == '70.57.234.155') {$ipN = " <FONT COLOR=#44AAFF>Chris Holland</FONT>"; $color='#444'; $color2='#777'; $emp++;}
                    if ($ipA == '71.32.36.107') {$ipN = " <FONT COLOR=#44AAFF>Chris Holland</FONT>"; $color='#444'; $color2='#777'; $emp++;}
                    if ($ipA == '64.32.159.97') {$ipN = " <FONT COLOR=#44AAFF>William Oswald</FONT>"; $color='#444'; $color2='#777'; $emp++;}
                    if ($ipA == '24.230.121.175') {$ipN = " <FONT COLOR=#44AAFF>Jim Huntley</FONT>"; $color='#444'; $color2='#777'; $emp++;}
                    if ($ipA == '24.220.155.107') {$ipN = "&nbsp;"; $color='#444'; $color2='#777';}
                    if (strpos($ipN,'googlebot.com') !== false ) { $ipN = "<FONT COLOR=#00FF00>$ipN</FONT>"; $google++; }
                    if (strpos($ipN,'msnbot.msn.com') !== false ) { $ipN = "<FONT COLOR=#00FF00>$ipN</FONT>"; $msn++; }
                    if (strpos($ipN,'inktomisearch.com') !== false ) { $ipN = "<FONT COLOR=#00FF00>$ipN</FONT> (Yahoo!)"; $other++; }
                    if (strpos($ipN,'ask.com') !== false ) { $ipN = "<FONT COLOR=#00FF00>$ipN</FONT>"; $other++; }
                    if (strpos($ipN,'whois.sc') !== false ) { $ipN = "<FONT COLOR=#00FF00>$ipN</FONT>"; $other++; }
                    $br = $browser[$row['browser_id']];
                    $mo = $mozilla[$row['moz_id']];
                    if ($br == 'Unknown --') { $br = "<FONT COLOR=#882222>$mo</FONT>"; }
                    $o = $os[$row['os_id']];
                    if (trim($o) == 'U') { $o = "<FONT COLOR=#222222>? ? ? ?</FONT>"; }
                    $rf = $referer[$row['refered']];
                    if (trim($rf) == '') { $rf = '&nbsp;'; }
                    if (trim($rf) == 'http:///') { $rf = '&nbsp;'; }
                    if ($rf != '&nbsp') { $rf = "<A HREF=\"$rf\">$rf</A>"; }
                    $res = $row['s_res_x'] . ' x ' . $row['s_res_y'];
                    if ($res == '0 x 0') { $res = '&nbsp;'; }
                    $clicks = $row['clicks'];
                    if ((substr($br,0,5) == '<FONT') or (substr($ipN,0,5) == '<FONT')) {
                        $bots++;
                    } else {
                        $people++;
                    }
                    $pl .= '<TR>';
                    $pl .= "<TD NOWRAP CLASS=\"auth\" STYLE=\"color:$color2;padding:1px 4px 1px 4px;border-bottom:1px solid #444;$bg\">$o</TD>";
                    $pl .= "<TD NOWRAP CLASS=\"auth\" STYLE=\"color:$color2;padding:1px 4px 1px 4px;border-bottom:1px solid #444;$bg\">$br</TD>";
                    $pl .= "<TD NOWRAP CLASS=\"auth\" STYLE=\"color:$color;padding:1px 4px 1px 4px;border-bottom:1px solid #444;$bg\">$res</TD>";
                    $pl .= "<TD NOWRAP CLASS=\"auth\" STYLE=\"color:$color;padding:1px 4px 1px 4px;border-bottom:1px solid #444;$bg\">$ipN</TD>";
                    $pl .= "<TD NOWRAP CLASS=\"auth\" STYLE=\"color:$color;padding:1px 4px 1px 4px;border-bottom:1px solid #444;$bg\">$clicks</TD>";
                    $pl .= "<TD NOWRAP CLASS=\"auth\" STYLE=\"color:$color2;padding:1px 4px 1px 4px;border-bottom:1px solid #444;$bg\">$rf</TD>";
                    $pl .= "</TR>";
                }
                if (($people + $bots) > 0) {
                    // Bot Summary Line
                    $pl .= '<TR>';
                    $guest = $people - $emp;
                    $summary = "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=700><TR><TD CLASS=auth>People: $guest (+$emp)</TD><TD CLASS=auth>Bots: $bots</TD><TD CLASS=auth>MSN: $msn</TD><TD CLASS=auth>GOOGLE: $google</TD><TD CLASS=auth>Other: $other</TD></TR></TABLE>";
                    $pl .= "<TD NOWRAP COLSPAN=5 CLASS=\"auth\" STYLE=\"font-weight:bold;color:#4AF;padding:1px 4px 1px 4px;border-bottom:1px solid #08F;background-color:#024;\">$summary</TD>";
                    $pl .= '</TR>';
                    $people = 0; $bots = 0; $msn = 0; $google = 0; $other = 0;
                }
                $pl .= '</TABLE>';
            }
            if ($act == 'info') {
                echo "<PRE>".print_r(get_loaded_extensions(),true)."</PRE>";
                phpinfo();
            }
            if ($act == 'sp') {
                include_once("searchbot.php");
                $Bot = new searchBot;
                if (isset($_REQUEST['sec'])) {
                    $sec = $_REQUEST['sec'];
                    if ($sec == 'doSearch') {
                        // Show results
                        $e = intval($_REQUEST['e']);
                        $k = intval($_REQUEST['k']);
                        $pl .= $Bot->doSearch($e,$k);
                        $sec = 'sum';
                    }
                    if ($sec == 'sum') {
                        // Configuration
                        if ($AdminAccount->has_access(40)) {
                            $pl .= $Bot->showSearches();
                        } else {
                            $pl .= "You do not have Database Level Access";
                        }
                    }
                    if ($sec == 'pl') {
                        // Placement
                        $pl .= $Bot->showRank('www.5muses.com');
                        $pl .= $Bot->showRank('learnspanishonline.net');
                        $pl .= $Bot->showRank('www.studyspanish.com');
                    }
                    if ($sec == 'mv') {
                        // Placement
                        $pl .= $Bot->showMovement('www.5muses.com');
                        $pl .= $Bot->showMovement('learnspanishonline.net');
                        $pl .= $Bot->showMovement('www.studyspanish.com');
                    }
                    if ($sec == 'res') {
                        // Show results
                        $e = intval($_REQUEST['e']);
                        $k = intval($_REQUEST['k']);
                        $pl .= $Bot->showAll($e,$k);
                    }
                    if ($sec == 'chart') {
                        if (isset($_REQUEST['month'])) {
                            $month = $_REQUEST['month'];
                        } else {
                            $month = date('Y-m');
                        }
                        $pl .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=500 STYLE="border-top: 1px solid #ccc;">';
                        $months = array('n/a','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
                        for ($y=2005;$y<=2007;$y++) {
                            $pl .= '<TR>';
                            $pl .= "<TD CLASS=resc WIDTH=50>$y</TD>";
                            for ($m=1;$m<=12;$m++) {
                                $mn = $months[$m];
                                $pl .= "<TD CLASS=resd WIDTH=30><A HREF=\"?act=sp&sec=chart&month=$y-$m\">$mn</A></TD>";
                            }
                            $pl .= '</TR>';
                        }
                        $pl .= '</TABLE><BR>';
                        
                        $pl .= '<IMG SRC=searchbot.php?graph=1&domain=www.5muses.com&month='.$month.'><BR><BR>';
                        $pl .= '<IMG SRC=searchbot.php?graph=2&domain=www.5muses.com&month='.$month.'><BR><BR>';
                        $pl .= '<IMG SRC=searchbot.php?graph=4&domain=www.5muses.com&month='.$month.'><BR><BR><HR>';
                        $pl .= '<IMG SRC=searchbot.php?graph=1&domain=learnspanishonline.net&month='.$month.'><BR><BR>';
                        $pl .= '<IMG SRC=searchbot.php?graph=2&domain=learnspanishonline.net&month='.$month.'><BR><BR>';
                        $pl .= '<IMG SRC=searchbot.php?graph=4&domain=learnspanishonline.net&month='.$month.'><BR><BR><HR>';
                        $pl .= '<IMG SRC=searchbot.php?graph=1&domain=www.studyspanish.com&month='.$month.'><BR><BR>';
                        $pl .= '<IMG SRC=searchbot.php?graph=2&domain=www.studyspanish.com&month='.$month.'><BR><BR>';
                        $pl .= '<IMG SRC=searchbot.php?graph=4&domain=www.studyspanish.com&month='.$month.'><BR><BR>';
                    }
                    if ($sec == 'test') {
                        // Show results
                        
                        //$data = $Bot->googleAPI('learn german free',10);
                        //$data = $Bot->fetch('http://search.yahoo.com/search?p=learn+german+free&prssweb=Search&ei=UTF-8&n=100&fl=0&pstart=1&fr=fp-top&b=101');
                        //$data = $Bot->fetch('http://search.yahoo.com/search?_adv_prop=web&x=op&ei=UTF-8&fr=fp-top&va=learn+german+free&va_vt=any&vp_vt=any&vo_vt=any&ve_vt=any&vd=all&vst=0&vf=all&vm=i&fl=0&n=100');
                        //$sweep = $Bot->sweepYahoo($data['html']);
                        //$html = htmlspecialchars($Bot->hexdump($data['html']));
                        
                        //$data = "<PRE>".htmlspecialchars(print_r($Bot->searchMSN('learn spanish'),true))."</PRE>";
                        //$pl .= $data;
                    }
                } else {
                    $pl .= "<SPAN CLASS=TITLE STYLE=\"border-bottom:1px solid #888;\">Search Engine Placements</SPAN><BR><BR>";
                    // Now just a general overview type thing...
                }
            }
            if ($act == 'rp') {
                // Reports
                //genMonth($month,$year)
                // Report Type
                // Month
                if (isset($_REQUEST['rtype'])) {
                    $reportType = $_REQUEST['rtype'];
                    
                    if (isset($_REQUEST['month'])) {
                        $month = $_REQUEST['month'];
                        $numbers = $Reports->genMonth($month,2005);
                        
                        // Funky Graph
                        $max = 0;
                        $pl .= "<SPAN CLASS=TITLE STYLE=\"border-bottom:1px solid #888;\">Visitors Report for $month</SPAN><BR><BR>";
                        $pl .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 HEIGHT='.$height.'>';
                        $pl .= '<TR>';
                        foreach($numbers as $day => $values) {
                            $max = ($values[0] > $max) ? $values[0] : $max;
                            $max = ($values[1] > $max) ? $values[1] : $max;
                            $days = $day;
                        }
                        for ($i=1;$i<=$days;$i++) {
                            $pl .= '<TD COLSPAN=2 CLASS=auth>'.$i.'</TD>';
                            $pl .= '<TD VALIGN=BOTTOM ALIGN=CENTER WIDTH=5><IMG SRC="images/1px.gif" WIDTH=1 HEIGHT=20 STYLE="background-color:#444;"></TD>';
                        }
                        $pl .= '</TR>';
                        $height = 100;
                        $pl .= '<TR>';
                        for ($i=1;$i<=$days;$i++) {
                            $values = $numbers[$i];
                            $mHeight = intval(($values[1] / $max) * $height);
                            $gHeight = intval(($values[0] / $max) * $height);
                            $pl .= '<TD ROWSPAN=2 VALIGN=BOTTOM><IMG SRC="images/graph1.png" WIDTH=7 HEIGHT='.$gHeight.'></TD>';
                            $pl .= '<TD ROWSPAN=2 VALIGN=BOTTOM><IMG SRC="images/graph2.png" WIDTH=7 HEIGHT='.$mHeight.'></TD>';
                            $pl .= '<TD ROWSPAN=2 VALIGN=BOTTOM ALIGN=CENTER WIDTH=5><IMG SRC="images/1px.gif" WIDTH=1 HEIGHT='.$height.' STYLE="background-color:#444;"></TD>';
                        }
                        $pl .= '<TD STYLE="border-top:1px solid #444;" CLASS=auth VALIGN=TOP>'.$max.'</TD>';
                        $pl .= '</TR><TR>';
                        $max = intval($max / 2);
                        $pl .= '<TD STYLE="border-top:1px solid #444;" CLASS=auth VALIGN=TOP>'.$max.'</TD>';
                        $pl .= '</TR><TR>';
                        for ($i=1;$i<=$days;$i++) {
                            $dow = substr(date('D',strtotime("$i $month 2005")),0,1);
                            if ($dow == 'S') {
                                $pl .= '<TD COLSPAN=3 CLASS=auth ALIGN=MIDDLE STYLE="color:#444">'.$dow.'</TD>';
                            } else {
                                $pl .= '<TD COLSPAN=3 CLASS=auth ALIGN=MIDDLE STYLE="color:#888">'.$dow.'</TD>';
                            }
                        }
                        $pl .= '</TR></TABLE><BR>';
                        
                        
                        // Static Sheet of Numbers
                        $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=300>";
                        $pl .= "<TR>";
                        $pl .= "<TD CLASS=resc>Day</TD>";
                        $pl .= "<TD CLASS=resc>Guests <IMG SRC=\"images/graph1.png\" WIDTH=7 HEIGHT=7></TD>";
                        $pl .= "<TD CLASS=resc>Members <IMG SRC=\"images/graph2.png\" WIDTH=7 HEIGHT=7></TD>";
                        $pl .= "<TD CLASS=resc>Total</TD>";
                        $pl .= "</TR>";
                        for ($i=1;$i<=$days;$i++) {
                            $values = $numbers[$i];
                            $totalDay = 0;
                            $pl .= "<TR>";
                            $pl .= "<TD CLASS=resd>$i&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd>{$values[0]}&nbsp;</TD>";
                            $pl .= "<TD CLASS=resd>{$values[1]}&nbsp;</TD>";
                            $members += $values[1];
                            $guests += $values[0];
                            $totalDay = $values[1] + $values[0];
                            $totalVisits += $totalDay;
                            $pl .= "<TD CLASS=resd>$totalDay</TD>";
                            $pl .= "</TR>";
                        }
                        $pl .= "<TR>";
                        $pl .= "<TD CLASS=resc>Total</TD>";
                        $pl .= "<TD CLASS=resc>$guests&nbsp;</TD>";
                        $pl .= "<TD CLASS=resc>$members&nbsp;</TD>";
                        $pl .= "<TD CLASS=resc>$totalVisits&nbsp;</TD>";
                        $pl .= "</TR>";
                        $pl .= "</TABLE>";
                        
                    }
                    if (isset($_REQUEST['refer'])) {
                        // Show some stats of referers and what they were
                    }
                }
            }
            if ($act == 'mn') {
                $AdminSession->post("/w2/n/np",5300);
                $mnSession = $_REQUEST['mnss'];
                
                $result = mysqli_query($db_link, "SELECT *,(NOW() - sCreated) as Age FROM Session WHERE sSessionID='$mnSession'");
                $srow = mysqli_fetch_assoc($result);
                //$userData = $row['sSessionData'];
                
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                    
                
                $userData = unserialize(base64_decode($srow['sSessionData']));
                $logID = $userData['log']['logid'];
                $logState = $userData['log']['logstate'];
                $result = mysqli_query($db_link, "SELECT * FROM Account WHERE uSessionID='$mnSession'");
                if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                    $conn['acct'] = 1;
                    $row = mysqli_fetch_assoc($result);
                    $conn['FName'] = $row['uFName'];
                    $conn['LName'] = $row['uLName'];
                    $conn['Addr'] = $row['uAddress1'];
                    $conn['City'] = $row['uCity'];
                    $conn['State'] = $row['uState'];
                    $conn['Zip'] = $row['uZip'];
                    $conn['Visits'] = $row['uVisits'];
                    $conn['Country'] = $row['uCountry'];
                    $conn['Province'] = $row['uProvince'];
                    $conn['AcctCreated'] = $row['uCreated'];
                    $conn['EMail'] = $row['uEmail'];
                    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                    $OnlineSessions[$i] = $conn;
                } else {
                    $conn = $OnlineSessions[$i];
                    $conn['acct'] = 0;
                    $conn['FName'] = '---';
                    $conn['LName'] = '---';
                    $conn['EMail'] = '---';
                    $OnlineSessions[$i] = $conn;
                }
                // Log Information
                $result = mysqli_query($db_link, "SELECT *,INET_NTOA(ip_address) as ip_addr FROM log_enter WHERE logID='$logID'");
                if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                    $row = mysqli_fetch_assoc($result);
                    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                    $ip = $row['ip_addr'];
                    $port = $row['ip_port'];
                    $sres = $row['s_res_x'].'x'.$row['s_res_y'].' '.$row['s_res_d'].'bit';
                    $cres = $row['c_res_x'].'x'.$row['c_res_y'];
                    $bid = $row['browser_id'];
                    $oid = $row['os_id'];
                    $mid = $row['moz_id'];
                    if ((intval($row['s_res_x']) < 1280) and (intval($row['s_res_y']) < 1024) and (intval($row['s_res_d']) < 15)) {
                        $sres_warn = '<IMG BORDER=0 SRC=images/warning.gif>';
                    } else {
                        $sres_warn = '';
                    }
                }
                //$ipName = @gethostbyaddr($ip);
                $ipName = $Reports->dnsHostname($ip);
                if ($ipName == $ip) $ipName = '<SPAN CLASS=dark>Unavailable</SPAN>';
                $result = mysqli_query($db_link, "SELECT * FROM log_browser WHERE ID='$bid'");
                if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                    $row = mysqli_fetch_assoc($result);
                    $browser = $row['browser']." ".$row['ver'];
                    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                }
                $result = mysqli_query($db_link, "SELECT * FROM log_os WHERE ID='$oid'");
                if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                    $row = mysqli_fetch_assoc($result);
                    $os = $row['os'];
                    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                }
    
                if ($sres == '0x0 0bit') $sres = '<SPAN CLASS=dark>Unavailable</SPAN>';
                if ($cres == '0x0') $cres = '<SPAN CLASS=dark>Unavailable</SPAN>';
                $nav = strval($userData['w2']['n']['np']);
                $navPlace = $nav1dec[$nav];
                list ($vSearch,$vBrowse,$vSearchMode,$vFilter,$vLang,$vDBPage,$vDBCount,$vDBTotal,$vDBPages) = explode('|',$userData['w2']['v']);
                list ($lName,$lSavedName,$lPos,$lRepeat,$lLang,$lWords) = explode('|',$userData['w2']['l']);
                list ($fVolume,$fSize,$fTheme,$fCycle,$fRepeat,$fDelay,$fFormat,$flPos,$flWordCount,$flRepeat,$flCycle,$flDelay,$flAudioLength) = explode('|',$userData['w2']['f']);
    
                //$pl .= "<DIV CLASS=uinfo><DIV CLASS=uinfot>CPU Indetification</DIV>";
                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                /*
                $pl .= "<TR CLASS=uinfot><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Computer Indetification</TD></TR>";
                $pl .= "<TR><TD>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>IP Address:</TD><TD CLASS=uinfod>$ip:$port</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Hostname:</TD><TD CLASS=uinfod>$ipName</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Browser:</TD><TD CLASS=uinfod>$browser</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>OS:</TD><TD CLASS=uinfod>$os</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Screen Res:</TD><TD CLASS=uinfod>$sres$sres_warn</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Browser Res:</TD><TD CLASS=uinfod>$cres</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Logging:</TD><TD CLASS=uinfod>$logState ($logID)</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Cookie Name:</TD><TD CLASS=uinfod>{$userData['log']['cookie']}&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>SessionID:</TD><TD CLASS=uinfod>{$srow['sSessionID']}&nbsp;</TD</TR>";
                    $pl .= "</TABLE>";
                $pl .= "</TD></TR>";
                */
                // Contact Info
                $pl .= "<TR CLASS=uinfot><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Contact Information</TD></TR>";
                $pl .= "<TR><TD>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Name:</TD><TD CLASS=uinfod>{$conn['FName']} {$conn['LName']}</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Address:</TD><TD CLASS=uinfod>{$conn['Addr']}</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>&nbsp;</TD><TD CLASS=uinfod>{$conn['City']},&nbsp;{$conn['State']}&nbsp;&nbsp;{$conn['Zip']}</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Country:</TD><TD CLASS=uinfod>{$conn['Country']}&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Province:</TD><TD CLASS=uinfod>{$conn['Province']}&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Visits:</TD><TD CLASS=uinfod>{$conn['Visits']}</TD</TR>";
                    $pl .= "</TABLE>";
                $pl .= "</TD></TR>";
                // Nav Location
                $pl .= "<TR CLASS=uinfot><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Navigation</TD></TR>";
                $pl .= "<TR><TD>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Web Page Place:</TD><TD CLASS=uinfod>$navPlace&nbsp;</TD</TR>";
                    $pl .= "</TABLE>";
                $pl .= "</TD></TR>";
                $pl .= "</TABLE>";
                // Vocabulary Info...
                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=49% ALIGN=LEFT>";
                $pl .= "<TR CLASS=uinfot><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Vocabulary</TD></TR>";
                $pl .= "<TR><TD>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    if ($vFilter == 'true') {
                        if ($vSearchMode == 'true') {
                            $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Searching for '$vSearch'</TD</TR>";
                        } else {
                            $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Browsing by '$vBrowse'</TD</TR>";
                        }
                    } else {
                        $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Viewing Mode:</TD><TD CLASS=uinfod>Browse All</TD</TR>";
                    }
                    $lang = $langDec["$vLang"];
                    $pl .= "<TR><TD CLASS=uinfoc>Language:</TD><TD CLASS=uinfod>$lang</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>SQL Page:</TD><TD CLASS=uinfod>$vDBPage of $vDBPages</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>SQL Count:</TD><TD CLASS=uinfod>$vDBCount of $vDBTotal</TD</TR>";
                    $pl .= "</TABLE>";
                $pl .= "</TD></TR>";
                // Lesson Info...
                $pl .= "<TR CLASS=uinfot><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Lesson</TD></TR>";
                $pl .= "<TR><TD>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    $lang = $langDec["$lLang"];
                    $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Language:</TD><TD CLASS=uinfod>$lang</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Name:</TD><TD CLASS=uinfod>$lName&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Saved Name:</TD><TD CLASS=uinfod>$lSavedName&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Pos / Repeat:</TD><TD CLASS=uinfod>$lPos / $lRepeat</TD</TR>";
                    $words = unserialize(base64_decode($lWords));;
                    $wordCount = count($words);
                    $pl .= "<TR><TD CLASS=uinfoc>Words:</TD><TD CLASS=uinfod>$wordCount</TD</TR>";
                    $pl .= "</TABLE>";
                $pl .= "</TD></TR>";
                $pl .= "</TABLE>";
                // FastCard Info...
                $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=49% ALIGN=RIGHT>";
                $pl .= "<TR CLASS=uinfot><TD STYLE=\"border-bottom:1px solid #448;padding:0px 4px 0px 4px;\" COLSPAN=2>Dynamic Fast Card Program</TD></TR>";
                $pl .= "<TR><TD>";
                    $pl .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>";
                    $lang = $langDec["$lLang"];
                    $pl .= "<TR><TD WIDTH=150 CLASS=uinfoc>Volume:</TD><TD CLASS=uinfod>$fVolume%</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Text Size:</TD><TD CLASS=uinfod>$fSize&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Theme:</TD><TD CLASS=uinfod>$fTheme&nbsp;</TD</TR>";
                    //$pl .= "<TR><TD CLASS=uinfoc>Lesson Cycle:</TD><TD CLASS=uinfod>$fCycle&nbsp;</TD</TR>";
                    //$pl .= "<TR><TD CLASS=uinfoc>Word Repeat:</TD><TD CLASS=uinfod>$fRepeat&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Speed (0-5):</TD><TD CLASS=uinfod>".sprintf("%01.1f",(5000-$fDelay)/1000)."&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Audio Format:</TD><TD CLASS=uinfod>$fFormat&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Pos (Lesson):</TD><TD CLASS=uinfod>$flPos&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Words (Lesson):</TD><TD CLASS=uinfod>$flWordCount&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Word Repeat (Lesson):</TD><TD CLASS=uinfod>$flRepeat of $fRepeat&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Lesson Cycle (Lesson):</TD><TD CLASS=uinfod>$flCycle of $fCycle&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Speed (0-5) (Lesson):</TD><TD CLASS=uinfod>".sprintf("%01.1f",(5000 - floatval($flDelay))/1000)."&nbsp;</TD</TR>";
                    $pl .= "<TR><TD CLASS=uinfoc>Audio Length (Lesson):</TD><TD CLASS=uinfod>".sprintf("%01.1f",floatval($flAudioLength)/1000)."&nbsp;</TD</TR>";
                    $pl .= "</TABLE>";
                $pl .= "</TD></TR>";
                $pl .= "</TABLE>";
            
            }
        } else {
            // Nothing set... default main page...
            // Shoutbox!
            $CB = new CommBoard('messageboard');
            if (isset($_REQUEST['cbPost'])) {
                // Post a message first...
                $message = stripslashes($_REQUEST['cbMessage']);
                $CB->post($AdminAccount->getID(),$message);
            }
            $entries = $CB->get();
            
            $pl .= '<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>';
            $pl .= '<TR>';
            $pl .= '<TD CLASS=task3 COLSPAN=2 STYLE="font-size:100%;border: none;">Message Board</TD>';
            $pl .= '</TR><TR>';
            $pl .= '<TD CLASS=task3 COLSPAN=2>&nbsp;</TD>';
            $pl .= '</TR>';
            if (is_array($entries)) foreach ($entries as $entry) {
                $time = date("g:ia",$entry['created']);
                $date = date("M j, Y",$entry['created']);
                if (time() - $entry['created'] < 172800) { $date = '<SPAN STYLE="color:#8CE;">Yesterday</SPAN>'; }
                if (time() - $entry['created'] < 86400) { $date = '<SPAN STYLE="color:#EC8;">Today</SPAN>'; }  //#EC8
                $pl .= '<TR>';
                $user = $AdminAccount->finger($entry['user']);
                $pl .= '<TD CLASS=message1 ALIGN=LEFT>'. $user['uFName'] . ' ' . $user['uLName'].'</TD>';
                $pl .= "<TD CLASS=message2 ALIGN=RIGHT>$date at $time</TD>";
                $pl .= '</TR><TR>';
                $pl .= '<TD CLASS=message3 COLSPAN=2>'.$entry['message'].'</TD>';
                $pl .= '</TR><TR>';
                $pl .= '<TD COLSPAN=2 HEIGHT=8><IMG SRC=images/1px.gif></TD>';
                $pl .= '</TR>';
            }
            $pl .= '</TABLE>';
            
            // Now for adding a submission
            
            $pl .= '<FORM NAME=subMessage METHOD=POST>';
            $pl .= '<INPUT TYPE=HIDDEN NAME=cbPost VALUE=true>';
            $pl .= '<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>';
            $pl .= '<TD ALIGN=LEFT COLLSPAN=2 CLASS=auth VALIGN=TOP>Add a message:</TD>';
            $pl .= '</TR><TR>';
            $pl .= '<TD ALIGN=LEFT CLASS=auth COLSPAN=3><TEXTAREA NAME=cbMessage CLASS=t1 ROWS=3 COLS=90></TEXTAREA></TD>';
            //$pl .= '</TR><TR>';
            $pl .= '<TD ALIGN=LEFT CLASS=auth COLSPAN=4><INPUT TYPE=SUBMIT CLASS=s1 VALUE=POST></TD>';
            $pl .= '</TR>';
            $pl .= '</TABLE>';
            $pl .= '</FORM>';
            
            //This is the first message for the [B]message board[/B]... Hopefully we can all use this as a tool to communicate to each other like a [I]Messaging System[/I]. Think of it as a [COLOR=yellow]Post-It[/COLOR] note that everyone can read, and the janitor can't throw away... 
        }
    }
#   +----------------------------------------------------
#   | Authenticate
#   +----------------------------------------------------


    if ($AdminAccount->has_access(39)) {
        // Admin
        $html = "<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=100% HEIGHT=600>";
        if ($act == 'mn') {
            $html .= '<meta http-equiv="refresh" content="3">';
        }
        $html .= "<TR>";
#       +----------------------------------------------------
#       | Nav Panel
#       +---------------------------------------------------- 
        $html .= "<TD VALIGN=TOP WIDTH=201>";
        $html .= "<DIV CLASS=\"nav\">";
        $html .= "<DIV CLASS=\"navi\">";
        $html .= "navigation";
        $html .= "</DIV>";
        $html .= "</DIV>";
        $html .= "<DIV CLASS=\"nav\" STYLE=\"border-top:none;padding:0px 3px 3px 3px;\">";
        $html .= "<DIV CLASS=\"navit\">";
        $html .= "<A HREF=\"?home\">Message Board</A><BR>";
        $html .= "<A HREF=\"?act=todo&tview=overview\">Task List</A><BR>";
        if ($act == 'todo') {
            $html .= "<UL>";
            $html .= "<LI><A HREF=\"?act=todo&tview=view\" STYLE=\"padding:0px 0px 0px 10px;\">Active Tasks</A></LI>";
            $html .= "<LI><A HREF=\"?act=todo&tview=viewc\" STYLE=\"padding:0px 0px 0px 10px;\">Finished Tasks</A></LI>";
            $html .= "<DIV STYLE=\"margin:0px 0px 3px 10px;border-bottom:1px solid #444;width:100px;\"><IMG BORDER=0 SRC=images/1px.gif></DIV>";
            $html .= "<LI><A HREF=\"?act=todo&tview=viewall\" STYLE=\"padding:0px 0px 0px 10px;\">All Tasks</A></LI>";
            $html .= "<LI><A HREF=\"?act=todo&tview=new\" STYLE=\"padding:0px 0px 0px 10px;\">New Task</A></LI>";
            $html .= "</UL>";
        }
        $html .= "<DIV STYLE=\"margin:1px 0px 3px 10px;border-bottom:1px solid #444;width:175px;\"><IMG BORDER=0 SRC=images/1px.gif></DIV>";
        $html .= "<A HREF=\"?act=uo\">Users Online</A><BR>";
        $html .= "<A HREF=\"?act=rp\">Reports</A><BR>";
        if ($act == 'rp') {
            $html .= "<UL>";
            $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits\" STYLE=\"padding:0px 0px 0px 10px;\">Monthly Visits</A></LI>";
            if ($reportType == 'monthlyVisits') {
                $html .= "<UL>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=Janurary\" STYLE=\"padding:0px 0px 0px 10px;\">Janurary</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=February\" STYLE=\"padding:0px 0px 0px 10px;\">February</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=March\" STYLE=\"padding:0px 0px 0px 10px;\">March</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=April\" STYLE=\"padding:0px 0px 0px 10px;\">April</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=May\" STYLE=\"padding:0px 0px 0px 10px;\">May</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=June\" STYLE=\"padding:0px 0px 0px 10px;\">June</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=July\" STYLE=\"padding:0px 0px 0px 10px;\">July</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=August\" STYLE=\"padding:0px 0px 0px 10px;\">August</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=September\" STYLE=\"padding:0px 0px 0px 10px;\">September</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=October\" STYLE=\"padding:0px 0px 0px 10px;\">October</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=November\" STYLE=\"padding:0px 0px 0px 10px;\">November</A></LI>";
                $html .= "<LI><A HREF=\"?act=rp&rtype=monthlyVisits&month=December\" STYLE=\"padding:0px 0px 0px 10px;\">December</A></LI>";
                $html .= "</UL>";
            
            }
            $html .= "</UL>";
        }
        $html .= "<A HREF=\"?act=vl&span=86400\">Visitor Logs</A><BR>";
        if ($act == 'vl') {
            $html .= "<UL>";
            $html .= "<LI><A HREF=\"?act=vl&span=604800\" STYLE=\"padding:0px 0px 0px 10px;\">Past Week</A></LI>";
            $html .= "<LI><A HREF=\"?act=vl&span=2678400\" STYLE=\"padding:0px 0px 0px 10px;\">Past Month</A></LI>";
            $html .= "</UL>";
        }
        $html .= "<DIV STYLE=\"margin:1px 0px 3px 10px;border-bottom:1px solid #444;width:175px;\"><IMG BORDER=0 SRC=images/1px.gif></DIV>";
        $html .= "<A HREF=\"?act=db\">Databases</A><BR>";
        if ($act == 'db') {
            $html .= "<UL>";
            $html .= "<LI><A HREF=\"?act=db&table=session\" STYLE=\"padding:0px 0px 0px 10px;\">Session</A></LI>";
            if ($dbTable == 'session') {
                $html .= "<UL>";
                $html .= "<LI><A HREF=\"?act=db&table=session&time=60\">1 Hour</A></LI>";
                $html .= "<LI><A HREF=\"?act=db&table=session&time=1440\">24 Hours</A></LI>";
                $html .= "<LI><A HREF=\"?act=db&table=session&time=43200\">1 Month</A></LI>";
                // $html .= "<LI><A HREF=\"?act=db&table=session&time=525600\">1 Year</A></LI>";
                $html .= "</UL>";
            }
            $html .= "<LI><A HREF=\"?act=db&table=account\" STYLE=\"padding:0px 0px 0px 10px;\">Account</A></LI>";
            if ($dbTable == 'account') {
                $html .= "<UL>";
                $html .= "<LI><A HREF=\"?act=db&table=account&type=active\">Top Active</A></LI>";
                $html .= "<LI><A HREF=\"?act=db&table=account&type=created\">Top Created</A></LI>";
                $html .= "<LI><A HREF=\"?act=db&table=account&type=visits\">Top Visits</A></LI>";
                //$html .= "<LI><A HREF=\"?act=db&table=account&time=525600\">Disoriented</A></LI>";
                $html .= "</UL>";
            }
            $html .= "<LI><A HREF=\"?act=db&table=records\" STYLE=\"padding:0px 0px 0px 10px;\">Records</A></LI>";
            if ($dbTable == 'records') {
                $html .= "<UL>";
                $html .= "<LI><A HREF=\"?act=db&table=records&span=cday\" STYLE=\"padding:0px 0px 0px 10px;\">Today</A></LI>";
                $html .= "<LI><A HREF=\"?act=db&table=records&span=cmonth\" STYLE=\"padding:0px 0px 0px 10px;\">This Month</A></LI>";
                $html .= "<LI><A HREF=\"?act=db&table=records&span=cyear\" STYLE=\"padding:0px 0px 0px 10px;\">This Year</A></LI>";
                $html .= "<DIV STYLE=\"margin:0px 0px 3px 10px;border-bottom:1px solid #444;width:100px;\"><IMG BORDER=0 SRC=images/1px.gif></DIV>";
                $html .= "<LI><A HREF=\"?act=db&table=records&span=lmonth\" STYLE=\"padding:0px 0px 0px 10px;\">Last Month</A></LI>";
                $html .= "<LI><A HREF=\"?act=db&table=records&span=lyear\" STYLE=\"padding:0px 0px 0px 10px;\">Last Year</A></LI>";
                $html .= "</UL>";
            }
            $html .= "<LI><A HREF=\"?act=db&table=lessons\" STYLE=\"padding:0px 0px 0px 10px;\">lessons</A></LI>";
            if ($dbTable == 'lessons') {
                $html .= "<UL>";
                $html .= "<LI><A HREF=\"?act=db&table=lessons&t=search\" STYLE=\"padding:0px 0px 0px 10px;\">Search</A></LI>";
                $html .= "</UL>";
            }
            $html .= "</UL>";
        }
        $html .= "<A HREF=\"?act=sp\">Search Placements</A><BR>";
        if ($act == 'sp') {
            $html .= "<UL>";
            $html .= "<LI><A HREF=\"?act=sp&sec=chart\" STYLE=\"padding:0px 0px 0px 10px;\">Chart</A></LI>";
            $html .= "<LI><A HREF=\"?act=sp&sec=mv\" STYLE=\"padding:0px 0px 0px 10px;\">Movement</A></LI>";
            if ($AdminAccount->has_access(40)) {
                $html .= "<LI><A HREF=\"?act=sp&sec=pl\" STYLE=\"padding:0px 0px 0px 10px;\">Placements</A></LI>";
            }
            $html .= "<LI><A HREF=\"?act=sp&sec=sum\" STYLE=\"padding:0px 0px 0px 10px;\">DB Summary</A></LI>";
            $html .= "<LI><A HREF=\"?act=sp&sec=res\" STYLE=\"padding:0px 0px 0px 10px;\">Results</A></LI>";
            //$html .= "<LI><A HREF=\"?act=sp&sec=test\" STYLE=\"padding:0px 0px 0px 10px;\"><SPAN STYLE=\"color:#F00;\">test</SPAN></A></LI>";
            $html .= "</UL>";
        }
        $html .= "</DIV>";
        $html .= "</DIV>";
        // Online Users
        // Visitor Logs
        // 
        $html .= "</TD>";
        
#       +----------------------------------------------------
#       | Main Content
#       +----------------------------------------------------
        $html .= "<TD>";
        $html .= "<DIV CLASS=\"navc\">";
        $html .= "<DIV CLASS=\"navci\">";
        $html .= $pl;
        $html .= "</DIV>";
        $html .= "</DIV>";
        $html .= "</TD>";
        
#       +----------------------------------------------------
#       | All Done
#       +----------------------------------------------------
        $html .= "</TR>";
        $html .= "</TABLE>";
    } else {
        $AdminSession->post("/w2/n/np",5000);
        $html .= "<CENTER>";
        $html .= "<DIV CLASS=\"nav\" STYLE=\"margin:40px 3px 3px 3px;width:400px\">";
        $html .= "<DIV CLASS=\"navi\">";
        $html .= "Authenticate";
        $html .= "</DIV>";
        $html .= "</DIV>";
        $html .= "<DIV CLASS=\"nav\" STYLE=\"border-top:none;padding:0px 3px 3px 3px;width:400px;\">";
        $html .= "<DIV CLASS=\"navit\">";
        $html .= "<FORM METHOD=POST NAME=auth>";
        $html .= "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>";
        $html .= "<TR><TD CLASS=auth ALIGN=RIGHT>Login:</TD><TD><INPUT SIZE=32 CLASS=t1 NAME=alogin></TD></TR>";
        $html .= "<TR><TD CLASS=auth ALIGN=RIGHT>Password:</TD><TD><INPUT SIZE=18 CLASS=t1 TYPE=PASSWORD NAME=apass></TD></TR>";
        $html .= "<TR><TD CLASS=auth ALIGN=CENTER COLSPAN=2><INPUT CLASS=s1 TYPE=SUBMIT VALUE=VALIDATE></TD></TR>";
        $html .= "<INPUT TYPE=HIDDEN NAME=act VALUE=login>";
        $html .= "</TABLE>";
        $html .= "</FORM>";
        $html .= "</DIV>";
        $html .= "</DIV>";
        $html .= "</CENTER>";
    }
    //print_r($AdminSession->agentID('Googlebot/2.1 (+http://www.google.com/bot.html)'));
    echo "$html\n</BODY>";
    
    $AdminSession->save();
    
    //$MajorData = file_get_contents('yahooData.txt');
    //echo "<PRE>".print_r($Bot->sweepYahoo($MajorData),true)."</PRE>";  
?>
