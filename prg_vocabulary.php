<?php
/***************************************************************************
 *   Copyright (C) 2004 by 5Muses Software LLC                             *
 *   info@5muses.com                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


class fmPrgVocabulary {

    var $vSearch       = "";                // Last Search String
    var $vBrowse       = "A";               // Browsing Letter
    var $vSearchMode   = true;              // SubSearch Mode
    var $vFilter       = false;             // Are we filtering the results?
    var $vLanguage     = 2;                 // Current Language
    var $vIndex        = 1;                 // Index of place where we are looking
    var $vSQLError     = "";
    var $vSQLCount     = 0;
    var $vSQLTotal     = 0;
    var $vSQLPage      = 0;
    var $vSQLPageCount = 0;
    var $vSearchAction = "unknown";

    //------------------------------------------------------------
    //  Initializes the base variables
    //------------------------------------------------------------
    function fetch_words($limit=25,$lang=0) {
        global $db_link;
        // This returns an array of stuff either browsing or searching
        
        // Calculate the offset for the query
        $this->vIndex = $this->vSQLPage * $limit;
        $this->vSearchAction = "none";
        if ($this->vFilter) {
            // Filter active... do a sub-search
            if ($this->vSearchMode) {
                $sql = "SELECT count(wID) as total FROM Language WHERE ((wNative LIKE '%{$this->vSearch}%') OR (wForeign LIKE '%{$this->vSearch}%')) AND wLang = '{$this->vLanguage}'";
                $count_result = mysqli_query( $db_link, $sql);
                $row = mysqli_fetch_array($count_result);
                $this->vSQLTotal = $row['total'];
                ((mysqli_free_result($count_result) || (is_object($count_result) && (get_class($count_result) == "mysqli_result"))) ? true : false);
                
                // Search by a term
                $sql = "SELECT wID, wNative, wForeign, wFlag2, wLang, wSHA1 FROM \n";
                $sql .= "Language WHERE \n";
                $sql .= "((wNative LIKE '%{$this->vSearch}%') OR (wForeign LIKE '%{$this->vSearch}%')) AND wLang = '{$this->vLanguage}' LIMIT {$this->vIndex},$limit";
                $s_result = mysqli_query( $db_link, $sql);
                $this->vSearchAction = "filter:search";
            } else {
                $sql = "SELECT count(wID) as total FROM Language WHERE (wForeign LIKE '{$this->vBrowse}%') AND wLang = '{$this->vLanguage}'";
                $count_result = mysqli_query( $db_link, $sql);
                $row = mysqli_fetch_array($count_result);
                $this->vSQLTotal = $row['total'];
                ((mysqli_free_result($count_result) || (is_object($count_result) && (get_class($count_result) == "mysqli_result"))) ? true : false);
                
                // Browse by a letter
                $sql = "SELECT wID, wNative, wForeign, wFlag2, wLang, wSHA1 FROM \n";
                $sql .= "Language WHERE \n";
                $sql .= "(wForeign LIKE '{$this->vBrowse}%') AND wLang = '{$this->vLanguage}' LIMIT {$this->vIndex},$limit";
                $s_result = mysqli_query( $db_link, $sql);
                $this->vSearchAction = "filter:browse";
            }
        } else {
            $sql = "SELECT count(wID) as total FROM Language WHERE wLang = '{$this->vLanguage}'";
            $count_result = mysqli_query( $db_link, $sql);
            $row = mysqli_fetch_array($count_result);
            $this->vSQLTotal = $row['total'];
            ((mysqli_free_result($count_result) || (is_object($count_result) && (get_class($count_result) == "mysqli_result"))) ? true : false);
            
            // Filter not active, fetch all words
            $sql = "SELECT wID, wNative, wForeign, wFlag2, wLang, wSHA1 FROM \n";
            $sql .= "Language WHERE wLang = '{$this->vLanguage}' LIMIT {$this->vIndex},$limit";
            $s_result = mysqli_query( $db_link, $sql);
            $this->vSearchAction = "list";
        }
        $this->vSQLPageCount = intval(ceil($this->vSQLTotal / $limit));
        $this->vSQLError = ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
        $this->vSQLCount = mysqli_num_rows($s_result);
        // Got the result set, now git da werdz
        $res = array(); 
        while ($row = mysqli_fetch_object($s_result)) {
            $resline = array("ID" => $row->wID, "F" => $row->wForeign, "N" => $row->wNative, "SHA1" => $row->wSHA1, "Flag" => $row->wFlag2);
            $res[] = $resline;
        }
        return $res;
    }
    
    function preserve() {
        // This returns a string to preserve the state of the vocab engine
        $pres = "";
        $pres .= "{$this->vSearch}|";
        $pres .= "{$this->vBrowse}|";
        $pres .= $this->vSearchMode ? "true|" : "false|";
        $pres .= $this->vFilter ? "true|" : "false|";
        $pres .= "{$this->vLanguage}|";
        $pres .= "{$this->vSQLPage}|";
        $pres .= "{$this->vSQLCount}|";
        $pres .= "{$this->vSQLTotal}|";
        $pres .= "{$this->vSQLPageCount}";
        
        return $pres;
    }

    function nextpage() {
        if ($this->vSQLPage < $this->vSQLPageCount) $this->vSQLPage++;
    }
    
    function prevpage() {
        if ($this->vSQLPage > 0) $this->vSQLPage--;
    }
    
    function gotopage($page) {
        $this->vSQLPage = (($page <= $this->vSQLPageCount) and ($page >= 0)) ? $page : $this->vSQLPage;
    }
    
    function lang_prefix() {
        $lang = array("na_","na_","sp_","ge_","fr_");
        return $lang[$this->vLanguage];
    }
    
    function restore($data) {
        // This returns a string to preserve the state of the vocab engine
        list($this->vSearch,$this->vBrowse,$sm,$f,$this->vLanguage,$this->vSQLPage,$this->vSQLCount,$this->vSQLTotal,$this->vSQLPageCount) = explode('|',$data);
        $this->vSearchMode = ($sm == 'true');
        $this->vFilter = ($f == 'true');
    }
            
}            
/*
            $d1 = $Session->get('/program/dview/dpos');
            if ($search === true) {
                $searchvalue = mysql_escape_string($searchvalue);
                $sql = "SELECT wID, wNative, wForeign, wFlag2, wLang, wSHA1 FROM \n";
                $sql .= "Language WHERE \n";
                $sql .= "((wNative LIKE '%$searchvalue%') OR (wForeign LIKE '%$searchvalue%')) AND wLang = '$l' LIMIT $d1,15";
                $Timer->start('sql');
                $s_result = mysql_query($sql, $db_link);
                $Timer->stop('sql');
                $sql = "SELECT count(wID) as total FROM \n";
                $sql .= "Language WHERE \n";
                $sql .= "((wNative LIKE '%$searchvalue%') OR (wForeign LIKE '%$searchvalue%')) AND wLang = '$l'";
                $Timer->start('sql');
                $count_result = mysql_query($sql, $db_link);
                $Timer->stop('sql');
                $row = mysql_fetch_array($count_result);
                $s_count = $row['total'];
                mysql_free_result($count_result);
            } else {
                // Get Count
                $sql = "SELECT count(wID) as total FROM Language WHERE wLang = '$l'";
                $Timer->start('sql');
                $count_result = mysql_query($sql, $db_link);
                $Timer->stop('sql');
                $row = mysql_fetch_array($count_result);
                $s_count = $row['total'];
                mysql_free_result($count_result);

                //$Timer->start('wordsearch');
                $Timer->start('sql');
                $s_result = mysql_query("SELECT wID, wNative, wForeign, wFlag2, wSHA1 FROM Language WHERE wLang = '$l' LIMIT $d1,15",$db_link);
                //$Timer->stop('wordsearch');
                $Timer->stop('sql');
            }
*/          
?>
