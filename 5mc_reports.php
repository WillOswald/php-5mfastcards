<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//
class fmcReports {

    //------------------------------------------------------------
    //  Variable Declarations
    //------------------------------------------------------------
    
    //------------------------------------------------------------
    //  Initializes the base variables
    //------------------------------------------------------------
    function init() {
        // Calculate time for the basic intervals
        
    }
    
    function genMonth($month,$year) {
        global $db_link;
        
        $begin = strtotime("1 $month $year");
        $end = strtotime("+1 month",$begin) - 1;
        list($fom,$days) = explode(',',date("w,t"));
        $month = array();
        
        $result = mysqli_query($db_link, "SELECT logID,entry_time,member FROM log_enter WHERE entry_time > $begin and entry_time < $end");
        while ($row = mysqli_fetch_assoc($result)) {
            $day = intval(date("j",$row['entry_time']));
            if (!is_array($month[$day])) {
                $month[$day] = array();
                $month[$day][0] = 0;
                $month[$day][1] = 0;
            }
            $month[$day][intval($row['member'])]++;
        }
        // echo "$begin<BR>$end<BR>";
        return $month;
    }
    //------------------------------------------------------------
    //  How many online right now...
    //------------------------------------------------------------
    function usersOnline() {
        global $db_link;
        
        $on_members = 0;
        $on_guests = 0;
        $result = mysqli_query($db_link, "SELECT sSessionID FROM Session WHERE NOW() <= sExpires");
        if ($result !== false) {
            // Get all the Sessions...
            $data = array();
            while ($row = mysqli_fetch_assoc($result)) {
                //$pl .= $row['sSessionID']."<BR>";
                $data[] = $row['sSessionID'];
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            // Get Account Info For Each Session...
            reset($data);
            foreach($data as $sid) {
                $result = mysqli_query($db_link, "SELECT count(uID) as total FROM Account WHERE uSessionID='$sid'");
                $row = mysqli_fetch_assoc($result);
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                if (intval($row['total']) > 0) {
                    // Got a member
                    $on_members++;
                } else {
                    // Got a guest
                    $on_guests++;
                }
            }
        }
        $online = array('guests' => $on_guests, 'members' => $on_members);
        return $online;
    }
    
    //------------------------------------------------------------
    //  List who is online right now.
    //------------------------------------------------------------
    function listOnline() {
        global $db_link;
        
        $memberList = array();
        $guestList = array();
        $result = mysqli_query($db_link, "SELECT * FROM Session WHERE NOW() <= sExpires");
        if ($result !== false) {
            // Get all the Sessions...
            $data = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $line = array();
                foreach ($row as $field => $value) {
                    $line[$field] = $value;
                }
                $data["{$row['sSessionID']}"] = $line;
                
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            // Get Account Info For Each Session...
            reset($data);
            foreach($data as $sid => $session) {
                $result = mysqli_query($db_link, "SELECT * FROM Account WHERE uSessionID='$sid'");
                if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                    $row = mysqli_fetch_assoc($result);
                    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                    // Got a member
                        // Name, Email, City, State, Visits, Place, Browser, Res, 
                    $memberData = array();
                    $memberData['name'] = $row['uLName'] . ', ' . $row['uFName'];
                    $memberData['email'] = $row['uEmail'];
                    $memberData['city'] = $row['uCity'];
                    $memberData['state'] = $row['uState'];
                    $memberData['visits'] = $row['uVisits'];
                    $sessionData = unserialize(base64_decode($session['sSessionData']));
                    $memberData['place'] = $sessionData['w2']['n']['np'];
                    $logEntry = $this->getLogEntry($sessionData['log']['logid']);
                    $memberData['ident'] = ($logEntry['browser'] == 'Unknown') ? $logEntry['mozName'] : $logEntry['browser'];
                    $memberData['os'] = $logEntry['os'];
                    $memberData['sres'] = $logEntry['sres'];
                    $memberData['hostname'] = $logEntry['hostname'];
                    $memberData['ipAddress'] = $logEntry['ipAddress'];
                    $memberData['port'] = $logEntry['port'];
                    $memberData['clicks'] = $logEntry['clicks'];
                    $memberList[] = $memberData;
                } else {
                        // IP Address, Hostname, Place, Browser, Res, 
                    $memberData = array();
                    $sessionData = unserialize(base64_decode($session['sSessionData']));
                    $memberData['place'] = $sessionData['w2']['n']['np'];
                    $memberData['placeName'] = $this->navDecode($memberData['place']);
                    $logEntry = $this->getLogEntry($sessionData['log']['logid']);
                    $memberData['ident'] = ($logEntry['browser'] == 'Unknown') ? $logEntry['mozName'] : $logEntry['browser'];
                    $memberData['os'] = $logEntry['os'];
                    $memberData['sres'] = $logEntry['sres'];
                    $memberData['hostname'] = $logEntry['hostname'];
                    $memberData['ipAddress'] = $logEntry['ipAddress'];
                    $memberData['port'] = $logEntry['port'];
                    $memberData['clicks'] = $logEntry['clicks'];
                    $guestList[] = $memberData;
                }
            }
        }
        $online = array('guests' => $guestList, 'members' => $memberList);
        return $online;
    }
    
    function getVisitData($count,$frame,$interval) {
        // (3,day,day)  (1,month,day)  (1,day,hour)
        if (strpos("jan,feb,mar,apr,may,jun,jul,aug,sep",$frame) !== false)
        $begining = strtotime("$count $frame");
        
        // ????
        
        $t1result = mysqli_query($db_link, "SELECT COUNT(*) as Total FROM log_enter WHERE entry_time > (UNIX_TIMESTAMP(CURDATE())) GROUP BY member");
        $row = mysqli_fetch_assoc($t1result);
        $num_today = intval($row['Total']);
        ((mysqli_free_result($t1result) || (is_object($t1result) && (get_class($t1result) == "mysqli_result"))) ? true : false);
        $t2result = mysqli_query($db_link, "SELECT COUNT(*) as Total FROM log_enter WHERE entry_time > (UNIX_TIMESTAMP(CURDATE() - INTERVAL 1 DAY)) GROUP BY member");
        $row = mysqli_fetch_assoc($t2result);
        $num_yesterday = intval($row['Total']) - $num_today;
        ((mysqli_free_result($t2result) || (is_object($t2result) && (get_class($t2result) == "mysqli_result"))) ? true : false);
        $t3result = mysqli_query($db_link, "SELECT COUNT(*) as Total FROM log_enter WHERE entry_time > (UNIX_TIMESTAMP(CURDATE() - INTERVAL 2 DAY)) GROUP BY member");
        $row = mysqli_fetch_assoc($t3result);
        $num_daybefore = intval($row['Total']) - $num_today - $num_yesterday;
        ((mysqli_free_result($t3result) || (is_object($t3result) && (get_class($t3result) == "mysqli_result"))) ? true : false);
        $blah = array($num_today,$num_yesterday,$num_daybefore);
        return $blah;
    }
    
    //------------------------------------------------------------
    //  List who is online right now.
    //------------------------------------------------------------
    function chartVisitsDaily($when='this week') {
        global $db_link;
        
        if ($when == 'this week') {
            // Calculate time span
            $dayofweek = date('w');
            $begining = strtotime("-$dayofweek day");
            $ending = $beginging + 604800;
        }
        $sql = "SELECT logID FROM log_enter WHERE entry_time > FROM_UNIXTIME($begining) AND entry_time < FROM_UNIXTIME($ending)";
    }
    
    //------------------------------------------------------------
    //  Function for the referal statistics
    //------------------------------------------------------------
    function refererStats($from=0,$to=0) {
        global $db_link;
    
        $result = mysqli_query($db_link, "SELECT count(refer) as total,refer FROM log_enter WHERE refer IS NOT NULL ORDER BY refer");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            while ($row = mysqli_fetch_assoc($result)) {
                // Here we get all the rows of the referers
            }
        }
    }
    
    //------------------------------------------------------------
    //  Query DNS Cache before Querying server
    //------------------------------------------------------------
    function dnsHostname($ip) {
        global $db_link;
        $result = mysqli_query($db_link, "SELECT ip_hostname FROM log_dnscache WHERE ip_address=INET_ATON('$ip')");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            // Got the IP and Hostname from the cache...
            $row = mysqli_fetch_assoc($result);
            $res = $row['ip_hostname'];
            if ($res == '') {
                $res = "[$ip]";
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            //echo "Cache: HIT ......(".print_r($res,true).") ".mysql_error()."<BR>";
            return $res;
        } else {
            // Cache miss, query DNS and update cache
            $ipName = @gethostbyaddr($ip);
            $eipName = ($ipName == $ip) ? "NULL" : "'".((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $ipName) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""))."'";
            mysqli_query($db_link, "INSERT INTO log_dnscache SET ip_hostname = $eipName, ip_address=INET_ATON('$ip')");
            //echo "Cache: MISS ...... ".mysql_error()."<BR>";
            return $ipName;
        }
        
    }
    
    //------------------------------------------------------------
    //  Count New Accounts for a specific Day
    //------------------------------------------------------------
    function countNewAccounts($year,$month,$day) {
        global $db_link;
        
        $start = date('Y-m-d H:i:s',strtotime("$year-$month-$day 00:00:00")); 
        $end = date('Y-m-d H:i:s',strtotime("$year-$month-$day 23:59:59")); 
        $result = mysqli_query($db_link, "SELECT count(*) as total FROM Account WHERE uCreated > '$start' AND uCreated < '$end'");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            // Got the IP and Hostname from the cache...
            $row = mysqli_fetch_assoc($result);
            $res = $row['total'];
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        } else {
            $res = -1;
        }
        return $res;
        //return $start;
    }
    
    //------------------------------------------------------------
    //  Get data from a log entry
    //------------------------------------------------------------
    function getLogEntry($logID) {
        global $db_link;
        // Log Information
        $result = mysqli_query($db_link, "SELECT *,INET_NTOA(ip_address) as ip_addr FROM log_enter WHERE logID='$logID'");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            $row = mysqli_fetch_assoc($result);
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            $ip = $row['ip_addr'];
            $port = $row['ip_port'];
            $sres = $row['s_res_x'].'x'.$row['s_res_y'].' '.$row['s_res_d'].'bit';
            $cres = $row['c_res_x'].'x'.$row['c_res_y'];
            $bid = $row['browser_id'];
            $oid = $row['os_id'];
            $mid = $row['moz_id'];
            $rid = $row['refered'];
            $clicks = $row['clicks'];
            //$ipName = @gethostbyaddr($ip);
            $ipName = $this->dnsHostname($ip);
            //if ($ipName == $ip) $ipName = $ip;
            $result = mysqli_query($db_link, "SELECT * FROM log_browser WHERE ID='$bid'");
            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                $row = mysqli_fetch_assoc($result);
                $browser = $row['browser']." ".$row['ver'];
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            }
            $result = mysqli_query($db_link, "SELECT * FROM log_moz WHERE ID='$bid'");
            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                $row = mysqli_fetch_assoc($result);
                $mozName = $row['name'];
                $mozVer = $row['ver'];
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            }
            $result = mysqli_query($db_link, "SELECT * FROM log_os WHERE ID='$oid'");
            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                $row = mysqli_fetch_assoc($result);
                $os = $row['os'];
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            }
            $result = mysqli_query($db_link, "SELECT * FROM log_refer WHERE ID='$rid'");
            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                $row = mysqli_fetch_assoc($result);
                $rDomain = $row['domain'];
                $rArgv = $row['argv'];
                ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            }
            $res = array();
            $res['ipAddress'] = $ip;
            $res['port'] = $port;
            $res['hostname'] = $ipName;
            $res['sres'] = $sres;
            $res['cres'] = $cres;
            $res['bres'] = $bres;
            $res['browser'] = $browser;
            $res['os'] = $os;
            $res['mozName'] = $mozName;
            $res['mozVer'] = $mozVer;
            $res['refer'] = $rDomain;
            $res['referArgv'] = $rArgv;
            $res['clicks'] = $clicks;
            return $res;
        } else {
            return false;
        }
    }
    
    function navDecode($place) {
        $nav1dec = array(
                '1000' => 'Web Page / Main Home Page',
                '1100' => 'Web Page / Latest News',
                '1110' => 'Web Page / About The Company',
                '1120' => 'Web Page / 5m History',
                '1130' => 'Web Page / Contact Us',
                '1140' => 'Web Page / Learn Spanish',
                '1150' => 'Web Page / Learn French',
                '1160' => 'Web Page / Learn German',
                '1170' => 'Web Page / Support',
                '1180' => 'Web Page / Vocabulary',
                '1190' => 'Web Page / Tutorial',
                '2000' => 'Fast Cards',
                '2100' => 'Fast Cards / Introduction',
                '2100' => 'Fast Cards / Tutorial',
                '2110' => 'Fast Cards / Pre-Made Lessons',
                '2120' => 'Fast Cards / Purchasing',
                '2130' => 'Fast Cards / Word Database',
                '2140' => 'Fast Cards / Lesson Builder',
                '2150' => 'Fast Cards / Show Flash Cards',
                '2151' => 'Fast Cards / Show Flash Cards (running)',
                '3000' => 'Account',
                '3100' => 'Account / Overview',
                '3110' => 'Account / Billing History',
                '3120' => 'Account / Edit Information',
                '4000' => 'Support',
                '4100' => 'Support / Checklist',
                '4110' => 'Support / Common Questions',
                '4120' => 'Support / Send Feedback',
                '5000' => 'Admin Panel - Authorizing',
                '5100' => 'Admin Panel',
                '5110' => 'Admin Panel - Users Online',
                '5400' => 'Admin Panel - Reports',
                '5401' => 'Admin Panel - Reports - Monthly Visits',
                '5120' => 'Admin Panel - View Logs',
                '5210' => 'Admin Panel - [DB] Session',
                '5220' => 'Admin Panel - [DB] Account',
                '5230' => 'Admin Panel - [DB] Records',
                '5240' => 'Admin Panel - [DB] Transactions',
                '5300' => 'Admin Panel - Monitoring Session',
                );
        return $navDecode["$place"];
    }
    /*
        Online
            Now
            Week
            Month
        Browsers
            Today
            Week
            Month
        Screen Resolutions
            Today
            Week
            Month
        Visits
            Today
            Week
            Month
    */
    
}

?>
