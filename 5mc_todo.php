<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//


// This is a little "todo thing" to help me keep track of what the heck am I keeping track of?

class fmcTodo {
    
    function fmcTodo() {
        global $db_link;
        
        // Initialize some stuff
    }
    
    function newTask($owner, $subject, $priA, $priB, $notes) {
        global $db_link;
        
        // Post a new note
        $eSubject = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $subject) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $eNotes = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $notes) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        mysqli_query($db_link, "INSERT INTO Tasks SET owner='$owner', subject='$eSubject', priorityA='$priA', priorityB='$priB', started=NOW(), notes='$eNotes', deleted=0");
        //echo mysql_error;
    }
    
    function alterTask($id, $owner, $subject, $priA, $priB, $notes) {
        global $db_link;
        
        // Post a new note
        $eSubject = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $subject) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $eNotes = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $notes) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        //echo "<PRE>$eNotes</PRE><HR><PRE>$notes</PRE>";
        mysqli_query($db_link, "UPDATE Tasks SET owner='$owner', subject='$eSubject', priorityA='$priA', priorityB='$priB', updated=NOW(), notes=CONCAT(notes,'$eNotes') WHERE taskID='$id'");
    }
    
    function finishTask($id) {
        global $db_link;
        
        // Mark a task as complete
        $eid = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $id) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        mysqli_query($db_link, "UPDATE Tasks SET completed=NOW() WHERE taskID='$eid'");
    }
    
    function unfinishTask($id) {
        global $db_link;
        
        // Mark a task as complete
        $eid = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $id) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        mysqli_query($db_link, "UPDATE Tasks SET completed=NULL WHERE taskID='$eid'");
    }
    
    function listTasks($owner=false) {
        global $db_link;
        
        if ($owner === false) {
            $result = mysqli_query($db_link, "SELECT notes as tnotes,taskID,subject,priorityA,priorityB,started,updated,completed,owner from Tasks WHERE deleted=0 ORDER BY owner, priorityA, priorityB");
            $completed = array();
            $incomplete = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $data = array();
                foreach ($row as $field => $value) {
                    $data[$field] = $value;
                }
                if ($row['completed'] == '') {
                    $incomplete[] = $data;
                } else {
                    $completed[] = $data;
                }
            }
        } else {
            // List tasks for this person
            $result = mysqli_query($db_link, "SELECT notes as tnotes,taskID,subject,priorityA,priorityB,started,updated,completed from Tasks WHERE owner='$owner' AND deleted=0 ORDER BY priorityA, priorityB");
            $completed = array();
            $incomplete = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $data = array();
                foreach ($row as $field => $value) {
                    $data[$field] = $value;
                }
                if ($row['completed'] == '') {
                    $incomplete[] = $data;
                } else {
                    $completed[] = $data;
                }
            }
        }
        $aggregate = array('completed' => $completed, 'incomplete' => $incomplete);
        return $aggregate;
    }
    
    function getTask($taskid) {
        global $db_link;
        
        // Fetch a specific task...
        $result = mysqli_query($db_link, "SELECT * from Tasks WHERE taskID=$taskid");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            $row = mysqli_fetch_assoc($result);
            $data = array();
            foreach ($row as $field => $value) {
                $data[$field] = $value;
            }
        }
        return $data;
    }
    
    function listTaskOwners() {
        global $db_link;
        
        // Need I say more?
        $result = mysqli_query($db_link, "SELECT count(*) as total, owner FROM Tasks GROUP BY OWNER");
        $owners = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $data = array();
            $data['total'] = $row['total'];
            $data['id'] = $row['owner'];
            $oresult = mysqli_query($db_link, "SELECT uFName,uLName FROM Account WHERE uSHA1='{$data['id']}'");
            if (($oresult !== false) && (mysqli_num_rows($oresult) > 0)) {
                $orow = mysqli_fetch_assoc($oresult);
                $data['name'] = $orow['uFName'] . ' ' . $orow['uLName'];
                ((mysqli_free_result($oresult) || (is_object($oresult) && (get_class($oresult) == "mysqli_result"))) ? true : false);
            } else {
                $data['name'] = 'Unknown';
            }
            $owners[] = $data;
        }
        return $owners;
    }
    
    
}

?>
