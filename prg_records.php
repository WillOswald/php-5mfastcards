<?PHP
/***************************************************************************
 *   Copyright (C) 2004 by 5Muses Software LLC                                   *
 *   info@5muses.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//
// Finacial unit that handles the objects for the paypal interface
// and the account activation/deactivation 

$pipn_sp_1m_bn_enc = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="psp1m"><input type="hidden" name="cmd" value="_s-xclick"><input type="hidden" name="custom" value="TheBeerWasHere"><input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/x-click-but23.gif" border="0" name="submit" alt="Make payments with PayPal - it\'s fast, free and secure!"><input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHugYJKoZIhvcNAQcEoIIHqzCCB6cCAQExggE6MIIBNgIBADCBnjCBmDELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExETAPBgNVBAcTCFNhbiBKb3NlMRUwEwYDVQQKEwxQYXlQYWwsIEluYy4xFjAUBgNVBAsUDXNhbmRib3hfY2VydHMxFDASBgNVBAMUC3NhbmRib3hfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMA0GCSqGSIb3DQEBAQUABIGABC3TUykU5LxJ7iHjOEBRkCMrqULZ3yU0Qp6s4KawzamxrqgyFIRnRE1582PkeOuSHgS30Q1AfNe381XDAlOLxNQQIfryKsuLY94ddo/pWP6GTbFiwWdsmxokDb3GrPi4Sm69TDBwBZQUnnUuUhWdxZCJIYQZc/cqCsiMxrwIQJIxCzAJBgUrDgMCGgUAMIIBBAYJKoZIhvcNAQcBMBQGCCqGSIb3DQMHBAhf27yuYrd6dICB4I1okwBfLYqFCg5ZusjpPFr5qUQAqZl8CjHEpAXn5RmEChpzjolKgcTx/2MN1mUNSQTd9h78NQLJkyA+CrVrz/clzRlE8xnO2UT2uQFbrpl6h4OUJy6/oSIxsZzUg3yPLNlotEQym4w6yn9Kj7SzQEfuT3962ypuSAfXVeS4sFu1VzD0Slw64At4FjsrxuNWcYmA3ztnzAwh0iUusYYH2k2RLx+7cXuB8q95ksjqd9VaYcLr+QNIkiz69N4HkC+tsHjybYOCYQ6IXQP+d+k0qbn6G/92tWjnyRhS7rd+nlMKoIIDpTCCA6EwggMKoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgZgxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMREwDwYDVQQHEwhTYW4gSm9zZTEVMBMGA1UEChMMUGF5UGFsLCBJbmMuMRYwFAYDVQQLFA1zYW5kYm94X2NlcnRzMRQwEgYDVQQDFAtzYW5kYm94X2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbTAeFw0wNDA0MTkwNzAyNTRaFw0zNTA0MTkwNzAyNTRaMIGYMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTERMA8GA1UEBxMIU2FuIEpvc2UxFTATBgNVBAoTDFBheVBhbCwgSW5jLjEWMBQGA1UECxQNc2FuZGJveF9jZXJ0czEUMBIGA1UEAxQLc2FuZGJveF9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALeW47/9DdKjd04gS/tfi/xI6TtY3qj2iQtXw4vnAurerU20OeTneKaE/MY0szR+UuPIh3WYdAuxKnxNTDwnNnKCagkqQ6sZjqzvvUF7Ix1gJ8erG+n6Bx6bD5u1oEMlJg7DcE1k9zhkd/fBEZgc83KC+aMH98wUqUT9DZU1qJzzAgMBAAGjgfgwgfUwHQYDVR0OBBYEFIMuItmrKogta6eTLPNQ8fJ31anSMIHFBgNVHSMEgb0wgbqAFIMuItmrKogta6eTLPNQ8fJ31anSoYGepIGbMIGYMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTERMA8GA1UEBxMIU2FuIEpvc2UxFTATBgNVBAoTDFBheVBhbCwgSW5jLjEWMBQGA1UECxQNc2FuZGJveF9jZXJ0czEUMBIGA1UEAxQLc2FuZGJveF9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQBXNvPA2Bl/hl9vlj/3cHV8H4nH/q5RvtFfRgTyWWCmSUNOvVv2UZFLlhUPjqXdsoT6Z3hns5sN2lNttghq3SoTqwSUUXKaDtxYxx5l1pKoG0Kg1nRu0vv5fJ9UHwz6fo6VCzq3JxhFGONSJo2SU8pWyUNW+TwQYxoj9D6SuPHHRTGCAaQwggGgAgEBMIGeMIGYMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTERMA8GA1UEBxMIU2FuIEpvc2UxFTATBgNVBAoTDFBheVBhbCwgSW5jLjEWMBQGA1UECxQNc2FuZGJveF9jZXJ0czEUMBIGA1UEAxQLc2FuZGJveF9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTA1MDIyODE5NTY1NVowIwYJKoZIhvcNAQkEMRYEFL+rv3fuDASM69eNRnnpu0KhChWzMA0GCSqGSIb3DQEBAQUABIGAm9Xb8uL/2yKOob2wwpmwW4/d/S54zHKGcFUhdkXNEX3q+/Uh7Gabnp3v/e50vaoZpiwVXo5IQyqNzKvZRyc3YuUZ+ql4EOBEcuyoXp4F+m7yfRH3APxFu0vJ2nPV3Gf1zmKnBOONrnitGE8lbnG+3vc87caosoRe5nubRupTsOo=-----END PKCS7-----"></form>';
$pipn_sp_1m_bn = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name=psp1m><input type="hidden" name="cmd" value="_xclick"><input type="hidden" name="custom" value="TheBeerWasHere"><input type="hidden" name="business" value="testpaypal@5muses.com"><input type="hidden" name="undefined_quantity" value="1"><input type="hidden" name="item_name" value="Spanish Dynamic Fast Cards - 1 Month"><input type="hidden" name="item_number" value="402001"><input type="hidden" name="amount" value="9.95"><input type="hidden" name="no_shipping" value="1"><input type="hidden" name="return" value="/"><input type="hidden" name="no_note" value="1"><input type="hidden" name="currency_code" value="USD"><input type="hidden" name="lc" value="US"><input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/x-click-but23.gif" border="0" name="submit" alt="Make payments with PayPal - it\'s fast, free and secure!"></form>';

function paypal_button($account_id,$item_number) {
    global $db_link;
    // Paypal Button Generator....
    // Look up the item in the database.
    $sql = "SELECT * FROM p_itemcodes WHERE item_number='$item_number'";
    $result = mysqli_query($db_link, $sql);
    if (($result !== false) and (mysqli_num_rows($result) == 1)) {
        $row = mysqli_fetch_array($result);
        $item_name = $row['item_name'];
        $item_amount = $row['item_amount'];
    } else {
        return false;
    }
    $code = '';
    $time = strval(time());
    $fname = substr(sha1("pipn:$time"),32);
    //$code .= '<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="z'.$fname.'">';
    $code .= '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="z'.$fname.'">';
    $code .= '<input type="hidden" name="cmd" value="_xclick">';
    // insert the account number to where this purchase goes.
    $code .= '<input type="hidden" name="custom" value="'.$account_id.'">';
    // insert the business paypal account
    $code .= '<input type="hidden" name="business" value="paypal@5muses.com">';
    $code .= '<input type="hidden" name="undefined_quantity" value="1">';
    // insert the description
    $code .= '<input type="hidden" name="item_name" value="'.$item_name.'">';
    // insert the item number
    $code .= '<input type="hidden" name="item_number" value="'.$item_number.'">';
    // insert the amount
    $code .= '<input type="hidden" name="amount" value="'.$item_amount.'">';
    $code .= '<input type="hidden" name="no_shipping" value="1">';
    // insert the return website...
    $code .= '<input type="hidden" name="return" value="/main.php?act=acct&ibTag=3130&item='.$item_number.'">';
    $code .= '<input type="hidden" name="no_note" value="1">';
    $code .= '<input type="hidden" name="currency_code" value="USD">';
    $code .= '<input type="hidden" name="lc" value="US">';
    $code .= '<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but23.gif" border="0" name="submit" alt="Make payments with PayPal - it\'s fast, free and secure!">';
    $code .= '</form>';
    return $code;
}

class fmRecords {

    var $_ContactInfo        = array();  // Name, addie, e-mail, etc...

    function find_transaction($txn_id) {
        // Here we go look for a txn_id, and return true if
        // it already exists, false if not...
        global $db_link;
        
        $etxn_id = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $txn_id) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $result=mysqli_query($db_link, "SELECT * FROM p_transaction WHERE txn_id='$etxn_id'");
        $rows = mysqli_num_rows($result);
        @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        return ($rows > 0);
    }
    
    function post_transaction($data) {
        // Here we go look for a txn_id, and return true if
        // it already exists, false if not...
        global $db_link;
        
        // Go grab current fields and use against array?
        $sql = 'INSERT INTO p_transaction SET ';
        foreach ($data as $key => $value) {
            $evalue = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $value) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
            $ekey = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $key) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
            $sql .= "$ekey='$evalue', ";
        }
        $sql .= "POSTDATE=NOW() ";
        @mysqli_query($db_link, $sql);
        if (((is_object($db_link)) ? mysqli_errno($db_link) : (($___mysqli_res = mysqli_connect_errno()) ? $___mysqli_res : false)) != 0) {
            return ((is_object($db_link)) ? mysqli_error($db_link) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
        } else {
            return true;
        }
    }
    
    function fetch_purchases($AccountID) {
        // Here we go grab all the transactions for a perticular
        // Account and return them as an array...
        global $db_link;
        
        $result=mysqli_query($db_link, "SELECT * FROM p_purchases WHERE f_account='$AccountID'");
        $set = array(); 
        if ($result !== false) {
            while ($row = mysqli_fetch_array($result)) {
                $entry = array();
                foreach ($row as $key => $value) {
                    $entry[$key] = $value;
                }
                $set[] = $entry;
            }
            @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        }
        return $set;
    }
    
    function get_purchase($id) {
        // Here we go grab all the transactions for a perticular
        // Account and return them as an array...
        global $db_link;
        
        $result=mysqli_query($db_link, "SELECT * FROM p_purchases WHERE fID='$id'");
        if ($result !== false) {
            $row = mysqli_fetch_assoc($result);
            $entry = array();
            foreach ($row as $key => $value) {
                $entry[$key] = $value;
            }
            return $entry;
        }
    }
    
    function activate_subscription($id, $AccountID) {
        global $db_link;
        $result = mysqli_query($db_link, "SELECT * FROM p_purchases WHERE fID='$id' AND f_account='$AccountID'");
        if ($result !== false) {
            $row = mysqli_fetch_assoc($result);
            $item = $this->find_item($row['f_item_number']);
            if ($item !== false) {
                $time = strtoupper($item['item_duration']);
                $result = mysqli_query($db_link, "UPDATE p_purchases set f_activated = NOW(), f_expires = ((CURDATE() + INTERVAL '11:59:59' HOUR_SECOND) + INTERVAL $time) WHERE fID='$id' and f_account='$AccountID'");
            } else {
                return false;
            }
        } else {
            return false;
        }
        
        
        return true;
    }
    
    function find_item($item_number) {
        // Here we go look for an item and return it in an array...
        global $db_link;
                    
        $eitem_number = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $item_number) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $result = mysqli_query($db_link, "SELECT * FROM p_itemcodes WHERE item_number='$eitem_number'");
        if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            $return = array();
            foreach($row as $field => $data) {
                $return["$field"] = $data;
            }
            @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            return $return;
        } else {
            @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            return false;
        }
    
    }
    
    function db_init($database) {
        global $db_link;
        //$fp = fopen('records.log','ab');
        // Check the database for the tables p_transaction and p_finance
        $pt = false;
        $pf = false;
        $pc = false;
        $pi = false;
        $result = mysqli_query($db_link, "SHOW TABLES from $database");
        while ($row = mysqli_fetch_array($result)) {
            if ($row[0] == 'p_transactions') { $pt = true; }
            if ($row[0] == 'p_purchases') { $pf = true; }
            if ($row[0] == 'p_itemcodes') { $pc = true; }
            if ($row[0] == 'p_invoice') { $pi = true; }
            //fputs($fp,"{$row[0]}\n");
        }
        //fputs($fp,"---------------------\n");
        if (!$pt) {
            // Create the transaction database
            $tdb = "CREATE TABLE {$database}.p_transaction (";
            $tdb .= 'tID bigint unsigned NOT NULL auto_increment PRIMARY KEY, ';
            $tdb .= 'REQUEST_METHOD varchar(10), ';
            $tdb .= 'HTTP_USER_AGENT varchar(128), ';
            $tdb .= 'REMOTE_HOST varchar(128), ';
            $tdb .= 'REMOTE_ADDR varchar(15), ';
            $tdb .= 'REMOTE_PORT integer, ';
            $tdb .= 'SERVER_PORT integer, ';
            $tdb .= 'VALIDATED tinyint, ';
            $tdb .= 'CLEARED tinyint, ';
            $tdb .= 'POSTDATE datetime, ';
            $tdb .= 'business varchar(127), ';
            $tdb .= 'receiver_email varchar(127), ';
            $tdb .= 'receiver_id varchar(13), ';
            $tdb .= 'item_name varchar(127), ';
            $tdb .= 'item_number varchar(127), ';
            $tdb .= 'quantity integer, ';
            $tdb .= 'invoice varchar(64), ';
            $tdb .= 'custom varchar(64), ';
            $tdb .= 'memo varchar(255), ';
            $tdb .= 'note blob, ';
            $tdb .= 'tax double, ';
            $tdb .= 'payment_status varchar(64), ';
            $tdb .= 'pending_reason varchar(64), ';
            $tdb .= 'reason_code varchar(64), ';
            $tdb .= 'payment_date datetime, ';
            $tdb .= 'txn_id varchar(17), ';
            $tdb .= 'parent_txn_id varchar(17), ';
            $tdb .= 'txn_type varchar(64), ';
            $tdb .= 'payment_type varchar(64), ';
            // multiple currencies
            $tdb .= 'mc_gross varchar(16), ';
            $tdb .= 'mc_fee varchar(16), ';
            $tdb .= 'mc_currency varchar(16), ';
            $tdb .= 'mc_handling varchar(16), ';
            $tdb .= 'mc_shipping varchar(16), ';
            $tdb .= 'settle_amount varchar(16), ';
            $tdb .= 'settle_currency varchar(16), ';
            $tdb .= 'exchange_rate varchar(16), ';
            $tdb .= 'payment_gross varchar(16), ';
            $tdb .= 'payment_fee varchar(16), ';
            // Buyer Information
            $tdb .= 'first_name varchar(64), ';
            $tdb .= 'last_name varchar(64), ';
            $tdb .= 'payer_business_name varchar(127), ';
            $tdb .= 'address_name varchar(127), ';
            $tdb .= 'address_street varchar(200), ';
            $tdb .= 'address_city varchar(40), ';
            $tdb .= 'address_state varchar(40), ';
            $tdb .= 'address_zip varchar(20), ';
            $tdb .= 'address_country varchar(64), ';
            $tdb .= 'address_status varchar(16), ';
            $tdb .= 'payer_email varchar(127), ';
            $tdb .= 'payer_id varchar(13), ';
            $tdb .= 'payer_status varchar(16), ';
            // IPN Information
            $tdb .= 'notify_version varchar(20), ';
            // Security Information
            $tdb .= 'verify_sign varchar(255), ';
            // Mass Pay has been omitted
            // Subscription postback
            $tdb .= 'subscr_date datetime, ';
            $tdb .= 'subscr_effective datetime, ';
            $tdb .= 'period1 varchar(16), ';
            $tdb .= 'period2 varchar(16), ';
            $tdb .= 'period3 varchar(16), ';
            $tdb .= 'amount1 double, ';
            $tdb .= 'amount2 double, ';
            $tdb .= 'amount3 double, ';
            $tdb .= 'mc_amount1 double, ';
            $tdb .= 'mc_amount2 double, ';
            $tdb .= 'mc_amount3 double, ';
            //$tdb .= 'mc_currency varchar(16), ';
            $tdb .= 'recurring tinyint, ';
            $tdb .= 'reattempt tinyint, ';
            $tdb .= 'retry_at datetime, ';
            $tdb .= 'recur_times integer, ';
            $tdb .= 'username varchar(64), ';
            $tdb .= 'password varchar(64), ';
            $tdb .= 'subscr_id varchar(19), ';
            $tdb .= 'test_ipn tinyint DEFAULT 0, ';
            $tdb .= 'index(payer_email), ';
            $tdb .= 'index(invoice), ';
            $tdb .= 'index(last_name,first_name), ';
            $tdb .= 'index(payment_date), ';
            $tdb .= 'index(txn_id) ';
            $tdb .= ' )';
            mysqli_query($db_link, $tdb);
            //fputs($fp,mysql_error()."\n");
            
        }
        if (!$pf) {
            $tdb = "CREATE TABLE {$database}.p_purchases (";
            $tdb .= 'fID bigint unsigned NOT NULL auto_increment PRIMARY KEY, ';
            $tdb .= 'txn_id varchar(17), ';
            $tdb .= 'f_account varchar(40), ';
            $tdb .= 'f_item_number varchar(64), ';
            $tdb .= 'f_purchased datetime, ';
            $tdb .= 'f_received datetime, ';
            $tdb .= 'f_activated datetime, ';
            $tdb .= 'f_expires datetime, ';
            $tdb .= 'f_status varchar(20) DEFAULT \'Pending\', ';
            $tdb .= 'f_valid tinyint DEFAULT 0, ';
            $tdb .= 'f_vacation tinyint DEFAULT 0, ';
            $tdb .= 'index(f_account), ';
            $tdb .= 'index(f_purchased), ';
            $tdb .= 'index(f_expires), ';
            $tdb .= 'index(f_received) ';
            $tdb .= ' )';
            mysqli_query($db_link, $tdb);
            //fputs($fp,mysql_error()."\n");
        }
        if (!$pc) {
            $tdb = "CREATE TABLE {$database}.p_itemcodes (";
            $tdb .= 'fID bigint unsigned NOT NULL auto_increment PRIMARY KEY, ';
            $tdb .= 'item_number varchar(64), ';
            $tdb .= 'item_heading varchar(64), ';
            $tdb .= 'item_name varchar(255), ';
            $tdb .= 'item_amount double, ';
            $tdb .= 'item_duration varchar(64), '; // '+1 month' '+4 months' '+1 year'
            $tdb .= 'item_note blob, ';
            $tdb .= 'index(item_number), ';
            $tdb .= 'index(item_duration) ';
            $tdb .= ' )';
            mysqli_query($db_link, $tdb);
            //fputs($fp,mysql_error()."\n");
        }
        if (!$pi) {
            $tdb = "CREATE TABLE {$database}.p_invoice (";
            $tdb .= 'fID bigint unsigned NOT NULL auto_increment PRIMARY KEY, ';
            $tdb .= 'inv_number bigint, ';
            $tdb .= 'inv_account varchar(40), ';
            $tdb .= 'inv_date datetime, ';
            $tdb .= 'inv_amount_net double, ';
            $tdb .= 'inv_amount_tax double, ';
            $tdb .= 'inv_amount_shipping double, ';
            $tdb .= 'inv_amount_gross double, ';
            $tdb .= 'inv_amount_paid double, ';
            $tdb .= 'inv_completed tinyint, ';
            $tdb .= 'index(inv_account), ';
            $tdb .= 'index(inv_date), ';
            $tdb .= 'index(inv_completed) ';
            $tdb .= ' )';
            mysqli_query($db_link, $tdb);
            //fputs($fp,mysql_error()."\n");
        }
    }

} 


?>
