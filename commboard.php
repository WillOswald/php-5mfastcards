<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

class CommBoard {

    var $tableName;
    
    function CommBoard($name) {
        global $db_link;
        // Initialization stuff here...
        // Check for the table cb_tb_$name
        $this->tableName = "cb_tb_$name";
        // Create it if it isn't there...
        // Subject, User, Time, Message
        $result = mysqli_query($db_link, 'SHOW TABLES');
        $tables = array();
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            while ($row = mysqli_fetch_array($result)) {
                $tables[] = $row[0];
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        }
        if (!in_array($this->tableName,$tables)) {
            // Create the table
            $tableDef = "(
                ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                user varchar(80),
                message blob,
                created datetime,
                index(created)
                )";
            mysqli_query($db_link, "CREATE TABLE {$this->tableName} $tableDef");
        }
    }
    
    function post($user,$message) {
        global $db_link;

        $eUser = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $user) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $eMessage = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $this->validate($message)) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        mysqli_query($db_link, "INSERT INTO {$this->tableName} SET user='$eUser', message='$eMessage', created=NOW()");
    }
    
    function get($entries=25) {
        global $db_link;
    
        $eEntries = intval($entries);
        $result = mysqli_query($db_link, "SELECT * FROM {$this->tableName} ORDER BY created DESC LIMIT $eEntries");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            $messages = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $message = array();
                $message['created'] = strtotime($row['created']);
                $message['user'] = $row['user'];
                $message['message'] = $this->parse($row['message']);
                $messages[] = $message;
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        }        
        return $messages;
    }
    
    function parse($message) {
        // Parse a message turning BBCode into HTML
        // [B] [I] [U] [COLOR=red] [URL=http][/URL] [IMG=http]name[/IMG] [QUOTE] [CODE] [LIST][*][/LIST]
        // [FONT] [SIZE] 
        //
        // highlight.string    #CC0000 PHP_INI_ALL
        // highlight.comment   #FF9900 PHP_INI_ALL
        // highlight.keyword   #006600 PHP_INI_ALL
        // highlight.bg        #FFFFFF PHP_INI_ALL
        // highlight.default   #0000CC PHP_INI_ALL
        // highlight.html      #000000 PHP_INI_ALL

        ini_set('highlight.string','#FF8888');
        ini_set('highlight.comment','#555555');
        ini_set('highlight.keyword','#88FF88');
        ini_set('highlight.bg','#222222');
        ini_set('highlight.default','#7788FF');
        ini_set('highlight.html','#CCCCCC');
        $outMessage = " $message ";
        $codeS = array('[',']');
        $codeR = array('&#x5B;','&#x5D;');
        $test = '[ph'.'p]';
        if (stristr($outMessage,$test) !== false) {
            // Got a code shit crap fucking poop and then some.
            $phpmatch = preg_match('/(.*)(\[PHP\].*?\[\/PHP\])(.*)/si',$outMessage,$matches);
            if ($phpmatch) {
                $outMessage = nl2br($matches[1]) . $matches[2] . nl2br($matches[3]);
            }
        } 
        $test = '[co'.'de]';
        if (stristr($outMessage,$test) !== false) {
            // Got a code shit crap fucking poop and then some.
            $codematch = preg_match('/(.*?)(\[CODE\].*?\[\/CODE\])(.*?)/si',$outMessage,$matches);
            if ($codematch) {
                $outMessage = nl2br($matches[1]) . $matches[2] . nl2br($matches[3]);
                //echo "<DIV CLASS=code>".print_r($matches,true)."</DIV><HR>";
            }
        } 
        $outMessage = preg_replace('/\[PHP\](.*?)\[\/PHP\]/ise',"'<DIV CLASS=code>'.highlight_string('<?PHP\n'.stripslashes('\\1').'\n?>',true).'</DIV>'",$outMessage);
        $outMessage = preg_replace('/\[CODE\](.*?)\[\/CODE\]/ise',"'<DIV CLASS=code>'.nl2br(str_replace(\$codeS,\$codeR,stripslashes('\\1'))).'</DIV>'",$outMessage);
        $outMessage = preg_replace('/\[IMG\](.*?)\[\/IMG\]/is','<IMG SRC="\1">',$outMessage);
        $outMessage = preg_replace('/\[URL=(.*?)\](.*?)\[\/URL\]/i','<A STYLE="color:#89F;" HREF="\1">\2</A>',$outMessage);
        //$outMessage = str_replace('[/URL]','</A>',$outMessage);
        $outMessage = preg_replace('/\[COLOR=(red|blue|green|yellow|orange|cyan|while|silver|black)\](.*?)\[\/COLOR\]/i','<FONT COLOR="\1">\2</FONT>',$outMessage);
        //$outMessage = str_replace('[/COLOR]','</FONT>',$outMessage);
        $outMessage = str_replace('[B]','<B>',$outMessage);
        $outMessage = str_replace('[/B]','</B>',$outMessage);
        $outMessage = str_replace('[I]','<I>',$outMessage);
        $outMessage = str_replace('[/I]','</I>',$outMessage);
        $outMessage = str_replace('[U]','<U>',$outMessage);
        $outMessage = str_replace('[/U]','</U>',$outMessage);
        //$outMessage = htmlspecialchars($outMessage,ENT_NOQUOTES);
        if ($phpmatch or $codematch) {
            return $outMessage;
        } else {
            return nl2br($outMessage);
        }
    }
    
    function validate($message) {
        // Validate all the BBCode Tags and close them at the end if necessary
        $outMessage = $message;
        
        return $outMessage;
    }
}

?>
