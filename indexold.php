<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | General Includes
// +----------------------------------------------------------------------

    include_once("functions.php");
        $Timer->start('init');
    include_once("db.php");
    include_once("session.php");
    include_once("main_lib.php");

    // generate Tables...
    db_sanity();
// +----------------------------------------------------------------------
// | Cache Control
// +----------------------------------------------------------------------
    // FIXME: Change cache control to expire in 15 minutes.
    header ("Content-type: text/html");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

// +----------------------------------------------------------------------
// | Create and validate the current session  
// | FIXME!! Filter out bots from sessions, but still log them.
// +----------------------------------------------------------------------
    $Session = new fmSession;
    $Timer->start('session');
    $Session->init('FMC_FlashCard');
    $Timer->stop('session');
    $Session->post("/w2/n/np","1000");
    $Session->save();

// +----------------------------------------------------------------------
// | Misc Functions
// +----------------------------------------------------------------------
    function make_seed() {
        list($usec, $sec) = explode(' ', microtime());
        return (float) $sec + ((float) $usec * 100000);
    }

// +----------------------------------------------------------------------
// | Process requests / Logging
// +----------------------------------------------------------------------
    // FIXME: Need to log Referer and such...
    // FIXME: Output the javascript to grab info about the browser...
    
    $full_pagename = $_SERVER['SCRIPT_FILENAME'];
    $ar = preg_split('/\//',$full_pagename);
    $current_page = $ar[count($ar)-1];
    

// +----------------------------------------------------------------------
// | Setup the Inline CSS
// +----------------------------------------------------------------------
    $css = '<STYLE TYPE="text/css">'."\n";
    // Pull CSS From Here...Output...
    $result = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/css'");
    while ($row = mysqli_fetch_array($result)) {
        $output = strtr($row['wContent'],"\x09\x0A\x0D",'!!!');
        $output = str_replace('        ','',$output);
        $output = str_replace('!','',$output);
        $css .= $output."\n";
    }
    $css .= '</STYLE>'."\n";

// +----------------------------------------------------------------------
// | Setup the Javascript Stuff, if any
// +----------------------------------------------------------------------
    // FIXME: Write the javascript to grab browser information.
    
    //echo '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">';
    // Pull Javascript From Here...Output...
    //echo '</SCRIPT>';


// +----------------------------------------------------------------------
// | Setup Main HTML Body
// +----------------------------------------------------------------------

    // Output title and header
    $html .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
    $html .= '<HTML>';
    $html .= '<HEAD>';
    $html .= '<TITLE>Learn Spanish, French, and German Free at 5Muses Software L.L.C.</TITLE>';
    // META Tags? Do I even need them?
    $html .= '<meta name="description" content="Learn spanish, french, or german free online with this unique study aid designed to fit your needs.">';
    $html .= '<meta name="keywords" content="';
    $html .= 'learn, spanish, french, german, free, language, vocabulary, grammer, study, online, on-line, foreign, translation, translator, dictionary, verbs, nouns';
    $html .= '">'; 
    $html .= '<SCRIPT LANGUAGE="JavaScript" SRC="id.js" TYPE="text/javascript"></SCRIPT>';
    $html .= '</HEAD>';
    // Only if necessary...
    if ($Session->get('/log/logstate') != 'complete') {
        $html .= '<BODY BGCOLOR=#FFFFFF onload="idBrowser();">';
    }
    $html .= '<STYLE TYPE="text/css">';
    $html .= 'a.elink:link, a.elink:visited { color: #000000; text-decoration: none; }';
    $html .= 'a.elink:hover, a.elink:active { color: #3F4AFC; text-decoration: none; }';
    $html .= '</STYLE>';
    $html .= $css;
    // This is the main div enclosure for the margins
    $html .= '<CENTER><DIV ID="indexContent" STYLE="padding:0px 5px 0px 5px;margin: 8px 8px 8px 3px;width:980px;text-align:justify;border-left:1px solid #C0C0C0;border-right:1px solid #C0C0C0;">';
    
    $textdfc = "<SPAN STYLE=\"color:#2A33B1;font-weight:bold;\">Dynamic Fast Cards</SPAN>";
    $textcopy = "<CENTER><DIV STYLE=\"color:#AAAAAA;font-size:9px;width:750px;border-top:1px solid #888888;\">All Content Copyright 2005, 5 Muses Software L.L.C.&nbsp;&nbsp;:&nbsp;&nbsp;<A HREF=\"termsofuse.php\" TARGET=\"_blank\">Terms Of Use</A>&nbsp;&nbsp;:&nbsp;&nbsp;<A HREF=\"privacypolicy.php\" TARGET=\"_blank\">Privacy Policy</A></DIV></CENTER>";
    $html .= fmLibBanner('index');
    $html .= "<BR>";
    $html .= "<DIV CLASS=dfc1title>Welcome.<BR></DIV>";
    $html .= "<DIV STYLE=\"clear:all;padding:8px 8px 8px 8px;font-size:13px;\">";
    $html .= "<IMG STYLE=\"margin:0px 0px 5px 8px;\" SRC=images/index-1.jpg ALIGN=RIGHT>";
    $html .= "<P>Welcome to 5 Muses Software. We are dedicated to providing the best foreign language learning study supplements ";
    $html .= "designed around the needs of University & College students, High School students, or anyone wanting to learn spanish, ";
    $html .= "learn french, or to learn german. Our software program $textdfc is designed to ";
    $html .= "take advantage of the computer's greatest strengths to shorten the time it takes you to learn and memorize the ";
    $html .= "fundamentals of a foreign language. First, we identified the computer's greatest strengths and then, we identified ";
    $html .= "the 3 main causes of stress students experience while studying a foreign language. </P>\n";

    $html .= "<P>Our program is designed so that it works around your learning needs, whether you are a student and have a required ";
    $html .= "curriculum, or just want to brush up on your language vocabulary. $textdfc are flexible, you can create ";
    $html .= "your own lessons to match your curriculum or study needs, or choose one or several pre-made lessons of common ";
    $html .= "lists of words, such as cronological nouns to learn days of the week, or months of the year.</P>\n";

    $html .= "<P>Our company began in 1998 with the motivation to create a vocabulary study supplement that out performs all other ";
    $html .= "supplements. Since then we have built customizable audio vocabulary databases of more than 2700 words in each of spanish, french, and german ";
    $html .= "for a total of ".countVocabulary()." words. ";
    //$html .= "Click below to enter our site and begin using our $textdfc.</P>\n";
    $html .= "<FONT COLOR=#C00000>Our site will be online on June 1st of 2005, check back soon!</FONT></P>\n";
    
    $html .= "<CENTER><DIV STYLE=\"width:35%;margin:40px 0px 40px 0px;border-top:2px solid #808080;border-bottom:2px solid #808080;font-size:24px;font-weight:bold;\">";
    $html .= "<DIV STYLE=\"width:95%;margin:2px 0px 2px 0px;border-top:1px solid #C0C0C0;border-bottom:1px solid #C0C0C0;font-size:24px;font-weight:bold;\">";
    //$html .= "<A CLASS=\"elink\" HREF=main.php>ENTER SITE <SPAN ID=g1>&gt;</SPAN><SPAN ID=g3>&gt;</SPAN><SPAN ID=g2>&gt;</SPAN></A>";
    $html .= "Online on 6/1/05";
    $html .= "</DIV>";
    $html .= "</DIV></CENTER>";
    
    $html .= $textcopy;
    $html .= "</DIV></CENTER>";
    
    /*

    
    * Eliminate the guesswork in pronouncing new words. Our native speakers have the preferred dialects - simply listen to them and imitate the sounds.
    * See and hear all of your required vocabulary in less time time than is currently possible with any other method.
    * Associate the abstract meaning of the nearest English equivalent to the foreign vocabulary terms fast.
    * Our program will allow you to quiz yourself when you think it is necessary.

    I. Success with memorization by using all of your senses at once, repetitively

    At least half of your learning, like it or not, is memorization.
    
    */
    $html .= "</DIV>";



// +----------------------------------------------------------------------
// | Make Variable Replacements
// +----------------------------------------------------------------------
    $var = array();
    $sql = "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/variables'";
//     echo "<PRE>$sql</PRE>";
    $vresult = mysqli_query($db_link, $sql);
    if (is_resource($vresult)) {
        while ($row = mysqli_fetch_array($vresult)) {
            $var[$row['wItem']] = $row['wContent'];
        }
        foreach ($var as $key => $item) {
            $css = str_replace("%$key%",$item,$css);
            $js = str_replace("%$key%",$item,$js);
            $html = str_replace("%$key%",$item,$html);
        }
    }

// +----------------------------------------------------------------------
// | Save the session information. (if any)
// +----------------------------------------------------------------------
    $Session->save();

// +----------------------------------------------------------------------
// | Output the Main HTML
// +----------------------------------------------------------------------
    echo $html;


// +----------------------------------------------------------------------
// | End of file
// +----------------------------------------------------------------------
?>
