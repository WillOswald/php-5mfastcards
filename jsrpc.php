<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
// | 
// | Implementation of the Javascript RPC for 5Muses Software
// | Hope this works...
// | 
// +----------------------------------------------------------------------
// | Need to implement a simple content management system
// | 
// | Perhaps a table with the HTML stored in it, retrieved by the
// | ibTag value and sent to the browser
// | 
// | Also need to tweek it so that you can grab either full HTML
// | or just certain words etc...
// | 
// +----------------------------------------------------------------------

    include_once("db.php");
    include_once("session.php");
    include_once("prg_account.php");
    include_once("prg_lesson.php");
    include_once("prg_vocabulary.php");
    include_once("prg_records.php");
    include_once("prg_flash.php");
    include_once("main_lib.php");
    //include_once("debug_log.php");

// +----------------------------------------------------------------------
// | Create and validate the current session
// +----------------------------------------------------------------------
    $Session = new fmSession;
    $Timer->start('session');
    $Session->init('FMC_FlashCard');
    $Timer->stop('session');

// +----------------------------------------------------------------------
// | Initiate the Account Object
// +----------------------------------------------------------------------
    $Account = new fmAccount;
    $Account->init($Session->getID());

// +----------------------------------------------------------------------
// | Initiate the Vocabulary Object
// +----------------------------------------------------------------------
    $Vocabulary = new fmPrgVocabulary;
    if (strlen($Session->get("/w2/v")) > 1)
    $Vocabulary->restore($Session->get("/w2/v"));

// +----------------------------------------------------------------------
// | Initiate the Lesson Object
// +----------------------------------------------------------------------
    $Lesson = new fmPrgLesson;
    // Check to see if the account is valid or it's just a guest....
    $aid = $Account->getID();
    $sid = $Session->getID();
    $Lesson->init($aid,$sid);
    $Lesson->restore($Session->get("/w2/l"));
    // Load the last used lesson...
    $Lesson->load("[Current]");
    //$lID = $Session->get("/w2/l/id=".intval($lID));

// +----------------------------------------------------------------------
// | Initiate the Dynamic Flash Card Object
// +----------------------------------------------------------------------
    $FlashCard = new fmFlashCard;
    $FlashCard->restore($Session->get("/w2/f"));

// +----------------------------------------------------------------------
// | Initiate the Accounting and Records Object
// +----------------------------------------------------------------------
    $Records = new fmRecords;
    $Records->db_init($dfc_config['mysql_db']);
// +----------------------------------------------------------------------
// | Get request and initialize RPC JavaScript
// +----------------------------------------------------------------------
    $current_page = 'main.php';
    
    $rpcSend = $_REQUEST['rpcSend'];
    $ib_act = $_REQUEST['act'];
    $ib_tag = intval($_REQUEST['ibTag']);
    
    //$js = "fmrpcPayload = \"No Payload\";\n\n";
// +----------------------------------------------------------------------
// | Content Headers
// +----------------------------------------------------------------------
    if ($rpcSend == "true") {
        header ("Content-type: image/gif");
        //header ("Content-type: image/gif");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    } else {
        header ("Content-type: text/javascript");
        //header ("Content-type: image/gif");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

// +----------------------------------------------------------------------
// | Process Requests 
// +----------------------------------------------------------------------
    // Nav Stuff
    // Vocab Changes
    if (isset($_REQUEST['ibTag'])) {
        $Session->post("/w2/n/np=".intval($ib_tag));
    }
    $vss = $Vocabulary->vSearch;
// +----------------------------------------------------------------------
// | Functions
// +----------------------------------------------------------------------
    function strhex($string) {
        $out = "";
        for ($i=0;$i<strlen($string);$i++) {
            $out .= dechex($string[0]);
        }
        return $out;
    }
    
    function hexstr($hex) {
        $out = "";
        for ($i=0;$i<strlen($string);$i++) {
            $out .= dechex($string[0]);
        }
        return $out;
    }
// +----------------------------------------------------------------------
// | jsRPC return code...
// +----------------------------------------------------------------------
    $jsmain .= "
            var obj = document.getElementById(\"fmMainContent\");
            if (obj) {
                oldHTML = document.getElementById(\"rpcResult\");
                if (oldHTML) {
                    oldParent = oldHTML.parentNode;
                    oldParent.removeChild(oldHTML);
                    oldHTML = null;
                }
                newHTML = document.createElement('div');
                newHTML.setAttribute('id','rpcResult');
                newHTML.setAttribute('class','divResult');
                newText = document.createTextNode(\"Loading...\");
                newHTML.appendChild(newText);
                newHTML.innerHTML = fmrpcPayload;
                
                obj.appendChild(newHTML);
            }
    ";
    //<DIV ID="fmLogin" CLASS="loginp">
    $jslogin .= "
            var obj = document.getElementById(\"fmLogin\");
            while (obj.hasChildNodes() > 0) {
                obj.removeChild(obj.lastChild);
            }
            newHTML = document.createElement('div');
            newHTML.setAttribute('id','fmLGC');
            newText = document.createTextNode(\"Processing...\");
            newHTML.appendChild(newText);
            newHTML.innerHTML = fmLGPayload;
            
            obj.appendChild(newHTML);
    ";
    $jscurles .= "
            var obj = document.getElementById(\"fmLesson\");
            while (obj.hasChildNodes() > 0) {
                obj.removeChild(obj.lastChild);
            }
            newHTML = document.createElement('div');
            newHTML.setAttribute('id','fmLesson');
            newHTML.setAttribute('class','dfc1panel1');
            newText = document.createTextNode(\"Processing...\");
            newHTML.appendChild(newText);
            newHTML.innerHTML = fmCLPayload;
            
            obj.appendChild(newHTML);
    ";
    
    // Fix this so that it properly handles the MediaPlayer component
    // Fix this so that it updates the "src" of the media player, then plays.
    // fix this so that it updates JUST the words... That's it.
    $jsflash .= "
            var obj = document.getElementById(\"fmVScreen\");
            while (obj.hasChildNodes() > 0) {
                obj.removeChild(obj.lastChild);
            }
            
            newHTML = document.createElement('div');
            newHTML.setAttribute('id','fmVScreenContent');
            newText = document.createTextNode(\"Processing...\");
            newHTML.appendChild(newText);
            newHTML.innerHTML = fmVSPayload;
            
            obj.appendChild(newHTML);
    ";
//            newHTML.setAttribute('class','loginp');

// +----------------------------------------------------------------------
// | Main 
// +----------------------------------------------------------------------
    $naction = 0;
    $naction += ($ib_act == "set") ? 0x0010 : 0x0000 ;
    $naction += ($ib_act == "html") ? 0x0020 : 0x0000 ;
    $naction += ($ib_act == "dfc") ? 0x0040 : 0x0000 ;
    $naction += ($ib_act == "login") ? 0x0080 : 0x0000 ;
    $naction += ($ib_act == "logout") ? 0x0100 : 0x0000 ;
    $naction += ($ib_act == "acct") ? 0x0200 : 0x0000 ;
    $naction += ($ib_act == "head") ? 0x0400 : 0x0000 ;
    $naction += ($ib_act == "support") ? 0x0800 : 0x0000 ;
    
        //--- Default ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0000) {  // Default
            $naction = 0x0400; // "head"
            $ib_tag = 2000; // "Dynamic Fast Cards"
        }
    
        if ($naction == 0x0040) {   // Get an account before you buy!
            if ($Account->is_guest()) {
                if ($ib_tag == 2120) {
                    $na_message = '<B>You must sign up for a free account before purchasing. <BR>If you already have an account, please log in using the &quot;Login&quot; link above.</B>';
                    $naction = 0x0200;
                    $ib_tag = 3100;
                }
            }
        }
        
        
        //--- Set ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0010) {   // SET
            // Change some settings....
            //$Debugger->logline("ibAct: $ib_act");
/*DEBUG*/   //$Session->post("/w2/Debug/uri=".$_SERVER['REQUEST_URI']);
            if ($_REQUEST['vs'] == 's') { 
                $Vocabulary->vSearchMode = true; 
                if (isset($_REQUEST['vss'])) $Vocabulary->vSearch = $_REQUEST['vss'];
                $Vocabulary->vFilter = ($Vocabulary->vSearch != '');
                $Vocabulary->gotopage(0);
                $naction = 0x0040;
                $ib_tag = 2130;
                $vss = $Vocabulary->vSearch;
                if ($_REQUEST['from'] == 'lesson') {
                    $naction = 0x0040;
                    $ib_tag = 2140;
                }
            }
            if ($_REQUEST['vs'] == 'b') {
                $Vocabulary->vSearchMode = false; 
                $Vocabulary->vFilter = true; 
                if (isset($_REQUEST['vb'])) {
                    $Vocabulary->vBrowse = $_REQUEST['vb'];
                }
                $Vocabulary->gotopage(0);
                $naction = 0x0040;
                $ib_tag = 2130;
                if ($_REQUEST['from'] == 'lesson') {
                    $naction = 0x0040;
                    $ib_tag = 2140;
                }
            }
            if (isset($_REQUEST['vl'])) {
                $Vocabulary->vLanguage = $_REQUEST['vl'];
                $Lesson->_language = $_REQUEST['vl'];
                // ?? Do I really want to do this?
                $naction = 0x0040;
                //$ib_tag = 2130;
                $called = "vl={$_REQUEST['vl']}";
            }
            if ($_REQUEST['vn'] == 'n') {
                $Vocabulary->nextpage();
                $naction = 0x0040;
                $ib_tag = 2130;
                $called = "vn=n";
                if ($_REQUEST['from'] == 'lesson') {
                    $naction = 0x0040;
                    $ib_tag = 2140;
                }
            }
            if ($_REQUEST['vn'] == 'p') {
                $Vocabulary->prevpage();
                $naction = 0x0040;
                $ib_tag = 2130;
                $called = "vn=p";
                if ($_REQUEST['from'] == 'lesson') {
                    $naction = 0x0040;
                    $ib_tag = 2140;
                }
            }
            if (isset($_REQUEST['vnp'])) {
                $Vocabulary->gotopage($_REQUEST['vnp']);
                $naction = 0x0040;
                $ib_tag = 2130;
                $called = "vnp={$_REQUEST['vnp']}";
                if ($_REQUEST['from'] == 'lesson') {
                    $naction = 0x0040;
                    $ib_tag = 2140;
                }
            }
            if (isset($_REQUEST['lg'])) {
                // Got a login/logout attempt...
                // FIXME!!! Check for expired sessions/subscriptions/etc...
                //login_e / login_p
                $called = "lg={$_REQUEST['lg']}";
                if ($_REQUEST['lg'] == 'true') {
                    $login_e = $_REQUEST['login_e'];
                    $login_p = $_REQUEST['login_p'];
                    $lcode = $Account->login($login_e,$login_p,$Session->getID());
                    // Transfer Account settings to Session for holding.
                    switch ($lcode) {
                        case 0: // Login Sucessful...
                            $naction = 0x0080;
                            $ib_tag = 0;
                            break;
                        case 1: // Account not found...
                            $naction = 0x0080;
                            $ib_tag = 1;
                            break;
                        case 2: // Password Incorrect...
                            $naction = 0x0080;
                            $ib_tag = 2;
                            break;
                    }
                }
                if ($_REQUEST['lg'] == 'false') {
                    // Logout the user
                    $Account->logout();
                    $Lesson->blank($Vocabulary->vLanguage);
                    $naction = 0x0100;
                    $ib_tag = 0;
                }
                // Set the audio settings and such....
                /*
                $set = $Account->get_audio_settings();
                $volume = ($set >> 16) & 255;
                $delay = ($set >> 8) & 15;
                $repeat = ($set >> 4) & 15;
                $size = $set & 15;
                $Session->post("/program/settings/volume=$volume");
                $Session->post("/program/settings/delay=$delay");
                $Session->post("/program/settings/repeat=$repeat");
                $Session->post("/program/settings/size=$size");
                $Session->post('/program/dview/signup',700);
                $Session->post('/program/lesson/language',2);
                */
                
            }
            if (isset($_REQUEST['na'])) {
                // Set up a new account!
                // Check for errors in the registration fields.
                $na_email = trim($_REQUEST['na_email']);
                $na_pass1 = trim($_REQUEST['na_pass1']);
                $na_pass2 = trim($_REQUEST['na_pass2']);
                $na_fname = trim($_REQUEST['na_fname']);
                $na_lname = trim($_REQUEST['na_lname']);
                $na_addr1 = trim($_REQUEST['na_addr1']);
                $na_addr2 = trim($_REQUEST['na_addr2']);
                $na_city = trim($_REQUEST['na_city']);
                $na_state = trim($_REQUEST['na_state']);
                $na_zip = trim($_REQUEST['na_zip']);
                $na_ctry = trim($_REQUEST['na_ctry']);
                // if ctry == US then check state, else an option list ?
                $na_valid = true;
                // regex check email
                $preg_email = '/^([-\w]+)@[\w\.]+\.[\w]+$/i';
                $preg_zip = '/^[0-9]{5}([-]{1}[0-9]{4})?$/';
                // E-Mail
                if (preg_match($preg_email,$na_email) !== 1) {
                    // Invalid Email...
                    $na_valid = false;
                    $na_message .= ($na_email == '') ? '<B>E-Mail</B>: You must enter an e-mail address.<BR>' : '<B>E-Mail</B>: The e-mail address you entered is not a valid e-mail address.<BR>';
                    $nav_email = 'color:#E00000';
                }
                // Check to see if the account has already been registered....
                if ($Account->is_registered($na_email)) {
                    // Already present... Forgot password?
                    $na_valid = false;
                    $na_message .= '<B>E-Mail</B>: This E-Mail address is already registered. If you already have an account, please login above. If you have fogotten your password, click &quot;<A HREF=# onClick=\\"return false;\\">Forgot Password.&quot;</A><BR>';
                    $nav_email = 'color:#E00000';
                }
                // verify password...
                if ($na_pass1 != $na_pass2) {
                    // Passwords don't match...
                    $na_valid = false;
                    $na_message .= '<B>Password</B>: The passwords you entered do not match.<BR>';
                    $nav_pass1 = 'color:#E00000';
                    $nav_pass2 = 'color:#E00000';
                    $na_pass1 = '';
                    $na_pass2 = '';
                }
                if (strlen($na_pass1) < 8) {
                    // Short Password...
                    $na_valid = false;
                    $na_message .= (($na_pass1 == '') or ($na_pass2 == '')) ? '<B>Password</B>: You must enter and confirm a password for your account.<BR>' : '<B>Password</B>: Please use a password 8-20 characters long.<BR>';
                    $nav_pass1 = 'color:#E00000';
                    $nav_pass2 = 'color:#E00000';
                }
                $na_message .= ($na_fname == '') ? '<B>First Name</B>: You must enter your first name.<BR>' : '';
                $nav_fname .= ($na_fname == '') ? 'color:#E00000' : '';
                $na_message .= ($na_lname == '') ? '<B>Last Name</B>: You must enter your last name.<BR>' : '';
                $nav_lname .= ($na_lname == '') ? 'color:#E00000' : '';
                $na_message .= ($na_addr1 == '') ? '<B>Address</B>: You must enter your address.<BR>' : '';
                $nav_addr1 .= ($na_addr1 == '') ? 'color:#E00000' : '';
                $na_message .= ($na_city == '') ? '<B>City</B>: You must enter your city.<BR>' : '';
                $nav_city .= ($na_city == '') ? 'color:#E00000' : '';
                if ($na_ctry == 'US') {
                    // Got a US Customer... validate.
                    if (strlen($na_state) != 2) {
                        // Invalid State .. Check names?
                        $na_valid = false;
                        $na_message .= '<B>State</B>: Please use a 2-letter state such as &quot;MT&quot; or &quot;TX&quot;.<BR>';
                        $nav_state = 'color:#E00000';
                    } else {
                        //State Valid Check to see if it's a valid State?
                    }
                    if (preg_match($preg_zip,$na_zip) !== 1) {
                        // Invalid zip...
                        $na_valid = false;
                        $na_message .= ($na_zip == '') ? '<B>Zip Code</B>: You must enter a valid Zip Code.<BR>' : '<B>Zip Code</B>: The zip code you entered is not a valid U.S. Zip Code.<BR>';
                        $nav_zip = 'color:#E00000';
                    }
                } else {
                    // State is now Province
                    $na_province = $na_state;
                }
                
                // Validate Zip Code
                if (!$na_valid) {
                    $na_message = '<B>The fields in <FONT COLOR=#E00000>red</FONT> require your attention.</B><BR><FONT COLOR=#404040>'.$na_message.'</FONT>';
                    $naction = 0x0200;
                    $ib_tag = 3100;
                } else {
                    // Valid information... add account and process...
                    $actreg['uEmail'] = $na_email;
                    $actreg['uPassword'] = $na_pass1;
                    $actreg['uFName'] = $na_fname;
                    $actreg['uLName'] = $na_lname;
                    $actreg['uAddress1'] = $na_addr1;
                    $actreg['uAddress2'] = $na_addr2;
                    $actreg['uCity'] = $na_city;
                    if ($na_province == '') {
                        $actreg['uState'] = $na_state;
                    } else {
                        $actreg['uProvince'] = $na_province;
                    }
                    $actreg['uZip'] = $na_zip;
                    $actreg['uCountry'] = $na_ctry;
                    // Need POBox, DOB
                    $Account->register_account($actreg);
                    $Account->login($na_email, $na_pass1, $Session->getID());
                    $naction = 0x0080;
                    $ib_tag = 0;
                    // Code to show that the user is logged in
                }
            }
            if (isset($_REQUEST['lsave'])) {
                // Save your current lesson
                $cnl_name = $_REQUEST['cnl_name'];
                if (trim($cnl_name) == '') { 
                    $cnl_invalid = 0x00400001;  // No Name
                } else {
                    // Commit the save
                    $Lesson->save($cnl_name);
                }
                // Direct to Lesson Builder
                $naction = 0x0040;
                $ib_tag = 2140;
            }
            if (isset($_REQUEST['lnew'])) {
                // Create a new lesson
                $Session->post("/w2/n/np=".intval($ib_tag));
                $cnl_lang = intval($_REQUEST['cnl_lang']);
                $Vocabulary->vLanguage = $cnl_lang;
                $Lesson->blank($cnl_lang);
/*DEBUG*/       //$Session->post("/w2/Debug/cnl_lang=".$cnl_lang);
/*DEBUG*/       //$Session->post("/w2/Debug/lesson_lang=".$Lesson->_language);
                //$Lesson->save("[Current]",$Vocabulary->vLanguage);
                $Lesson->save("[Current]");
                // Direct to Lesson Builder
                $naction = 0x0040;
                $ib_tag = 2140;
            }
            if (isset($_REQUEST['ldelete'])) {
                // Create a new lesson
                $Session->post("/w2/n/np=".intval($ib_tag));
                $lid = intval($_REQUEST['lid']);
                $Lesson->delete($lid);
                // Direct to Lesson Builder
                $naction = 0x0040;
                $ib_tag = 2140;
            }
            if (isset($_REQUEST['ll'])) {
                // Load a saved lesson...
                $lID = intval($_REQUEST['ll']);
                $Lesson->load("",$lID);
                //$Session->post("/log/lessonLoad/error",$Lesson->_error);
                $Vocabulary->vLanguage = $Lesson->_language;
                // Save to session...
                $Lesson->resequence();
                $Session->post("/w2/l/id=".intval($lID));
                // Save as current lesson...
                $Lesson->save("[Current]");
                // Direct to Lesson Builder
                $naction = 0x0040;
                $ib_tag = 2140;
            }
            if (isset($_REQUEST['lbaw'])) {
                // Add a word to the lesson
                $lbaw_id = intval($_REQUEST['lbaw_id']);
                $Lesson->add_word($lbaw_id);
/*DEBUG*/       //$Session->post("/w2/Debug/lesson_lbaw=$lbaw_id");
                // Should prolly fix this.... Just another query that we don't need.
                $Lesson->save("[Current]");
                // Direct to Nowhere
                $naction = 0xFFFF;
                $ib_tag = -1;
                // Gonna add the html just to the Lesson part... not the whole section.
                
                $lesson_content = $Lesson->_words->dump();
                $rpc .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=98%>";
                if (is_array($lesson_content)) {
                    foreach ($lesson_content as $entry) {
                        list($lang,$sha1,$n,$f) = explode('|',$entry);
                        $rpc .= "<TR>";
                        $rpc .= "<TD CLASS=dfc1vocab>$n</TD>";// STYLE=\\\"width:140px;\\\"
                        $rpc .= "<TD ALIGN=RIGHT VALIGN=MIDDLE WIDTH=28><A HREF=# onClick=\\\"rpcFetch('act=set&lbdw=true&lbdw_sha1=$sha1');return false;\\\"><IMG BORDER=0 WIDTH=26 HEIGHT=14 SRC=images/dell.gif ALT=\\\"Remove this word.\\\"></A></TD>";
                        $rpc .= "</TR>";
                    }   
                }
                $rpc .= '</TABLE>';
                $js .= "fmCLPayload = \"$rpc\";\n";
                $js .= $jscurles;
                
            }
            if (isset($_REQUEST['lbdw'])) {
                // Add a word to the lesson
                $lbdw_sha1 = $_REQUEST['lbdw_sha1'];
                
                $Lesson->del_word($lbdw_sha1);
/*DEBUG*/       //$Session->post("/w2/Debug/lesson_lbdw=$lbdw_sha1");
                $Lesson->resequence();
                $Lesson->save("[Current]");
                // Direct to Lesson Builder
                $naction = 0x0040;
                $ib_tag = 2140;
            }
            if (isset($_REQUEST['acas'])) {
                $subid = intval($_REQUEST['acas']);
                // Activate the Subscription in the database
                $Records->activate_subscription($subid,$Account->getID());
                // If Successful then Grant them Access to the language
                $purchase = $Records->get_purchase($subid);
                        $Session->post("/w2/Debug/record_num",$purchase['f_item_number']);
                $iLang = substr($purchase['f_item_number'],strlen($purchase['f_item_number'])-4,1);
                        $Session->post("/w2/Debug/record_lang",$iLang);
                // FIXME!! Need to handle Gifts and such
                $gret = $Account->grant($iLang,'P');
                        $Session->post("/w2/Debug/record_grant",$gret);
                $naction = 0x0200;
                $ib_tag = 3100;
            }
            if ($rpcSend == 'true') {
                $rpcElement = $_REQUEST['element'];
                $rpcValue = $_REQUEST['value'];
                //$Session->post("/debug/element","$rpcElement");
                //$Session->post("/debug/value","$rpcValue");
                $stepMap = array( 2 => 0, 24 => 1, 46 => 2, 69 => 3, 91 => 4);
                //$stepMap = array( 0 => 0, 17 => 1, 35 => 2, 54 => 3, 73 => 4, 92 => 5);
                if ($rpcElement == 'tbVol') {
                    $FlashCard->Volume = ceil(intval($rpcValue) * 100 / 92);
                }
                if ($rpcElement == 'tbSpd') {
                    $FlashCard->Delay = (100 - intval($rpcValue)) * 50;
                }
                if ($rpcElement == 'tbCyc') {
                    $FlashCard->Cycle = $stepMap[intval($rpcValue)];
                }
                if ($rpcElement == 'tbRpt') {
                    $FlashCard->Repeat = $stepMap[intval($rpcValue)];
                }
                if ($rpcElement == 'lgData') {
                    // rpcValue = sres|bres|cres|browserID
                    list($sres,$bres,$cres,$agent) = explode('|',$rpcValue);

                    // Log is "new" or "res" or "complete"                    
                    $state = $Session->get("/log/logstate");
                    $Session->post("/log/logerror",":$state:");// if new...
                    if ($state == 'new') {
                        $result = $Session->agentID($agent);
                        $ip = $_SERVER['REMOTE_ADDR'];
                        $port = $_SERVER['REMOTE_PORT'];
                        $bid = $Session->addBrowser($result['browser']['name'],$result['browser']['ver']);
                        $oid = $Session->addOS($result['os']);
                        $mid = $Session->addMoz($result['moz']['name'],$result['moz']['ver']);
                        // http://www.google.com/search?hl=en&q=5+muses&btnG=Google+Search
                        $sql = "INSERT INTO log_enter SET ip_address='$ip', entry_time=NOW(), ip_port='$port',browser_id='$bid',os_id='$oid',moz_id='$mid'";
                        mysqli_query($db_link, $sql);
                        $idresult = mysqli_query($db_link, "select LAST_INSERT_ID() as ID;");
                        $row = mysqli_fetch_assoc($idresult);
                        $id = $row['ID'];
                        ((mysqli_free_result($idresult) || (is_object($idresult) && (get_class($idresult) == "mysqli_result"))) ? true : false);
                        $Session->post("/log/logstate",'res');  //New, Res, Complete
                        $Session->post("/log/logid",$id);  //ID of the Log Entry
                    }
                    if ($state == 'res') {
                        $id = $Session->get("/log/logid");
                        // Breakdown the resolution
                        $sres_p = ($sres != 'xx');
                        $bres_p = ($bres != 'x');
                        $cres_p = ($cres != 'x');
                        $sx=0;$sy=0;$sd=0;$bx=0;$by=0;$cx=0;$cy=0;
                        if ($sres_p) { list($sx,$sy,$sd) = explode('x',$sres); }
                        if ($bres_p) { list($bx,$by) = explode('x',$bres); }
                        if ($cres_p) { list($cx,$cy) = explode('x',$cres); }
                        $sql = "UPDATE log_enter SET s_res_x='$sx',s_res_y='$sy',s_res_d='$sd',c_res_x='$cx',c_res_y='$cy',b_res_x='$bx',b_res_y='$by' WHERE logID='$id'";
                        mysqli_query($db_link, $sql);
                        $Session->post("/log/logerror",((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));// if new...
                        $Session->post("/log/logstate",'complete');  //New, Res, Complete
                    }
                }
                // Spacer GIF... =)
                $js = base64_decode("R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==");
            }

            //$Vocabulary->vSearchMode ? $Debugger->logline("SearchMode: True") : $Debugger->logline("SearchMode: False");
            //$Vocabulary->vFilter ? $Debugger->logline("Filter: True") : $Debugger->logline("Filter: False");
            //$ib_search = $_REQUEST['dfcSearch'];
            //$ib_lessons = $_REQUEST['dfcLessons'];
            
            //break;
        }
        //--- Headings ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0400) { // Main Headlines
            //$Debugger->logline("ibAct: $ib_act");
            switch ($ib_tag) {
                case 2000: // Main Dynamic Fast Cards Page
                        $js .= "fmrpcPayload = \"<DIV CLASS=dfc1><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=714><TR>";
                        $js .= "<TD VALIGN=BOTTOM><DIV CLASS=dfc1title STYLE=\\\"width:714;\\\">Dynamic Fast Cards</DIV></TD></TR>";
                        $js .= "<TR><TD><DIV CLASS=dfc1vocabscroll STYLE=\\\"font-size:80%;width:714;height:360;\\\">";
                        
                        // Content Here...
                        $js .= '<TABLE BORDER=0 CELLPADDING=6 CELLSPACING=0 WIDTH=100% STYLE=\\"font-size:12px;\\">';
                        $js .= '<TR><TD VALIGN=TOP><B><U>Sign up for a free account.</U></B><BR>With a free account you can save your custom lessons for use later. You can also purchase our products with a free account.<BR><BR>Sign Up Now!</TD>';
                        $js .= '<TD VALIGN=TOP><B><U>Learn 400 words Free.</U></B><BR>Our quick start guide will get you started using and making your lessons quickly. Choose a language below to get started.<BR>';
                        $js .= '<BR><A HREF=\\"400free.php?lang=spanish\\" STYLE=\\"color:#076700;text-decoration:none;\\">Spanish Quick Start Guide</A>';
                        $js .= '<BR><A HREF=\\"400free.php?lang=french\\" STYLE=\\"color:#0C1B89;text-decoration:none;\\">French Quick Start Guide</A>';
                        $js .= '<BR><A HREF=\\"400free.php?lang=german\\" STYLE=\\"color:#670000;text-decoration:none;\\">German Quick Start Guide</A></TD></TR></TABLE><BR>';
                        $js .= '    <TABLE ALIGN=LEFT BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=230 STYLE=\\"font-size:12px;border-right: 1px dashed black;\\">';
                        $js .= '        <TR>';
                        $js .= '            <TD WIDTH=33% COLSPAN=2 STYLE=\\"font-size:14px;color:#076700;border-bottom:1px dashed black;font-weight:bold;\\">&nbsp;Spanish</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>Try the Spanish Online Demo</B><BR>View our online demo to see how it works.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>Spanish Lesson Builder</B><BR>Start building your own custom lesson.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>Spanish Pre-made Lessons</B><BR>Use our several pre-made lessons for quick learning.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR>';
                        $js .= '    </TABLE>';
                        $js .= '    <TABLE ALIGN=LEFT BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=230 STYLE=\\"font-size:12px;border-right: 1px dashed black;\\">';
                        $js .= '        <TR>';
                        $js .= '            <TD WIDTH=34% COLSPAN=2 STYLE=\\"font-size:14px;color:#0C1B89;border-bottom:1px dashed black;font-weight:bold;\\">&nbsp;French</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>Try the French Online Demo</B><BR>View our online demo to see how it works.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>French Lesson Builder</B><BR>Start building your own custom lesson.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>French Pre-made Lessons</B><BR>Use our several pre-made lessons for quick learning.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR>';
                        $js .= '    </TABLE>';
                        $js .= '    <TABLE ALIGN=LEFT BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=230 STYLE=\\"font-size:12px;\\">';
                        $js .= '        <TR>';
                        $js .= '            <TD WIDTH=34% COLSPAN=2 STYLE=\\"font-size:14px;color:#670000;border-bottom:1px dashed black;font-weight:bold;\\">&nbsp;German</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>Try the German Online Demo</B><BR>View our online demo to see how it works.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>German Lesson Builder</B><BR>Start building your own custom lesson.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD VALIGN=TOP><IMG SRC=images/pi.gif></TD><TD STYLE=\\"padding:0px 8px 0px 0px;\\"><B>German Pre-made Lessons</B><BR>Use our several pre-made lessons for quick learning.</TD>';
                        $js .= '        </TR><TR>';
                        $js .= '                <TD HEIGHT=16><IMG SRC=images/1px.gif></TD><TD HEIGHT=16><IMG SRC=images/1px.gif></TD>';
                        $js .= '        </TR>';
                        $js .= '    </TABLE>';
                        $js .= "</DIV></TD></TR></TABLE></DIV>\"\n$jsmain;";
                    break;
                case 3000: // Main Account Page
                        $js .= "fmrpcPayload = \"<DIV CLASS=dfc1><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=714><TR>";
                        $js .= "<TD VALIGN=BOTTOM><DIV CLASS=dfc1title STYLE=\\\"width:714;\\\">Your Account</DIV></TD></TR>";
                        $js .= "<TR><TD><DIV CLASS=dfc1vocabscroll STYLE=\\\"width:714;height:360;\\\">";
                        $js .= "</DIV></TD></TR></TABLE></DIV>\"\n$jsmain;";
                    break;
                case 4000: // Main Support Page
                        $js .= "fmrpcPayload = \"<DIV CLASS=dfc1><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=714><TR>";
                        $js .= "<TD VALIGN=BOTTOM><DIV CLASS=dfc1title STYLE=\\\"width:714;\\\">Support</DIV></TD></TR>";
                        $js .= "<TR><TD><DIV CLASS=dfc1vocabscroll STYLE=\\\"width:714;height:360;\\\">";
                        $js .= "</DIV></TD></TR></TABLE></DIV>\"\n$jsmain;";
                    break;
            }
        }
        //--- Login ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0080) { // Login 
            switch ($ib_tag) {
                // Change the "Login" to "Logout"
                case 0:
                    // Login Sucessful... Hide the login pane and update the status bar...
                    $js .= "fmLGPayload = \"".str_replace('"','\\"',fmlibLogin('reload0'))."\";\n$jslogin";
                    if ($Account->_Guest) {
                        $message = "Welcome Guest!";
                    } else {
                        $message = "Welcome {$Account->_ContactInfo['FName']}.";
                        if ($Account->has_access(39)) {
                            $message .= " <FONT COLOR=red>(Admin)</FONT>";
                        }
                    }
                    
                    // Change the message line....
                    $js .= "
                                var bParent = document.getElementById(\"bannerText\");
                                if (bParent) {
                                    var bContent = document.getElementById(\"bannerContent\");
                                    if (bContent) {
                                        oldParent = bContent.parentNode;
                                        oldParent.removeChild(bContent);
                                        bContent = null;
                                    }
                                    nContent = document.createElement('div');
                                    nContent.setAttribute('id','bannerContent');
                                    newText = document.createTextNode(\"Processing...\");
                                    nContent.appendChild(newText);
                                    nContent.innerHTML = \"$message\";
                                    bParent.appendChild(nContent);
                                }
                                var lParent = document.getElementById(\"bannerLogin\");
                                if (lParent) {
                                    lParent.removeChild(lParent.firstChild);
                                    nLogin = document.createElement('b');
                                    nText = document.createTextNode('Logout');
                                    nLogin.appendChild(nText);
                                    lParent.appendChild(nLogin);
                                }\n";
                        if ($na_valid) {
                            $naction = 0x0200;
                            $ib_tag = 3100;
                        } else {
                            $js .= "fireLogin('hide','transparent');\n";
                            $naction = 0x0200;
                            $ib_tag = 3100;
                        }
                    break;
                case 1:
                    // Login Failed... Show no account in login pane...
                    $js .= "fmLGPayload = \"".str_replace('"','\\"',fmlibLogin('reload1'))."\";\n$jslogin";
                    break;
                case 2:
                    // Login Failed... Show invalid password in login pane and a "forgot password" link...
                    $js .= "fmLGPayload = \"".str_replace('"','\\"',fmlibLogin('reload2',$login_e))."\";\n$jslogin";
                    break;
            }
        }
        //--- Logout ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0100) {  //Logout
            $js .= "fmLGPayload = \"".str_replace('"','\\"',fmlibLogin('default'))."\";\n$jslogin";
            $message = "Welcome Guest!";
            $js .= "
                    var bParent = document.getElementById(\"bannerText\");
                    if (bParent) {
                        var bContent = document.getElementById(\"bannerContent\");
                        if (bContent) {
                            oldParent = bContent.parentNode;
                            oldParent.removeChild(bContent);
                            bContent = null;
                        }
                        nContent = document.createElement('div');
                        nContent.setAttribute('id','bannerContent');
                        newText = document.createTextNode(\"Processing...\");
                        nContent.appendChild(newText);
                        nContent.innerHTML = \"$message\";
                        bParent.appendChild(nContent);
                    }
                    var lParent = document.getElementById(\"bannerLogin\");
                    if (lParent) {
                        lParent.removeChild(lParent.firstChild);
                        nLogin = document.createElement('b');
                        nText = document.createTextNode('Login');
                        nLogin.appendChild(nText);
                        lParent.appendChild(nLogin);
                    }
                    fireLogin('hide','transparent');
                    ";
            $naction = 0x0020;
            $ib_tag = 1000;
            
        }
        //--- Account ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0200) { // Account Stuff
            //$Debugger->logline("ibAct: $ib_act");
            switch ($ib_tag) {
                case 3100: // Overview / Signup
                    if ($Account->is_guest()) {
                        // Itsa guesta! Give them the form to create a new account
                        // Create account form....
                        $js .= "fmrpcPayload = \"<DIV CLASS=dfc1><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=714><TR>";
                        $js .= "<TD VALIGN=BOTTOM><DIV CLASS=dfc1title STYLE=\\\"width:714;\\\">Sign up for a free account!</DIV></TD></TR>";
                        $js .= "<TR><TD><DIV CLASS=dfc1vocabscroll STYLE=\\\"width:714;height:360;\\\">";
                        $js .= '<FORM ACTION=# METHOD=POST NAME=newacct onSubmit=\\"rpcFetch(\'act=set&na=true\',\'newacct\');return false;\\">';
                        $js .= '<BR><TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 WIDTH=694 STYLE=\\"font-size:12px;\\">';
                        
                        if ($na_message != '') {
                            $js .= "<TR><TD ALIGN=LEFT COLSPAN=2 STYLE=\\\"border:1px solid #FF0000;background-color:#FFEAC7;\\\">$na_message</TD></TR>";
                        }
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_email\\\">E-Mail Address:</TD><TD><INPUT CLASS=login1 STYLE=\\\"width:250;$nav_email\\\" NAME=na_email VALUE=\\\"$na_email\\\"></TD></TR>";
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_pass1\\\">Password:</TD><TD><INPUT CLASS=login1 TYPE=PASSWORD STYLE=\\\"width:100;$nav_pass1\\\" NAME=na_pass1 VALUE=\\\"$na_pass1\\\"></TD></TR>";
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_pass2\\\">Confirm Password:</TD><TD><INPUT CLASS=login1 TYPE=PASSWORD STYLE=\\\"width:100;$nav_pass2\\\" NAME=na_pass2 VALUE=\\\"$na_pass2\\\"></TD></TR>";
                        
                        $js .= '<TR><TD COLSPAN=2 STYLE=\\"height:8;border-bottom:1px solid #808080;\\">&nbsp;</TD></TR>';
                        $js .= '<TR><TD COLSPAN=2 STYLE=\\"height:8;\\">&nbsp;</TD></TR>';
                        
                        $na_ctry = ($na_ctry == '') ? 'US' : $na_ctry ;
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_fname\\\">First Name:</TD><TD><INPUT CLASS=login1 STYLE=\\\"width:160;$nav_fname\\\" NAME=na_fname VALUE=\\\"$na_fname\\\"></TD></TR>";
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_lname\\\">Last Name:</TD><TD><INPUT CLASS=login1 STYLE=\\\"width:160;$nav_lname\\\" NAME=na_lname VALUE=\\\"$na_lname\\\"></TD></TR>";
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_addr1\\\">Address 1:</TD><TD><INPUT CLASS=login1 STYLE=\\\"width:210;$nav_addr1\\\" NAME=na_addr1 VALUE=\\\"$na_addr1\\\"></TD></TR>";
                        $js .= "<TR><TD ALIGN=RIGHT>Address 2:</TD><TD><INPUT CLASS=login1 STYLE=\\\"width:210;\\\" NAME=na_addr2 VALUE=\\\"$na_addr2\\\"></TD></TR>";
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_city\\\">City:</TD><TD><INPUT CLASS=login1 STYLE=\\\"width:160;$nav_city\\\" NAME=na_city VALUE=\\\"$na_city\\\"></TD></TR>";
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_state\\\">State/Province:</TD><TD><INPUT CLASS=login1 STYLE=\\\"width:140;$nav_state\\\" NAME=na_state VALUE=\\\"$na_state\\\"></TD></TR>";
                        $js .= "<TR><TD ALIGN=RIGHT STYLE=\\\"font-size:12px;$nav_zip\\\">Zip Code:</TD><TD><INPUT CLASS=login1 STYLE=\\\"width:80;$nav_zip\\\" NAME=na_zip VALUE=\\\"$na_zip\\\"></TD></TR>";
                        $js .= '<TR><TD ALIGN=RIGHT>Country:</TD><TD>'.get_country_select('na_ctry','login1',$na_ctry).'</TD></TR>';
                        
                        $js .= '<TR><TD COLSPAN=2 STYLE=\\"height:8;border-bottom:1px solid #808080;\\">&nbsp;</TD></TR>';
                        $js .= '<TR><TD COLSPAN=2 STYLE=\\"height:8;\\">&nbsp;</TD></TR>';
                        $js .= '<TR><TD COLSPAN=2 ALIGN=CENTER><INPUT CLASS=loginb1 TYPE=BUTTON NAME=lbutton VALUE=Register onClick=\\"rpcFetch(\'act=set&na=true\',\'newacct\');\\"></TD></TR>';
                        $js .= '</TABLE>';
                        
                        // How about a brief survey?
                        $js .= "</DIV></TD></TR></TABLE></DIV>\"\n$jsmain;";
                    } else {
                        // Itsa Memba! Show them their activity, account duration, etc...
                        $js .= "fmrpcPayload = \"<DIV CLASS=dfc1><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=714><TR>";
                        $js .= "<TD VALIGN=BOTTOM><DIV CLASS=dfc1title STYLE=\\\"width:714;\\\">Account Overview</DIV></TD></TR>";
                        $js .= "<TR><TD><DIV CLASS=dfc1vocabscroll STYLE=\\\"width:714;height:360;\\\">";
                        $js .= "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 WIDTH=690>"; // Encapsulating Table for neatness
                        $js .= "<TR>";
                        
                        $js .= "<TD VALIGN=TOP>"; //  Account Name and address
                            $js .= "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 STYLE=\\\"font-size:12px;\\\" WIDTH=400>"; // Name and address etc...
                            $js .= "<TR><TD COLSPAN=3 STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;border-top:1px solid #A0A0A0;\\\"><B>Contact Information</B></TD></TR>";
                            $js .= "<TR><TD COLSPAN=3 STYLE=\\\"border-bottom:1px solid #C0C0C0;color:#000080;\\\">{$Account->_ContactInfo['email']}</TD></TR>";
                            $js .= "<TR><TD COLSPAN=3>{$Account->_ContactInfo['FName']} {$Account->_ContactInfo['LName']}</TD></TR>";
                            if (strlen($Account->_ContactInfo['POBox']) > 0) {
                                $js .= "<TR><TD COLSPAN=3>P.O. Box  #{$Account->_ContactInfo['POBox']}</TD></TR>";
                            } else {
                                $js .= "<TR><TD COLSPAN=3>{$Account->_ContactInfo['Address1']}</TD></TR>";
                                if (strlen($Account->_ContactInfo['Address2']) > 2) {
                                    $js .= "<TR><TD COLSPAN=3>{$Account->_ContactInfo['Address2']}</TD></TR>";
                                }
                            }
                            $js .= "<TR><TD>{$Account->_ContactInfo['City']}</TD><TD>{$Account->_ContactInfo['State']}</TD><TD>{$Account->_ContactInfo['Zip']}</TD></TR>";
                            $js .= "</TABLE>";
                        $js .= "</TD>";
                        
                        $js .= "<TD VALIGN=TOP WIDTH=200>"; //Status
                            $js .= "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 WIDTH=100% ALIGN=RIGHT STYLE=\\\"font-size:12px;\\\">"; // Account Status
                            $js .= "<TR><TD COLSPAN=2 STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;border-top:1px solid #A0A0A0;\\\"><B>Account Status</B></TD></TR>";
                            $ctime = strftime("%b %e, %Y",$Account->_Created);
                            $vtime = strftime("%b %e, %Y",$Account->_LastVisit);
                            $weblev = $Account->req_access(31);
                            $webaccess = '<FONT COLOR=#808080>Inactive</FONT>';
                            if ($weblev == 'G') { $webaccess = '<FONT COLOR=#008000>Active</FONT>'; }
                            if ($weblev == 'S') { $webaccess = '<FONT COLOR=#E00000>Suspended</FONT>'; }
                            if ($weblev == 'D') { $webaccess = '<FONT COLOR=#E00000>Deleted</FONT>'; }
                            
                            $js .= "<TR><TD COLSPAN=2>Your account is $webaccess.</TD></TR>";
                            $js .= "<TR><TD COLSPAN=2 HEIGHT=8></TD></TR>";
                            $js .= "<TR><TD STYLE=\\\"border-bottom:1px dashed #C0C0C0;\\\">Created:</TD><TD ALIGN=RIGHT STYLE=\\\"border-bottom:1px dashed #C0C0C0;\\\">$ctime</TD></TR>";
                            $js .= "<TR><TD STYLE=\\\"border-bottom:1px dashed #C0C0C0;\\\">Last Visit:</TD><TD ALIGN=RIGHT STYLE=\\\"border-bottom:1px dashed #C0C0C0;\\\">$vtime</TD></TR>";
                            $js .= "<TR><TD STYLE=\\\"border-bottom:1px dashed #C0C0C0;\\\">Visits:</TD><TD ALIGN=RIGHT STYLE=\\\"border-bottom:1px dashed #C0C0C0;\\\">{$Account->_Visits}</TD></TR>";
                            $js .= "</TABLE>";
                        $js .= "</TD>";

/*                        $js .= "<TD VALIGN=TOP COLSPAN=3>"; // Storage
                            $js .= "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 STYLE=\\\"font-size:12px;\\\" WIDTH=100%>"; // Lessons
                            //$Lesson->update_lesson_lang();
                            //$errors = $Lesson->fix_lesson_lang();
                            $summary = $Lesson->lesson_summary();
                            // Calc the graph and number of lessons... Also, how many lessons you can have...
                            $lessonmax = $Account->LessonStorage();
                            if ($weblev != 'G') { $lessonmax = 0; }
                            $used = $summary['l2'] + $summary['l3'] + $summary['l4'];
                            $perc = ($lessonmax == 0) ? 100 : intval(($used / $lessonmax) * 100);
                            $grm = $perc - 12;
                            if ($grm < 1) { $grm = 1; }
                            $js .= "<TR><TD COLSPAN=4 VALIGN=BOTTOM>";
                            $js .= "<DIV STYLE=\\\"float:right;width:100px;background-color:#666677;border-top:1px solid #000040;border-bottom:1px solid #000040;\\\">";
                            $js .= "<IMG SRC=images/pb3l.gif><IMG WIDTH=$grm HEIGHT=10 SRC=images/pb3m.gif><IMG SRC=images/pb3r.gif></DIV>";
                            $js .= "<B><U>Account Storage</U></B></TD></TR>";
                            $js .= "<TR><TD COLSPAN=4 STYLE=\\\"border-bottom:1px solid #C0C0C0;color:#404040;\\\">";
                            $js .= "You are using $used out of $lessonmax lessons.";
                            $js .= "</TD></TR>";
                            // Show a graph of used lessons....
                            // Saved Lessons:   3 of 50;  ###==========
                            //
                            //$js .= "<TR><TD COLSPAN=4>".str_replace('"','\\"',print_r($summary,true)."</TD></TR>";
                            if ($summary['l2'] > 0) {
                                // Spanish Stuff...
                                $js .= "<TR><TD COLSPAN=4 STYLE=\\\"border-bottom:1px solid #C0C0C0;color:#008000;\\\"><B>Spanish</B></TD></TR>";
//                                 $js .= "<TR><TD><FONT COLOR=#808080>Name</FONT></TD><TD><FONT COLOR=#808080>Words</FONT></TD><TD><FONT COLOR=#808080>Used</FONT></TD><TD><FONT COLOR=#808080>Created</FONT></TD></TR>";
//                                 foreach ($summary['litems'] as $entry) {
//                                     if (($entry['lang'] == 2) and ($entry['name'] != '[Current]')) {
//                                         $ctime = strftime("%b %e, %Y",$entry['created']);
//                                         $js .= "<TR><TD>{$entry['name']}</TD><TD>{$entry['words']}</TD><TD>{$entry['used']}</TD><TD>$ctime</TD></TR>";
//                                     }
//                                 }
                            }
                            if ($summary['l3'] > 0) {
                                // French Stuff...
                                $js .= "<TR><TD COLSPAN=4 STYLE=\\\"border-bottom:1px solid #C0C0C0;color:#000080;\\\"><B>French</B></TD></TR>";
//                                 $js .= "<TR><TD><FONT COLOR=#808080>Name</FONT></TD><TD><FONT COLOR=#808080>Words</FONT></TD><TD><FONT COLOR=#808080>Used</FONT></TD><TD><FONT COLOR=#808080>Created</FONT></TD></TR>";
//                                 foreach ($summary['litems'] as $entry) {
//                                     if (($entry['lang'] == 3) and ($entry['name'] != '[Current]')) {
//                                         $ctime = strftime("%b %e, %Y",$entry['created']);
//                                         $js .= "<TR><TD>{$entry['name']}</TD><TD>{$entry['words']}</TD><TD>{$entry['used']}</TD><TD>$ctime</TD></TR>";
//                                     }
//                                 }
                            }
                            if ($summary['l4'] > 0) {
                                // German Stuff...
                                $js .= "<TR><TD COLSPAN=4 STYLE=\\\"border-bottom:1px solid #C0C0C0;color:#800000;\\\"><B>German</B></TD></TR>";
//                                 $js .= "<TR><TD><FONT COLOR=#808080>Name</FONT></TD><TD><FONT COLOR=#808080>Words</FONT></TD><TD><FONT COLOR=#808080>Used</FONT></TD><TD><FONT COLOR=#808080>Created</FONT></TD></TR>";
//                                 foreach ($summary['litems'] as $entry) {
//                                     if (($entry['lang'] == 4) and ($entry['name'] != '[Current]')) {
//                                         $ctime = strftime("%b %e, %Y",$entry['created']);
//                                         $js .= "<TR><TD>{$entry['name']}</TD><TD>{$entry['words']}</TD><TD>{$entry['used']}</TD><TD>$ctime</TD></TR>";
//                                     }
//                                 }
                            }
//                             // Other Stuff
//                             $js .= "<TR><TD COLSPAN=4 STYLE=\\\"border-bottom:1px solid #C0C0C0;color:#808080;\\\"><B>Unknown</B></TD></TR>";
//                             $js .= "<TR><TD><FONT COLOR=#808080>Name</FONT></TD><TD><FONT COLOR=#808080>Words</FONT></TD><TD><FONT COLOR=#808080>Used</FONT></TD><TD><FONT COLOR=#808080>Created</FONT></TD></TR>";
//                             foreach ($summary['litems'] as $entry) {
//                                 $ctime = strftime("%b %e, %Y",$entry['created']);
//                                 $js .= "<TR><TD>{$entry['name']} ({$entry['lang']})</TD><TD>{$entry['words']}</TD><TD>{$entry['used']}</TD><TD>$ctime</TD></TR>";
//                             }
                            $js .= "</TABLE>";
                            //if (is_array($errors)) foreach ($errors as $line) {
                            //    $js .= "[-".str_replace('"','\\"',$line)."-]<BR>";
                            //}
                            
                        $js .= "</TD>";*/
                        
                        $js .= "</TR>";
                        $js .= "<TR><TD VALIGN=TOP COLSPAN=2 HEIGHT=16></TD></TR>"; 
                        $js .= "<TR>";
                        
                        $js .= "<TD VALIGN=TOP COLSPAN=3>"; // Subscriptions
                            $splev = $Account->req_access(1);
                            $frlev = $Account->req_access(2);
                            $gelev = $Account->req_access(3);
                            $spaccess = $Access_Codes[$splev];
                            $fraccess = $Access_Codes[$frlev];
                            $geaccess = $Access_Codes[$gelev];
                            $spaccess = (($splev == 'G') or ($splev == 'P') or ($splev == 'C')) ? "<FONT COLOR=#008000>$spaccess</FONT>" : ((($splev == 'S') or ($splev == 'D')) ? "<FONT COLOR=#E00000>$spaccess</FONT>" : "<FONT COLOR=#808080>$spaccess</FONT>");
                            $fraccess = (($frlev == 'G') or ($frlev == 'P') or ($frlev == 'C')) ? "<FONT COLOR=#008000>$fraccess</FONT>" : ((($frlev == 'S') or ($frlev == 'D')) ? "<FONT COLOR=#E00000>$fraccess</FONT>" : "<FONT COLOR=#808080>$fraccess</FONT>");
                            $geaccess = (($gelev == 'G') or ($gelev == 'P') or ($gelev == 'C')) ? "<FONT COLOR=#008000>$geaccess</FONT>" : ((($gelev == 'S') or ($gelev == 'D')) ? "<FONT COLOR=#E00000>$geaccess</FONT>" : "<FONT COLOR=#808080>$geaccess</FONT>");
                            
                            $js .= "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH=100% ALIGN=RIGHT STYLE=\\\"font-size:12px;\\\">"; // Financial Stuff
                            // Init the Financial Object... TODO: Write the Finanacial Object.
                            $js .= "<TR><TD COLSPAN=4 STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;border-top:1px solid #A0A0A0;\\\"><B>Subscriptions</B></TD></TR>";
                            $trans = $Records->fetch_purchases($Account->getID());
                            $strans = array();
                            foreach ($trans as $entry) {
                                $sentry = array();
                                foreach ($entry as $key => $value) {
                                    $sentry[$key] = $value;
                                }
                                $strans[$entry['f_received']] = $sentry;
                            }
                            // Order descending by received.
                            krsort($strans);
                            foreach($strans as $entry) {
                                // Show the Valid Entry.
                                // Decode Item Number...
                                $item = $Records->find_item($entry['f_item_number']);
                                $itemName = $item['item_name'];
                                $color = '#A0A0A0';
                                $iLang = substr($item['item_number'],strlen($item['item_number'])-4,1);
                                if ($iLang == '2') { $color = '#080'; }
                                if ($iLang == '3') { $color = '#800'; }
                                if ($iLang == '4') { $color = '#018'; }
                                $received = strtotime($entry['f_received']);
                                $activated = ($entry['f_activated'] != '') ? strtotime($entry['f_activated']) : 0 ;
                                $expires = ($entry['f_expires'] != '') ? strtotime($entry['f_expires']) : 0 ;
                                $vacation = ($entry['f_vacation'] == 1);
                                $status = $entry['f_status']; //??
                                
                                $js .= "<TR><TD COLPSAN=4 HEIGHT=6></TD></TR>";
                        
                                // Has been activated...
                                if (($expires != 0) and ($expires < time())) {
                                    // Expired
                                    $hActivated = strftime('%b %e, %G',$received);
                                    $hExpires = strftime('%b %e, %G',$expires);
                                    $hVacation = "Vacation";
                                    $js .= "<TR>";
                                    $js .= "<TD COLSPAN=4 STYLE=\\\"background-color:#EEE;color:#777;border-top:1px solid #006;border-bottom:1px solid #D8D8D8;\\\"><S>$itemName</S></TD>";
                                    $js .= "</TR>";
                                    $js .= "<TR>";
                                    $js .= "<TD WIDTH=20 STYLE=\\\"background-color:#EEE;border-bottom:1px solid #006;\\\">&nbsp;</TD>";
                                    $js .= "<TD STYLE=\\\"background-color:#EEE;color:#888;border-bottom:1px solid #006;\\\">Expired on $hExpires</TD>";
                                    $js .= "<TD STYLE=\\\"background-color:#EEE;color:#888;border-bottom:1px solid #006;\\\">&nbsp;</TD>";
                                    $js .= "<TD STYLE=\\\"background-color:#EEE;color:#888;border-bottom:1px solid #006;\\\">&nbsp;</TD>";
                                    $js .= "</TR>";
                                } else {
                                    if ($activated == 0) {
                                        // Can Activate...
                                        $hActivated = strftime('%b %e, %G',$received);
                                        $hExpires = "Expires";
                                        $hVacation = "Vacation";
                                        $js .= "<TR>";
                                        $js .= "<TD COLSPAN=4 STYLE=\\\"background-color:#FFD;color:$color;border-top:1px solid #006;border-bottom:1px dashed #CCC;\\\"><DIV STYLE=\\\"float:right;color:#22F;\\\">[ <A HREF=# onClick=\\\"rpcFetch('act=set&acas={$entry['fID']}');return false;\\\"><B>Use This Subscription</B></A> ]</DIV>$itemName</TD>";
                                        $js .= "</TR>";
                                        $js .= "<TR>";
                                        $js .= "<TD WIDTH=20 STYLE=\\\"background-color:#FFD;border-bottom:1px solid #006;\\\">&nbsp;</TD>";
                                        $js .= "<TD STYLE=\\\"background-color:#FFD;color:#888;border-bottom:1px solid #006;\\\">Received on $hActivated</TD>";
                                        $js .= "<TD STYLE=\\\"background-color:#FFD;color:#888;border-bottom:1px solid #006;\\\">&nbsp;</TD>";
                                        $js .= "<TD STYLE=\\\"background-color:#FFD;color:#888;border-bottom:1px solid #006;\\\">&nbsp;</TD>";
                                        $js .= "</TR>";
                                    } else {
                                        // In Use
                                        if ($vacation) {
                                            $hActivated = strftime('%b %e, %G',$activated);
                                            $hExpires = strftime('%b %e, %G',$expires);
                                            $days = intval(($expires - $activated) / 86400);
                                            $hVacation = "Vacation";
                                            $js .= "<TR>";
                                            $js .= "<TD COLSPAN=4 STYLE=\\\"background-color:#FFD;color:$color;border-top:1px solid #006;border-bottom:1px dashed #CCC;\\\"><DIV STYLE=\\\"float:right;color:#444;\\\">[ <B>On Vacation</B> ]</DIV>$itemName</TD>";
                                            $js .= "</TR>";
                                            $js .= "<TR>";
                                            $js .= "<TD WIDTH=20 STYLE=\\\"background-color:#FFD;border-bottom:1px solid #006;\\\">&nbsp;</TD>";
                                            $js .= "<TD STYLE=\\\"background-color:#FFD;color:#888;border-bottom:1px solid ##006;\\\">Used on $hActivated</TD>";
                                            $js .= "<TD STYLE=\\\"background-color:#FFD;color:#888;border-bottom:1px solid #006;\\\">Expires on $hExpires</TD>";
                                            $js .= "<TD STYLE=\\\"background-color:#FFD;color:#888;border-bottom:1px solid #006;\\\">$days days remain</TD>";
                                            $js .= "</TR>";
                                        } else {
                                            $hActivated = strftime('%b %e, %G',$activated);
                                            $hExpires = strftime('%b %e, %G',$expires);
                                            $days = intval(($expires - $activated) / 86400);
                                            $hVacation = "Vacation";
                                            $js .= "<TR>";
                                            $js .= "<TD COLSPAN=4 STYLE=\\\"background-color:#DFD;color:$color;border-top:1px solid #006;border-bottom:1px dashed #CCC;\\\"><DIV STYLE=\\\"float:right;color:#444;\\\">[ <B>In Use</B> ]</DIV>$itemName</TD>";
                                            $js .= "</TR>";
                                            $js .= "<TR>";
                                            $js .= "<TD WIDTH=20 STYLE=\\\"background-color:#DFD;border-bottom:1px solid #006;\\\">&nbsp;</TD>";
                                            $js .= "<TD STYLE=\\\"background-color:#DFD;color:#888;border-bottom:1px solid #006;\\\">Used on $hActivated</TD>";
                                            $js .= "<TD STYLE=\\\"background-color:#DFD;color:#888;border-bottom:1px solid #006;\\\">Expires on $hExpires</TD>";
                                            $js .= "<TD STYLE=\\\"background-color:#DFD;color:#888;border-bottom:1px solid #006;\\\">$days days remain</TD>";
                                            $js .= "</TR>";
                                        }
                                    }
                                }
                            }
                            $js .= "</TABLE>";
                        $js .= "</TD>";
                        
                        $js .= "</TR>";
                        $js .= "</TABLE>";
                        $js .= "</DIV></TD></TR></TABLE></DIV>\"\n$jsmain;";
                    }
                    break;
                case 3110: // Billing History
                    break;
                case 3120: // Edit Information
                    break;
                case 3130: // Thank you for your purchase
                        // Say Thanks and don't forget to activate your subscription!
                    break;
                }
            }
        //--- DFC ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0040) {  // Dynamic Flash Cards
            switch ($ib_tag) {
                case 2110: // Pre-Made Lessons
                    $js .= "fmrpcPayload = \"<DIV CLASS=dfc1>\" + ";
                    //--------------------------------------------------------------------
                    // Title and Languages
                    $js .= "\"<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=710>\" + ";
                    $js .= "\"<TR>\" + ";
                    $js .= "\"<TD VALIGN=BOTTOM STYLE=\\\"border-bottom:1px solid #008;\\\"><DIV CLASS=dfc1title>Pre-Made Lessons</DIV></TD>\" + ";
                    $js .= "\"<TD VALIGN=BOTTOM WIDTH=320 STYLE=\\\"border-bottom:1px solid #008;\\\"><DIV CLASS=dfc1lang0><A HREF=# onClick=\\\"rpcFetch('act=set&vl=3&ibTag=2110');return false;\\\"><IMG SRC=images/2130-6.jpg ALT=German></A></DIV>\" + ";
                    $js .= "\"<DIV CLASS=dfc1lang0><A HREF=# onClick=\\\"rpcFetch('act=set&vl=4&ibTag=2110');return false;\\\"><IMG SRC=images/2130-4.jpg ALT=French></A></DIV>\" + ";
                    $js .= "\"<DIV CLASS=dfc1lang0><A HREF=# onClick=\\\"rpcFetch('act=set&vl=2&ibTag=2110');return false;\\\"><IMG SRC=images/2130-5.jpg ALT=Spanish></A></DIV></TD></TR>\" + ";
                    $js .= "\"</TR>\" + ";
                    //--------------------------------------------------------------------
                    // Main enclosure for vocabulary
                    $js .= "\"<TR><TD COLSPAN=2><DIV CLASS=dfc1indent>\" + ";
                    //$js .= "\"<TR><TD COLSPAN=2><DIV CLASS=dfc1main STYLE=\\\"height:336px;\\\">\" + ";
                    
                    $summary = $Lesson->lesson_summary(true);
                    //$pml .= "<PRE>".str_replace("\n","<BR>",addslashes(print_r($summary,true)))."</PRE>";
                    //$pml .= print_r($Lesson,true);
                    $pml .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=678>";
                    $pml .= "<TR>";
                    $pml .= "<TD WIDTH=39><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">&nbsp;</DIV></TD>";
                    $pml .= "<TD><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Lesson Name</DIV></TD>";
                    $pml .= "<TD><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Language</DIV></TD>";
                    $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Words</DIV></TD>";
                    $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Used</DIV></TD>";
                    $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Created</DIV></TD>";
                    $pml .= "<TD WIDTH=42><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">&nbsp;</DIV></TD>";
                    $pml .= "</TR>";
                    //TODO: Make Columns Sortable (Use $Lesson->_listSortOrder) or something.
                    if ($summary !== false) foreach ($summary['litems'] as $entry) {
                        if (($entry['name'] != "[Current]") and ($entry['lang'] == $Lesson->_language)) {
                            // Use a field in the Lesson Table for designating if this is free
                            if (($entry['owner'] == 'PreMade-Free') or ($Account->has_access(0,$Lesson->_language)) ) {
                                $pml .= "<TR>";
                                // make anchors to load lessons...
                                if ($entry['lang'] == 2) { $hLang = "<FONT COLOR=#008000>Spanish</FONT>"; $hIcon = "<IMG SRC=images/bfs.gif BORDER=0>"; }
                                if ($entry['lang'] == 4) { $hLang = "<FONT COLOR=#000080>French</FONT>"; $hIcon = "<IMG SRC=images/bss.gif BORDER=0>"; }
                                if ($entry['lang'] == 3) { $hLang = "<FONT COLOR=#800000>German</FONT>"; $hIcon = "<IMG SRC=images/bgs.gif BORDER=0>"; }
                                if (($entry['lang'] < 2) or ($entry['lang'] > 4)){ $hLang = "<FONT COLOR=#808080>Unknown</FONT>"; $hIcon = "<IMG SRC=images/bgs.gif BORDER=0>"; }
                                $hTime = strftime("%b %e, %Y",$entry['created']);
                                $hUsed = intval($entry['used']);
                                $pml .= "<TD WIDTH=39><DIV CLASS=dfc1small>$hIcon</DIV></TD>";
                                // Link to load lesson...
                                $pml .= "<TD><DIV CLASS=dfc1small><A HREF=# STYLE=\\\"color:#000040;\\\"onClick=\\\"rpcFetch('act=set&ll={$entry['ID']}');return false;\\\">".$entry['name']."</A></DIV></TD>";
                                $pml .= "<TD><DIV CLASS=dfc1small>$hLang</DIV></TD>";
                                $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small>".$entry['words']."</DIV></TD>";
                                $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small>".$hUsed."</DIV></TD>";
                                $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small>$hTime</DIV></TD>";
                                if (!$Account->has_access(0,$Lesson->_language)) {
                                    $pml .= "<TD ALIGN=RIGHT VALIGN=MIDDLE WIDTH=42><IMG BORDER=0 WIDTH=35 HEIGHT=18 SRC=images/free.png></TD>";
                                }
                                $pml .= "</TR>";
                            } else {
                                $pml .= "<TR>";
                                // make anchors to load lessons...
                                if ($entry['lang'] == 2) { $hLang = "<SPAN STYLE=\\\"color:#8A8;\\\">Spanish</SPAN>"; $hIcon = "<IMG SRC=images/bfs.gif BORDER=0>"; }
                                if ($entry['lang'] == 4) { $hLang = "<SPAN STYLE=\\\"color:#88A;\\\">French</SPAN>"; $hIcon = "<IMG SRC=images/bss.gif BORDER=0>"; }
                                if ($entry['lang'] == 3) { $hLang = "<SPAN STYLE=\\\"color:#A88;\\\">German</SPAN>"; $hIcon = "<IMG SRC=images/bgs.gif BORDER=0>"; }
                                if (($entry['lang'] < 2) or ($entry['lang'] > 4)){ $hLang = "<FONT COLOR=#808080>Unknown</FONT>"; $hIcon = "<IMG SRC=images/bgs.gif BORDER=0>"; }
                                $hTime = strftime("%b %e, %Y",$entry['created']);
                                $hUsed = intval($entry['used']);
                                $pml .= "<TD WIDTH=39><DIV CLASS=dfc1small>$hIcon</DIV></TD>";
                                // Link to load lesson...
                                $pml .= "<TD><DIV CLASS=dfc1small><SPAN STYLE=\\\"color:#AAA;\\\">".$entry['name']."</DIV></TD>";
                                $pml .= "<TD><DIV CLASS=dfc1small STYLE=\\\"color:#AAA;\\\">$hLang</DIV></TD>";
                                $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"color:#AAA;\\\">".$entry['words']."</DIV></TD>";
                                $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"color:#AAA;\\\">".$hUsed."</DIV></TD>";
                                $pml .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"color:#AAA;\\\">$hTime</DIV></TD>";
                                //$pml .= "<TD ALIGN=RIGHT VALIGN=MIDDLE><A HREF=# onClick=\\\"rpcFetch('act=set&ldelete=true&lid={$entry['ID']}');return false;\\\"><IMG BORDER=0 WIDTH=26 HEIGHT=14 SRC=images/dell.gif ALT=\\\"Delete this Lesson\\\"></A></TD>";
                                $pml .= "</TR>";
                            }
                        }
                    }   
                    $pml .= "<TR><TD COLSPAN=7><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;border-top:1px solid #A0A0A0;\\\">&nbsp;</DIV></TD></TR>";
                    $pml .= '</TABLE>';
                    
                    // Add in the code....
                    $js .= "\"$pml\" + ";
                    // End of Main Enclosure
                    $js .= "\"</DIV>&nbsp;</TD></TR></TABLE>\" + ";

                    $js .= "\"</DIV>\";\n$jsmain";
                    
                    break;
                case 2120: // Purchasing
                    //$Debugger->logline("ibTag: $ib_tag");
                    // Determine if we are logged in first...
                    // Fetch 
                    // show the word dictionary
                    $js .= "fmrpcPayload = \"<DIV CLASS=dfc1>\" + ";
                    //--------------------------------------------------------------------
                    // Title and Languages
                    $js .= "\"<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=710>";
                    $js .= "<TR>";
                    $js .= "<TD VALIGN=BOTTOM><DIV CLASS=dfc1title>Purchase Dynamic Fast Cards</DIV></TD>";
                    $js .= "</TR>";
                    //--------------------------------------------------------------------
                    // Main enclosure for vocabulary
                    $js .= "<TR><TD COLSPAN=2><DIV CLASS=dfc1main>";
                    
                    // List of things to purchase...
                    // Spanish
                    $js .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 STYLE=\\"margin-bottom:32px;\\"WIDTH=670>'; // 
                    $js .= '<TR>';
                    $js .= '<TD COLSPAN=3 STYLE=\\"border-bottom:1px dashed #707080;color:#000040;font-size: 14px;font-weight:bold;\\">';
                    $js .= 'Spanish Vocabulary';
                    $js .= '</TD>';
                    $js .= '</TR>';
                    //FIXME:  SELECT * FROM p_itemcodes ORDER BY item_heading, item_number
                    // Then check for group changes and make titles as necessary.
                    $p_result = mysqli_query($db_link, "SELECT * FROM p_itemcodes WHERE item_number LIKE '402%'");
                    while ($row = mysqli_fetch_array($p_result)) {
                        $js .= '<TR>';
                        $js .= '<TD VALIGN=TOP STYLE=\\"padding-left:15px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">';
                        $js .= $row['item_name'];
                        $js .= '</TD>';
                        $js .= '<TD VALIGN=TOP ALIGN=RIGHT STYLE=\\"width:120px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">$';
                        $js .= $row['item_amount'];
                        $js .= '&nbsp;<SPAN STYLE=\\"color:#88AA88;font-size: 10px;\\">USD</SPAN></TD>';
                        $js .= '<TD VALIGN=TOP ALIGN=RIGHT STYLE=\\"width:120px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">';
                        $code = paypal_button($Account->getID(),$row['item_number']);
                        if ($code !== false) {
                            $js .= str_replace('"','\\"',$code);
                        } else {
                            $js .= 'Unavalable';
                        }
                        $js .= '</TD>';
                        $js .= '</TR>';
                    }
                    $js .= '</TABLE>';
                    // German
                    $js .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 STYLE=\\"margin-bottom:32px;\\"WIDTH=670>'; // 
                    $js .= '<TR>';
                    $js .= '<TD COLSPAN=3 STYLE=\\"border-bottom:1px dashed #707080;color:#000040;font-size: 14px;font-weight:bold;\\">';
                    $js .= 'German Vocabulary';
                    $js .= '</TD>';
                    $js .= '</TR>';
                    $p_result = mysqli_query($db_link, "SELECT * FROM p_itemcodes WHERE item_number LIKE '403%'");
                    while ($row = mysqli_fetch_array($p_result)) {
                        $js .= '<TR>';
                        $js .= '<TD VALIGN=TOP STYLE=\\"padding-left:15px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">';
                        $js .= $row['item_name'];
                        $js .= '</TD>';
                        $js .= '<TD VALIGN=TOP ALIGN=RIGHT STYLE=\\"width:120px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">$';
                        $js .= $row['item_amount'];
                        $js .= '&nbsp;<SPAN STYLE=\\"color:#88AA88;font-size: 10px;\\">USD</SPAN></TD>';
                        $js .= '<TD VALIGN=TOP ALIGN=RIGHT STYLE=\\"width:120px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">';
                        $code = paypal_button($Account->getID(),$row['item_number']);
                        if ($code !== false) {
                            $js .= str_replace('"','\\"',$code);
                        } else {
                            $js .= 'Unavalable';
                        }
                        $js .= '</TD>';
                        $js .= '</TR>';
                    }
                    $js .= '</TABLE>';
                    // French
                    $js .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 STYLE=\\"margin-bottom:32px;\\"WIDTH=670>'; // 
                    $js .= '<TR>';
                    $js .= '<TD COLSPAN=3 STYLE=\\"border-bottom:1px dashed #707080;color:#000040;font-size: 14px;font-weight:bold;\\">';
                    $js .= 'French Vocabulary';
                    $js .= '</TD>';
                    $js .= '</TR>';
                    $p_result = mysqli_query($db_link, "SELECT * FROM p_itemcodes WHERE item_number LIKE '404%'");
                    while ($row = mysqli_fetch_array($p_result)) {
                        $js .= '<TR>';
                        $js .= '<TD VALIGN=TOP STYLE=\\"padding-left:15px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">';
                        $js .= $row['item_name'];
                        $js .= '</TD>';
                        $js .= '<TD VALIGN=TOP ALIGN=RIGHT STYLE=\\"width:120px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">$';
                        $js .= $row['item_amount'];
                        $js .= '&nbsp;<SPAN STYLE=\\"color:#88AA88;font-size: 10px;\\">USD</SPAN></TD>';
                        $js .= '<TD VALIGN=TOP ALIGN=RIGHT STYLE=\\"width:120px;border-bottom:1px dashed #707080;color:#111155;font-size: 12px;\\">';
                        $code = paypal_button($Account->getID(),$row['item_number']);
                        if ($code !== false) {
                            $js .= str_replace('"','\\"',$code);
                        } else {
                            $js .= 'Unavalable';
                        }
                        $js .= '</TD>';
                        $js .= '</TR>';
                    }
                    $js .= '</TABLE>';
                    // End of Main Enclosure
                    $js .= "</DIV>&nbsp;</TD></TR></TABLE>\" + ";
                            
                    $js .= "\"</DIV>\";\n$jsmain";
                    
                    break;
                case 2130:   // Vocabulary Database
                    //$Debugger->logline("ibTag: $ib_tag");
                    // Determine if we are logged in first...
                    // Fetch 
                    // show the word dictionary
                    $js .= "fmrpcPayload = \"<DIV CLASS=dfc1>\" + ";
                    //--------------------------------------------------------------------
                    // Title and Languages
                    $js .= "\"<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=710>\" + ";
                    $js .= "\"<TR>\" + ";
                    $js .= "\"<TD><DIV CLASS=dfc1title>Vocabulary Database</DIV></TD>\" + ";
                    $js .= "\"<TD VALIGN=BOTTOM WIDTH=320><DIV CLASS=dfc1lang0><A HREF=# onClick=\\\"rpcFetch('act=set&vl=3&ibTag=2130');return false;\\\"><IMG SRC=images/2130-6.jpg ALT=German></A></DIV>\" + ";
                    $js .= "\"<DIV CLASS=dfc1lang0><A HREF=# onClick=\\\"rpcFetch('act=set&vl=4&ibTag=2130');return false;\\\"><IMG SRC=images/2130-4.jpg ALT=French></A></DIV>\" + ";
                    $js .= "\"<DIV CLASS=dfc1lang0><A HREF=# onClick=\\\"rpcFetch('act=set&vl=2&ibTag=2130');return false;\\\"><IMG SRC=images/2130-5.jpg ALT=Spanish></A></DIV></TD></TR>\" + ";
                    //--------------------------------------------------------------------
                    // Main enclosure for vocabulary
                    $js .= "\"<TR><TD COLSPAN=2><DIV CLASS=dfc1main STYLE=\\\"height:336px;\\\">\" + ";
                    
                    // Right Side Enclosure Container
                    $js .= "\"<DIV CLASS=dfc1encr>\" + ";
                    //####################################################################
                        //--------------------------------------------------------------------
                        // Search and Browse
                        // Determine if we are searching or Browsing
                        //$Debugger->logline("Vocabulary->preserve(): ".$Vocabulary->preserve() );
                        if ($Vocabulary->vSearchMode) {
                            // Titles
                            $js .= "\"<DIV CLASS=dfc1find1 onClick=\\\"rpcFetch('act=set&vs=s');\\\">Search</DIV>\" + ";
                            $js .= "\"<DIV CLASS=dfc1find1 STYLE=\\\"background-color:#CAD2FF;\\\" onClick=\\\"rpcFetch('act=set&vs=b');\\\">Browse</DIV>\" + ";
                            $js .= "\"<DIV CLASS=dfc1find0>&nbsp;</DIV>\" + ";
                            // Main Body
                            $js .= "\"<DIV CLASS=dfc1findsec>\" + ";
                            $js .= "\"<FORM NAME=search2130 METHOD=POST ACTION=\\\"#\\\" onSubmit=\\\"rpcFetch('act=set&vs=s','search2130'); return false;\\\">\" + ";
                            $js .= "\"<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 HEIGHT=42>\" + " ;
                            $js .= "\"<TR>\" + ";
                            $js .= "\"<TD STYLE=\\\"padding:2px 0px 0px 5px\\\"><INPUT CLASS=vocab TYPE=TEXT SIZE=25 NAME=vss VALUE=\\\"$vss\\\"></TD>\" + ";
                            $js .= "\"<TD STYLE=\\\"padding:2px 0px 0px 5px\\\"><IMG SRC=images/button4.gif onClick=\\\"rpcFetch('act=set&vs=s','search2130');\\\"></TD></TR>\" + ";
                            $js .= "\"<TR>\" + ";
                            $js .= "\"<TD COLSPAN=2 STYLE=\\\"padding:0px 0px 0px 5px\\\"><SPAN CLASS=dfc1small><A HREF=# onClick=\\\"rpcFetch('act=set&vs=s&vss=');return false;\\\">Clear Search</A></SPAN></TD>\" + ";
                            $js .= "\"</TR>\" + ";
                            $js .= "\"</FORM>\" + ";
                            $js .= "\"</TABLE>\" + ";
                            $js .= "\"</DIV>\" + ";
                        } else {
                            // Titles
                            $js .= "\"<DIV CLASS=dfc1find1 STYLE=\\\"background-color:#CAD2FF;\\\" onClick=\\\"rpcFetch('act=set&vs=s');\\\">Search</DIV>\" + ";
                            $js .= "\"<DIV CLASS=dfc1find1 onClick=\\\"rpcFetch('act=set&vs=b');\\\">Browse</DIV>\" + ";
                            $js .= "\"<DIV CLASS=dfc1find0>&nbsp;</DIV>\" + ";
                            // Main Body
                            $js .= "\"<DIV CLASS=dfc1findsec>\" + ";
                            $js .= "\"<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 HEIGHT=30 WIDTH=200 STYLE=\\\"margin:6px 0px 0px 8px;\\\">\" + " ;
                            $js .= "\"<TR>\" + "; 
                            $html2 = "";
                            for ($i=0;$i<13;$i++) {
                                $html2 .= "<TD ALIGN=CENTER CLASS=dfc1bl onClick=\\\"rpcFetch('act=set&vs=b&vb=".chr(65+$i)."');\\\" onMouseOver=\\\"fmHighlight(this,'#FFFF00');\\\" onMouseOut=\\\"fmUnHighlight(this,'transparent');\\\">".chr(65+$i)."</TD>";
                            }
                            $js .= "\"$html2</TR><TR>\" + "; 
                            $html2 = '';
                            for ($i=0;$i<13;$i++) {
                                $html2 .= "<TD ALIGN=CENTER CLASS=dfc1bl onClick=\\\"rpcFetch('act=set&vs=b&vb=".chr(78+$i)."');\\\" onMouseOver=\\\"fmHighlight(this,'#FFFF00');\\\" onMouseOut=\\\"fmUnHighlight(this,'transparent');\\\">".chr(78+$i)."</TD>";
                            }
                            $js .= "\"$html2</TR>\" + "; 
                            $js .= "\"</TABLE>\" + ";
                            $js .= "\"</DIV>\" + ";
                        }
                        //--------------------------------------------------------------------
                        // Right Side Quick link to add to lesson...
                        // If not registered, register. If not Purchased, Buy now. If else, Add to lesson.
                        $js .= "\"<BR><BR><DIV CLASS=dfc1find1 STYLE=\\\"width:160;\\\">Quick Links...</DIV>\" + ";
                        $js .= "\"<DIV CLASS=dfc1find0>&nbsp;</DIV>\" + ";
                        $js .= "\"<DIV CLASS=dfc1quicksec>\" + ";
                        // Show words from browsing or searching
                        $js .= "\"Comming Soon...\" + ";
                        
                        $js .= "\"</DIV>\" + ";
                    //####################################################################
                    $js .= "\"</DIV>\" + ";
                    
                    // Left Side Enclosure Container
                    $js .= "\"<DIV CLASS=dfc1encl>\" + \n";
                    //####################################################################
                    $js .= "\"" . fmlibVocabulary() . "\" + \n";
                    //####################################################################
                    $js .= "\"</DIV>\" + \n";
                    // End of Main Enclosure
                    $js .= "\"</DIV><BR>&nbsp;</TD></TR></TABLE>\" + ";
                            
                    $js .= "\"</DIV>\";\n$jsmain";
                    
                    break;
                case 2140: // Lesson Builder
                    //$Debugger->logline("ibTag: $ib_tag");
                    // Determine if we are logged in first...
                    // Fetch 
                    // show the word dictionary
                    $js .= "fmrpcPayload = \"<DIV CLASS=dfc1>\" + ";
                    //--------------------------------------------------------------------
                    // Title and Languages
                    $js .= "\"<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=710>\" + ";
                    $js .= "\"<TR>\" + ";
                    $js .= "\"<TD VALIGN=BOTTOM><DIV CLASS=dfc1title>Lesson Builder</DIV></TD>\" + ";
                    $js .= "\"</TR>\" + ";
                    //--------------------------------------------------------------------
                    // Main enclosure for vocabulary
                    $js .= "\"<TR><TD COLSPAN=2><DIV CLASS=dfc1indent>\" + ";
                    
                    $js .= "\"" . fmlibLesson($cnl_invalid) . "\" + ";
                    // End of Main Enclosure
                    $js .= "\"</DIV>&nbsp;</TD></TR></TABLE>\" + ";
                            
                    $js .= "\"</DIV>\";\n$jsmain";
                    
                    break;
                case 2150: // Run Lesson...
                    // Here goes...  (fmVSPayload)
                    $FlashCard->begin();
                    // Turn on the DIV for the flash cards...
                    // Use a Javascript function to enable this and set it's dimensions. ?
                    $js =  "vScreen = document.getElementById(\"fmVScreen\");\n";
                    $js .= "vScreen.style.display = \"block\";\n";
                    // Grab the content for the vScreen
                    $js .= $FlashCard->generateFlashScript($Lesson);
                    //$js .= "fmVSPayload = \"".$FlashCard->generateFlashCard($Lesson)."\";\n";
                    $js .= "initToolbar();\n";
                    //$js .= "dfcDelayTick({$FlashCard->lAudioLength});\n";
                    //$js .= "dfcDelayTick(4000);\n";
                    /*
                    $duration = $row['wDuration'] + (1000 * $l_delay);
                    echo "function doittoit() { ";
                    echo "  document.location.href = \"lesson.php?run=next\"; ";
                    echo "} ";
                    */
                    //$js .= $jsflash;
                    break;
                case 2151: // RPC For Running Lesson Tick()...
                    // Grab the content for the vScreen
                    if ($FlashCard->tick($Lesson)) {
                        // Still Running...
                        //$js .= "fmVSPayload = \"".$FlashCard->generateFlashCard($Lesson)."\";\n";
                        $js .= $FlashCard->generateFlashScript($Lesson);
                        //$js .= "dfcDelayTick({$FlashCard->lAudioLength});\n";
                        //$js .= "dfcDelayTick(4000);\n";
                        //$js .= $jsflash;
                    } else {
                        // Run Complete...
                        $js =  "vScreen = document.getElementById(\"fmVScreen\");\n";
                        $js .= "vScreen.style.display = \"none\";\n";
                        $js .= "closeToolbar();\n";
                        // Check for a redirect  location.replace("/$redirect");
                        $redir = $Session->get("/w2/n/adfc");
                        if (substr($redir,0,5) == 'redir') {
                            $location = substr($redir,6);
                            $js .= "location.replace(\"/$location\");\n";
                            $Session->post("/w2/n/adfc","none:");
                        }
                        // UnInit the Toolbar
                    }
                    break;
                case 2152: // RPC For Exit Button
                    // Run Complete...
                    $js =  "vScreen = document.getElementById(\"fmVScreen\");\n";
                    $js .= "vScreen.style.display = \"none\";\n";
                    $js .= "closeToolbar();\n";
                    // Check for a redirect  location.replace("/$redirect");
                    $redir = $Session->get("/w2/n/adfc");
                    if (substr($redir,0,5) == 'redir') {
                        $location = substr($redir,6);
                        $js .= "location.replace(\"/$location\");\n";
                        $Session->post("/w2/n/adfc","none:");
                    }
                    // UnInit the Toolbar
                    break;
            }
        }
        //--- Admin ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0200) {  // Admin
            switch ($ib_tag) {
                case 5100:
                    //$Debugger->logline("ibTag: $ib_tag");
                    $js .= "fmrpcPayload = \"<H2><B><U>Screen Properties</U></B></H2>\" + 
                            screen.width + \"x\" + screen.height + \" - \" + 
                            screen.availWidth + \"x\" + screen.availHeight + \"<BR CLEAR=ALL><BR CLEAR=ALL>\" +
                            \"<H2><B><U>Window Properties</U></B></H2>\" + 
                            window.outerWidth + \"x\" + window.outerHeight + \" - \" + 
                            window.innerWidth + \"x\" + window.innerHeight + \"<BR CLEAR=ALL><BR CLEAR=ALL>\"
                            ;\n$jsmain";
                    break;
                case 5110:  // Web Content Editor
                    //$Debugger->logline("ibTag: $ib_tag");
                    $fTag = $_REQUEST['fTag'];
                    $fSelect = $_REQUEST['fSelect'];
                    $fData = $_REQUEST['fData'];
                    $js .= "// fTag = $fTag\n";
                    $js .= "// fSelect = $fSelect\n";
                    $js .= "// fData = $fData\n";
                    if ($fSelect == "none") {
                        // Posting
                        $Session->post('/web/main/admin/edit',$fTag);
                        //Check to see if it exists
                        $result = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/html' AND wItem='$fTag'");
                        if (mysqli_num_rows($result) > 0) {
                            $sql = "UPDATE WebContent SET wPage='$current_page', wContent='$fData' WHERE wType='text/html' AND wItem='$fTag'";
                        } else {
                            $sql = "INSERT INTO WebContent SET wPage='$current_page', wContent='$fData', wType='text/html', wItem='$fTag'";
                        }
                        $js .= "// SQL = $sql\n";
                        $js .= "// SQLerr = ".((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))."\n";
                        //@mysql_free_result($result);
                        mysqli_query($db_link, $sql);
                    } else {
                        // Selecting
                        $Session->post('/web/main/admin/edit',$fTag);
                        $fData = "";
                        $fTag = "";
                        $result = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/html' AND wItem='$fSelect'");
                        if (mysqli_num_rows($result) > 0) {
                            $row = mysqli_fetch_array($result);
                            $fData = $row['wContent'];
                            $fTag = $row['wItem'];
                        }
                        
                    }
                    // What was the last item you edited? DUH...
                    // Dropdown selector for existing tags..
                    $js .= 'fmrpcPayload = "<H2><B><U>Web Content Editor</U></B></H2>" + 
                            "<FORM NAME=adminWebEdit>" +
                            "<INPUT CLASS=fade1 NAME=fTag TYPE=TEXT SIZE=6 VALUE=\"'.$fTag.'\">" + 
                            "<SELECT CLASS=fade1 NAME=fSelect onChange=\"rpcPost(\'act=admin&ibTag=5110\');\">" +
                            "<OPTION VALUE=none>---</OPTION>" +
                            ';
                            
                    $result = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/html'");
                    while ($row = mysqli_fetch_array($result)) {
                        $js .= "\"<OPTION VALUE={$row['wItem']}>{$row['wItem']}</OPTION>\" +";
                    }
                    $fData = str_replace('"','&quot;',$fData);
                    $ar = explode("\n",$fData);
                    $fDataOut = '';
                    foreach($ar as $line) {
                        $fDataOut .= '"' . $line . '" + \'\n\' + ';
                    }
                    $fDataOut = str_replace("\n",'\n',$fData);
                    $js .=  '"</SELECT><BR>" + 
                            "<TEXTAREA CLASS=fade1 STYLE=\"overflow-x:scroll;\" NAME=fData ROWS=18 COLS=100>" +
                            "'.$fDataOut.'" + 
                            "</TEXTAREA><BR>" +
                            "<INPUT TYPE=Button VALUE=Post onClick=\"rpcPost(\'act=admin&ibTag=5110\');\">" +
                            "</FORM>"
                            ;'."\n$jsmain";
                    break;
                        
                //$js .= "alert(fmrpcPayload);\n";
            }
        }
        //--- HTML Block ----------------------------------------------------------------------------------------------------------------
        if ($naction == 0x0020) {  // HTML
            //$Debugger->logline("ibAct: $ib_act");
            $output = "<FONT COLOR=red>No Data</FONT>";
            // 
            // +----------------------------------------------------------------------
            // | Load from the database the HTML needed...
            // +----------------------------------------------------------------------
            $result = mysqli_query($db_link, "SELECT * FROM WebContent WHERE wPage='$current_page' AND wType='text/html' AND wItem='$ib_tag'");
            // If no content.. do domething trick...
            while ($row = mysqli_fetch_array($result)) {
                
                $output = str_replace('"','&quot;',$row['wContent']);
                $ar = explode("\n",$output);
                $fDataOut = '';
                foreach($ar as $line) {
                    $fDataOut .= '"' . $line . '" + \'\n\' + ';
                }
                $fDataOut = str_replace("\n",'\n',$fDataOut);
                $fDataOut .= '"";';
            }
            if ($fDataOut == "") { $fDataOut = '""'; }
            $js .= 'fmrpcPayload = '.$fDataOut.";\n$jsmain" ; 
            
        }
    
    // Set the main content area for this rpc

    /*
            if (obj.innerHTML) {
                obj.innerHTML = fmrpcPayload;
            } else {
                obj.firstChild.data = fmrpcPayload;
            }
    
    */
    
// +----------------------------------------------------------------------
// | Save Session Information
// +----------------------------------------------------------------------
    $Session->post("/w2/v=".$Vocabulary->preserve());
    $Session->post("/w2/l=".$Lesson->preserve());
    $Session->post("/w2/f=".$FlashCard->preserve());
    $Session->save();
    //$Debugger->savelog('debug.log');
    
// +----------------------------------------------------------------------
// | Send the stuff
// +----------------------------------------------------------------------
    echo $js;

?>
