<?php
/***************************************************************************
 *   Copyright (C) 2004 by 5Muses Software LLC                                   *
 *   info@5muses.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//
    include_once("db.php");
    include_once("session.php");
    include_once("prg_account.php");
    include_once("prg_lesson.php");
    include_once("prg_vocabulary.php");
    include_once("main_lib.php");
    

// +----------------------------------------------------------------------
// | Create and validate the current session
// +----------------------------------------------------------------------
    $Session = new fmSession;
    $Timer->start('session');
    $Session->init('FMC_FlashCard');
    $Timer->stop('session');

// +----------------------------------------------------------------------
// | Initiate the Account Object
// +----------------------------------------------------------------------
    $Account = new fmAccount;
    $Account->init($Session->getID());

// +----------------------------------------------------------------------
// | Initiate the Vocabulary Object
// +----------------------------------------------------------------------
    $Vocabulary = new fmPrgVocabulary;
    if (strlen($Session->get("/w2/v")) > 1)
    $Vocabulary->restore($Session->get("/w2/v"));
    
// +----------------------------------------------------------------------
// | Initiate the Lesson Object
// +----------------------------------------------------------------------
    $Lesson = new fmPrgLesson;
    // Check to see if the account is valid or it's just a guest....
    $acctid = $Account->getID();
    $lid = ($acctid == 'none') ? $Session->getID() : $acctid ;
    $lflag = ($acctid == 'none') ? 1 : 0 ;  // 1 guest, 0 logged in account.
    $Lesson->init($lid,$lflag);
    $Lesson->restore($Session->get("/w2/l"));
    // Load the last used lesson...
    $Lesson->load("[Current]");
    //$lID = $Session->get("/w2/l/id=".intval($lID));

// +----------------------------------------------------------------------
// | Setup the Inline CSS
// +----------------------------------------------------------------------
    $css = '<STYLE TYPE="text/css">'."\n";
    // Pull CSS From Here...Output...
    $result = mysqli_query($db_link, "SELECT * FROM 5mc_dfc.WebContent WHERE wPage='main.php' AND wType='text/css'");
    while ($row = mysqli_fetch_array($result)) {
        $output = strtr($row['wContent'],"\x09\x0A\x0D",'!!!');
        $output = str_replace('        ','',$output);
        $output = str_replace('!','',$output);
        $css .= $output."\n";
    }
    $css .= '</STYLE>'."\n";

    if (isset($_REQUEST['v_vSearch'])) { $Vocabulary->vSearch = $_REQUEST['v_vSearch']; }
    if (isset($_REQUEST['v_vBrowse'])) { $Vocabulary->vBrowse = $_REQUEST['v_vBrowse']; }
    $_REQUEST['v_vSearchMode'] == "true" ? $Vocabulary->vSearchMode = true : $Vocabulary->vSearchMode = false;
    $_REQUEST['v_vFilter'] == "true" ? $Vocabulary->vFilter = true : $Vocabulary->vFilter = false;
    if (isset($_REQUEST['v_vLanguage'])) { $Vocabulary->vLanguage = $_REQUEST['v_vLanguage']; }
    if (isset($_REQUEST['v_vCount'])) { $Vocabulary->vCount = $_REQUEST['v_vCount']; }
    if (isset($_REQUEST['v_vPage'])) { $Vocabulary->vPage = $_REQUEST['v_vPage']; }
    if (isset($_REQUEST['v_vPageCount'])) { $Vocabulary->vPageCount = $_REQUEST['v_vPageCount']; }
    if (isset($_REQUEST['v_vTotal'])) { $Vocabulary->vTotal = $_REQUEST['v_vTotal']; }
    $poop = $Vocabulary->fetch_words();
    
    function d_makeTable($array,$count = false) {
        $html = "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 STYLE=\"margin:4px;border:1px solid black;\">";
        $html .= "<TR>";
        if ($count) { $html .= "<TD STYLE=\"color:#6080A0;pading:1px 4px 1px 4px;background-color: #B0D0FF;border-right:1px solid #406080;\" ALIGN=RIGHT>&nbsp;::&nbsp;</TD>"; }
        foreach ($array[0] as $name => $value) {
            $html .= "<TD STYLE=\"color:#6080A0;pading:1px 4px 1px 4px;border-bottom:1px solid #808080;border-right:1px solid #406080;background-color: #B0D0FF;\" ALIGN=RIGHT>&nbsp;$name&nbsp;</TD>";
        }
        $html .= "</TR>";
        $alt = true;
        $c = 0;
        foreach ($array as $row) {
            $alt = !$alt;
            $html .= "<TR>";
            if ($count) {
                $html .= "<TD STYLE=\"color:#6080A0;pading:1px 4px 1px 4px;background-color: #B0D0FF;border-right:1px solid #406080;\" ALIGN=RIGHT>&nbsp;$c&nbsp;</TD>"; 
            }
            foreach ($row as $cell) {
                if ($alt) { 
                    $html .= "<TD STYLE=\"pading:1px 4px 1px 4px;background-color: #E0F0FF;border-right:1px solid #C0C0C0;\" ALIGN=RIGHT>&nbsp;$cell&nbsp;</TD>"; 
                } else {
                    $html .= "<TD STYLE=\"pading:1px 4px 1px 4px;background-color: #FFFFFF;border-right:1px solid #C0C0C0;\" ALIGN=RIGHT>&nbsp;$cell&nbsp;</TD>"; 
                }
            }
            $html .= "</TR>";
            $c++;
        }
    $html .= "</TABLE>";
    return $html;
    }

echo "<HTML><HEAD><TITLE>Debugger</TITLE></HEAD><BODY>\n";
echo $css;
echo "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>";

echo "<TR>";
echo "<TD BGCOLOR=#E0E0E0>Session</TD>";
echo "<TD>";
echo "</TD>";
echo "</TR>";

echo "<TR><TD COLSPAN=2 HEIGHT=2 BGCOLOR=#000000></TD></TR>";

echo "<TR>";
echo "<TD BGCOLOR=#E0E0E0>Account</TD>";
echo "<TD>";
    echo "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>";
    echo "<TR><TD>_ContactInfo</TD><TD><INPUT SIZE=10 NAME=v_vBrowse VALUE=\"".     print_r($Account->_ContactInfo,true)      ."\"></TD></TR>";
    echo "<TR><TD>_Tracking</TD><TD><INPUT SIZE=10 NAME=v_vSearchMode VALUE=\"".    print_r($Account->_Tracking,true)   ."\"></TD></TR>";
    echo "<TR><TD>_Visits</TD><TD><INPUT SIZE=5 NAME=v_vFilter VALUE=\"".          print_r($Account->_Visits,true)      ."\"></TD></TR>";
    echo "<TR><TD>_LastVisit</TD><TD><INPUT SIZE=10 NAME=v_vLanguage VALUE=\"".     print_r($Account->_LastVisit,true)    ."\"></TD></TR>";
    echo "<TR><TD>_Created</TD><TD><INPUT SIZE=10 NAME=v_vIndex VALUE=\"".          print_r($Account->_Created,true)       ."\"></TD></TR>";
    echo "<TR><TD>_Session</TD><TD><INPUT SIZE=40 NAME=v_vSQLError VALUE=\"".       print_r($Account->_Session,true)    ."\"></TD></TR>";
    echo "<TR><TD>_Guest</TD><TD><INPUT SIZE=10 NAME=v_vSQLCount VALUE=\"".         print_r($Account->_Guest,true)    ."\"></TD></TR>";
    echo "<TR><TD>_Access</TD><TD><INPUT SIZE=40 NAME=v_vSQLTotal VALUE=\"".        print_r($Account->_Access,true)    ."\"></TD></TR>";
    echo "<TR><TD>_AudioSettings</TD><TD><INPUT SIZE=7 NAME=v_vSQLPage VALUE=\"".  print_r($Account->_AudioSettings,true)    ."\"></TD></TR>";
    echo "<TR><TD>_Account</TD><TD><INPUT SIZE=40 NAME=v_vSQLPageCount VALUE=\"".   print_r($Account->_Account,true)    ."\"></TD></TR>";
    echo "</TABLE>";
echo "</TD>";
echo "</TR>";

/*
    var $_words        = null;                // fmList of words in the lesson
    var $_lessons      = null;                // List of Saved Lessons
    var $_name         = "New Lesson";        // Lesson Name
    var $_account_id   = null;                // Client's Account ID (needed for SQL Queries)
    var $_session_id   = null;                // Client's Session ID (needed for SQL Queries)
    var $_pos          = 1;                   // Position in the list of words
    var $_repeat       = 0;                   // How many times to repeat the lesson
    var $_language     = 2;                   // Language of the lesson
*/
echo "<TR><TD COLSPAN=2 HEIGHT=2 BGCOLOR=#000000></TD></TR>";

echo "<TR>";
echo "<TD BGCOLOR=#E0E0E0>Lesson</TD>";
echo "<TD>";
    echo "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>";
    echo "<TR><TD>_words</TD><TD><PRE>".      print_r($Lesson->rds,true)        ."</PRE></TD></TR>";
    echo "<TR><TD>_lessons</TD><TD><PRE>".    print_r($Lesson->_lessons,true)   ."</PRE></TD></TR>";
    echo "<TR><TD>_name</TD><TD><INPUT SIZE=30 NAME=v_vFilter VALUE=\"".          print_r($Lesson->_name,true)        ."\"></TD></TR>";
    echo "<TR><TD>_account_id</TD><TD><INPUT SIZE=40 NAME=v_vLanguage VALUE=\"".  print_r($Lesson->_account_id,true)  ."\"></TD></TR>";
    echo "<TR><TD>_session_id</TD><TD><INPUT SIZE=40 NAME=v_vIndex VALUE=\"".     print_r($Lesson->_session_id,true)  ."\"></TD></TR>";
    echo "<TR><TD>_pos</TD><TD><INPUT SIZE=4 NAME=v_vSQLError VALUE=\"".          print_r($Lesson->_pos,true)         ."\"></TD></TR>";
    echo "<TR><TD>_repeat</TD><TD><INPUT SIZE=4 NAME=v_vSQLCount VALUE=\"".       print_r($Lesson->_repeat,true)      ."\"></TD></TR>";
    echo "<TR><TD>_language</TD><TD><INPUT SIZE=4 NAME=v_vSQLTotal VALUE=\"".     print_r($Lesson->_language,true)    ."\"></TD></TR>";
    echo "</TABLE>";
echo "</TD>";
echo "</TR>";

echo "<TR><TD COLSPAN=2 HEIGHT=2 BGCOLOR=#000000></TD></TR>";

echo "<TR>";
echo "<TD BGCOLOR=#E0E0E0>Paypal</TD>";
echo "<TD>";
echo "<FORM NAME=pipn ACTION=pipn5m_0_1.php METHOD=POST>";
echo "<INPUT TYPE=TEXT SIZE=25 NAME=txn_type VALUE=web_accept><BR>";
echo '<INPUT TYPE=TEXT SIZE=25 NAME=payment_date VALUE="16:09:26 Feb 28, 2005 PST"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=last_name VALUE="Holland"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=item_name VALUE="Spanish Dynamic Fast Cards - 1 Month Access"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=payment_gross VALUE="9.95"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=mc_currency VALUE="USD"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=business VALUE="testpaypal@5muses.org"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=payment_type VALUE="instant"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=payer_status VALUE="unverified"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=verify_sign VALUE="AGcS7BAAfh3hgM8zLQ6ic4smZQ3cA.uhLL3tOxXlXezSsrBhuGabWuc1"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=test_ipn VALUE="1"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=payer_email VALUE="anheuser50@yahoo.com"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=tax VALUE="0.00"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=txn_id VALUE="6YE719769R2787546"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=receiver_email VALUE="testpaypal@5muses.org"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=quantity VALUE="1"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=first_name VALUE="Chris"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=payer_id VALUE="LS9Z5C5M7VAH2"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=receiver_id VALUE="HFDEQ63TU88F6"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=item_number VALUE="402001"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=payment_status VALUE="Completed"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=payment_fee VALUE="0.59"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=mc_fee VALUE="0.59"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=mc_gross VALUE="9.95"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=custom VALUE="629d7d6284e66e37d5599a5281b8069d6b935123"><BR>';
echo '<INPUT TYPE=TEXT SIZE=25 NAME=notify_version VALUE="1.6"><BR>';
echo '<INPUT TYPE=SUBMIT VALUE=Process>';
echo '</FORM>';
echo "</TD>";
echo "</TR>";

echo "<TR><TD COLSPAN=2 HEIGHT=2 BGCOLOR=#000000></TD></TR>";

echo "<TR>";
echo "<TD BGCOLOR=#E0E0E0>Vocabulary</TD>";
echo "<TD>";
echo "<FORM NAME=vocab ACTION=debugger.php METHOD=POST>";
echo "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>";
$filter = $Vocabulary->vFilter ? "true" : "false";
$mode = $Vocabulary->vSearchMode ? "true" : "false";
echo "<TR><TD>vSearch</TD><TD><INPUT SIZE=10 NAME=v_vSearch VALUE=\"".     print_r($Vocabulary->vSearch,true)      ."\"></TD></TR>";
echo "<TR><TD>vBrowse</TD><TD><INPUT SIZE=10 NAME=v_vBrowse VALUE=\"".     print_r($Vocabulary->vBrowse,true)      ."\"></TD></TR>";
echo "<TR><TD>vSearchMode</TD><TD><INPUT SIZE=10 NAME=v_vSearchMode VALUE=\"$mode\"></TD></TR>";
echo "<TR><TD>vFilter</TD><TD><INPUT SIZE=10 NAME=v_vFilter VALUE=\"".     $filter      ."\"></TD></TR>";
echo "<TR><TD>vLanguage</TD><TD><INPUT SIZE=10 NAME=v_vLanguage VALUE=\"".   print_r($Vocabulary->vLanguage,true)    ."\"></TD></TR>";
echo "<TR><TD>vIndex</TD><TD><INPUT SIZE=10 NAME=v_vIndex VALUE=\"".      print_r($Vocabulary->vIndex,true)       ."\"></TD></TR>";
echo "<TR><TD>vSQLError</TD><TD><INPUT SIZE=10 NAME=v_vSQLError VALUE=\"".   print_r($Vocabulary->vSQLError,true)    ."\"></TD></TR>";
echo "<TR><TD>vSQLCount</TD><TD><INPUT SIZE=10 NAME=v_vSQLCount VALUE=\"".   print_r($Vocabulary->vSQLCount,true)    ."\"></TD></TR>";
echo "<TR><TD>vSQLTotal</TD><TD><INPUT SIZE=10 NAME=v_vSQLTotal VALUE=\"".   print_r($Vocabulary->vSQLTotal,true)    ."\"></TD></TR>";
echo "<TR><TD>vSQLPage</TD><TD><INPUT SIZE=10 NAME=v_vSQLPage VALUE=\"".   print_r($Vocabulary->vSQLPage,true)    ."\"></TD></TR>";
echo "<TR><TD>vSQLPageCount</TD><TD><INPUT SIZE=10 NAME=v_vSQLPageCount VALUE=\"".   print_r($Vocabulary->vSQLPageCount,true)    ."\"></TD></TR>";
echo "<TR><TD>vSearchAction</TD><TD><INPUT SIZE=10 NAME=v_vSearchAction VALUE=\"".   print_r($Vocabulary->vSearchAction,true)    ."\"></TD></TR>";
echo "<TR><TD COLSPAN=2><INPUT TYPE=SUBMIT VALUE=\"Refresh Changes\"></TD></TR>";
echo "<TR><TD COLSPAN=2 HEIGHT=2 BGCOLOR=#000000></TD></TR>";
echo "</TABLE>";
echo "</FORM>";
echo $Vocabulary->preserve() . "<BR>";
echo "\$Vocabulary->fetch_words();<HR>";
echo d_makeTable($poop,true);
echo "<HR>";
echo $Vocabulary->preserve() . "<BR>";
echo fmlibVocabulary();
echo "<HR>";
echo $Vocabulary->preserve();
echo "</TD>";
echo "</TR>";

echo "</TABLE>";
 
 
    $Session->post("/w2/v=".$Vocabulary->preserve());
    $Session->save();

 
?>