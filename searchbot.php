<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//


class searchBot {

    //------------------------------------------------------------
    //  Variable Declarations
    //------------------------------------------------------------
    
    //------------------------------------------------------------
    //  Misc Functions
    //------------------------------------------------------------
    function hexdump($data) {
        $index = 0;
        while (strlen($data) > 0 ) {
            $chunk = substr($data,0,16);
            $data = substr($data,16);
            if (strlen($data) > 0) {
                $h = dechex($index);
                if (strlen($h) == 1) { $h = "000$h"; }
                if (strlen($h) == 2) { $h = "00$h"; }
                if (strlen($h) == 3) { $h = "0$h"; }
                $res .= "$h: ";
                for ($i=0;$i<16;$i++) {
                    $h = dechex(ord(substr($chunk,$i,1)));
                    if (strlen($h) == 1) { $h = "0$h"; }
                    $res .= "$h ";
                }
                $res .= " | ";
                for ($i=0;$i<16;$i++) {
                    $ch = substr($chunk,$i,1);
                    if ((ord($ch) >= 32) and (ord($ch) <= 126)) {
                        $res .= $ch;
                    } else {
                        $res .= ".";
                    }
                }
                $index = $index + 16;
            }
            $res .= "\n";
        }
        return $res;
    }
    
    function fetch($url,$post=false,$cookie=false,$referer=false) {
        // This will go out and fetch a URL returning the HTML
        // Format MUST be http://domain/(optional page and arguments)
        preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$url,$matches);
        $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[1]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $args = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[2]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
            
        /*
            GET / HTTP/1.1
            Host: www.5muses.com
            User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050621 Firefox/1.0.4
            Connection: close        
        */
        
        $buffer = '';
        $fp = fsockopen("$domain", 80, $errno, $errstr);
        if ($fp !== false) {
            // Got connected... let's continue
            if ($post === false) {
                $request = "GET /$args  HTTP/1.0\r\n";
            } else {
                $request = "POST /$args  HTTP/1.0\r\n";
                $request .= "Content-length: ".strlen($post)."\r\n";
            }
            $request .= "Host: $domain\r\n";
            $request .= "User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050621 Firefox/1.0.4\r\n";
            $request .= "Connection: close\r\n";
            $request .= "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n";
            $request .= "Accept-Language: en-us,en;q=0.5\r\n";
            $request .= "Accept-Encoding: gzip,deflate\r\n";
            $request .= "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n";
            
            if ($referer !== false) {
                $request .= "Referer: $referer\r\n";
            }
            if ($cookie !== false) {
                $request .= "Cookie: $cookie\r\n";
            }
            $request .= "\r\n";
            //echo "<PRE>".print_r($request,true)."</PRE>";
            fputs($fp, $request);
            if ($post !== false) {
                // Send any form POST data if neccesary
                fwrite($fp, $post);
            }
            // Read back the responce
            while (!feof($fp)) {
                $buffer .= fgets($fp, 128);
            }
            fclose($fp);  
            
            // Split the Headers from the Content
            $sep = strpos($buffer,"\r\n\r\n");
            $header = substr($buffer,0,$sep);
            $html = substr($buffer,$sep+4);
            if (strpos($header,'Content-Encoding: gzip') !== false) {
                $html = gzinflate(substr($html,10));
            }
            $res = array('headers' => "$header", 'html' => "$html");
            return $res;      
            //return $buffer;      
        } else {
            // Error... site unreachable?
            return false;
        }
    }
    
    function sweepYahoo($html) {
        // This pulls the info out of a Yahoo result page
        
        /*
        (The search Results, and counter)
            <div id=yschinfo><h1>Search Results</h1><p>Results <strong>1 - 100</strong> of about <strong>16,100,000</strong> for <strong>
        
        (The begining of the search results)
            <div id=yschweb><div class=yschhd><h2>WEB RESULTS</h2></div><ol start=1>
        
        (The first search result)
            <li><div><a class=yschttl id="http://www.studyspanish.com/" href="http://rds.yahoo.com/S=2766679/K=learn+spanish/v=2/SID=e/TID=F561_72/l=WS1/R=1/SS=8121031/MI=free/IPC=us/SHE=0/H=0/;_ylt=AhKsa00twqKmQMd2673zJIFXNyoA/SIG=1771sh6n1/EXP=1119183443/*-http%3A//rdre1.yahoo.com/click?u=http://www.studyspanish.com/&y=024853745C51C33F&i=482&c=9107&q=02%5ESSHPM%5BL7sz~mq%3Flo~qvlw6&e=utf-8&r=0&d=wow~F561-en-us&n=E9J45H75NQPK40D4&s=15&t=&m=42B410D2&x=0118F94D48EAA80F">
            <b>Learn</b> <b>Spanish</b></a> 

        (New Window Icon)
            <a href="http://rds.yahoo.com/S=2766679/K=learn+spanish/v=2/SID=e/TID=F561_72/l=WS1/R=1/SS=8121031/MI=free/IPC=us/SHE=0/H=0/NW=1/;_ylt=AlcIwgTNVzfuv9r_Tl5UN2RXNyoA/SIG=1771sh6n1/EXP=1119183443/*-http%3A//rdre1.yahoo.com/click?u=http://www.studyspanish.com/&y=024853745C51C33F&i=482&c=9107&q=02%5ESSHPM%5BL7sz~mq%3Flo~qvlw6&e=utf-8&r=0&d=wow~F561-en-us&n=E9J45H75NQPK40D4&s=15&t=&m=42B410D2&x=0118F94D48EAA80F" target=_blank><img src="http://us.i1.yimg.com/us.yimg.com/i/us/sch/bn/nw2.gif" height=11 width=11 border=0 alt="Open this result in new window"></a>
            
            </div>
            <div class=yschabstr> <b>Learn Spanish Learn Spanish</b> This is a commercial site but it also offers many free online resources. Included are cultural notes; an online tutorial; and vocabulary with accompanying games such as flash cards, word searches, matching, and ...</div>
            <div> Category: <a href="http://rds.yahoo.com/S=2766679/K=learn+spanish/v=2/SID=e/TID=F561_72/l=WS2/R=1/CS=2005433/;_ylt=Akph.Hjzx7zVUv3NV7tovjhXNyoA/SIG=14q75vrjm/EXP=1119183443/*-http%3A//dir.yahoo.com/Social_Science/Linguistics_and_Human_Languages/Languages/Specific_Languages/Spanish/Lessons_and_Tutorials_Online/"><b>Spanish</b> Language &gt; Online Lessons</a></div>
        
        (The URL of the site)
            <em class=yschurl>www.study<b>spanish</b>.com</em>
        

        (More from this site)
            - <a href="http://rds.yahoo.com/S=2766679/K=learn+spanish/v=2/SID=e/TID=F561_72/l=WS3/R=1/;_ylt=Ak2tcb2P3gUHVZ746wBnUe1XNyoA/SIG=14natn8d3/EXP=1119183443/*-http%3A//search.yahoo.com/search?p=learn+spanish&_adv_prop=web&ei=UTF-8&vst=0&vm=i&fl=0&n=100&fr=fp-top&vst=0&vs=www.studyspanish.com">More from this site</a>
        (SAVE) 
            <span class=yschprs><em> - <a href="http://myweb.search.yahoo.com/myresults/insertresult?myfr=srp&.done=http%3A%2F%2Fsearch.yahoo.com%2Fsearch%3F_adv_prop%3Dweb%26ei%3DUTF-8%26va%3Dlearn%2Bspanish%26va_vt%3Dany%26vd%3Dall%26vst%3D0%26vf%3Dall%26vm%3Di%26fl%3D0%26n%3D100&u=http%3A%2F%2Fwww.studyspanish.com%2F&p=learn+spanish&ei=UTF-8&.scrumb=&arch=1">Save</a> 
        (Block) 
            - <a href="http://myweb.search.yahoo.com/myresults/block?myfr=srp&url=http%3A%2F%2Fwww.studyspanish.com%2F&domain=1&.done=http%3A%2F%2Fsearch.yahoo.com%2Fsearch%3F_adv_prop%3Dweb%26ei%3DUTF-8%26va%3Dlearn%2Bspanish%26va_vt%3Dany%26vd%3Dall%26vst%3D0%26vf%3Dall%26vm%3Di%26fl%3D0%26n%3D100&js=0&.scrumb=">Block</a></em></span>
        
        (Next Page)
            <big><b><a href="http://rds.yahoo.com/S=2766679/K=learn+spanish/v=2/SID=e/TID=F561_72/l=WPGN/R=next/;_ylt=AqOcUU6_M_dmVYkic9DRJM5XNyoA/SIG=1a31e5c8o/EXP=1119183443/*-http%3A//search.yahoo.com/search?_adv_prop=web&ei=UTF-8&va=learn+spanish&va_vt=any&vst=0&vf=all&vm=i&fl=0&n=100&xargs=12KPjg1hVStoGmmvmnEOOIMLrcmUsOkZ7Fo5h7DOV5CtdYjC0%2DALwDEsBlqfEmEK5y7XrRpcCOYZdU8Mbh%2DMuSij68MBvYSqdX6z3xzMMKLtOR6ez7tai491Lc0rl%5Ffi5YOTKPPeSlKt7%5FcYXsPApL378z&pstart=6&fr=fp-top&b=101">Next</a></b></big>
        
        ----------------
        Grab all the results by <ol start=???></ol>
        
        */  
        $results = array();
        $results["results"] = array();
        // Fetch the Counts
        preg_match('/<h1>Search Results<\/h1><p>Results <strong>([\d]+) - ([\d]+)<\/strong> of about <strong>([\d,]+)<\/strong> for/is',$html,$result_matches);
        $results['from'] = intval($result_matches[1]);
        $results['to'] = intval($result_matches[2]);
        $results['count'] = intval(str_replace(',','',$result_matches[3]));
        unset($result_matches);
        
        // Fetch the Next Page link
        //preg_match('<big><b><a href="http://rds.yahoo.com/S=2766679/K=learn+spanish/v=2/SID=e/TID=F561_72/l=WPGN/R=next/;_ylt=AqOcUU6_M_dmVYkic9DRJM5XNyoA/SIG=1a31e5c8o/EXP=1119183443/*-http%3A//search.yahoo.com/search?_adv_prop=web&ei=UTF-8&va=learn+spanish&va_vt=any&vst=0&vf=all&vm=i&fl=0&n=100&xargs=12KPjg1hVStoGmmvmnEOOIMLrcmUsOkZ7Fo5h7DOV5CtdYjC0%2DALwDEsBlqfEmEK5y7XrRpcCOYZdU8Mbh%2DMuSij68MBvYSqdX6z3xzMMKLtOR6ez7tai491Lc0rl%5Ffi5YOTKPPeSlKt7%5FcYXsPApL378z&pstart=6&fr=fp-top&b=101">Next</a></b></big>',$html,$next_matches);
        
        // Get the chunk of results
        preg_match('/<ol start=([\d]+)(.*?)<\/ol>/is',$html,$entry_matches);
        $results['start'] = intval($entry_matches[1]);
        $chunk_results = $entry_matches[2];
        unset($entry_matches);
        
        // Split the results into an array
        $array_results = explode('<li>',$chunk_results);
        unset($cunk_results);
        
        // Operate on the array to sweep the data from the results
        for ($i=1;$i<count($array_results);$i++) {
            $entry = $array_results[$i];
            preg_match('/<em class=yschurl>(.*?)<\/em>/si',$entry,$entry_matches);
            $host = strip_tags($entry_matches[1]);
            unset($entry_matches);
            preg_match('/\/\*-http%3A\/\/(.*?)">(.*?)<\/a>/si',$entry,$url_matches);
            $url = strip_tags($url_matches[1]);
            $title = strip_tags($url_matches[2]);
            unset($url_matches);
            preg_match('/<div class=yschabstr>(.*?)<\/div>/si',$entry,$desc_matches);
            $desc = strip_tags($desc_matches[1]);
            unset($desc_matches);
            $result = array();
            
            $result['site_title'] = $title;
            $result['site_url'] = $url;
            $result['site_desc'] = $desc;
            $result['site_host'] = $host;
            $rank = $i + $results['start'] - 1;
            $result['rank'] = $rank;
            $results["results"][] = $result;
        }
        
        //$results['data'] = $array_results;
        
        return $results;
    }

    function sweepMSN ($html) {
        //<div id="results" class="flank"><h2>Results</h2><ul>
        preg_match('@<h5>Page ([\d]*) of ([\d,]*) results containing@is',$html,$matches);
        $page = intval($matches[1]);
        
        $st = strpos($html,'<div id="results" class="flank"><h2>Results</h2><ul>');
        $chunk = substr($html,$st);
        $ed = strpos($chunk,'<div id="ads_rightC" class=""><h2>SPONSORED SITES</h2><ul>');
        $chunk = substr($chunk,0,$ed);
        
        //$array_results = explode('<h3>',$chunk_results);
        $count = preg_match_all('@<h3><a href="(http://[^"]*)">(.*?)</a></h3><p>(.*?)</p><ul><li[\w=" ]*>(.*?)</li>@is',$chunk,$matches);
        $results = array();
        $results["results"] = array();
        
        $results['from'] = ($page - 1) * 10;
        $results['to'] = $results['from'] + 9;
        $results['count'] = $count;
        for ($i=0;$i<10;$i++) {
            $result = array();
            
            $result['site_title'] = strip_tags($matches[2][$i]);
            $result['site_url'] = $matches[1][$i];
            $result['site_desc'] = strip_tags($matches[3][$i]);
            $rank = $i + (($page - 1) * 10) + 1;
            $result['rank'] = $rank;
            $results["results"][] = $result;
        }
        return $results;
    }
        
    function googleAPI ($keyword,$start=0) {
    
        $parameters = array('key'         => 'xiYpme1QFHJYDBpJtH1LP9lYVp8Wcf9M',
                            'q'           => $keyword,
                            'start'       => $start,
                            'filter'      => 'true',
                            'maxResults'  => 10);
        require_once('nusoap.php');
    
        $soap = new SoapClient('http://api.google.com/GoogleSearch.wsdl', 'wsdl');
        $fp = fopen('google_api.txt','ab');
        $google = $soap->call('doGoogleSearch', $parameters);    
        // if error, sleep 5 secs then try again
        if ($google === false) { 
            sleep(5); 
            fputs($fp,"Communication Error - Try 2.\n");
            $google = $soap->call('doGoogleSearch', $parameters); 
        }
        // if error, sleep 5 secs then try one last time.
        if ($google === false) { 
            sleep(5); 
            fputs($fp,"Communication Error - Try 3.\n");
            $google = $soap->call('doGoogleSearch', $parameters); 
        }
        if ($google === false) { 
            fputs($fp,"Communication Error - Giving up.\n");
            fclose($fp);
            $results = false;
        } else {
            $count = count($google['resultElements']);
            fputs($fp,"Results: $count (+{$google['startIndex']})   Time: {$google['searchTime']}     Estimate: {$google['estimatedTotalResultsCount']}\n");
            fclose($fp);
            // End of Debug
            $results = array();
            $results["results"] = array();
            $results['from'] = $google['startIndex'];
            $results['to'] = $google['endIndex'];
            $results['count'] = $google['estimatedTotalResultsCount'];
            $results['start'] = $google['startIndex'];
            for ($i=0;$i<count($google['resultElements']);$i++) {
                $entry = $google['resultElements'][$i];
                $result = array();
                $result['site_title'] = strip_tags($entry['title']);
                $result['site_url'] = strip_tags($entry['URL']);
                $result['site_desc'] = strip_tags($entry['snippet']);
                $rank = $i + $google['startIndex'];
                $result['rank'] = $rank;
                $results['results'][] = $result;
            }
        }
        return $results;
    }
    
    function getKeywords() {
        global $db_link;

        $keys = false;
        $result = mysqli_query($db_link, 'SELECT * FROM stat_keywords');        
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            $keys = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $keys[$row['ID']] = $row['keyword'];
            }
        }
        return $keys;
    }
    
    function getSearchEngines() {
        global $db_link;
        
        $search = false;
        $result = mysqli_query($db_link, 'SELECT * FROM stat_searchengines');        
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            $search = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $engine = array();
                $engine['name'] = $row['name'];
                $engine['url'] = $row['url'];
                $search[$row['ID']] = $engine;
            }
        }
        return $search;
    }
    
    function searchYahoo($keyword,$results=1000) {
        $eKeyword = urlencode($keyword);
        
        // Yahoo
        $engine = 1;
        $massresults = array();
        $queries = $results / 100;
        $count = 100;
        echo "<PRE>|--------|\n";
        for ($i=1;$i<=$queries;$i++) {
            if ($count < 100) break;
            if ($i == 1) {
                // First Query
                $begin = $i * 100 - 99;
                $data = $this->fetch("http://search.yahoo.com/search?_adv_prop=web&x=op&ei=UTF-8&fr=fp-top&va=$eKeyword&va_vt=any&vp_vt=any&vo_vt=any&ve_vt=any&vd=all&vst=0&vf=all&vm=i&fl=0&n=100",false,"B=3rqsech1bs6b9&b=2","http://search.yahoo.com/web/advanced?ei=UTF-8");
                $last_refer = "http://search.yahoo.com/search?_adv_prop=web&x=op&ei=UTF-8&fr=fp-top&va=$eKeyword&va_vt=any&vp_vt=any&vo_vt=any&ve_vt=any&vd=all&vst=0&vf=all&vm=i&fl=0&n=100";
            } else {
                $begin = $i * 100 - 99;
                $data = $this->fetch("http://search.yahoo.com/search?p=$eKeyword&prssweb=Search&ei=UTF-8&n=100&fl=0&pstart=1&fr=fp-top&b=$begin",false,"B=3rqsech1bs6b9&b=2",$last_refer);
                $last_refer = "http://search.yahoo.com/search?p=$eKeyword&prssweb=Search&ei=UTF-8&n=100&fl=0&pstart=1&fr=fp-top&b=$begin";
            }
            // Got the results, now sweep them
            $results = $this->sweepYahoo($data['html']);
//            $fp = fopen('yahooSearch.html','wb');
//             fputs($fp,$data['html']);
//             fclose($fp);
//             $fp = fopen('yahooSearch.var','wb');
//             fputs($fp,print_r($results,true));
//             fclose($fp);
            // Make sure there are more results
            $from = intval($results['from']);
            $to = intval($results['to']);
            $count = $to - $from + 1;
            $massresults = array_merge($results['results'],$massresults);
            sleep(5);
            echo ".";
            ob_flush();
            flush();
        }
        echo "</PRE>";
        return $massresults;
    }
    
    function searchMSN($keyword,$search_results=250) {
        $eKeyword = urlencode($keyword);

        //$data = $this->fetch("http://search.msn.com/results.aspx?q=learn+spanish&FORM=QBRE");        
        //return $this->sweepMSN($data['html']);
        //return $data;
        
        // MSN
        $engine = 4;
        $massresults = array();
        $queries = $search_results / 10;
        $count = 9;
        echo "<PRE>|--------|---------|----|\n";
        for ($i=1;$i<=$queries;$i++) {
            if ($count < 9) break;
            if ($i == 1) {
                // First Query
                $begin = $i * 10 - 9;
                $data = $this->fetch("http://search.msn.com/results.aspx?q=$eKeyword&FORM=QBRE");
            } else {
                $begin = $i * 10 - 9;
                $data = $this->fetch("http://search.msn.com/results.aspx?q=$eKeyword&first=$begin&FORM=PORE");
            }
            // Got the results, now sweep them
            $results = $this->sweepMSN($data['html']);
//            $fp = fopen('yahooSearch.html','wb');
//             fputs($fp,$data['html']);
//             fclose($fp);
//             $fp = fopen('yahooSearch.var','wb');
//             fputs($fp,print_r($results,true));
//             fclose($fp);
            // Make sure there are more results
            $from = intval($results['from']);
            $to = intval($results['to']);
            $count = $to - $from + 1;
            $massresults = array_merge($results['results'],$massresults);
            echo ".";
            ob_flush();
            flush();
            //sleep(1);
        }
        echo "</PRE>";
        return $massresults;
    }
    
    function searchGoogle($keyword,$search_results=1000) {
        // Google
        $engine = 2;
        $massresults = array();
        $queries = $search_results / 10;
        $count = 10;
        $fp = fopen('google_api.txt','wb');
        fputs($fp,"Begin Search - Google ($keyword)\n------------------------------------------\n");
        fclose($fp);
        echo "<PRE STYLE=\"font-size:8px;\">|--------|---------|---------|---------|---------|---------|---------|---------|---------|---------|\n";
        for ($i=0;$i<$queries;$i++) {
            if ($count < 10) break;
            $start = $i * 10;
            $google_results = $this->googleAPI($keyword,$start);
            // Store the results in the database
            if ($google_results !== false) {
                $from = intval($google_results['from']);
                $to = intval($google_results['to']);
                $count = $to - $from + 1;
                $massresults = array_merge($google_results['results'],$massresults);
            } else {
                break;
            }
            echo ".";
            ob_flush();
            flush();
        }
        $fp = fopen('google_api.txt','ab');
        fputs($fp,"------------------------------------------\nEnd of Search - Google ($keyword)\n------------------------------------------\n\n\n");
        fclose($fp);
        echo "</PRE>";
        return $massresults;
    
    }

    /*
    function fetchRanking($engine) {
        global $db_link;
        
        // Get the min and max dates ?
        $result = mysql_query("SELECT * FROM stat_ranking WHERE (site_domain LIKE '%$domain%' OR site_url LIKE '%$domain%') AND search_engine = '$engine' ORDER BY keyword,site_url,pulled ASC",$db_link);
        if (($result !== false) && (mysql_num_rows($result) > 0)) {
            // Get the results
            $ranks = array();
            while ($row = mysql_fetch_assoc($result)) {
            
            }
    } 
    
    */   
    //------------------------------------------------------------
    //  Class Functions
    //------------------------------------------------------------
    
    
    
    function search($keyword,$keyword_num) {
        global $db_link;
    
        // This goes out to all the search engines configured and pulls down as many results as it can.
        
        // Grab a list of search engines
        $ret = '';
        echo "search($keyword);\n";
        // Query Yahoo
        $yahooResults = $this->searchYahoo($keyword);
        $pulled = date('Y-m-d');
        if (is_array($yahooResults)) {
            mysqli_query($db_link, "delete from stat_ranking WHERE keyword='$keyword_num' AND search_engine='1' AND pulled='$pulled'");
            foreach($yahooResults as $value) {
                // It's an entry
                $site_title = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_title'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_url = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_url'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_desc = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_desc'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_host = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_host'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $rank = trim($value['rank']);
                if (strlen($site_host) > 2) {
                    list($domain,$other) = explode('/',$site_host);
                } else {
                    preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$site_url,$matches);
                    $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[1]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                }
                // Post the data
                mysqli_query($db_link, "INSERT INTO stat_ranking SET pulled='$pulled', site_title='$site_title', site_domain='$domain', site_url='$site_url', site_desc='$site_desc', rank='$rank', search_engine='1', keyword='$keyword_num'");
            }
        }
        
        // Query Google
        $googleResults = $this->searchGoogle($keyword);
        if (is_array($googleResults)) {
            mysqli_query($db_link, "delete from stat_ranking WHERE keyword='$keyword_num' AND search_engine='2' AND pulled='$pulled'");
            foreach($googleResults as $value) {
                // It's an entry
                $site_title = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_title'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_url = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_url'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_desc = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_desc'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_host = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_host'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $rank = trim($value['rank']);
                if (strlen($site_host) > 2) {
                    list($domain,$other) = explode('/',$site_host);
                } else {
                    preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$site_url,$matches);
                    $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[1]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                }
                // Post the data
                mysqli_query($db_link, "INSERT INTO stat_ranking SET pulled='$pulled', site_title='$site_title', site_domain='$domain', site_url='$site_url', site_desc='$site_desc', rank='$rank', search_engine='2', keyword='$keyword_num'");
            }
        }
    
        // Query MSN
        $msnResults = $this->searchMSN($keyword);
        if (is_array($msnResults)) {
            mysqli_query($db_link, "delete from stat_ranking WHERE keyword='$keyword_num' AND search_engine='4' AND pulled='$pulled'");
            foreach($msnResults as $value) {
                // It's an entry
                $site_title = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_title'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_url = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_url'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_desc = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_desc'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $site_host = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_host'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                $rank = trim($value['rank']);
                if (strlen($site_host) > 2) {
                    list($domain,$other) = explode('/',$site_host);
                } else {
                    preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$site_url,$matches);
                    $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[1]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                }
                // Post the data
                mysqli_query($db_link, "INSERT INTO stat_ranking SET pulled='$pulled', site_title='$site_title', site_domain='$domain', site_url='$site_url', site_desc='$site_desc', rank='$rank', search_engine='4', keyword='$keyword_num'");
            }
        }
    }
    
    function scan() {
        // Base function to scan all the search engines configured
        $keywords = $this->getKeywords();
        
        foreach($keywords as $num => $word) {
            // Run the search for each word
            if ($num > 3) break;
            $this->search($word,$num);
        }
        //return $this->showAll();
        return $this->showRank('5muses');
    }
    
    function showRank($domain) {
        global $db_link;
    
        $keywords = $this->getKeywords();
        $engines = $this->getSearchEngines();
        
        $ret = "<SPAN CLASS=TITLE STYLE=\"border-bottom:1px solid #888;\">Search Ranking For \"$domain\"</SPAN><BR><BR>";
        $result = mysqli_query($db_link, "SELECT * FROM stat_ranking WHERE site_domain LIKE '%$domain%' OR site_url LIKE '%$domain%' ORDER BY search_engine,keyword,pulled DESC,rank");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            $ret .= "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0>";
            while ($row = mysqli_fetch_assoc($result)) {
                $e = intval($row['search_engine']);
                $engine = $engines[$e]['name'];
                if ($e != $old_e) {
                    $ret .= "<TR><TD CLASS=message3 COLSPAN=5>$engine</TD></TR>";
                    $old_e = $e;
                }
                $k = intval($row['keyword']);
                $keyword = $keywords[$k];
                
                $ret .= "<TR>";
                $rank = $row['rank'];
                $domain = $row['site_domain'];
                $pulled = $row['pulled'];
                $url = $row['site_url'];
                if ($pulled == date('Y-m-d')) { $pulled = '<FONT COLOR=#4455CC>Today</FONT>'; }
                if ($pulled == date('Y-m-d',time() - 86400)) { $pulled = '<FONT COLOR=#FF9922>Yesterday</FONT>'; }
                
                if (strpos($domain,'5muses') > 0) { $doman = "<FONT COLOR=green>$domain</FONT>"; }
                if ($k != $old_k) {
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px solid #555;\">$keyword</TD>";
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-left:1px solid #333;border-top:1px solid #555;\">$rank</TD>";
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px solid #555;\">$pulled</TD>";
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px solid #555;\">$domain</TD>";
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px solid #555;\"><A HREF=\"$url\" TARGET=\"_NEW\">$url</A></TD>";
                    $old_k = $k;
                } else {
                    $ret .= "<TD CLASS=message2>&nbsp;</TD>";
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-left:1px solid #333;border-top:1px dashed #333;\">$rank</TD>";
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px dashed #333;\">$pulled</TD>";
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px dashed #333;\">$domain</TD>";
                    $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px dashed #333;\"><A HREF=\"$url\" TARGET=\"_NEW\">$url</A></TD>";
                }
                $ret .= "</TR>";
            }
            $ret .= "</TABLE>";
        }
        
        return $ret;
        
    }
    
    function showMovement($domain) {
        global $db_link;
    
        $keywords = $this->getKeywords();
        $engines = $this->getSearchEngines();
        
        $ret = "<SPAN CLASS=TITLE STYLE=\"border-bottom:1px solid #888;\">Search Ranking For \"$domain\"</SPAN><BR><BR>";
        $result = mysqli_query($db_link, "SELECT * FROM stat_ranking WHERE site_domain LIKE '%$domain%' OR site_url LIKE '%$domain%' ORDER BY search_engine,keyword,site_url,pulled ASC");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            // Get the results
            $ranks = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $url = $row['site_url'];
                $e = intval($row['search_engine']);
                $engine = $engines[$e]['name'];
                $k = intval($row['keyword']);
                $keyword = $keywords[$k];
                if (($k != $old_k) and ($e != $old_e)) {
                    // New Keyword
                    $ranks["$engine ($keyword)"] = array();
                    $old_k = $k;
                    $old_e = $e;
                } else {
                    // Same Keyword
                    if ($url != $old_url) {
                        // Got a new url;
                        $urlrank = array();
                        $urlrank['old'] = ($engine == 4) ? 250 : 1000;
                        $urlrank['new'] = intval($row['rank']);
                        $urlrank['count'] = 1;
                        $ranks["$engine ($keyword)"][$url] = $urlrank;
                        $old_url = $url;
                    } else {
                        // Continue on a current URL
                        $urlrank['old'] = $urlrank['new'];
                        $urlrank['new'] = intval($row['rank']);
                        $urlrank['count'] = $urlrank['count'] + 1;
                        $ranks["$engine ($keyword)"][$url] = $urlrank;
                    }
                }
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            $ret .= "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0>";
            foreach ($ranks as $keyword => $data) {
                $ret2 .= "<TR><TD CLASS=message3 COLSPAN=5>$keyword</TD></TR>";
                foreach ($data as $url => $stats) {
                    if ($stats['count'] >= 2) {
                        if (strlen($ret2) > 1) { $ret .= $ret2; $ret2 = ''; }
                        $diff = $stats['old'] - $stats['new'];
                        if ($diff < 0) { $climb = "<TD CLASS=message2 NOWRAP STYLE=\"color:#F00;border-top:1px solid #555;\">($diff)&nbsp;</TD>"; }
                        if ($diff > 0) { $climb = "<TD CLASS=message2 NOWRAP STYLE=\"color:#0F0;border-top:1px solid #555;\">(+$diff)&nbsp;</TD>"; }
                        if ($diff == 0) { $climb = "<TD CLASS=message2 NOWRAP STYLE=\"color:#888;border-top:1px solid #555;\">($diff)&nbsp;</TD>"; }
                        $ret .= "<TR>";
                        $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px solid #555;\">{$stats['new']}</TD>";
                        $ret .= $climb;
                        $ret .= "<TD CLASS=message2 NOWRAP STYLE=\"border-top:1px solid #555;\"><A HREF=\"$url\" TARGET=\"_NEW\">$url</A></TD>";
                        $ret .= "</TR>";
                    }
                }
            }
            $ret .= "</TABLE><BR><BR>";
        }
        
        return $ret;
        
    }
    
    function showAll($engnum=0,$keynum=0) {
        global $db_link;
        
        // Show the first table
        $keywords = $this->getKeywords();
        $engines = $this->getSearchEngines();
        $ret = '<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH=100%>';
        foreach($keywords as $k => $keyword) {
            $ret .= "<TR><TD CLASS=resd>$keyword</TD>";
            $c = true;
            foreach($engines as $e => $engine) {
                $engine_name = $engine['name'];
                $color = ($c) ? 'BGCOLOR=#112233' : 'BGCOLOR=#223344';
                $ret .= "<TD NOWRAP CLASS=resd $color><A HREF=\"?act=sp&sec=res&e=$e&k=$k\">$engine_name</A></TD>";
                $c = !$c;
            }
            $ret .= '</TR>';
        }
        $ret .= '</TABLE><BR>';
        
        if (($engnum != 0) and ($keynum != 0)) {
            $result = mysqli_query($db_link, "SELECT * FROM stat_ranking WHERE search_engine='$engnum' AND keyword='$keynum' ORDER BY search_engine,keyword,pulled,rank");
            
            if (($result !== false) && (mysqli_num_rows($result) > 0)) {
                $ret .= "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 WIDTH=100%>";
                while ($row = mysqli_fetch_assoc($result)) {
                    $ret .= "<TR>";
                    $rank = $row['rank'];
                    $domain = $row['site_domain'];
                    $url = $row['site_url'];
                    $color = (strpos($domain,'5muses') > 0) ? ' BGCOLOR=#445588' : '';
                    $ret .= "<TD NOWRAP CLASS=resd$color>$rank</TD>";
                    $ret .= "<TD NOWRAP CLASS=resd$color><A HREF=\"$url\" TARGET=dfc_outside>$domain</A></TD>";
                    $ret .= "</TR>";
                }
                $ret .= "</TABLE>";
            }
        }
        
        return $ret;
    }
    
    function showSearches() {
        //SELECT search_engine, keyword, count(*) as total, pulled FROM stat_ranking GROUP BY search_engine, keyword
        global $db_link;
        
        $keywords = $this->getKeywords();
        $engines = $this->getSearchEngines();
    
        $tags = array();
        $result = mysqli_query($db_link, "SELECT search_engine, keyword, count(*) as total, pulled FROM stat_ranking GROUP BY search_engine, keyword, pulled");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            while ($row = mysqli_fetch_assoc($result)) {
                $tag = array();
                $tag['search_engine'] = $row['search_engine'];
                $tag['keyword'] = $row['keyword'];
                $e = intval($row['search_engine']);
                $k = intval($row['keyword']);
                $tag['total'] = $row['total'];
                $tag['pulled'] = $row['pulled'];
                $tags["$e:$k"] = $tag;
            }
        }
        
        $ret = "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH=400 STYLE=\"border: 1px solid #444444;\">";
        foreach($engines as $e => $engine) {
            $engine_name = $engine['name'];
            $ret .= "<TR><TD COLSPAN=3 CLASS=auth BGCOLOR=#404040>$engine_name</TD></TR>";
            foreach($keywords as $k => $keyword) {
                $ret .= "<TR>";
                $row = $tags["$e:$k"];
                //$db .= "$e:$k\n";
                $total = $row['total'];
                $pulled = $row['pulled'];
                if ($pulled == date('Y-m-d')) { $pulled = '<FONT COLOR=#5566FF>Today</FONT>'; }
                if ($pulled == date('Y-m-d',time() - 86400)) { $pulled = '<FONT COLOR=#FF9922>Yesterday</FONT>'; }
                
                if (strpos($domain,'5muses') > 0) { $doman = "<FONT COLOR=green>$domain</FONT>"; }
                $ret .= "<TD NOWRAP CLASS=auth STYLE=\"padding-left: 20px;border-bottom: 1px solid #222;\"><A HREF=\"?act=sp&sec=doSearch&e=$e&k=$k\">$keyword</A></TD>";
                $ret .= "<TD NOWRAP CLASS=auth STYLE=\"border-bottom: 1px solid #222;\">$total</TD>";
                $ret .= "<TD NOWRAP CLASS=auth STYLE=\"border-bottom: 1px solid #222;\">$pulled</TD>";
                $ret .= "</TR>";
            }
        }
        $ret .= "</TABLE>";
        //$ret .= "<PRE>".print_r($tags,true)."\n$db\n</PRE>";
        
        return $ret;
    }
    
    function doSearch($engine,$keynum) {
        global $db_link;
    
        $keywords = $this->getKeywords();
        $keyword = $keywords[$keynum];
        
        $keyword_num = $keynum;
        
        $pulled = date('Y-m-d');
        if ($engine == 1) {
            $yahooResults = $this->searchYahoo($keyword);
            if (is_array($yahooResults)) {
                mysqli_query($db_link, "delete from stat_ranking WHERE keyword='$keyword_num' AND search_engine='1' AND pulled='$pulled'");
                foreach($yahooResults as $value) {
                    // It's an entry
                    $site_title = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_title'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_url = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_url'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_desc = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_desc'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_host = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_host'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $rank = trim($value['rank']);
                    if (strlen($site_host) > 2) {
                        list($domain,$other) = explode('/',$site_host);
                    } else {
                        preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$site_url,$matches);
                        $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[1]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    }
                    // Post the data
                    mysqli_query($db_link, "INSERT INTO stat_ranking SET pulled='$pulled', site_title='$site_title', site_domain='$domain', site_url='$site_url', site_desc='$site_desc', rank='$rank', search_engine='1', keyword='$keyword_num'");
                }
            }
        }
        
        if ($engine == 2) {
            $googleResults = $this->searchGoogle($keyword);
            if (is_array($googleResults)) {
                mysqli_query($db_link, "delete from stat_ranking WHERE keyword='$keyword_num' AND search_engine='2' AND pulled='$pulled'");
                foreach($googleResults as $value) {
                    // It's an entry
                    $site_title = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_title'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_url = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_url'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_desc = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_desc'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_host = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_host'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $rank = trim($value['rank']);
                    if (strlen($site_host) > 2) {
                        list($domain,$other) = explode('/',$site_host);
                    } else {
                        preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$site_url,$matches);
                        $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[1]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    }
                    $pulled = date('Y-m-d');
                    // Post the data
                    mysqli_query($db_link, "INSERT INTO stat_ranking SET pulled='$pulled', site_title='$site_title', site_domain='$domain', site_url='$site_url', site_desc='$site_desc', rank='$rank', search_engine='2', keyword='$keyword_num'");
                }
            }
        }
        
        if ($engine == 4) {
            $msnResults = $this->searchMSN($keyword);
            if (is_array($msnResults)) {
                mysqli_query($db_link, "delete from stat_ranking WHERE keyword='$keyword_num' AND search_engine='4' AND pulled='$pulled'");
                foreach($msnResults as $value) {
                    // It's an entry
                    $site_title = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_title'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_url = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_url'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_desc = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_desc'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $site_host = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], trim($value['site_host'])) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    $rank = trim($value['rank']);
                    if (strlen($site_host) > 2) {
                        list($domain,$other) = explode('/',$site_host);
                    } else {
                        preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$site_url,$matches);
                        $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[1]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
                    }
                    // Post the data
                    mysqli_query($db_link, "INSERT INTO stat_ranking SET pulled='$pulled', site_title='$site_title', site_domain='$domain', site_url='$site_url', site_desc='$site_desc', rank='$rank', search_engine='4', keyword='$keyword_num'");
                }
            }
        }
        // End of function
    }
    
    
    // End of class
}
    
/*
learn german free   - Yahoo
    29  www.5muses.com  Today
    77  www.5muses.com  Today
    132 www.5muses.com  2005-07-01
    75  www.5muses.com  2005-06-30
    63  www.5muses.com  2005-06-29
    78  www.5muses.com  2005-06-29
learn german free   - MSN
    42  www.5muses.com  Today
    90  www.5muses.com  Today
    36  www.5muses.com  2005-07-01
    240 www.5muses.com  2005-07-01
*/
if (isset($_REQUEST['graph'])) {

    $gDebug = false;
    // Need to gen a graph
    // http://dev.5muses.com/web1/searchbot.php?graph=1&domain=www.5muses.com
    include_once('db.php');
    if ($gDebug) {
        header("Content-type: text/html;");
    } else {
        header("Content-type: image/png;");
    }
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    $engine = intval($_REQUEST['graph']);
    $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['domain']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
    $monthReq = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['month']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : "")); // 2005-7
    list($year,$month) = explode('-',$monthReq);
    $year = intval($year);
    $month = intval($month);
    $monthStart = strtotime("$year-$month-1") - 86400;
    $monthEnd = strtotime("+1 month 1 day",$monthStart);
    
    if ($gDebug) echo '<PRE STYLE="font-size:10px;">Graph Routine'."\n-----------------------------\n\n";
    if ($gDebug) echo "Year:$year  Month:$month\n";
    if ($gDebug) { echo "$monthStart - $monthEnd <BR>"; }

    // Get the stats
    $SBot = new searchBot();
    $kw = $SBot->getKeywords();
    $se = $SBot->getSearchEngines();
    $terms = false;
    $timeStart = date('Y-m-d',$monthStart);
    $timeEnd = date('Y-m-d',$monthEnd);
    $imageSQL = "SELECT * FROM stat_ranking WHERE (site_domain LIKE '%$domain%' OR site_url LIKE '%$domain%') AND (pulled >= '$timeStart' AND pulled <= '$timeEnd') AND search_engine = '$engine' ORDER BY keyword,site_url,pulled ASC";
    $result = mysqli_query($db_link, $imageSQL);
    if ($gDebug) echo $imageSQL."<BR>";
    if ($gDebug) echo "Rows: ".mysqli_num_rows($result)."<BR>";
    if ($gDebug) echo ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))."<BR><BR>";
    //imagestring($graph,2,635,13,$imageSQL,$key2);
    if (($result !== false) && (mysqli_num_rows($result) > 0)) {
        $graphdata = array();
        while ($row = mysqli_fetch_assoc($result)) {
            // $graphdata[keyword][url][date] = rank;
            $date = strtotime($row['pulled']);
            $rank = intval($row['rank']);
            $keyword = $row['keyword'];
            $url = $row['site_url'];
            if (!is_array($graphdata[$keyword])) { $graphdata[$keyword] = array(); }
            if (!is_array($graphdata[$keyword][$url])) { $graphdata[$keyword][$url] = array(); }
            $graphdata[$keyword][$url][$date] = $rank;
        }
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
    }    
    if ($gDebug) print_r($graphdata);
    /*
    $result = mysql_query("SELECT * FROM stat_ranking WHERE search_engine='$engine' AND (site_domain LIKE '%$domain%' OR site_url LIKE '%$domain%') ORDER BY keyword,pulled,rank DESC",$db_link);
    //echo mysql_error()."\n";
    if (($result !== false) && (mysql_num_rows($result) > 0)) {
        $terms = array(); $dates = array();
        while ($row = mysql_fetch_assoc($result)) {
            // Terms[term][date] = rank
            $date = strtotime($row['pulled']);
            $rank = $row['rank'];
            $keyword = $row['keyword'];
            //echo "$keyword:$date:$rank\n";
            if (!is_array($terms[$keyword])) {
                $keyarray = array();
                $keyarray[$date] = $rank;
                $terms[$keyword] = $keyarray;
            } else {
                $terms[$keyword][$date] = $rank;
            }
        }
        mysql_free_result($result);
    }
      
      
    // go today - 30 days ? or month?
    // Calc low and high for dates...
    
    $low = 2147483647;
    $high = 0;
    if (is_array($terms)) {
        foreach ($terms as $term => $datearray) {
            foreach ($datearray as $date => $rank) {
                $low = ($date < $low) ? $date : $low ;
                $high = ($date > $high) ? $date : $high ;
            }
        }
        $days = intval(($high - $low) / 86400);
    }
    // Recalculate the full array for dates and keywords
    $data = array();
    
    foreach($kw as $id => $key) {
        $data[$id] = array();
        for ($d=$low;$d<=$high;$d=$d+86400) {
            if (isset($terms[$id][$d])) {
                $data[$id][$d] = $terms[$id][$d];
            } else {
                $data[$id][$d] = -1;
            }
        }
    }
    */
    
    //print_r($data);
    //print_r($terms);
    // Got the stuff in an array, now 
    // 1=yahoo
    // 2=google
    // 3=ask.com
    // 4=msn
    //echo "\n-----------------------------\n\n";
    //echo "LOW: $low     HIGH: $high      DAYS: $days \n";
    //print_r($terms);
    //echo "</PRE>";
    
    $graph = imagecreate(1000,440);
    $colors = array();
    $background = imagecolorallocate($graph,0,0,0);
    $foreground = imagecolorallocate($graph,255,255,255);
    $lines = imagecolorallocate($graph,64,64,64);
    $goal = imagecolorallocate($graph,32,32,64);
    $key1 = imagecolorallocate($graph,64,72,128);
    $key2 = imagecolorallocate($graph,96,128,255);
    $red = imagecolorallocate($graph,255,64,64);
    $green = imagecolorallocate($graph,64,255,64);
    // Create colors for Spanish French and German?
    $colors['ge'] = array();
    $colors['fr'] = array();
    $colors['sp'] = array();
    for ($i=1;$i<=10;$i++) {
        $r1 = 255 - ($i * 8);
        $r2 = 192 - ($i * 19);
        $colors['ge'][$i] = imagecolorallocate($graph,$r1,$r2,$r2);
        $colors['fr'][$i] = imagecolorallocate($graph,$r2,$r2,$r1);
        $colors['sp'][$i] = imagecolorallocate($graph,$r2,$r1,$r2);
    }
    imagefill($graph,1,1,$background);
    if ($engine != 4) {
        imagefilledrectangle($graph,20,20,630,28,$goal);
        for ($i=1; $i<20; $i++) {
            imageline($graph,21,$i*20+20,629,$i*20+20,$lines);
            imagestring($graph,2,635,$i*20+13,$i*50,$foreground);
        }
    } else {
        imagefilledrectangle($graph,20,20,630,52,$goal);
        for ($i=1; $i<20; $i++) {
            imageline($graph,21,$i*20+20,629,$i*20+20,$lines);
            if (($i % 2) == 0) {
                imagestring($graph,2,635,$i*20+13,$i*12.5,$foreground);
            }
        }
    }
    imagestring($graph,2,635,13,'Rank',$key2);
    imagerectangle($graph,20,20,630,420,$foreground);
    $today = intval(date('d'));
    for ($i=1; $i<31; $i++) {
        if ($i != $today) {
            imageline($graph,20*$i+20,21,20*$i+20,419,$lines);
        } else {
            imageline($graph,20*$i+20,21,20*$i+20,419,$key2);
        }
        imagestring($graph,2,20*$i+20,421,$i,$foreground);
    }
    imagestring($graph,2,5,421,'Day',$key2);
    imagestring($graph,2,20,5,$se[$engine]['name'].' - '.$domain,$foreground);
    $placements = 0;
    if (is_array($graphdata)) {
        $count = 1;
        $c_sp = 1; $c_ge = 1; $c_fr = 1;
        foreach ($graphdata as $keyword => $urlarray) {
            if ($gDebug) echo "\nKeyword: $keyword\n";
            foreach ($urlarray as $url => $datearray) {
                if ($gDebug) echo "URL: $url\n";
                ksort($datearray);
                $yt = $count * 9 + 20;
                if (strpos($kw[$keyword],'spanish') !== false) { $color = $colors['sp'][$c_sp]; $c_sp ++; $dStyle = (($c_sp % 2) == 1); }
                if (strpos($kw[$keyword],'german') !== false) { $color = $colors['ge'][$c_ge]; $c_ge ++; $dStyle = (($c_ge % 2) == 1); }
                if (strpos($kw[$keyword],'french') !== false) { $color = $colors['fr'][$c_fr]; $c_fr ++; $dStyle = (($c_fr % 2) == 1); }
                $style = array($color, $color, $background, $background);
                if ($dStyle) {
                    imagesetstyle($graph, $style);
                    imageline($graph,670,$yt,690,$yt,IMG_COLOR_STYLED);
                } else {
                    imageline($graph,670,$yt,690,$yt,$color);
                }
                imagestring($graph,1,700,$yt-5,$kw[$keyword],$key2);
                imagestring($graph,1,830,$yt-5,$url,$key1);
                $rColor = $foreground;
                for ($i=0;$i<=31;$i++) { // Days of the month
                    $x = ($i - 1) * 20 + 40;
                    $day = mktime(0,0,0,$month,$i,$year);
                    if ($gDebug) echo "$day:".$datearray[$day]."\n";
                    if ($datearray[$day] > 0) {
                        $rank = $datearray[$day];
                        $y = ($engine == 4) ? intval($rank / 0.625) + 20: intval($rank / 2.5) + 20;
                        if ($ox != 0) { 
                            if ($dStyle) {
                                //imagesetstyle($graph, $style);
                                imageline($graph,$ox,$oy,$x,$y,IMG_COLOR_STYLED);
                            } else {
                                imageline($graph,$ox,$oy,$x,$y,$color);
                            }
                        }
                        $rColor = ($oRank < $rank) ? $red : $green;
                        $rColor = ($oRank == $rank) ? $foreground : $rColor;
                        //echo "$ox,$oy    $x,$y\n";
                        $ox = $x;
                        $oy = $y;
                        $oRank = $rank;
                    } else {
                        if ($i < $today) {
                            $rColor = $lines;
                        }
                    }
                }
                if ($rColor != $lines) $placements++;
                imagestring($graph,1,800,$yt-5,$rank,$rColor);
                $ox = 0;
                $oy = 0;
                $count++;
            }
            /*
            foreach ($datearray as $date => $rank) {
                if ($rank > 0) {
                    $x = 30 + ((($date - $low) / 86400) * 10);
                    $y = ($engine == 4) ? intval($rank / 1.25) + 20: intval($rank / 5) + 20;
                    if ($ox == 0) { $ox = 21; $oy = 219; }
                    imageline($graph,$ox,$oy,$x,$y,$colors[$term]);
                    //echo "$ox,$oy    $x,$y\n";
                    $ox = $x;
                    $oy = $y;
                }
            }
            $ox = 0;
            $oy = 0;
            */
        }
    imagestring($graph,2,460,5,"Total Placements: ",$foreground);
    imagestring($graph,2,570,5,$placements,$key2);
    }
    
    //imageline($graph,21,220,329,220,$lines);
    imagepng($graph);
}


?>
