<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//
// Something to check information about the browser...


?>
<HTML>
<HEAD>
<TITLE>PHP/JS Browser Information</TITLE>
</HEAD>
<BODY STYLE="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;" onLoad="afterLoad();">


<SCRIPT language="JavaScript">
<!--

function afterLoad() {
    document.infoform.screenx.value = screen.width;
    document.infoform.screeny.value = screen.height;
    myBigTable = document.getElementById("bigtable");
    document.infoform.btablex.value = myBigTable.offsetWidth;
    document.infoform.btabley.value = myBigTable.offsetHeight;
}

-->
</SCRIPT>


<!-- Try to figure out the total screen width and height -->
<DIV><TABLE ID=bigtable BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100% HEIGHT=100% BGCOLOR=#CCCCFF>
    <TR>
        <TD>
<!-- Form to hold some information -->
<FORM NAME="infoform">
<TABLE BORDER=1 CELLPADDING=1 CELLSPACING=1 BGCOLOR=#FFFFFF>
    <TR>
        <TD ALIGN=RIGHT>Screen X/Y:</TD>
        <TD ALIGN=LEFT><INPUT NAME=screenx TYPE=TEXT SIZE=8><INPUT NAME=screeny TYPE=TEXT SIZE=8></TD>
    </TR>
    <TR>
        <TD ALIGN=RIGHT>BigTable X/Y:</TD>
        <TD ALIGN=LEFT><INPUT NAME=btablex TYPE=TEXT SIZE=8><INPUT NAME=btabley TYPE=TEXT SIZE=8></TD>
    </TR>
</TABLE>
</FORM>

        </TD>
    </TR>
</TABLE></DIV>

</BODY>

<?php
    // Not much here.....
?>
