<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

/**
 * uncomment this for dev to report all mysqli errors
 * @see http://www.php.net/manual/en/mysqli-driver.report-mode.php#refsect1-mysqli-driver.report-mode-parameters
 */
//mysqli_report(MYSQLI_REPORT_ALL); //to fatal error
mysqli_report(MYSQLI_REPORT_ERROR); //to warn

// This holds some silly config stuff for the web interface

// DB_CONF_TYPE constant indicates how db was loaded
//	appfog
//	local : loaded from prg_config.local.php
//	defualt

// Load from VCAP_SERVICES environmental variable
// appfog uses this.  See https://docs.appfog.com/services/mysql
if($af_config_json = getenv("VCAP_SERVICES")){
    define('DB_CONF_TYPE','appfog');
    $af_config = json_decode($af_config_json, true);
    $af_mysql_config = $af_config["mysql-5.1"][0]["credentials"];
    //load for local use
    $dfc_config['mysql_host'] = $af_mysql_config["hostname"];
    $dfc_config['mysql_user'] = $af_mysql_config["username"];
    $dfc_config['mysql_pass'] = $af_mysql_config["password"];
    $dfc_config['mysql_db'] = $af_mysql_config["name"];
    // Load local config if available
}elseif(file_exists("prg_config.local.php")){
    define('DB_CONF_TYPE','local');
    require("prg_config.local.php");
    // Use defaults (localhost/root) <- For local development only!
}else{
    define('DB_CONF_TYPE','default');
    $dfc_config['mysql_host'] =             '127.0.0.1';
    $dfc_config['mysql_user'] =             'root';
    $dfc_config['mysql_pass'] =             '';
    $dfc_config['mysql_db'] =               'fastcards';
}
?>
