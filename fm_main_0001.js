// =====================================================================
// ===== Global Variable Declarations
// =====================================================================

    // +----------------------------------------------------------------------
    // | RPC Interface
    // +----------------------------------------------------------------------
    var fmrpcPayload;

    // +----------------------------------------------------------------------
    // | Navigation System
    // +----------------------------------------------------------------------
    // Need to incorporate these into the RPC
    var nv_sizes = Array(4,6,3,3,1);
    var nv_open = Array(false,false,false,false,false);
    var nv_selected = Array(false,false,false,false,false);
    var nv_places = Array(0,1,2,3,4);
    var nv_names = Array("iFMmain","iFMdfc","iFMaccount","iFMsupport","iFMadmin");
    var loginVisible = false;
    
    // +----------------------------------------------------------------------
    // | Preload Images and "Loading..." screen
    // +----------------------------------------------------------------------
    var pl_count = 0;
    var introComplete = false;
    var introLoading = false;

    // +----------------------------------------------------------------------
    // | Browser Detect **DEPRECIATED**
    // +----------------------------------------------------------------------
    
    var detect = navigator.userAgent.toLowerCase();
    var OS,browser,version,total,thestring;
    var tickID = true;
    var fmClkToggle = true;
    var fmExp = "";
    var fmDebugCount = 0;
    var dObj = new Object;
        dObj.x = 0;
        dObj.origin = 0;
        dObj.active = false;
        dObj.element = null;

    var bName = navigator.appName;
    var bAgent = navigator.userAgent;
    var pl_max = 5000;

    // +----------------------------------------------------------------------
    // | Browser Detect **DEPRECIATED**
    // +----------------------------------------------------------------------
    var cookies = new Object();



// =====================================================================
// ===== Retrieve the Browsers Cookies into the "cookies" global var
// =====================================================================
    function getCookies() {
        var name, value;
        var beginning, middle, end;
        
        for (name in cookies) {
            cookies = new Object();
            break;
        }
        beginning = 0;
        while (beginning < document.cookie.length) {
            middle = document.cookie.indexOf('=', beginning);
            end = document.cookie.indexOf(';', beginning);
            if (end == -1) end = document.cookie.length;
            if ((middle > end)||(middle == -1)) {
                name = document.cookie.substring(beginning, end);
                value = '';
            } else {
                name = document.cookie.substring(beginning, middle);
                value = document.cookie.substring(middle + 1, end);
            }
            cookies[name] = unescape(value);
            beginning = end + 2;
        }
    }

// =====================================================================
// ===== Login / Logout
// =====================================================================
    function findOffset(obj) {
        findOffset.dx += obj.offsetLeft;
        findOffset.dy += obj.offsetTop;
        findOffset.dt += "[" + obj.nodeName + "] : " + obj.offsetLeft + "x" + obj.offsetTop + "  " + obj.offsetWidth + "x" + obj.offsetHeight + "  \n";
        if (obj.offsetParent) {
            findOffset(obj.offsetParent);
        }
    }
    function fireLogin(task,color) {
        // Send Login RPC Stuff and hide login pane
        if (!loginVisible) {
            //Show Login
            loginPlace = document.getElementById("bannerLogin");
            loginPane = document.getElementById("fmLogin");
            loginPane.style.display = "block";
            findOffset.dx = 0;
            findOffset.dy = 0;
            findOffset.dt = "";
            findOffset(loginPlace);
            lTop = findOffset.dx + loginPlace.offsetWidth - loginPane.offsetWidth;
            lLeft = findOffset.dy + loginPlace.offsetHeight;
            loginPane.style.backgroundColor = color;
            loginPlace.style.backgroundColor = color;
            loginPane.style.left = lTop + "px";
            loginPane.style.top = lLeft + "px";
            //alert(findOffset.dt);
            loginVisible = true;
        } else {
            //Show Login
            loginPane = document.getElementById("fmLogin");
            loginPane.style.display = "none";
            loginVisible = false;
            loginPlace.style.backgroundColor = "transparent";
        }
    }
// =====================================================================
// ===== RPC Interface
// =====================================================================
    function rpcFetch() {    
        //firecontrol(ibTag);
        // If Konqueror... try an Iframe?
        // Go get the form data and submit it too...
        switch (rpcFetch.arguments.length) {
            case 0: 
                return false;
                break;
            case 1:
                urldata = rpcFetch.arguments[0];
                break;
            case 2:
                urldata = rpcFetch.arguments[0];
                // Form Stuff...
                subform = document.forms[rpcFetch.arguments[1]];
                for (i=0;i<subform.elements.length;i++) {
                    urldata += "&" + subform.elements[i].name + "=" + subform.elements[i].value;
                }
                break;
        }
        oldScript = document.getElementById("rpcScript");
        if (oldScript) {
            oldParent = oldScript.parentNode;
            oldParent.removeChild(oldScript);
            oldScript = null;
        }
        script = document.createElement('script');
        script.setAttribute('src','jsrpc.php?'+urldata);
        script.setAttribute('id','rpcScript');
        script.setAttribute('language','Javascript');
        script.setAttribute('type','text/javascript');
        document.getElementsByTagName('head').item(0).appendChild(script);
    }

    function rpcPost(prefix) {    
        // Change this to set an invisible image for sending data.
        subForm = document.adminWebEdit;
        url = prefix + "&fTag=" + escape(subForm.fTag.value) + "&fSelect=" + escape(subForm.fSelect.value) + "&fData=" + escape(subForm.fData.value);
        rpcFetch(url);
    }
    
    function fmHighlight(obj,color) {
        obj.style.backgroundColor=color;
        obj.cursor = 'hand';
    }
    function fmUnHighlight(obj,color) {
        obj.style.backgroundColor=color;
        obj.cursor = 'arrow';
    }
// =====================================================================
// ===== Toolbar Routines...
// =====================================================================

    function initToolbar() {
        // Set document onMouseDown, onMouseMove, onMouseUp Handlers?
        document.onmousedown = tbBeginDrag;
        document.onmousemove = tbDragging;
        document.onmouseup = tbEndDrag;
    }

    function closeToolbar() {
        // Set document onMouseDown, onMouseMove, onMouseUp Handlers?
        document.onmousedown = null;
        document.onmousemove = null;
        document.onmouseup = null;
    }
    
    function showToolbar() {
    
    }
    
    function hideToolbar() {
    
    }
    
    function tbBeginDrag(tbEvent) {
        if (!tbEvent) {
            evt = window.event;
        } else {
            evt = tbEvent;
        }
        cX = evt.clientX;
        cY = evt.clientY;
        if (!evt.srcElement) {
            sE = evt.target;
        } else {
            sE = evt.srcElement;
        }
        if (sE.id.substr(0,2) == "tb") {
            // Get the location of the item,
            // Strip out the "px" part and get only a number
            dObj.x = parseInt(sE.style.left);
            dObj.origin = cX;
            dObj.active = true;
            dObj.element = sE.id;
            // Calculate the Steps if any,
            if (sE.id == "tbVol") { dObj.step = false; }
            if (sE.id == "tbSpd") { dObj.step = false; }
            if (sE.id == "tbCyc") { dObj.step = true; }
            if (sE.id == "tbSpd") { dObj.step = true; }
            // Stop the Timer... (pause)
            dfcClearTick();
            //alert(sE + ' - ' + cX + 'x' + cY);
        }
    }
    
    function tbDragging(tbEvent) {
        // Are we dragging?
        if (dObj.active) {
            // If we missed the event, then clear it.
            if (dfcTickID) {
                dfcClearTick();
            }
            // Get the event stuff
            if (!tbEvent) {
                evt = window.event;
            } else {
                evt = tbEvent;
            }
            cX = evt.clientX;
            // Calculate the Delta
            dPos = cX - dObj.origin;
            newX = dObj.x + dPos;
            //alert(newX);
            // Move if within bounds
            if (newX > 92) { newX = 92; }
            if (newX < 0) { newX = 0; }
            // Do we step this one?
            document.getElementById(dObj.element).style.left = newX + "px";
            // Adjust Volume of the player?
        }
    }
    
    function tbEndDrag(tbEvent) {
        // Unset the dObj
        dObj.active = false;
        // rpcPost to send the info to the browser
        
        // Restart the timer...
        dfcDelayTick(2000);
    }
        
// =====================================================================
// ===== Functions for the FlashCards
// =====================================================================
    function dfcDelayTick(dfcTickTime) {
//        if (!dfcTickID) {
            dfcTickID = setTimeout("dfcTick()", dfcTickTime);
//        }
    }
    function dfcClearTick() {
//        if (!dfcTickID) {
            clearTimeout(dfcTickID);
            dfcTickID = null;
//        }
    }
    function dfcTick() {
        if (dfcTickID) { clearTimeout(dfcTickID); dfcTickID = 0; }
        // Go do something cool... like grab the next word?
        rpcFetch('act=dfc&ibTag=2151');
    }
// =====================================================================
// ===== General Timer (Clock, IMG Preload)
// =====================================================================
    function fmTick() {
        if (tickID) { clearTimeout(tickID); tickID = 0; }
        var fmDate = new Date();
        var fmap = "a"; 
        var fmHour = fmDate.getHours()
        var fmMin = fmDate.getMinutes();
        var fmMinPre = "";
        if ( fmHour > 12 ) { fmap = "p"; fmHour = fmHour - 12; }
        if ( fmHour == 12 ) { fmap = "p"; }
        if ( fmHour == 0 ) { fmHour = 12; }
        if ( fmMin < 10 ) { fmMinPre = "0"; }
        if ( fmClkToggle ) { document.fmClock.clkColon.value = ""; } else { document.fmClock.clkColon.value = ":"; }
        fmClkToggle = !fmClkToggle;
        document.fmClock.clkHour.value = "" + fmHour;
        document.fmClock.clkMin.value = fmMinPre + fmDate.getMinutes();
        document.fmClock.clkAP.value = "" + fmap;
        tickID = setTimeout("fmTick()", 500);
        // For the "Loading..." screen
        if ((pl_max <= pl_count) && !introComplete) {
            divPreload = document.getElementById("fmPreLoad");
            divPreload.style.display = "none";
            divPreload.style.width = "0px";
            divPreload.style.height = "0px";
            document.getElementById("fmPostLoad").style.display = "block";
            introComplete = true;
    
            rpcFetch("begin");
        }
    }

// =====================================================================
// ===== Browser Initialize and Finalize stuff... FIXME!! Move Browser Ident out
// =====================================================================
    function fmFinalize() { 
        if(tickID) { 
            clearTimeout(tickID); tickID  = 0; 
        }
    }
    
    function checkIt(string) {
        place = detect.indexOf(string) + 1;
        thestring = string;
        return place;
    }
    function fmInitialize() {
        // Start Clock
        tickID = setTimeout("fmTick()", 500);
        
        getCookies();
        
        pl_max = parseInt(document.screenSetup.pl_max.value);
        nv_sizes[0] = parseInt(document.screenSetup.navACount.value);
        nv_sizes[1] = parseInt(document.screenSetup.navBCount.value);
        nv_sizes[2] = parseInt(document.screenSetup.navCCount.value);
        nv_sizes[3] = parseInt(document.screenSetup.navDCount.value);
        nv_sizes[4] = parseInt(document.screenSetup.navECount.value);
        
        // Browser Detect...
        if (checkIt("konqueror"))
        {
            browser = "Konqueror";
            OS = "Linux";
        }
        else if (checkIt("safari")) browser = "Safari"
        else if (checkIt("omniweb")) browser = "OmniWeb"
        else if (checkIt("opera")) browser = "Opera"
        else if (checkIt("webtv")) browser = "WebTV";
        else if (checkIt("icab")) browser = "iCab"
        else if (checkIt("msie")) browser = "IE"
        else if (!checkIt("compatible"))
        {
            browser = "Netscape Navigator"
            version = detect.charAt(8);
        }
        else browser = "unknown";
        
        if (!version) version = detect.charAt(place + thestring.length);
        
        if (!OS)
        {
            if (checkIt("linux")) OS = "Linux";
            else if (checkIt("x11")) OS = "Unix";
            else if (checkIt("mac")) OS = "Mac"
            else if (checkIt("win")) OS = "Windows"
            else OS = "unknown";
        }
        
        // IF Internet Exploder Change backgroundAttachment on all images...Blah...
        if (browser == "IE") {
            //window.alert("Browser ID: " + bName + " \n " + bAgent);
            var fmmain = document.getElementById("fmMainContent");
            fmmain.style.backgroundAttachment = "fixed";
            fmmain.style.height = 420;
            fmmain.style.width = 766;

        }
        
        // IF Konqueror, Change backgroundAttachment and Position.
        if (browser == "Konqueror") {
            //window.alert("Browser ID: " + bName + " \n " + bAgent);
            var fmmain = document.getElementById("fmMainContent");
            //window.alert("Position: " + fmmain.style.backgroundPosition);
            fmmain.style.backgroundAttachment = "fixed";
            fmmain.style.left = 222;
            fmmain.style.top = 128;
            var poobah = fmmain.style.left + " " + fmmain.style.top;
            fmmain.style.backgroundPosition = poobah;
        }
        do_preload();
    }
    
// =====================================================================
// ===== Part of the IMG Preload
// =====================================================================
    function fm_preload(obj) {
        if (introLoading) {
            document.getElementById("pl_bar"+pl_count).style.backgroundColor = "#4040FF";
            pl_count++;
        }
    }

// =====================================================================
// ===== Mouseover Image Changing stuff
// =====================================================================
    function fm_setbackgrounds() {
        for (i=0;i<4;i++) {
            if (nv_selected[i]) { 
                if (document.getElementById(nv_names[i]).style.background != "url('images/sc_nav_s"+nv_places[i]+".jpg')") {
                    document.getElementById(nv_names[i]).style.background = "url('images/sc_nav_s"+nv_places[i]+".jpg')"; 
                }
//                 if (document.getElementById("fmMainContent").style.background != "url('images/sc_main_"+i+".jpg')") { 
//                     document.getElementById("fmMainContent").style.background = "url('images/sc_main_"+i+".jpg')";
//                     if (browser == "IE") { document.getElementById("fmMainContent").style.backgroundAttachment = "fixed"; }
//                     
//                 }
                if (document.getElementById(nv_names[i] + "T").src != "images/sc_nav_text-s"+(i+1)+".gif") {
                    document.getElementById(nv_names[i] + "T").src = "images/sc_nav_text-s"+(i+1)+".gif";
                }
            } else {
                if (document.getElementById(nv_names[i]).style.background != "url('images/sc_nav_"+nv_places[i]+".jpg')") {
                    document.getElementById(nv_names[i]).style.background = "url('images/sc_nav_"+nv_places[i]+".jpg')"; 
                }
                if (document.getElementById(nv_names[i] + "T").src != "images/sc_nav_text-"+(i+1)+".gif") {
                    document.getElementById(nv_names[i] + "T").src = "images/sc_nav_text-"+(i+1)+".gif";
                }
            }
        }
    }
    
    function fm_open(fmitem) {
        
        document.getElementById(nv_names[fmitem] + "Sub").style.display = "block";
        document.getElementById(nv_names[fmitem] + "P").src = "images/sc_nav_minus.gif";
        for (i=fmitem+1;i<4;i++) {
            nv_places[i] += nv_sizes[fmitem];
        }
        nv_open[fmitem] = true;
        nv_selected[0] = false; nv_selected[1] = false; nv_selected[2] = false; nv_selected[3] = false; nv_selected[4] = false;
        nv_selected[fmitem] = true;
    }
    
    function fm_close(fmitem) {
        document.getElementById(nv_names[fmitem] + "Sub").style.display = "none";
        document.getElementById(nv_names[fmitem] + "P").src = "images/sc_nav_plus.gif";
        
        for (i=fmitem+1;i<4;i++) {
            nv_places[i] -= nv_sizes[fmitem];
        }
        nv_selected[0] = false; nv_selected[1] = false; nv_selected[2] = false; nv_selected[3] = false; nv_selected[4] = false;
        nv_open[fmitem] = false;
    }
    
    function fm_toggle(fmitem,fmtag) { // Number , "open"/"close"
        
        if (!nv_selected[fmitem] && !nv_open[fmitem]) { // open and select
            fm_open(fmitem);
        } else if (nv_selected[fmitem] && nv_open[fmitem]) { // close
            fm_close(fmitem);
        } else if (!nv_selected[fmitem] && nv_open[fmitem]) { // select
            nv_selected[0] = false; nv_selected[1] = false; nv_selected[2] = false; nv_selected[3] = false; nv_selected[4] = false;
            nv_selected[fmitem] = true; //  
        } else if (nv_selected[fmitem] && !nv_open[fmitem]) { // open
            fm_open(fmitem);
        }
        fm_setbackgrounds();
    //     var myDiv = document.getElementById("iFMct").firstChild;
    //     myDiv.data = "This is menu item #" + fmitem;
        // RPC code to transfer data from the server to the client and display in the main content area
        rpcFetch('act=html&ibTag='+fmtag);
        //help();
    }
    
    function fm_highlight(fmitem) {
        if (document.getElementById(nv_names[fmitem]).style.background != "url('images/sc_nav_h"+nv_places[fmitem]+".jpg')") {
            document.getElementById(nv_names[fmitem]).style.background = "url('images/sc_nav_h"+nv_places[fmitem]+".jpg')"; 
        }
        if (document.getElementById(nv_names[fmitem] + "T").src != "images/sc_nav_text-s"+(fmitem+1)+".gif") {
            document.getElementById(nv_names[fmitem] + "T").src = "images/sc_nav_text-s"+(fmitem+1)+".gif";
        }
    }
    
    function fm_unhighlight(fmitem) {
        if (nv_selected[fmitem]) {
            document.getElementById(nv_names[fmitem]).style.background = "url('images/sc_nav_s"+nv_places[fmitem]+".jpg')"; 
            document.getElementById(nv_names[fmitem] + "T").src = "images/sc_nav_text-s"+(fmitem+1)+".gif";
        } else {
            document.getElementById(nv_names[fmitem]).style.background = "url('images/sc_nav_"+nv_places[fmitem]+".jpg')"; 
            document.getElementById(nv_names[fmitem] + "T").src = "images/sc_nav_text-"+(fmitem+1)+".gif";
        }
    }
    
    function fm_subhl(bitem) {
        document.getElementById("sub" + bitem).style.color = "#008000";
        document.getElementById("sub" + bitem).style.textDecoration = "underline";
        document.getElementById("bullet" + bitem).src = "images/sc_nav_hb3.gif";
    }

    function fm_subuhl(bitem) {
        document.getElementById("sub" + bitem).style.color = "#000000";
        document.getElementById("sub" + bitem).style.textDecoration = "none";
        document.getElementById("bullet" + bitem).src = "images/sc_nav_b3.gif";
    }
    
// =====================================================================
// ===== Help Subsystem  FIXME!! Move to seperate Unit.
// =====================================================================
    var resdata = "";
    
    function help(section) {
        // Debug Code - kinda
        fmDebugCount++;
        var db_sizes = " Sizes: " + nv_sizes[0] + ", " + nv_sizes[1] + ", " + nv_sizes[2] + ", " + nv_sizes[3];
        var db_open = " Open: " + nv_open[0] + ", " + nv_open[1] + ", " + nv_open[2] + ", " + nv_open[3];
        var db_selected = " Selected: " + nv_selected[0] + ", " + nv_selected[1] + ", " + nv_selected[2] + ", " + nv_selected[3];
        var db_places = " Places: " + nv_places[0] + ", " + nv_places[1] + ", " + nv_places[2] + ", " + nv_places[3];
        var db_out = "Debug "+fmDebugCount+"\n::" + db_sizes + "\n::" + db_open + "\n::" + db_selected + "\n::" + db_places + "\n:: " + "url('images/sc_nav_"+nv_places[1]+".jpg')";
        //document.getElementById("fmjsrpc").src = "jsrpc.php?act=" + fmitem;
        //var cookieinfo = "";
        //getCookies();
        //for (var prop in cookies) {
            //indentText = "";
            //for (i=0;i<indent;i++) {
            //    indentText += "|-";
            //}
        //    cookieinfo = cookies.nodeName + "['" + prop + "'] = " + cookies[prop] + "<BR />";
        //}
        //var payload = cookies["fmRPC_Payload"];
        //var obj = document.getElementById("fmMainContent").firstChild;
        //obj.data = document.getElementById("fmjsrpc").src;
        //obj.data = fmrpcPayload;
        
        //alert(fmrpcPayload);
        if (section == "DOM") {
            DOMViewerObj=null;
            window.open('domviewer.html');
            return false;
        }
        if (section == "tree") {
            //DOM Tree
            tree_walk.resdata = "<PRE>";
            tree_walk(document,0);
            tree_walk.resdata += "</PRE>";
            fmrpcPayload = tree_walk.resdata;
            
            var obj = document.getElementById("fmMainContent");
            oldHTML = document.getElementById("rpcResult");
            if (oldHTML) {
                oldParent = oldHTML.parentNode;
                oldParent.removeChild(oldHTML);
                oldHTML = null;
            }
            newHTML = document.createElement('div');
            newHTML.setAttribute('id','rpcResult');
            newHTML.setAttribute('class','divResult');
            newText = document.createTextNode("Loading...");
            newHTML.appendChild(newText);
            newHTML.innerHTML = fmrpcPayload;
            
            obj.appendChild(newHTML);            
        }
    }


function tree_walk(t_obj,depth) {
    if (depth < 3) {
        for (i=0;i<t_obj.childNodes.length;i++) {
            for (l=0;l<depth;l++) { tree_walk.resdata += "-"; }
            tree_walk.resdata += t_obj.childNodes[i].nodeName + " [" + t_obj.childNodes[i].nodeType + "]<BR>";
            tree_walk(t_obj.childNodes[i],depth+1);
        }
    }
}

function test123() {
    var bParent = document.getElementById("bannerText");
    if (bParent) {
        var bContent = document.getElementById("bannerContent");
        if (bContent) {
            oldParent = bContent.parentNode;
            oldParent.removeChild(bContent);
            bContent = null;
        } else {
            alert('Cannot Find ID:bannerContent!');
        }
        nContent = document.createElement('div');
        nContent.setAttribute('id','bannerContent');
        newText = document.createTextNode("Processing...");
        nContent.appendChild(newText);
        nContent.innerHTML = "Blah Blah Blah";
        bParent.appendChild(nContent);
    } else {
        alert('Cannot Find ID:bannerText!');
    }

}