<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

require_once("db.php");
require_once("session.php");
require_once("prg_account.php");

$MySession = new fmSession;
$fmSession = $MySession->init('FMC_Debug');
$PrgSession = new fmSession;
$pSession = $PrgSession->init('FMC_FlashCard');

$MyAccount = new fmAccount;
$MyAccount->init($PrgSession->getID());

$servertime = strftime("%Y-%m-%d %T");

// Parse Form Data
    if (isset($_REQUEST['fmprg_command'])) {
        if ($_REQUEST['fmprg_command'] == 'acct_login') {
            // Login Request
            $email = $_REQUEST['log_email'];
            $pass = $_REQUEST['log_pass'];
            $loginres = $MyAccount->login($email,$pass,$MySession->getID());
            echo "Login: $loginres from $email with password $pass";
        }
        if ($_REQUEST['fmprg_command'] == 'acct_logout') {
            // Logout Request
            $loginres = $MyAccount->logout($email,$pass,$MySession->getID());
        }
        if ($_REQUEST['fmprg_command'] == 'debug_options') {
            // Debugging Options
            if ($_REQUEST['dbg_Account'] == 'on') { $MySession->post('/debug/show/account=true');} else { $dbg_Account_s = ''; $MySession->post('/debug/show/account=false');}
            if ($_REQUEST['dbg_Session'] == 'on') { $MySession->post('/debug/show/session=true');} else { $dbg_Session_s = ''; $MySession->post('/debug/show/session=false');}
            if ($_REQUEST['dbg_MySql'] == 'on') { $MySession->post('/debug/show/mysql=true');} else { $dbg_MySql_s = ''; $MySession->post('/debug/show/mysql=false');}
            if ($_REQUEST['dbg_Cookies'] == 'on') { $MySession->post('/debug/show/cookies=true');} else { $dbg_MySql_s = ''; $MySession->post('/debug/show/cookies=false');}
            if ($_REQUEST['dbg_s_debug'] == 'on') { $MySession->post('/debug/show/s_debug=true');} else { $dbg_s_debug_s = ''; $MySession->post('/debug/show/s_debug=false');}
            if ($_REQUEST['dbg_s_flash'] == 'on') { $MySession->post('/debug/show/s_flash=true');} else { $dbg_s_flash_s = ''; $MySession->post('/debug/show/s_flash=false');}
            if ($_REQUEST['dbg_s_detail'] == 'on') { $MySession->post('/debug/show/s_detail=true');} else { $dbg_s_detail_s = ''; $MySession->post('/debug/show/s_detail=false');}
            $MySession->save();
        }
    }

// Testing and Setup...
    if ($MySession->get('/debug/show/account')) { $dbg_Account_s = " CHECKED"; }
    if ($MySession->get('/debug/show/session')) { $dbg_Session_s = " CHECKED"; }
    if ($MySession->get('/debug/show/mysql')) { $dbg_MySql_s = " CHECKED"; }
    if ($MySession->get('/debug/show/cookies')) { $dbg_Cookies_s = " CHECKED"; }
    if ($MySession->get('/debug/show/s_debug')) { $dbg_s_debug_s = " CHECKED"; }
    if ($MySession->get('/debug/show/s_flash')) { $dbg_s_flash_s = " CHECKED"; }
    if ($MySession->get('/debug/show/s_detail')) { $dbg_s_detail_s = " CHECKED"; }



// Heading
    echo "<BODY BGCOLOR=#FFFFFF>";

// Debugging Chooser
    $cspan = 1;
    echo "<FORM ACTION=debug.php METHOD=POST NAME=DebugOpt>";
    echo "<TABLE WIDTH=95% CELLPADDING=0 CELLSPACING=2 BORDER=0>";
    echo "<TR>";
    echo "<TD BGCOLOR=#505080><FONT COLOR=white>Debugging Options</FONT></TD>";
    if ($MySession->get('/debug/show/account')) {
        echo "<TD BGCOLOR=#805050><FONT COLOR=white>Account Options</FONT></TD>";
        $cspan++;
    }
    if ($MySession->get('/debug/show/session')) {
        echo "<TD BGCOLOR=#505080><FONT COLOR=white>Session Options</FONT></TD>";
        $cspan++;
    }
    if ($MySession->get('/debug/show/mysql')) {
        echo "<TD BGCOLOR=#505080><FONT COLOR=white>MySql Options</FONT></TD>";
        $cspan++;
    }
    if ($MySession->get('/debug/show/cookies')) {
        echo "<TD BGCOLOR=#505080><FONT COLOR=white>Cookies</FONT></TD>";
        $cspan++;
    }
    echo "</TR>";
    echo "<TR>";
    echo "<TD BGCOLOR=#E0D0FF>";
//     echo "<B>Debug Options</B><BR>";
    // Debugging Options
    echo "<INPUT TYPE=HIDDEN NAME=fmprg_command VALUE=\"debug_options\">";
    echo "<INPUT TYPE=CHECKBOX NAME=dbg_Account $dbg_Account_s>Account<BR>";
    echo "<INPUT TYPE=CHECKBOX NAME=dbg_Session $dbg_Session_s>Session<BR>";
    echo "<INPUT TYPE=CHECKBOX NAME=dbg_MySql $dbg_MySql_s>MySql<BR>";
    echo "<INPUT TYPE=CHECKBOX NAME=dbg_Cookies $dbg_Cookies_s>Cookies<BR>";
    echo "</TD>";
    if ($MySession->get('/debug/show/account')) {
        echo "<TD BGCOLOR=#FFD0E0 VALIGN=TOP>";
        //echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_debug $dbg_s_debug_s>FMC_Debug<BR>";
        //echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_flash $dbg_s_flash_s>FMC_FlashCard<BR>";
        echo "</TD>";
    }
    if ($MySession->get('/debug/show/session')) {
        echo "<TD BGCOLOR=#E0D0FF VALIGN=TOP>";
        echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_debug $dbg_s_debug_s>FMC_Debug<BR>";
        echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_flash $dbg_s_flash_s>FMC_FlashCard";
        echo "<HR>";
        echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_detail $dbg_s_detail_s>Session Detail<BR>";
        echo "</TD>";
    }
    if ($MySession->get('/debug/show/mysql')) {
        echo "<TD BGCOLOR=#E0D0FF VALIGN=TOP>";
        //echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_debug $dbg_s_debug_s>FMC_Debug<BR>";
        //echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_flash $dbg_s_flash_s>FMC_FlashCard<BR>";
        echo "</TD>";
    }
    if ($MySession->get('/debug/show/cookies')) {
        echo "<TD BGCOLOR=#E0D0FF VALIGN=TOP>";
        //echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_debug $dbg_s_debug_s>FMC_Debug<BR>";
        //echo "<INPUT TYPE=CHECKBOX NAME=dbg_s_flash $dbg_s_flash_s>FMC_FlashCard<BR>";
        echo "</TD>";
    }
    echo "</TR>";
    echo "<TR>";
    echo "<TD BGCOLOR=white COLSPAN=$cspan ALIGN=RIGHT>";
    echo "<INPUT CLASS=fmbutton1 TYPE=SUBMIT VALUE=\"&nbsp;Update&nbsp;\"></TD>";
    echo "</TD>";
    echo "</TR>";
    echo "</FORM>";

// Debugging Printouts
// Account Stuff...

    echo "<TABLE WIDTH=95% CELLPADDING=0 CELLSPACING=0 BORDER=0>";
    if ($MySession->get('/debug/show/account')) {
        echo '<TR><TD BGCOLOR=#805050><FONT COLOR=white>Account Information</FONT></TD></TR>';
        echo '<TR>';
        echo '<TD BGCOLOR=#FFD0E0 VALIGN=TOP>';
        echo '<DIV STYLE="font-family:arial,tahoma,verdana;font-size: 10pt;border-top: solid 1px #602020;border-left:  solid 1px #602020;border-right:  solid 1px #602020;border-bottom: solid 1px #602020; padding: 8px 8px 8px 8px; margin: 4px 4px 4px 4px;">';
        if ($MyAccount->is_guest()) { echo '<B>--Guest--</B><BR>'; } else  { echo '<B>Registered</B><BR>'; }
        echo "<B>Account ID:</B> {$MyAccount->_Account}<BR>";
        echo '<B>Program Rights:</B> ';
        if ($MyAccount->req_access(1) == "G") { echo 'Spanish<B>(1)</B>'; }
        if ($MyAccount->req_access(2) == "G") { echo ', French<B>(2)</B> '; }
        if ($MyAccount->req_access(3) == "G") { echo ', German<B>(3)</B> '; }
        if ($MyAccount->req_access(4) == "G") { echo ', Russian<B>(4)</B> '; }
        if ($MyAccount->req_access(5) == "G") { echo ', Italian<B>(5)</B> '; }
        echo '<BR>';
        echo '<B>Admin Rights:</B> ';
        if ($MyAccount->req_access(36) == "G") { echo 'Account<B>(36)</B>'; }
        if ($MyAccount->req_access(37) == "G") { echo ', Program<B>(37)</B> '; }
        if ($MyAccount->req_access(38) == "G") { echo ', Log<B>(38)</B> '; }
        if ($MyAccount->req_access(39) == "G") { echo ', Admin<B>(39)</B> '; }
        if ($MyAccount->req_access(40) == "G") { echo ', DB<B>(40)</B> '; }
        echo '<BR>';
        echo '<B>Account Info:</B><DIV STYLE="font-family:courier,lucida console,terminal;color: black;font-size: 8pt;background-color: #E0B0C0;border-top: solid 1px #602020;border-left:  solid 1px #602020;border-right:  solid 1px #602020;border-bottom: solid 1px #602020; padding: 8px 8px 8px 8px;margin: 16px 16px 16px 16px"><PRE>';
        echo '[FName]: ' . $MyAccount->getinfo('FName') . "\n";
        echo '[LName]: ' . $MyAccount->getinfo('LName') . "\n";
        echo '[email]: ' . $MyAccount->getinfo('email');
        echo "[DEBUGGING ACCOUNT]: {$MyAccount->$_Debug}\n" . '</PRE></DIV>';
        echo '</DIV>';
        echo "</TD>";
        echo "</TR>";
    }
    if ($MySession->get('/debug/show/session')) {
        echo "<TR><TD BGCOLOR=#505080><FONT COLOR=white>Session Debugging</FONT></TD></TR>";
        echo "<TR>";
        echo "<TD BGCOLOR=#E0D0FF VALIGN=TOP>";
        echo "<B>Your Browser:</B> {$_SERVER['REMOTE_ADDR']}<BR>";
        echo "<B>Your IP:</B> {$_SERVER['HTTP_USER_AGENT']}<BR>";
        echo "<BR>";

        if ($MySession->get('/debug/show/s_debug')) {
            // Already initialized (were using it)
            $data = base64_encode(serialize($MySession->_SessionData));
            $dataraw = $MySession->_SessionData;
            $datalen = strlen($data);
            echo "<B>Cookie Name:</B> {$MySession->_CookieName}<BR>";
            echo '<DIV STYLE="font-family:arial,tahoma,verdana;font-size: 10pt;border-top: solid 1px #202060;border-left:  solid 1px #202060;border-right:  solid 1px #202060;border-bottom: solid 1px #202060; padding: 8px 8px 8px 8px; margin: 4px 4px 4px 4px;">';
            echo "<B>Session ID:</B> {$MySession->_SessionID}<BR>";
            echo "<B>Server:</B> {$MySession->_Server}<BR>";
            echo "<B>Client Browser:</B> {$MySession->_UserAgent}<BR>";
            echo "<B>Client IP:</B> {$MySession->_UserIP}<BR>";
            echo "<B>Session Data Length:</B> Base64 Length is <B>$datalen</B><BR>";
            if ($MySession->get('/debug/show/s_detail')) {
                echo '<B>Session Data:</B><DIV STYLE="font-family:courier,lucida console,terminal;color: black;font-size: 8pt;background-color: #C0B0E0;border-top: solid 1px #202060;border-left:  solid 1px #202060;border-right:  solid 1px #202060;border-bottom: solid 1px #202060; padding: 8px 8px 8px 8px;margin: 16px 16px 16px 16px"><PRE>';
                print_r($dataraw);
                echo "</PRE></DIV>";
            }
            echo "</DIV>";
        }
        if ($MySession->get('/debug/show/s_flash')) {
            $data = base64_encode(serialize($PrgSession->_SessionData));
            $dataraw = $PrgSession->_SessionData;
            $datalen = strlen($data);
            echo "<B>Cookie Name:</B> {$PrgSession->_CookieName}<BR>";
            echo '<DIV STYLE="font-family:arial,tahoma,verdana;font-size: 10pt;border-top: solid 1px #202060;border-left:  solid 1px #202060;border-right:  solid 1px #202060;border-bottom: solid 1px #202060; padding: 8px 8px 8px 8px;margin: 4px 4px 4px 4px;">';
            echo "<B>Session ID:</B> {$PrgSession->_SessionID}<BR>";
            echo "<B>Server:</B> {$PrgSession->_Server}<BR>";
            echo "<B>Client Browser:</B> {$PrgSession->_UserAgent}<BR>";
            echo "<B>Client IP:</B> {$PrgSession->_UserIP}<BR>";
            echo "<B>Session Data Length:</B> Base64 Length is <B>$datalen</B><BR>";
            if ($MySession->get('/debug/show/s_detail')) {
                echo '<B>Session Data:<DIV STYLE="font-family:courier,lucida console,terminal;color: black;font-size: 8pt;background-color: #C0B0E0;border-top: solid 1px #202060;border-left:  solid 1px #202060;border-right:  solid 1px #202060;border-bottom: solid 1px #202060; padding: 8px 8px 8px 8px;margin: 16px 16px 16px 16px"><PRE></B><BR>';
                print_r($dataraw);
                echo "</PRE></DIV></FONT>";
            }
            echo "</DIV>";
        }

        echo "</TD>";
        echo "</TR>";
        echo "</TABLE>";
    }
    if ($MySession->get('/debug/show/mysql')) {
        $did = $MySession->getID();
        $pid = $PrgSession->getID();
        echo "<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0>";
        echo "<TR><TD BGCOLOR=#508060><FONT COLOR=white>MySQL Transactions</FONT></TD></TR>";
        echo "<TR>";
        echo "<TD BGCOLOR=#E0FFD0 VALIGN=TOP>";
        echo '<DIV STYLE="font-family:arial,tahoma,verdana;font-size: 10pt;border-top: solid 1px #206020;border-left:  solid 1px #206020;border-right:  solid 1px #206020;border-bottom: solid 1px #206020; padding: 8px 8px 8px 8px;margin: 4px 4px 4px 4px;">';
        printf("<B>MySQL Client Version:</B> %s<BR>", mysqli_get_client_info());
        printf("<B>MySQL Server Version:</B> %s<BR>", ((is_null($___mysqli_res = mysqli_get_server_info($db_link))) ? false : $___mysqli_res));
        printf("<B>MySQL Host Info:</B> %s<BR>", ((is_null($___mysqli_res = mysqli_get_host_info($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res));
        $status = explode('  ', mysqli_stat($db_link));
        echo '<B>MySQL Stats:</B><DIV STYLE="font-family:courier,lucida console,terminal;color: black;font-size: 8pt;background-color: #B0E0C0;border-top: solid 1px #206020;border-left:  solid 1px #206020;border-right:  solid 1px #206020;border-bottom: solid 1px #206020; padding: 8px 8px 8px 8px;margin: 16px 16px 16px 16px"><PRE>';
        print_r($status);
        echo '</PRE></DIV>';
        // Query on session and on account...
        if ($MySession->get('/debug/show/s_flash')) {
            $fetch_sql = "SELECT * FROM 5mc_dfc.Session WHERE sExpires > now();";
            $result = mysqli_query($db_link, $fetch_sql);
            $datac = mysqli_num_rows($result);
            if ($datac > 0) {
                $data = mysqli_fetch_assoc($result);
                echo "<B>Session Database:</B> $pid - ($datac)<DIV STYLE=\"font-family:courier,lucida console,terminal;color: black;font-size: 8pt;background-color: #B0E0C0;border-top: solid 1px #206020;border-left:  solid 1px #206020;border-right:  solid 1px #206020;border-bottom: solid 1px #206020; padding: 8px 8px 8px 8px;margin: 16px 16px 16px 16px\">";
                echo '<TABLE BGCOLOR=black BORDER=0 CELLPADDING=2 CELLSPACING=1 WIDTH=95%>';
                echo '<TR>';
                foreach ($data as $field => $cell) {
                    echo "<TD BGCOLOR=#B0E0C0><B>$field</B></TD>";
                }
                echo '</TR>';
                echo '<TR>';
                foreach ($data as $field => $cell) {
                    if ($field == 'sSessionData') { $cell = 'Length: ' . strlen($cell); }
                    $append = '';
                    if ($field == 'sExpires') { if (strtotime($cell) < time()) { $append=' COLOR=red'; } }
                    if ($field == 'sSessionID') { if ($cell == $pid) { $append=' COLOR=blue'; } }
                    if ($field == 'sSessionID') { if ($cell == $did) { $append=' COLOR=green'; } }
                    echo "<TD BGCOLOR=#B0E0C0 VALIGN=TOP><FONT SIZE=2$append>$cell</FONT></B></TD>";
                }
                echo '</TR>';
                while ($data = mysqli_fetch_assoc($result)) {
                    echo '<TR>';
                    foreach ($data as $field => $cell) {
                        if ($field == 'sSessionData') { $cell = 'Length: ' . strlen($cell); }
                        $append = '';
                        if ($field == 'sExpires') { if (strtotime($cell) < time()) { $append=' COLOR=red'; } }
                        if ($field == 'sSessionID') { if ($cell == $pid) { $append=' COLOR=blue'; } }
                        if ($field == 'sSessionID') { if ($cell == $did) { $append=' COLOR=green'; } }
                        echo "<TD BGCOLOR=#B0E0C0 VALIGN=TOP><FONT SIZE=2$append>$cell</FONT></B></TD>";
                    }
                    echo '</TR>';
                }
                echo '</TABLE></DIV>';
            } else {
                echo "<B>Program Session:</B> $pid - ($datac)<BR>";
            }
            // Account Database
            $fetch_sql = "SELECT * FROM 5mc_dfc.Account LIMIT 25";
            $result = mysqli_query($db_link, $fetch_sql);
            $datac = mysqli_num_rows($result);
            if ($datac > 0) {
                $data = mysqli_fetch_assoc($result);
                echo "<B>Account Database:</B> ($datac)<DIV STYLE=\"font-family:courier,lucida console,terminal;color: black;font-size: 8pt;background-color: #B0E0C0;border-top: solid 1px #206020;border-left:  solid 1px #206020;border-right:  solid 1px #206020;border-bottom: solid 1px #206020; padding: 8px 8px 8px 8px;margin: 16px 16px 16px 16px\">";
                echo '<TABLE BGCOLOR=black BORDER=0 CELLPADDING=2 CELLSPACING=1 WIDTH=95%>';
                echo '<TR>';
                $ar = array('uEmail','uCreated','uAccessed','uVisits','uSHA1','uSessionID');
                foreach ($ar as $field) {
                    echo "<TD BGCOLOR=#B0E0C0><B>$field</B></TD>";
                }
                echo '</TR>';
                echo '<TR>';
                foreach ($ar as $field) {
                    if ($field == 'sSessionData') { $cell = 'Length: ' . strlen($cell); }
                    $append = '';
                    if ($field == 'uSHA1') { if ($data[$field] == $MyAccount->getID()) {$append = ' COLOR=navy'; } }
                    if ($field == 'uSessionID') { if ($data[$field] == $pid) { $append=' COLOR=blue'; } }
                    if ($field == 'uSessionID') { if ($data[$field] == $did) { $append=' COLOR=green'; } }
                    echo "<TD BGCOLOR=#B0E0C0 VALIGN=TOP><FONT SIZE=2$append>{$data[$field]}</FONT></B></TD>";
                }
                echo '</TR>';
                while ($data = mysqli_fetch_assoc($result)) {
                    echo '<TR>';
                    foreach ($ar as $field) {
                        if ($field == 'sSessionData') { $cell = 'Length: ' . strlen($cell); }
                        $append = '';
                        if ($field == 'uSHA1') { if ($data[$field] == $MyAccount->getID()) {$append = ' COLOR=navy'; } }
                        if ($field == 'uSessionID') { if ($data[$field] == $pid) { $append=' COLOR=blue'; } }
                        if ($field == 'uSessionID') { if ($data[$field] == $did) { $append=' COLOR=green'; } }
                        echo "<TD BGCOLOR=#B0E0C0 VALIGN=TOP><FONT SIZE=2$append>{$data[$field]}</FONT></B></TD>";
                    }
                    echo '</TR>';
                }
                echo '</TABLE></DIV>';
            } else {
                echo "<B>Account :</B> $pid - ($datac)<BR>";
            }
            // Lesson Database
            $fetch_sql = "SELECT * FROM 5mc_dfc.Lesson";
            $result = mysqli_query($db_link, $fetch_sql);
            $datac = mysqli_num_rows($result);
            if ($datac > 0) {
                $data = mysqli_fetch_assoc($result);
                echo "<B>Lesson Database:</B> ($datac)<DIV STYLE=\"font-family:courier,lucida console,terminal;color: black;font-size: 8pt;background-color: #B0E0C0;border-top: solid 1px #206020;border-left:  solid 1px #206020;border-right:  solid 1px #206020;border-bottom: solid 1px #206020; padding: 8px 8px 8px 8px;margin: 16px 16px 16px 16px\">";
                echo '<TABLE BGCOLOR=black BORDER=0 CELLPADDING=2 CELLSPACING=1 WIDTH=95%>';
                echo '<TR>';
                $ar = array('uEmail','uCreated','uAccessed','uVisits','uSHA1','uSessionID');
                foreach ($data as $field => $cell) {
                    echo "<TD BGCOLOR=#B0E0C0><B>$field</B></TD>";
                }
                echo '</TR>';
                echo '<TR>';
                foreach ($data as $field => $cell) {
                    $append = '';
                    if ($field == 'lOwner') { if ($cell == $MyAccount->getID()) {$append = ' COLOR=navy'; } }
                    if ($field == 'lFile') {
                        $words = unserialize(base64_decode($cell));
                        $cell = '<PRE>' . print_r($words,true) . '</PRE>';
                    }
                    echo "<TD BGCOLOR=#B0E0C0 VALIGN=TOP><FONT SIZE=2$append>$cell</FONT></B></TD>";
                }
                echo '</TR>';
                while ($data = mysqli_fetch_assoc($result)) {
                    echo '<TR>';
                    foreach ($data as $field => $cell) {
                        $append = '';
                        if ($field == 'lOwner') { if ($cell == $MyAccount->getID()) {$append = ' COLOR=navy'; } }
                        if ($field == 'lFile') {
                            $words = unserialize(base64_decode($cell));
                            $cell = '<PRE>' . print_r($words,true) . '</PRE>';
                        }
                        echo "<TD BGCOLOR=#B0E0C0 VALIGN=TOP><FONT SIZE=2$append>$cell</FONT></B></TD>";
                    }
                    echo '</TR>';
                }
                echo '</TABLE></DIV>';
            } else {
                echo "<B>Lesson Database:</B> $pid - ($datac)<BR>";
            }
        }
        echo '</DIV>';
        echo "</TD>";
        echo "</TR>";
    }
    if ($MySession->get('/debug/show/cookies')) {
        echo "<TR><TD BGCOLOR=#505080><FONT COLOR=white>Cookies</FONT></TD></TR>";
        echo "<TR>";
        echo "<TD>";
        foreach ($_COOKIE as $cookie => $data) {
            echo "<B>\$_COOKIE['$cookie']</B><BR>";
        }
        echo "<PRE>";
        print_r($_COOKIE);
//         print_r($_SERVER['HTTP_REFERER']);
//         print_r($_ENV);
//         var_dump($HTTP_COOKIE_VARS);
        echo "</PRE>";
        echo "</TD>";
        echo "</TR>";
    }
    echo "<TR><TD BGCOLOR=#505080><FONT COLOR=white>-- End Of Line --</FONT></TD></TR>";
    echo "</TABLE>";

    //echo base64_decode('YTo1OntzOjU6IndvcmRzIjthOjc6e2k6MDtzOjU3OiIyfGQwY2UzMTRlNzEwMjIzYzA3MmIxMzdiZDMwZTJjNTI3NDJjYjlhMDl8U3VuZGF5fGRvbWluZ28iO2k6MTtzOjU1OiIyfDg2MjQyYjQxM2M3NWYxZDk1NTVhZjdhMzZiMDVjNjBkNjJmYjdlMmF8TW9uZGF5fGx1bmVzIjtpOjI7czo1NzoiMnxmZjc3M2I5YmI1MGM5MzlhOWY5ODI2ZjVlMGU3NmZkNGZjZTc2Y2Y5fFR1ZXNkYXl8bWFydGVzIjtpOjM7czo2MjoiMnxjNGQyZGYwMzVhZWFiMGU1MGUzMDQ5OGE0MDk3NTM4ZWMxZTE2ZDY1fFdlZG5lc2RheXxtaelyY29sZXMiO2k6NDtzOjU4OiIyfDJhZGNhNTUyYmRhODFhM2I3MTQxMzkyNmM5ODA4NDI4NjIyYzA1Njh8VGh1cnNkYXl8anVldmVzIjtpOjU7czo1NzoiMnxkNTljMGE5YmYyOTUyY2RlODZmMDU2YTIzOGI5NzFhMjc1MGU4ODZjfEZyaWRheXx2aWVybmVzIjtpOjY7czo1ODoiMnw0ODM0ZjUxMDY0MjQ5MjM0YTc0NjZkMWU0Y2ZiODYxMjFmNDQ2YWMyfFNhdHVyZGF5fHPhYmFkbyI7fXM6ODoibGFuZ3VhZ2UiO2k6MjtzOjQ6Im5hbWUiO047czozOiJwb3MiO2k6MDtzOjY6InJlcGVhdCI7aToxO30=');
?>
