<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//


// Are we logged in? How did they get here?

// Did the form get submitted?
$justMadeAccount = false;
if (isset($_REQUEST['newAccount'])) {
    // Process the form information
    // Check for errors in the registration fields.
    $na_email = trim($_REQUEST['na_email']);
    $na_pass1 = trim($_REQUEST['na_pass1']);
    $na_pass2 = trim($_REQUEST['na_pass2']);
    $na_fname = trim($_REQUEST['na_fname']);
    $na_lname = trim($_REQUEST['na_lname']);
    $na_addr1 = trim($_REQUEST['na_addr1']);
    $na_addr2 = trim($_REQUEST['na_addr2']);
    $na_city = trim($_REQUEST['na_city']);
    $na_state = trim($_REQUEST['na_state']);
    $na_zip = trim($_REQUEST['na_zip']);
    $na_ctry = trim($_REQUEST['na_ctry']);
    // if ctry == US then check state, else an option list ?
    $na_valid = true;
    // regex check email
    $preg_email = '/^([-\w]+)@[\w\.]+\.[\w]+$/i';
    $preg_zip = '/^[0-9]{5}([-]{1}[0-9]{4})?$/';
    // E-Mail
    if (!check_email_address($na_email)) {
        // Invalid Email...
        $na_valid = false;
        $na_message .= ($na_email == '') ? '<B>E-Mail</B>: You must enter an e-mail address.<BR><BR>' : '<B>E-Mail</B>: The e-mail address you entered is not a valid e-mail address.<BR><BR>';
        $nav_email = 'color:#E00000';
    }
    // verify password...
		$nav_pass1 = '';
		$nav_pass2 = '';
    if ($na_pass1 != $na_pass2) {
        // Passwords don't match...
        $na_valid = false;
        $na_message .= '<B>Password</B>: The passwords you entered do not match.<BR><BR>';
        $nav_pass1 = 'color:#E00000';
        $nav_pass2 = 'color:#E00000';
        $na_pass1 = '';
        $na_pass2 = '';
    }
    if (strlen($na_pass1) < 8) {
        // Short Password...
        $na_valid = false;
        $na_message .= (($na_pass1 == '') or ($na_pass2 == '')) ? '<B>Password</B>: You must enter and confirm a password for your account.<BR><BR>' : '<B>Password</B>: Please use a password 8-20 characters long.<BR><BR>';
        $nav_pass1 = 'color:#E00000';
        $nav_pass2 = 'color:#E00000';
    }
		if(!isset($na_message)){ $na_message = ''; }
		if(!isset($nav_fname)){ $nav_fname = ''; }
		if(!isset($nav_lname)){ $nav_lname = ''; }
		if(!isset($nav_addr1)){ $nav_addr1 = ''; }
		if(!isset($nav_city)){ $nav_city = ''; }
    $na_message .= ($na_fname == '') ? '<B>First Name</B>: You must enter your first name.<BR><BR>' : '';
    $nav_fname .= ($na_fname == '') ? 'color:#E00000' : '';
    $na_message .= ($na_lname == '') ? '<B>Last Name</B>: You must enter your last name.<BR><BR>' : '';
    $nav_lname .= ($na_lname == '') ? 'color:#E00000' : '';
    $na_message .= ($na_addr1 == '') ? '<B>Address</B>: You must enter your address.<BR><BR>' : '';
    $nav_addr1 .= ($na_addr1 == '') ? 'color:#E00000' : '';
    $na_message .= ($na_city == '') ? '<B>City</B>: You must enter your city.<BR><BR>' : '';
    $nav_city .= ($na_city == '') ? 'color:#E00000' : '';
    if ($na_ctry == 'US') {
        // Got a US Customer... validate.
        if (strlen($na_state) != 2) {
            // Invalid State .. Check names?
            $na_valid = false;
            $na_message .= '<B>State</B>: Please use a 2-letter state such as &quot;MT&quot; or &quot;TX&quot;.<BR><BR>';
            $nav_state = 'color:#E00000';
        } else {
            $nav_state = '';
            //State Valid Check to see if it's a valid State?
        }
        if (preg_match($preg_zip,$na_zip) !== 1) {
            // Invalid zip...
            $na_valid = false;
            $na_message .= ($na_zip == '') ? '<B>Zip Code</B>: You must enter a valid Zip Code.<BR>' : '<B>Zip Code</B>: The zip code you entered is not a valid U.S. Zip Code.<BR><BR>';
            $nav_zip = 'color:#E00000';
        }else{
            $nav_zip = '';
        }
    } else {
        // State is now Province
        $na_province = $na_state;
    }
    
    // Check to see if the account has already been registered....
    if ($Account->is_registered($na_email)) {
        // Already present... Forgot password?
        $na_valid = false;
        $na_message = '<B>E-Mail</B>: This E-Mail address is already registered. If you already have an account, please login using the form to the far left. If you have fogotten your password, click &quot;<A HREF=forgot.php>Forgot Password.</A>&quot;<BR><BR>';
        $nav_email = 'color:#E00000';
        //break;
		}else{
			$nav_email ='';
		}
    if (!$na_valid) {
        $na_message = '<B>The <FONT COLOR=#E00000>red</FONT> fields require your attention.</B><BR><HR><DIV STYLE="padding:10px 10px 10px 20px">'.$na_message.'</DIV>';
        $naction = 0x0200;
        $ib_tag = 3100;
    } else {
        // Valid information... add account and process...
        $actreg['uEmail'] = $na_email;
        $actreg['uPassword'] = $na_pass1;
        $actreg['uFName'] = $na_fname;
        $actreg['uLName'] = $na_lname;
        $actreg['uAddress1'] = $na_addr1;
        $actreg['uAddress2'] = $na_addr2;
        $actreg['uCity'] = $na_city;
        if (!isset($na_province) || $na_province == '') {
            $actreg['uState'] = $na_state;
        } else {
            $actreg['uProvince'] = $na_province;
        }
        $actreg['uZip'] = $na_zip;
        $actreg['uCountry'] = $na_ctry;
        // Need POBox, DOB
        $Account->register_account($actreg);
        $Account->login($na_email, $na_pass1, $Session->getID());
        $justMadeAccount = true;
        // Code to show that the user is logged in
    }
}
// Generate the form
if ($Account->is_guest()) {    
    $na_ctry = ($na_ctry == '') ? 'US' : $na_ctry ;
    $sgMessage = $na_message;
    $sgForm = '        <FORM ACTION=signup.php METHOD=POST NAME=newacct>
        <INPUT TYPE=HIDDEN NAME=newAccount VALUE=TRUE>
        <TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 STYLE="font-size:12px;">
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_email.'">E-Mail Address:</TD><TD><INPUT CLASS=login1 STYLE="width:250;'.$nav_email.'" NAME=na_email VALUE="'.$na_email.'"></TD></TR>
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_pass1.'">Password:</TD><TD><INPUT CLASS=login1 TYPE=PASSWORD STYLE="width:100;'.$nav_pass1.'" NAME=na_pass1 VALUE="'.$na_pass1.'"></TD></TR>
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_pass2.'" NOWRAP>Confirm Password:</TD><TD><INPUT CLASS=login1 TYPE=PASSWORD STYLE="width:100;'.$nav_pass2.'" NAME=na_pass2 VALUE="'.$na_pass2.'"></TD></TR>
        
            <TR><TD COLSPAN=2 STYLE="height:8;border-bottom:1px solid #808080;">&nbsp;</TD></TR>
            <TR><TD COLSPAN=2 STYLE="height:8;">&nbsp;</TD></TR>
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_fname.'">First Name:</TD><TD><INPUT CLASS=login1 STYLE="width:160;'.$nav_fname.'" NAME=na_fname VALUE="'.$na_fname.'"></TD></TR>
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_lname.'">Last Name:</TD><TD><INPUT CLASS=login1 STYLE="width:160;'.$nav_lname.'" NAME=na_lname VALUE="'.$na_lname.'"></TD></TR>
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_addr1.'">Address 1:</TD><TD><INPUT CLASS=login1 STYLE="width:210;'.$nav_addr1.'" NAME=na_addr1 VALUE="'.$na_addr1.'"></TD></TR>
            <TR><TD ALIGN=RIGHT>Address 2:</TD><TD><INPUT CLASS=login1 STYLE="width:210;" NAME=na_addr2 VALUE="'.$na_addr2.'"></TD></TR>
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_city.'">City:</TD><TD><INPUT CLASS=login1 STYLE="width:160;'.$nav_city.'" NAME=na_city VALUE="'.$na_city.'"></TD></TR>
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_state.'">State/Province:</TD><TD><INPUT CLASS=login1 STYLE="width:140;'.$nav_state.'" NAME=na_state VALUE="'.$na_state.'"></TD></TR>
            <TR><TD ALIGN=RIGHT STYLE="font-size:12px;'.$nav_zip.'">Zip Code:</TD><TD><INPUT CLASS=login1 STYLE="width:80;'.$nav_zip.'" NAME=na_zip VALUE="'.$na_zip.'"></TD></TR>
            <TR><TD ALIGN=RIGHT>Country:</TD><TD>
            '.stripslashes(get_country_select('na_ctry','login1',$na_ctry)).'
            </TD></TR>
        
            <TR><TD COLSPAN=2 STYLE="height:8;border-bottom:1px solid #808080;">&nbsp;</TD></TR>
            <TR><TD COLSPAN=2 STYLE="height:8;">&nbsp;</TD></TR>
            <TR><TD COLSPAN=2 ALIGN=CENTER><INPUT CLASS=submit1 TYPE=SUBMIT VALUE="Register"></TD></TR>
        </TABLE>
        </FORM>
        ';
} else {
    // Not a guest, just made an account or has gotten here somehow...
    if ($justMadeAccount) {
        $sgMessage = '';
        $sgForm = '        <TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 STYLE="font-size:12px;">
            <TR><TD>Congatulations! Your Free Account is Setup and Activated!</TD></TR>
            <TR><TD>
                Your Login is: <B>'.$na_email.'</B><BR>
                Please write down your password for future reference.
            </TD></TR>
            </TABLE>
        
            ';
    } else {
        /*
        $sgMessage = '';
        $sgForm = '        <TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 STYLE="font-size:12px;">
            <TR><TD>
                You are logged in as: <B>'.$na_email.'</B><BR>
            </TD></TR>
            </TABLE>
        
            ';
        */
    }
}

?>
