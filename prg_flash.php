<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

class fmFlashCard {
    
    var $Volume =               95;             // Volume in percent.
    var $Size =                 3;              // Font size in a range 1-5
    var $Theme =                'default';      // Theme for backgrounds/colors etc....
    var $Cycle =                0;              // How many times to cycle the entire lesson.
    var $Repeat =               0;              // How many times to repeat each word.
    var $Delay =                2000;           // Delay after word is spoken
    var $Format =               'mp3';          // mp3 / swf / snd -- Determine from the Diagnostic Section
    
    /*
        Track position, repeating, cycling, etc..
        
    */
    var $lPosition =            0;
    var $lWordCount =           0;
    var $lRepeat =              0;
    var $lCycle =               0;
    var $lDelay =               0;
    var $lRunning =             false;
    var $lAudioLength =         0;
    
    
    function restore($state) {
        // Restore state from the session
        if ($state != '') {
            list($this->Volume,$this->Size,$this->Theme,$this->Cycle,$this->Repeat,$this->Delay,$this->Format,$this->lPosition,$this->lWordCount,$this->lRepeat,$this->lCycle,$this->lDelay,$this->lAudioLength) = explode('|',$state);
        }
    }
    
    function preserve () {
        // Save state to the session
        $state = "{$this->Volume}|{$this->Size}|{$this->Theme}|{$this->Cycle}|{$this->Repeat}|{$this->Delay}|{$this->Format}";
        $state .= "|{$this->lPosition}|{$this->lWordCount}|{$this->lRepeat}|{$this->lCycle}|{$this->lDelay}|{$this->lAudioLength}";
        return $state;
    }
    
    function tick($objLesson) {
        // This gets called when the web page refreshes and wants the next word/repeat
        // FIXME: WriteMe....
        // FIXME: Do Calcs here (Duration, Size...) - Return false if done...
        // FIXME: Inc Position, Check for repeat, Check for Cycling...etc...
        
        $this->lWordCount = $objLesson->count();
        $this->Repeat = intval("0{$this->Repeat}");
        $this->Cycle = intval("0{$this->Cycle}");
        
        $this->lRepeat++;
        if ($this->Repeat < $this->lRepeat) {
            // No Repeat, or repeat complete...
            $this->lRepeat = 0;
            $this->lPosition++;
            if ($this->lWordCount <= $this->lPosition) {
                // Reached the end... Do we cycle?
                if ($this->lCycle < $this->Cycle) {
                    // Need to cycle the lesson
                    $this->lPosition = 0;
                    $this->lCycle++;
                } else {
                    // Not cycling, end of lesson
                    $this->lPosition = 0;
                    $this->lCycle = 0;
                    return false;
                }
            }
        }
        return true;
    }
    
    function begin() {
        // Here we start up the lesson for the first time.
        $this->lPosition = 0;
        $this->lRepeat = 0;
        $this->lDelay = 0;
        $this->lCycle = 0;
        $this->lAudioLength = 0;
        $this->lRunning = true;
    }
    
    function generateFlashCard($objLesson) {
        // Here we return the HTML for the vScreen.
        // Grab the Next Word
        $line = $objLesson->get_word($this->lPosition);
        list($fLang,$fSHA1,$fNative,$fForiegn) = explode("|",$line);
        
        $html = $this->makeCard(htmlentities($fNative),htmlentities($fForiegn),$fSHA1);
        //$html = $this->makeToolbar()
        return $html;
    }
    
    function generateFlashScript($objLesson) {
        global $db_link;
        // Here we return the HTML for the vScreen.
        // Grab the Next Word
        $line = $objLesson->get_word($this->lPosition);
        list($fLang,$fSHA1,$fNative,$fForeign) = explode("|",$line);
        
        $result = mysqli_query($db_link, "SELECT wDuration FROM Language WHERE wSHA1='$fSHA1'");
        $row = mysqli_fetch_array($result);
        $this->lAudioLength = intval($row['wDuration']) + ($this->Delay);
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        $m_volume = intval((100 - $this->Volume) * 40.95);
        
        //$html = $this->makeCard(htmlentities($fNative),htmlentities($fForeign),$fSHA1);
        $script = '';
        $script .= '
                        function fmCheckAudio() {
                            if ( document.dfcAudio2.PlayState == 0 ) { 
                                clearTimeout(fmATimer); 
                                fmATimer = null;
                                dfcDelayTick(' . strval($this->Delay) .');
                            } else {
                                fmATimer = setTimeout("fmCheckAudio()", 250);
                            }
                            
                        }
                        
                        var obj = document.getElementById("dfcFlN");
                        obj.innerHTML = "'.htmlentities($fNative).'";
                        var obj = document.getElementById("dfcFlF");
                        obj.innerHTML = "'.htmlentities($fForeign).'";
                        
                        if ( document.dfcAudio2.ShowControls ) {
                            document.dfcAudio2.fileName = "audio.php?af='.$fSHA1.'";
                            document.dfcAudio2.Volume = -'.$m_volume .';
                            document.dfcAudio2.play();
                            tbPlaying = true;
                            tbfResume();
                        } else {
                                tbfResume();
                                tbPlaying = true;
                        }
                        
        ';
        // set the delayTick
        return $script;
    }
    
    function makeCard($native,$foriegn,$audio) {
        global $db_link;
        
        // This generates the HTML to create a flash card with audio...
        // Calculate the font size....
        switch ($this->Size) {
            case 0: 
                $size = 7;  // PDA?
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                $size = 16 * $this->Size; 
                break;
        }
        $size = "{$size}pt";
        
        // Grab the duration... Might just want a timer to check to see if the player is running, then wait, then tick()
        $result = mysqli_query($db_link, "SELECT wDuration FROM Language WHERE wSHA1='$audio'");
        $row = mysqli_fetch_array($result);
        $this->lAudioLength = intval($row['wDuration']) + ($this->Delay);
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        
        // Calculate the volume for the mp3/wm object
        $m_volume = intval((100 - $this->Volume) * 40.95);
        
        // Random Debugging Stuff
        //$code = rand(0,5000); 
        $flashcard = '';
        
        $flashcard .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100% HEIGHT=100% BGCOLOR=#040673 ALIGN=LEFT>";
        $flashcard .= "<TR>";
        $flashcard .= "<TD VALIGN=BOTTOM ALIGN=CENTER HEIGHT=50%>";
        // The Foriegn Word
        $flashcard .= "<SPAN STYLE=\\\"font-size:$size;color:white;\\\">$foriegn</SPAN>";
        $flashcard .= "</TD>";
        $flashcard .= "</TR>";
        $flashcard .= "<TR>";
        $flashcard .= "<TD VALIGN=TOP ALIGN=CENTER HEIGHT=50% BGCOLOR=#0691CA>";
        // The Native Word
            $flashcard .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100% HEIGHT=100%>";
            $flashcard .= "<TR>";
            $flashcard .= "<TD VALIGN=TOP ALIGN=CENTER BGCOLOR=#0691CA>";
            // The Embedded Audio Piece
            //$flashcard .= '<OBJECT ID=\"dfcaudio\" TYPE=\"application/x-mplayer2\" WIDTH=\"280\" HEIGHT=\"50\">'; //CLASSID=\"clsid:22d6f312-b0f6-11d0-94ab-0080c74c7e95\"
            //$flashcard .= "<param name=\\\"fileName\\\" value=\\\"audio.php?af=$audio\\\">";
            //$flashcard .= '<param name=\"animationatStart\" value=\"false\">';
            //$flashcard .= '<param name=\"transparentatStart\" value=\"true\">';
            //$flashcard .= '<param name=\"autoStart\" value=\"true\">';
            //$flashcard .= '<param name=\"showControls\" value=\"true\">';
            //$flashcard .= "<param name=\\\"volume\\\" value=\\\"-$m_volume\\\">";
            $flashcard .= "<EMBED SRC='audio.php?af=$audio' TYPE='application/x-mplayer2' autostart='true'�hidden='true' volume='-$m_volume' loop='false' width='0' height='0'>";
            //$flashcard .= "</OBJECT>";
            $flashcard .= "<SPAN STYLE=\\\"font-size:$size;color:#040673;\\\">$native</SPAN>";
            $flashcard .= "</TD>";
            $flashcard .= "</TR>";
            $flashcard .= "<TR>";
            $flashcard .= "<TD VALIGN=BOTTOM ALIGN=CENTER HEIGHT=64>";
            /*
                <object id="MediaPlayer1" classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701" standby="Loading Microsoft Windows� Media Player components..." type="application/x-oleobject" width="440" height="330">
                <param name="fileName" value="http://media2.big-boys.com/content/wdmal.wmv">
                <param name="animationatStart" value="true">
                <param name="transparentatStart" value="true">
                <param name="autoStart" value="true">
                <param name="showControls" value="true">
                <param name="Volume" value="-10">
                <embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" src="http://media2.big-boys.com/content/wdmal.wmv" width=440 height=330 autostart=1 showcontrols=1 volume=-20></embed> 
                </object>            
            */
            //$flashcard .= "<SPAN STYLE=\\\"font-size:$size;color:#040673;\\\">$native($code)</SPAN>";
            // DEBUG
            //$flashcard .= "<BR>lPosition: {$this->lPosition} / {$this->lWordCount}<BR>";
            //$flashcard .= "lCycle: {$this->lCycle} / {$this->Cycle}<BR>";
            //$flashcard .= "lRepeat: {$this->lRepeat} / {$this->Repeat}<BR>";
            //$flashcard .= "lDelay: {$this->lDelay} / {$this->Delay}<BR>";
            // DEBUG
            $flashcard .= $this->makeToolbar();
            $flashcard .= "</TD>";
            $flashcard .= "</TR>";
            $flashcard .= "</TABLE>";
        $flashcard .= "</TD>";
        $flashcard .= "</TR>";
        $flashcard .= "</TABLE>";
        return $flashcard;        
    }
    
    function makeToolbar($number=1) {
        // This generates the HTML for the toolbar
        //tb_slider.gif
        // Main Div, Toolbar Div, Options Div (float left)
        
        // Okay.... Here goes...
        if ($number == 1) {
            $stepMap = array( 0 => 0, 1 => 17, 2 => 35, 3 => 54, 4 => 73, 5 => 92);
            $volPos = intval($this->Volume * 0.92);
            $spdPos = 100 - intval($this->Delay / 50);
            $rptPos = $stepMap[$this->Repeat];
            $cycPos = $stepMap[$this->Cycle];
            $html .= "<TABLE BORDER=0 STYLE=\\\"width:900px;height:51px;background-image:url('images/tb_back4.jpg');\\\" CELLPADDING=0 CELLSPACING=0 onSelectStart=\\\"return false;\\\" onDrag=\\\"return true;\\\">";
            $html .= "<TR>";
            // Play / Pause
            $html .= "<TD WIDTH=50 HEIGHT=34>&nbsp</TD>";
            // Volume
            $html .= "<TD WIDTH=100>";
            $html .= "<DIV STYLE=\\\"height:34px;width:100px;background-image:url('images/tb_ramp.gif');\\\">";
            $html .= "<IMG ID=\\\"tbVol\\\" SRC=images/tb_button.gif STYLE=\\\"position:relative;left:{$volPos}px;top:12px;\\\">";  //  onousedown=\\\"tbBeginDrag()\\\"
            $html .= "</DIV>";
            $html .= "</TD>";
            // Spacer
            $html .= "<TD WIDTH=25 HEIGHT=34>&nbsp</TD>";
            // Speed
            $html .= "<TD WIDTH=100>";
            $html .= "<DIV STYLE=\\\"height:34px;width:100px;background-image:url('images/tb_bar.gif');\\\">";
            $html .= "<IMG ID=\\\"tbSpd\\\" SRC=images/tb_button.gif STYLE=\\\"position:relative;left:{$spdPos}px;top:12px;\\\">";
            $html .= "</DIV>";
            $html .= "</TD>";
            // Spacer
            $html .= "<TD WIDTH=25 HEIGHT=34>&nbsp</TD>";
            // Cycle
            $html .= "<TD WIDTH=100>";
            $html .= "<DIV STYLE=\\\"height:34px;width:100px;background-image:url('images/tb_step.gif');\\\">";
            $html .= "<IMG ID=\\\"tbCyc\\\" SRC=images/tb_button.gif STYLE=\\\"position:relative;left:{$cycPos}px;top:12px;\\\">";
            $html .= "</DIV>";
            $html .= "</TD>";
            // Spacer
            $html .= "<TD WIDTH=25 HEIGHT=34>&nbsp</TD>";
            // Repeat
            $html .= "<TD WIDTH=100>";
            $html .= "<DIV STYLE=\\\"height:34px;width:100px;background-image:url('images/tb_step.gif');\\\">";
            $html .= "<IMG ID=\\\"tbRpt\\\" SRC=images/tb_button.gif STYLE=\\\"position:relative;left:{$rptPos}px;top:12px;\\\">";
            $html .= "</DIV>";
            $html .= "</TD>";
            // Spacer
            $html .= "<TD WIDTH=25 HEIGHT=34>&nbsp</TD>";
            // Lesson List
            $html .= "<TD WIDTH=100>";
            $html .= "<DIV STYLE=\\\"height:34px;width:300px;\\\">";
            //$html .= "<IMG SRC=images/tb_button.gif STYLE=\\\"position:relative;left:0px;top:10px;\\\">";
            $html .= "</DIV>";
            $html .= "</TD>";
            // Spacer
            $html .= "<TD HEIGHT=34>&nbsp</TD>";
            $html .= "</TR>";
            $html .= "<TR>";
            $html .= "<TD COLSPAN=11 HEIGHT=17>&nbsp</TD>";
            $html .= "</TR>";
            $html .= "</TABLE>";
        }
        if ($number == 2) {
            $stepMap = array( 0 => 2, 1 => 24, 2 => 46, 3 => 69, 4 => 91);
            $volPos = intval($this->Volume * 0.92);
            $spdPos = 100 - intval($this->Delay / 50);
            $rptPos = $stepMap[$this->Repeat];
            $cycPos = $stepMap[$this->Cycle];
            $html .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 HEIGHT=40 WIDTH=100%>';
            $html .= '<TR><TD><IMG SRC=\\"images/1px.gif\\" WIDTH=54 HEIGHT=1></TD><TD WIDTH=7><IMG SRC=\\"images/obar3l.gif\\"></TD>';
            $html .= '<TD WIDTH=746 HEIGHT=40 STYLE=\\"overflow:hidden;height:40px;\\">';
            
                $html .= '<TABLE BORDER=0 WIDTH=746 HEIGHT=40 STYLE=\\"width:746px;height:40px;overflow:hidden;background-image:url(\'images/obar3m.png\');\\" CELLPADDING=0 CELLSPACING=0 onSelectStart=\\"return false;\\" onDrag=\\"return true;\\">';
                $html .= '<TR>';
                $html .= "<TD></TD>";
                $html .= "<TD WIDTH=101 HEIGHT=18 VALIGN=BOTTOM STYLE=\\\"font-size:10px;color:white;text-align:center;overflow:hidden;\\\">Volume</TD>";
                $html .= "<TD></TD>";
                $html .= "<TD WIDTH=101 HEIGHT=18 VALIGN=BOTTOM STYLE=\\\"font-size:10px;color:white;text-align:center;overflow:hidden;\\\">Speed</TD>";
                $html .= "<TD></TD>";
                $html .= "<TD WIDTH=101 HEIGHT=18 VALIGN=BOTTOM STYLE=\\\"font-size:10px;color:white;text-align:center;overflow:hidden;\\\">Lesson Cycle</TD>";
                $html .= "<TD></TD>";
                $html .= "<TD WIDTH=101 HEIGHT=18 VALIGN=BOTTOM STYLE=\\\"font-size:10px;color:white;text-align:center;overflow:hidden;\\\">Word Repeat</TD>";
                $html .= "<TD HEIGHT=18 WIDTH=71 VALIGN=BOTTOM><IMG SRC=\\\"images/1px.gif\\\"></TD>";
                $html .= "<TD HEIGHT=18 WIDTH=71 VALIGN=BOTTOM><IMG SRC=\\\"images/1px.gif\\\"></TD>";
                $html .= "<TD HEIGHT=40 ROWSPAN=3 VALIGN=CENTER ALIGN=CENTER WIDTH=60></TD>"; // onClick Pause/Play
                $html .= "<TD HEIGHT=40 ROWSPAN=3 VALIGN=CENTER ALIGN=CENTER WIDTH=30><A HREF=# onClick=\\\"tbfTogglePlaying();return false;\\\"><IMG ID=\\\"tbPlay\\\" SRC=images/obar3_pl.gif></A></TD>"; // OnClick exit
                $html .= "</TR>";
                
                $html .= "<TR>";
                // Play / Pause
                $html .= "<TD WIDTH=10 HEIGHT=17><IMG SRC=\\\"images/1px.gif\\\"></TD>";
                // Volume
                $html .= "<TD WIDTH=101>";
                $html .= "<DIV STYLE=\\\"height:17px;width:101px;background-image:url('images/obar3ss.gif');\\\">";
                $html .= "<IMG ID=\\\"tbVol\\\" SRC=images/obar3sl.png STYLE=\\\"position:relative;left:{$volPos}px;top:0px;\\\">";  //  onmousedown=\\\"tbBeginDrag()\\\"
                $html .= "</DIV>";
                $html .= "</TD>";
                // Spacer
                $html .= "<TD WIDTH=25 HEIGHT=17><IMG SRC=\\\"images/1px.gif\\\"></TD>";
                // Speed
                $html .= "<TD WIDTH=101>";
                $html .= "<DIV STYLE=\\\"height:17px;width:101px;background-image:url('images/obar3ss.gif');\\\">";
                $html .= "<IMG ID=\\\"tbSpd\\\" SRC=images/obar3sl.png STYLE=\\\"position:relative;left:{$spdPos}px;top:0px;\\\">";
                $html .= "</DIV>";
                $html .= "</TD>";
                // Spacer
                $html .= "<TD WIDTH=25 HEIGHT=17><IMG SRC=\\\"images/1px.gif\\\"></TD>";
                // Cycle
                $html .= "<TD WIDTH=101>";
                $html .= "<DIV STYLE=\\\"height:17px;width:101px;background-image:url('images/obar3ns.gif');\\\">";
                $html .= "<IMG ID=\\\"tbCyc\\\" SRC=images/obar3sl.png STYLE=\\\"position:relative;left:{$cycPos}px;top:0px;\\\">";
                $html .= "</DIV>";
                $html .= "</TD>";
                // Spacer
                $html .= "<TD WIDTH=25 HEIGHT=17><IMG SRC=\\\"images/1px.gif\\\"></TD>";
                // Repeat
                $html .= "<TD WIDTH=101>";
                $html .= "<DIV STYLE=\\\"height:17px;width:101px;background-image:url('images/obar3ns.gif');\\\">";
                $html .= "<IMG ID=\\\"tbRpt\\\" SRC=images/obar3sl.png STYLE=\\\"position:relative;left:{$rptPos}px;top:0px;\\\">";
                $html .= "</DIV>";
                $html .= "</TD>";
                // Spacer
                $html .= "<TD WIDTH=25 HEIGHT=17><IMG SRC=\\\"images/1px.gif\\\"></TD>";
                // Lesson List
                $html .= "<TD ALIGN=RIGHT>"; //71
                $html .= "<DIV STYLE=\\\"height:17px;\\\">";
                //$html .= "<IMG SRC=images/tb_button.gif STYLE=\\\"position:relative;left:0px;top:10px;\\\">";
                $html .= "</DIV>";
                $html .= "</TD>";
                // Spacer
                $html .= "<TR>";
                $html .= "<TD COLSPAN=10 HEIGHT=5><IMG SRC=\\\"images/1px.gif\\\"></TD>";
                $html .= "</TR>";
                $html .= "</TABLE>";
            $html .= '</TD><TD WIDTH=7 VALIGN=BOTTOM><IMG SRC=\\"images/obar3r.gif\\"></TD>';
            $html .= "<TD VALIGN=BOTTOM><A HREF=# onClick=\\\"tbfExit();return false;\\\"><IMG ID=\\\"tbExit\\\" STYLE=\\\"float:right;\\\" SRC=images/obar3_ex.gif></A></TD>"; //STYLE=\\\"float:right;\\\"
            $html .= '</TD></TR></TABLE>';
                
        }
        
        return $html;
    }
}

?>
