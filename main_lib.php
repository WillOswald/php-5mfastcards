<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | Prepare Banner
// +----------------------------------------------------------------------
function fmlibBanner($style = "default") {
    global $Account, $Session;
    
    if ($style == "default") {$st = 0x0010; }
    if ($style == "index") {$st = 0x0020; }
    if ($style == "index2") {$st = 0x0030; }
    
    $banner = '';
    switch ($st) {
        case 0x0010:
            $banner .= '<DIV STYLE="height:98;width:980;overflow: hidden;background: url(\'images/sc_banner_03p.jpg\');">';
            $banner .= '<TABLE STYLE="height:98;width:975;border:none;" CELLPADDING=0 CELLSPACING=0>';
            $banner .= '<TR><TD HEIGHT=80 COLSPAN=2></TD></TR>';
            $banner .= '<TR><TD></TD><TD NOWRAP>';
            // Clock
            $banner .= '<DIV STYLE="font-size:12; width:80px; clear: right; float:right; display:block; text-align:right;">';
            $banner .= '<FORM NAME="fmClock" ACTION="">';
            $banner .= '<INPUT CLASS="iclock" TYPE=TEXT NAME=clkHour STYLE="width:20; height:18; text-align:right;" VALUE="0">';
            $banner .= '<INPUT CLASS="iclock" TYPE=TEXT NAME=clkColon STYLE="width:6; height:18; text-align:center;" VALUE=":">';
            $banner .= '<INPUT CLASS="iclock" TYPE=TEXT NAME=clkMin STYLE="width:20; height:18; text-align:left;" VALUE="00">';
            $banner .= '<INPUT CLASS="iclock" TYPE=TEXT NAME=clkAP STYLE="width:8; height:18; text-align:center;" VALUE="a">';
            $banner .= '</FORM>';
            //$banner .= date("g:i a");
            $banner .= '</DIV>';
            // Next Item in banner....
            if ($Account->_Guest) {
                $lmsg = "Login";
            } else {
                $lmsg = "Logout";
            }
            $banner .= '<DIV ID="bannerLogin" CLASS="bfont12" STYLE="width:70px; float:right; text-align:right;" onclick="fireLogin(\'show\',\'#9090FF\');"><B>'.$lmsg.'</B></DIV>';
            $banner .= '<DIV ID="bannerContact" CLASS="bfont12" STYLE="width:90px; float:right; text-align:right;"><B>Contact</B></DIV>';
            //$banner .= '<DIV ID="bannerHelp" CLASS="bfont12" STYLE="width:70px; float:right; text-align:right;" onclick="help(\'DOM\');"><B>Help</B></DIV>';
            $banner .= '<DIV ID="bannerHelp" CLASS="bfont12" STYLE="width:70px; float:right; text-align:right;"><B>Help</B></DIV>';
            // This is the message queue thingy
            if ($Account->_Guest) {
                $message = "Welcome Guest!";
            } else {
                $message = "Welcome {$Account->_ContactInfo['FName']}.";
                //if ($Account->has_access(39)) {
                //    $message .= " <FONT COLOR=red>(Admin)</FONT>";
                //}
            }
            $banner .= '<DIV ID="bannerText" CLASS="bfont12" STYLE="width:360px; float:right; color:#000;"><DIV ID="bannerContent">'.$message.'</DIV></DIV>';
            // End of banner stuff
            $banner .= '</TD></TR>';
            $banner .= '</TABLE>';
            $banner .= '</DIV>';
            break;
        case 0x0020:
            $banner .= '<DIV STYLE="height:98;width:980;overflow: hidden;background: url(\'images/sc_banner_03p.jpg\');">';
            $banner .= '</DIV>';
            break;
        case 0x0030:
            $banner .= '<DIV STYLE="height:98;width:980;overflow: hidden;background: url(\'images/sc_banner_03.jpg\');">';
            $banner .= '</DIV>';
            break;
    }
    return $banner;
}

// +----------------------------------------------------------------------
// | Prepare Login Part...
// +----------------------------------------------------------------------
function fmlibLogin($style = "default",$email='') {
    global $Account;
    $nstyle = 0;
    $nstyle += ($style == "default") ? 0x0000 : 0x0000;
    $nstyle += ($style == "reload0") ? 0x0010 : 0x0000;
    $nstyle += ($style == "reload1") ? 0x0020 : 0x0000;
    $nstyle += ($style == "reload2") ? 0x0040 : 0x0000;
    switch ($nstyle) {
        case 0x0000: // Login Form
            $login .= '<DIV ID="fmLGC">';
            $login .= '<FORM NAME=loginf METHOD=POST ACTION=# onSubmit="rpcFetch(\'act=set&lg=true\',\'loginf\');return false;">';
            $login .= '<TABLE STYLE="border:none;" CELLPADDING=2 CELLSPACING=0>';
            $login .= '<TR><TD CLASS=t1 ALIGN=RIGHT>E-Mail:&nbsp;</TD><TD ALIGN=RIGHT><INPUT CLASS=login1 TYPE=TEXT SIZE=25 NAME=login_e></TD></TR>';
            $login .= '<TR><TD CLASS=t1 ALIGN=RIGHT>Password:&nbsp;</TD><TD ALIGN=RIGHT><INPUT CLASS=login1 TYPE=PASSWORD SIZE=25 NAME=login_p></TD></TR>';
            $login .= '<TR><TD ALIGN=CENTER COLSPAN=2><INPUT CLASS=loginb1 TYPE=BUTTON NAME=lbutton VALUE=Login onClick="rpcFetch(\'act=set&lg=true\',\'loginf\');return false;"></TD></TR>'; //onClick="rpcFetch(\'act=set&lg=true\',\'loginf\');return false;"
            $login .= '</TABLE>';
            $login .= '</FORM>';
            $login .= '</DIV>';
            break;
        case 0x0010: // Logged in
            $login .= '<DIV ID="fmLGC">';
            $login .= "<FORM ACTION=# NAME=loginf onSubmit=\"rpcFetch('act=set&lg=false');return false;\">";
            $login .= '<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>';
            $login .= "<TR><TD CLASS=t1 ALIGN=LEFT>You are logged in as {$Account->_ContactInfo['FName']} {$Account->_ContactInfo['LName']}</TD></TR>";
            $login .= "<TR><TD CLASS=t1 ALIGN=LEFT><INPUT CLASS=loginb1 TYPE=BUTTON NAME=lbutton VALUE=Logout onClick=\"rpcFetch('act=set&lg=false');return false;\"></TD></TR>";
            $login .= '</TABLE>';
            $login .= '</FORM>';
            $login .= '</DIV>';
            break;
        case 0x0020: // Account not found
            $login .= '<DIV ID="fmLGC">';
            $login .= '<FORM ACTION=# NAME=loginf onSubmit="rpcFetch(\'act=set&lg=true\',\'loginf\');return false;">';
            $login .= '<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>';
            $login .= '<TR><TD CLASS=t1 ALIGN=CENTER COLSPAN=2>Account not found.</TD></TR>';
            $login .= '<TR><TD CLASS=t1 ALIGN=RIGHT>E-Mail:&nbsp;</TD><TD ALIGN=RIGHT><INPUT CLASS=login1 TYPE=TEXT SIZE=25 NAME=login_e></TD></TR>';
            $login .= '<TR><TD CLASS=t1 ALIGN=RIGHT>Password:&nbsp;</TD><TD ALIGN=RIGHT><INPUT CLASS=login1 TYPE=PASSWORD SIZE=25 NAME=login_p></TD></TR>';
            $login .= '<TR><TD ALIGN=CENTER COLSPAN=2><INPUT CLASS=loginb1 TYPE=BUTTON NAME=lbutton VALUE=Login onClick="rpcFetch(\'act=set&lg=true\',\'loginf\');return false;"></TD></TR>';
            $login .= '</TABLE>';
            $login .= '</FORM>';
            $login .= '</DIV>';
            break;
        case 0x0040: // Invalid Password
            $login .= '<DIV ID="fmLGC">';
            $login .= '<FORM ACTION=# NAME=loginf onSubmit="rpcFetch(\'act=set&lg=true\',\'loginf\');return false;">';
            $login .= '<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>';
            $login .= '<TR><TD CLASS=t1 ALIGN=CENTER COLSPAN=2>Invalid Password.</TD></TR>';
            $login .= "<TR><TD CLASS=t1 ALIGN=RIGHT>E-Mail:&nbsp;</TD><TD ALIGN=RIGHT><INPUT CLASS=login1 TYPE=TEXT SIZE=25 NAME=login_e VALUE=\"$email\"></TD></TR>";
            $login .= '<TR><TD CLASS=t1 ALIGN=RIGHT>Password:&nbsp;</TD><TD ALIGN=RIGHT><INPUT CLASS=login1 TYPE=PASSWORD SIZE=25 NAME=login_p></TD></TR>';
            $login .= '<TR><TD ALIGN=CENTER COLSPAN=2><INPUT CLASS=loginb1 TYPE=BUTTON NAME=lbutton VALUE=Login onClick="rpcFetch(\'act=set&lg=true\',\'loginf\');return false;"></TD></TR>';
            $login .= '</TABLE>';
            $login .= '</FORM>';
            $login .= '</DIV>';
            break;
    }
    return $login;
}

// +----------------------------------------------------------------------
// | Prepare an entire nav guide
// +----------------------------------------------------------------------
function fmlibNavGuide($headings) {
    $navguide .= '<DIV STYLE="position:relative;overflow-x:hidden;height:420;width:200;clear:left;float:left;border:1px solid black;margin:7px 0px 0px 0px;padding:0px 0px 0px 0px;overflow: auto;background-color:#E5FEF0;background: url(\'images/sc_nav_01.jpg\');">'."\n";
    $navguide .= '<TABLE CELLPADDING=0 CELLSPACING=0 STYLE="width:100%;padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;borner:none;">'."\n";
    foreach($headings as $heading) {
        $navguide .= $heading."\n";
    }
    $navguide .= '</TABLE>'."\n";
    $navguide .= '</DIV>'."\n";
    return $navguide;
}

// +----------------------------------------------------------------------
// | Prepare a Main Heading with Subheading Block with sub headings
// +----------------------------------------------------------------------
function fmlibNavHeading($id, $alt, $ibTag = 0, $block = "", $style = "") {
    $imgnum = ($ibTag / 1000) -2;
    $heading .= '<TR><TD VALIGN=TOP>'."\n";
    $heading .= '<DIV ID="'.$id.'" CLASS="navh" STYLE="background: url(\'images/sc_nav_'.$imgnum.'.jpg\');" onClick="fm_toggle('.$imgnum.','.$ibTag.');" onMouseOver="fm_highlight('.$imgnum.');" onMouseOut="fm_unhighlight('.$imgnum.');">'."\n";
    //$heading .= '<IMG ALT="+" TITLE="" BORDER=0 ID="'.$id.'P" SRC="images/sc_nav_plus.gif" STYLE="width:20;height:20;margin:0px 0px 0px 0px;">';
    if ($style == "") {
        $heading .= '<IMG ALIGN=TOP ALT="'.$alt.'" TITLE="" BORDER=0 ID="'.$id.'T" SRC="images/sc_nav_text-'. ($imgnum+2) .'.gif" STYLE="margin:3px 0px 2px 6px;">'."\n";
    } else {
        $heading .= '<IMG ALIGN=TOP ALT="'.$alt.'" TITLE="" BORDER=0 ID="'.$id.'T" SRC="images/sc_nav_text-'. ($imgnum+2) .'.gif" STYLE="margin:3px 0px 2px 6px;'.$style.'">'."\n";
    }
    $heading .= '</DIV>'."\n";
    $heading .= $block."\n";
    $heading .= '</TD></TR>'."\n";
    return $heading;
}

// +----------------------------------------------------------------------
// | Prepare a sub heading block with some sub headings
// +----------------------------------------------------------------------
function fmlibNavSubHeadingBlock($id,$subs) {
    $height = (count($subs) * 20) - 1;
    $block .= '<DIV ID="'.$id.'" CLASS="navshb" STYLE="height:'.$height.'">'."\n";
    $block .= '<TABLE CELLPADDING=0 CELLSPACING=0 STYLE="width:100%;height:'.$height.';padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;">'."\n";
    foreach($subs as $sub) {
        $block .= $sub."\n";
    }
    $block .= '</TABLE>'."\n";
    $block .= '</DIV>'."\n";
    return $block;
}

// +----------------------------------------------------------------------
// | Prepare a sub heading
// +----------------------------------------------------------------------
function fmlibNavSubHeading($content, $ibTag = 0, $color = "#000000", $onClick = "") {
    //FIXME!! URL Navigation...
    $subheading .= '<TR><TD ID="sub'.$ibTag.'" CLASS="navsh" STYLE="color:'.$color.';cursor:hand;" onClick="'.$onClick.'" onMouseOver="fm_subhl('.$ibTag.')" onMouseOut="fm_subuhl('.$ibTag.')">'."\n";
    $subheading .= '<IMG ID="bullet'.$ibTag.'" ALT="o" TITLE="" SRC="images/sc_nav_b3.gif">&nbsp;&nbsp;'.$content."\n";
    $subheading .= '</TD></TR>'."\n";
    return $subheading;
}

function users_online() {
    global $db_link;
    
    $on_members = 0;
    $on_guests = 0;
    $result = mysqli_query($db_link, "SELECT sSessionID FROM Session WHERE NOW() <= sExpires");
    if ($result !== false) {
        // Get all the Sessions...
        $data = array();
        while ($row = mysqli_fetch_assoc($result)) {
            //$pl .= $row['sSessionID']."<BR>";
            $data[] = $row['sSessionID'];
        }
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        // Get Account Info For Each Session...
        reset($data);
        foreach($data as $sid) {
            $result = mysqli_query($db_link, "SELECT count(uID) as total FROM Account WHERE uSessionID='$sid'");
            if(!empty($result)) {
            	$row = mysqli_fetch_assoc($result);
            	((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            	if (intval($row['total']) > 0) {
            	    // Got a member
            	    $on_members++;
            	} else {
                	// Got a guest
                	$on_guests++;
            	}
            }
        }
    }
    $online = array('guests' => $on_guests, 'members' => $on_members);
    return $online;
    
}

function visit_count() {
    global $db_link;
    $ret = array();
    $ret['today'] = 0;
    $ret['total'] = 0;
    $result = mysqli_query($db_link, "SELECT count(*) as total FROM log_enter WHERE entry_time > UNIX_TIMESTAMP( CURDATE() )");
    //$ret['todayErr'] = mysql_error();
    if (($result !== false) && (mysqli_num_rows($result) > 0)) {
        $row = mysqli_fetch_assoc($result);
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        $ret['today'] = $row['total'];
    }
    $result = mysqli_query($db_link, "SELECT count(*) as total FROM Account");
    if (($result !== false) && (mysqli_num_rows($result) > 0)) {
        $row = mysqli_fetch_assoc($result);
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        $ret['account'] = $row['total'];
    }
    $result = mysqli_query($db_link, "SELECT count(*) as total FROM log_enter");
    if (($result !== false) && (mysqli_num_rows($result) > 0)) {
        $row = mysqli_fetch_assoc($result);
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        $ret['total'] = $row['total'];
    }
    
    return $ret;
}

function fmlibVocabulary() {
    global $Vocabulary, $Account, $Lesson;
    // Render the Vocabulary Pane
    $langpre = $Vocabulary->lang_prefix();
    $html .= "<DIV CLASS=dfc1_{$langpre}res1>Vocabulary</DIV>";
    $html .= "<DIV CLASS=dfc1find0>&nbsp;</DIV>";
    $html .= "<DIV CLASS=dfc1_{$langpre}ressec>";
    // Show words from browsing or searching
    $vWords = $Vocabulary->fetch_words();
    $message = $Vocabulary->vFilter ? ( $Vocabulary->vSearchMode ? "Search Results..." : "Browsing by Letter [{$Vocabulary->vBrowse}]..." ) : "Viewing Full Database...";
    //$message = $Vocabulary->vFilter ? ( $Vocabulary->vSearchMode ? "Search Results... Viewing x - y of {$Vocabulary->vSQLTotal}" : "Browsing by {$Vocabulary->vBrowse}...Viewing x - y of {$Vocabulary->vSQLTotal}" ) : "Vocabulary Database... Viewing x - y of {$Vocabulary->vSQLTotal}";
    $placemark1 = ($Vocabulary->vSQLPage > 0) ? '<A HREF=# onClick=\"rpcFetch(\'act=set&vn=p\');return false;\">&lt;&lt;</A>' : "&lt;&lt;" ;
    // Figure range for 5 pages
    $pmin = ($Vocabulary->vSQLPage < 2) ? 0 : $Vocabulary->vSQLPage - 2;
    $pmax = (($pmin + 5) > $Vocabulary->vSQLPageCount) ? $Vocabulary->vSQLPageCount : $pmin + 5;
    $placemarkp = ""; $i = $pmin;
    while ($i < $pmax) {
        $j = $i + 1;
        $placemarkp .= ($i == $Vocabulary->vSQLPage) ? "$j&nbsp;" : "<A HREF=# onClick=\\\"rpcFetch('act=set&vnp=$i');return false;\\\">$j</A>&nbsp;";
        $i++;
    } 
    //$placemarkp .= ($i == $Vocabulary->vSQLPage) ? "$j" : "<A HREF=# onClick=\\\"rpcFetch('act=set&vnp=$i');return false;\\\">$j</A>";
    $placemark2 = (($Vocabulary->vSQLPage + 1) < $Vocabulary->vSQLPageCount) ? '<A HREF=# onClick=\"rpcFetch(\'act=set&vn=n\');return false;\">&gt;&gt;</A>' : "&gt;&gt;" ;
    $placemark = "$placemark1 $placemarkp $placemark2";
    $page = $Vocabulary->vSQLPage + 1;
    $titleline = "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=90%><TR><TD ALIGN=LEFT CLASS=dfc1small>$message</TD><TD ALIGN=RIGHT CLASS=dfc1small>Page $page of {$Vocabulary->vSQLPageCount} - $placemark</TD></TR></TABLE>";
    $html .= "<DIV CLASS=dfc1vocabtitle>$titleline</DIV>";
    $html .= "<DIV CLASS=dfc1vocabscroll>";
    $html .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 STYLE=\\\"width:419px;\\\">";
    
    if ($langpre == "sp_") { $idColor = "#BFE8AE"; }
    if ($langpre == "fr_") { $idColor = "#ACBDE6"; } 
    if ($langpre == "ge_") { $idColor = "#E7ADAE"; } 
    $idCount = 0;
    foreach ($vWords as $row) {
        $idCount++;
        $html .= "<TR ID=\\\"vr{$idCount}\\\"onMouseOver=\\\"document.getElementById('vr{$idCount}').style.backgroundColor='$idColor';\\\" onMouseOut=\\\"document.getElementById('vr{$idCount}').style.backgroundColor='transparent';\\\">";
        if (($row['Flag'] > 0) || ($Account->has_access(0,$Vocabulary->vLanguage))) {
            $html .= "<TD WIDTH=50%><DIV CLASS=dfc1vocab>".htmlentities($row['F'])."</DIV></TD>";
            $html .= "<TD><DIV CLASS=dfc1vocab>".htmlentities($row['N'])."</DIV></TD>";
        } else {
            $html .= "<TD WIDTH=50%><DIV CLASS=dfc1vocabr>".htmlentities($row['F'])."</DIV></TD>";
            $html .= "<TD><DIV CLASS=dfc1vocabr>".htmlentities($row['N'])."</DIV></TD>";
        }
        $html .= "</TR>";
    }
    $html .= '</TABLE>';
    //$html .= $Vocabulary->preserve();
    //$html .= "\"This is the shiznit...\" + ";
    $html .= "</DIV>";  // Scrollie
    $html .= "</DIV>";
    return $html;
}

function fmlibLesson($err_msg) {
    global $Vocabulary, $Account, $Lesson;
    // Render the Lesson Builder Pane
    $langpre = $Vocabulary->lang_prefix();
    $html = '';
    
    $html .= '<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>';
    $html .= '<TR>';
    $html .= '<TD VALIGN=TOP ALIGN=LEFT>';
    // Pane 1
        $html .= "<DIV CLASS=dfc1_{$langpre}res1>Vocabulary</DIV>";
        $html .= '<DIV CLASS=dfc1find0>&nbsp;</DIV>';
        $html .= "<DIV CLASS=dfc1_{$langpre}ressec>";
        // Content of the scrollie part
        $vWords = $Vocabulary->fetch_words();
        $message = $Vocabulary->vFilter ? ( $Vocabulary->vSearchMode ? "Search Results..." : "Browsing by Letter [{$Vocabulary->vBrowse}]..." ) : "Viewing Full Database...";
        $placemark1 = ($Vocabulary->vSQLPage > 0) ? '<A HREF=# onClick=\"rpcFetch(\'act=set&vn=p&from=lesson\');return false;\">&lt;&lt;</A>' : "&lt;&lt;" ;
        // Figure range for 5 pages
        $pmin = ($Vocabulary->vSQLPage < 2) ? 0 : $Vocabulary->vSQLPage - 2;
        $pmax = (($pmin + 5) > $Vocabulary->vSQLPageCount) ? $Vocabulary->vSQLPageCount : $pmin + 5;
        $placemarkp = ""; $i = $pmin;
        while ($i < $pmax) {
            $j = $i + 1;
            $placemarkp .= ($i == $Vocabulary->vSQLPage) ? "$j&nbsp;" : "<A HREF=# onClick=\\\"rpcFetch('act=set&vnp=$i&from=lesson');return false;\\\">$j</A>&nbsp;";
            $i++;
        } 
        $placemark2 = (($Vocabulary->vSQLPage + 1) < $Vocabulary->vSQLPageCount) ? '<A HREF=# onClick=\"rpcFetch(\'act=set&vn=n&from=lesson\');return false;\">&gt;&gt;</A>' : "&gt;&gt;" ;
        $placemark = "$placemark1 $placemarkp $placemark2";
        $page = $Vocabulary->vSQLPage + 1;
        // Search / Browse Combined
        $html .= "<DIV STYLE=\\\"width:433px;height:30px;overflow:hidden;margin:0px;padding:0px;\\\">"; //CLASS=dfc1vocabscroll 
        //$html .= "";
        $html .= "<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 STYLE=\\\"width:420px;height:30px;\\\">" ;
        $html .= "<TR>";
        // Search
            $vss = $Vocabulary->vSearch;
            $html .= "<TD VALIGN=TOP STYLE=\\\"width:28px;padding:9px 0px 0px 0px;\\\"><A HREF=# onClick=\\\"rpcFetch('act=set&vs=s&vss=&from=lesson');return false;\\\"><IMG BORDER=0 WIDTH=26 HEIGHT=14 SRC=images/delr.gif ALT=\\\"Clear Your Search\\\"></A></TD>";
            $html .= "<TD VALIGN=TOP STYLE=\\\"width:210px;padding:6px 0px 0px 0px;\\\"><FORM NAME=search2140 METHOD=POST ACTION=\\\"#\\\" onSubmit=\\\"rpcFetch('act=set&vs=s&from=lesson','search2140'); return false;\\\"><INPUT CLASS=login1 TYPE=TEXT NAME=vss VALUE=\\\"$vss\\\" STYLE=\\\"width:209px;\\\"></FORM></TD>";
            $html .= "<TD VALIGN=TOP STYLE=\\\"width:36px;padding:6px 0px 0px 0px;\\\"><IMG SRC=images/button4.gif onClick=\\\"rpcFetch('act=set&vs=s&from=lesson','search2140');\\\"></TD>";
        // Browse
            $html .= "<TD VALIGN=TOP STYLE=\\\"padding:0px 0px 0px 0px;\\\">";
            $html .= "<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 STYLE=\\\"margin:0px 0px 0px 0px;height:30px;\\\">" ;
            $html .= "<TR>"; 
            $html2 = '';
            for ($i=0;$i<13;$i++) {
                $html2 .= "<TD ALIGN=CENTER CLASS=dfc1bl STYLE=\\\"font-size:9px;width:12px;height:12px;\\\" onClick=\\\"rpcFetch('act=set&vs=b&from=lesson&vb=".chr(65+$i)."');\\\" onMouseOver=\\\"fmHighlight(this,'#FFFF00');\\\" onMouseOut=\\\"fmUnHighlight(this,'transparent');\\\">".chr(65+$i)."</TD>";
            }
            $html .= "$html2</TR><TR>"; 
            $html2 = '';
            for ($i=0;$i<13;$i++) {
                $html2 .= "<TD ALIGN=CENTER CLASS=dfc1bl STYLE=\\\"font-size:9px;width:12px;height:12px;\\\" onClick=\\\"rpcFetch('act=set&vs=b&from=lesson&vb=".chr(78+$i)."');\\\" onMouseOver=\\\"fmHighlight(this,'#FFFF00');\\\" onMouseOut=\\\"fmUnHighlight(this,'transparent');\\\">".chr(78+$i)."</TD>";
            }
            $html .= "$html2</TR>";
            $html .= "</TABLE>";
        // End of header search thingy
        $html .= "</TD></TR>";
        $html .= "</TABLE>";
        //$html .= "";
        $html .= "</DIV>";
        $html .= "<DIV CLASS=dfc1vocabscroll STYLE=\\\"width:433px;height:270px;\\\">";
        $titleline = "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=410><TR><TD ALIGN=LEFT CLASS=dfc1small>$message</TD><TD ALIGN=RIGHT CLASS=dfc1small>Page $page of {$Vocabulary->vSQLPageCount} - $placemark</TD></TR></TABLE>";
        $html .= "<DIV CLASS=dfc1vocabtitle STYLE=\\\"width:419px;border-bottom:1px solid #404040;\\\">$titleline</DIV>";
        $html .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 STYLE=\\\"width:419px;\\\">";
        
        if ($langpre == "sp_") { $idColor = "#BFE8AE"; }
        if ($langpre == "fr_") { $idColor = "#ACBDE6"; } 
        if ($langpre == "ge_") { $idColor = "#E7ADAE"; } 
        $idCount = 0;
        foreach ($vWords as $row) {
            $idCount++;
            $html .= "<TR ID=\\\"vr{$idCount}\\\" onMouseOver=\\\"document.getElementById('vr{$idCount}').style.backgroundColor='$idColor';\\\" onMouseOut=\\\"document.getElementById('vr{$idCount}').style.backgroundColor='transparent';\\\">";
            if (($row['Flag'] > 0) || ($Account->has_access(0,$Lesson->_language))) {
                $html .= "<TD CLASS=dfc1vocab>".htmlentities($row['N'])."</TD>";
                $html .= "<TD CLASS=dfc1vocab>".htmlentities($row['F'])."</TD>";
                $html .= "<TD WIDTH=40 STYLE=\\\"border-bottom:1px solid #808080;\\\"><A HREF=# onClick=\\\"rpcFetch('act=set&lbaw=true&lbaw_id={$row['ID']}');return false;\\\"><IMG BORDER=0 WIDTH=38 HEIGHT=16 SRC=images/add.gif ALT=\\\"Add this word to your lesson.\\\"></A></TD>";
            } else {
                $html .= "<TD CLASS=dfc1vocabr>".htmlentities($row['N'])."</TD>";
                $html .= "<TD CLASS=dfc1vocabr>".htmlentities($row['F'])."</TD>";
                $html .= "<TD STYLE=\\\"border-bottom:1px solid #808080;font-size:8px;\\\">&nbsp;</TD>";
            }
            $html .= "</TR>";
        }
        $html .= '</TABLE>';
        $html .= "</DIV>";  // Scrollie
        // If you don't have a current Lesson, tell them to make one or choose one.
        $html .= '</DIV>';
        // End Content of the scrollie part
        $html .= '</DIV>';
    $html .= '</TD><TD VALIGN=MIDDLE ALIGN=CENTER>&nbsp;';
    // Add and remove buttons
    //$html .= '<IMG SRC=images/button_l.gif><BR><IMG SRC=images/button_r.gif>';
    //-----------------
    $html .= '</TD><TD VALIGN=TOP ALIGN=LEFT WIDTH=200>';
    // Pane 2
        $html .= "<DIV CLASS=dfc1_{$langpre}res1 STYLE=\\\"background-color: #F0F0F0;\\\">Current Lesson</DIV>";
        $html .= '<DIV CLASS=dfc1find0>&nbsp;</DIV>';
        $html .= "<DIV CLASS=dfc1_{$langpre}ressecp1 STYLE=\\\"color: #333333;background-image: url('images/no_bvocab1p1.jpg');\\\">";
        // Content of the scrollie part
        $html .= '<DIV ID=fmLesson CLASS=dfc1panel1>';
        $lesson_content = $Lesson->_words->dump();
        $html .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=98%>";
        if (is_array($lesson_content)) {
            foreach ($lesson_content as $entry) {
                list($lang,$sha1,$n,$f) = explode('|',$entry);
                $html .= "<TR>";
                $html .= "<TD CLASS=dfc1vocab>$n</TD>";// STYLE=\\\"width:140px;\\\"
                $html .= "<TD ALIGN=RIGHT VALIGN=MIDDLE WIDTH=28><A HREF=# onClick=\\\"rpcFetch('act=set&lbdw=true&lbdw_sha1=$sha1');return false;\\\"><IMG BORDER=0 WIDTH=26 HEIGHT=14 SRC=images/dell.gif ALT=\\\"Remove this word.\\\"></A></TD>";
                $html .= "</TR>";
            }   
        }
        $html .= '</TABLE>';
        $html .= '</DIV>';
        // End Content of the scrollie part
        $html .= '</DIV>';
        $html .= "<CENTER><INPUT CLASS=loginb1 STYLE=\\\"background: url('images/button5.gif');width:140;\\\" TYPE=BUTTON NAME=lbutton VALUE=\\\"Run This Lesson\\\" onClick=\\\"rpcFetch('act=dfc&ibTag=2150');return false;\\\"></CENTER>";
    
    $html .= '</TD></TR>';
    
    if (!$Account->is_guest()) {
        // Lesson Info stuff below...
        $html .= '<TR><TD COLSPAN=3>';
        // Load? Save? What?
        // Pane 3
            $html .= "<DIV STYLE=\\\"padding-top:24px;border-bottom:1px solid #A0A0A0;color:#000040;\\\"><B>Saved Lessons</B></DIV><DIV CLASS=dfc1indent>";
            // Content of the scrollie part
            //$html .= '<DIV CLASS=dfc1panel1>';
            $summary = $Lesson->lesson_summary();
            $html .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=678>";
            $html .= "<TR>";
            $html .= "<TD WIDTH=39><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">&nbsp;</DIV></TD>";
            $html .= "<TD><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Lesson Name</DIV></TD>";
            $html .= "<TD><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Language</DIV></TD>";
            $html .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Words</DIV></TD>";
            $html .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Used</DIV></TD>";
            $html .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">Created</DIV></TD>";
            $html .= "<TD WIDTH=28><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;\\\">&nbsp;</DIV></TD>";
            $html .= "</TR>";
            //TODO: Make Columns Sortable (Use $Lesson->_listSortOrder) or something.
            if ($summary !== false) foreach ($summary['litems'] as $entry) {
                if (($entry['name'] != "[Current]" ) and ($entry['lang'] == $Vocabulary->vLanguage )){
                    $html .= "<TR>";
                    // make anchors to load lessons...
                    if ($entry['lang'] == 2) { $hLang = "<FONT COLOR=#008000>Spanish</FONT>"; $hIcon = "<IMG SRC=images/bfs.gif BORDER=0>"; }
                    if ($entry['lang'] == 4) { $hLang = "<FONT COLOR=#000080>French</FONT>"; $hIcon = "<IMG SRC=images/bss.gif BORDER=0>"; }
                    if ($entry['lang'] == 3) { $hLang = "<FONT COLOR=#800000>German</FONT>"; $hIcon = "<IMG SRC=images/bgs.gif BORDER=0>"; }
                    if (($entry['lang'] < 2) or ($entry['lang'] > 4)){ $hLang = "<FONT COLOR=#808080>Unknown</FONT>"; $hIcon = "<IMG SRC=images/bgs.gif BORDER=0>"; }
                    $hTime = strftime("%b %e, %Y",$entry['created']);
                    $hUsed = intval($entry['used']);
                    $html .= "<TD WIDTH=39><DIV CLASS=dfc1small>$hIcon</DIV></TD>";
                    // Link to load lesson...
                    $html .= "<TD><DIV CLASS=dfc1small><A HREF=# STYLE=\\\"color:#000040;\\\"onClick=\\\"rpcFetch('act=set&ll={$entry['ID']}');return false;\\\">".$entry['name']."</DIV></TD>";
                    $html .= "<TD><DIV CLASS=dfc1small>$hLang</DIV></TD>";
                    $html .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small>".$entry['words']."</DIV></TD>";
                    $html .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small>".$hUsed."</DIV></TD>";
                    $html .= "<TD ALIGN=RIGHT><DIV CLASS=dfc1small>$hTime</DIV></TD>";
                    $html .= "<TD ALIGN=RIGHT VALIGN=MIDDLE><A HREF=# onClick=\\\"rpcFetch('act=set&ldelete=true&lid={$entry['ID']}');return false;\\\"><IMG BORDER=0 WIDTH=26 HEIGHT=14 SRC=images/dell.gif ALT=\\\"Delete this Lesson\\\"></A></TD>";
                    $html .= "</TR>";
                }
            }   
            $html .= "<TR><TD COLSPAN=7><DIV CLASS=dfc1small STYLE=\\\"background-color:#E0E8FF;border-bottom:1px solid #A0A0A0;border-top:1px solid #A0A0A0;\\\">&nbsp;</DIV></TD></TR>";
            $html .= '</TABLE>';
        $html .= '</DIV></TD></TR>';
        
        // Lesson Info stuff below...
        $html .= '<TR><TD COLSPAN=1 VALIGN=TOP STYLE=\\"padding: 0px 32px 0px 0px;\\">';
        // Load? Save? What?
        // Pane 3
            $html .= "<DIV STYLE=\\\"padding-top:24px;border-bottom:1px solid #A0A0A0;color:#000040;\\\"><B>Save Your Current Lesson</B></DIV><DIV CLASS=dfc1indent>";
            // Content of the scrollie part
            //$html .= '<DIV CLASS=dfc1panel1>';
            $html .= '<FORM ACTION=# METHOD=POST NAME=lsave onSubmit=\"rpcFetch(\'act=set&lsave=true\',\'lsave\');return false;\">';
            $html .= "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=100%>";
            if ($err_msg == 0x00400001) {
                $html .= '<TR>';
                $html .= '<TD ALIGN=RIGHT></TD>';
                $html .= '<TD COLSPAN=2><SPAN CLASS=dfc1small STYLE=\"color:#E00000;\">You must enter a name for the lesson.</SPAN></TD>';
                $html .= '</TR>';
            }
            $html .= '<TR>';
            $html .= '<TD ALIGN=RIGHT><DIV CLASS=dfc1small>Lesson Name:</DIV></TD>';
            $html .= "<TD><INPUT CLASS=login1 TYPE=TEXT NAME=cnl_name VALUE=\\\"{$Lesson->_savedName}\\\"></TD>";
            $html .= '<TD ALIGN=RIGHT><INPUT CLASS=loginb1 TYPE=BUTTON NAME=cnl_sub VALUE=Save onClick=\"rpcFetch(\'act=set&lsave=true\',\'lsave\');return false;\"></TD>';
            $html .= '</TR>';
            $html .= '</TABLE>';
            $html .= '</FORM>';
            
        $html .= '</DIV></TD>';
        
        $html .= '<TD COLSPAN=2 VALIGN=TOP>';
    /*        $dbcrap = "";
            if ($Account->has_access(0,2)) { $dbcrap .= "Sp."; }
            if ($Account->has_access(0,3)) { $dbcrap .= "Ge."; }
            if ($Account->has_access(0,4)) { $dbcrap .= "Fr."; }*/
            $html .= "<DIV STYLE=\\\"padding-top:24px;border-bottom:1px solid #C0C0C0;color:#000040;\\\"><B>Create A New Lesson</B></DIV><DIV CLASS=dfc1indent>";
            // Content of the scrollie part
            //$html .= '<DIV CLASS=dfc1panel1>';
            $html .= '<FORM ACTION=# METHOD=POST NAME=lnew onSubmit=\"rpcFetch(\'act=set&lnew=true\',\'lnew\');return false;\">';
            $html .= "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=90%>";
            $html .= '<TR>';
            $html .= '<TD ALIGN=RIGHT><DIV CLASS=dfc1small>Language:</DIV></TD>';
            $html .= '<TD>';
                $html .= '<SELECT NAME=cnl_lang>';
                $html .= '<OPTION VALUE=2>Spanish</OPTION>';
                $html .= '<OPTION VALUE=3>German</OPTION>';
                $html .= '<OPTION VALUE=4>French</OPTION>';
                $html .= '</SELECT>';
            $html .= '</TD>';
            $html .= '<TD ALIGN=RIGHT><INPUT CLASS=loginb1 TYPE=BUTTON NAME=cnl_sub VALUE=Create onClick=\"rpcFetch(\'act=set&lnew=true\',\'lnew\');return false;\"></TD>';
            $html .= '</TR>';
            $html .= '</TABLE>';
            $html .= '</FORM>';
            // End Content of the scrollie part
        
        $html .= '</DIV></TD></TR>';
    }
    $html .= '</TABLE><BR>&nbsp;';
    
    // Show words from browsing or searching
    //$vWords = $Vocabulary->fetch_words();
    //$message = $Vocabulary->vFilter ? ( $Vocabulary->vSearchMode ? "Search Results... Viewing x - y of {$Vocabulary->vSQLCount}" : "Browsing by {$Vocabulary->vBrowse}...Viewing x - y of {$Vocabulary->vSQLCount}" ) : "Vocabulary Database... Viewing x - y of {$Vocabulary->vSQLCount}";
    //$html .= "<DIV CLASS=dfc1vocabtitle>$message</DIV>";
    //$html .= "<DIV CLASS=dfc1vocabscroll>";
    //$html .= "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>";
    /*
    foreach ($vWords as $row) {
        $html .= "<TR>";
        if (($row['Flag'] > 0) || ($Account->has_access(39))) {
            $html .= "<TD><DIV CLASS=dfc1vocab>".htmlentities($row['F'])."</DIV></TD>";
            $html .= "<TD><DIV CLASS=dfc1vocab>".htmlentities($row['N'])."</DIV></TD>";
        } else {
            $html .= "<TD><DIV CLASS=dfc1vocabr>".htmlentities($row['F'])."</DIV></TD>";
            $html .= "<TD><DIV CLASS=dfc1vocabr>".htmlentities($row['N'])."</DIV></TD>";
        }
        $html .= "</TR>";
    }
    $html .= '</TABLE>';
    $html .= $Vocabulary->preserve();
    //$html .= "\"This is the shiznit...\" + ";
    $html .= "</DIV>";  // Scrollie
    */
    //$html .= "</DIV>";
    return $html;
}

function get_country_select($name,$class='',$defcountry='US') {
    $countrycodes = array("AL" => "Albania", "DZ" => "Algeria", "AS" => "American Samoa", "AD" => "Andorra", "AI" => "Anguilla", "AG" => "Antigua Barbuda", "AR" => "Argentina", "AW" => "Aruba", "AU" => "Australia", "AT" => "Austria", "AP" => "Azores", "BS" => "Bahamas", "BH" => "Bahrain", "BD" => "Bangladesh", "BB" => "Barbados", "BY" => "Belarus", "BE" => "Belgium", "BZ" => "Belize", "BJ" => "Benin", "BM" => "Bermuda", "BO" => "Bolivia", "BL" => "Bonaire", "BA" => "Bosnia", "BW" => "Botswana", "BR" => "Brazil", "VG" => "British Virgin Islands", "BN" => "Brunei", "BG" => "Bulgaria", "BF" => "Burkina Faso", "BI" => "Burundi", "KH" => "Cambodia", "CM" => "Cameroon", "CA" => "Canada", "IC" => "Canary Islands", "CV" => "Cape Verde Islands", "KY" => "Cayman Islands", "CF" => "Central African Republic", "TD" => "Chad", "CD" => "Channel Islands", "CL" => "Chile", "CN" => "China", "CO" => "Colombia", "CG" => "Congo", "CK" => "Cook Islands", "CR" => "Costa Rica", "HR" => "Croatia", "CB" => "Curacao", "CY" => "Cyprus", "CZ" => "Czech Republic", "DK" => "Denmark", "DJ" => "Djibouti", "DM" => "Dominica", "DO" => "Dominican Republic", "EC" => "Ecuador", "EG" => "Egypt", "SV" => "El Salvador", "EN" => "England", "GQ" => "Equitorial Guinea", "ER" => "Eritrea", "EE" => "Estonia", "ET" => "Ethiopia", "FO" => "Faeroe Islands", "FM" => "Federated States of Micronesia", "FJ" => "Fiji", "FI" => "Finland", "FR" => "France", "GF" => "French Guiana", "PF" => "French Polynesia", "GA" => "Gabon", "GM" => "Gambia", "GE" => "Georgia", "DE" => "Germany", "GH" => "Ghana", "GI" => "Gibraltar", "GR" => "Greece", "GL" => "Greenland", "GD" => "Grenada", "GP" => "Guadeloupe", "GU" => "Guam", "GT" => "Guatemala", "GN" => "Guinea", "GW" => "Guinea-Bissau", "GY" => "Guyana", "HT" => "Haiti", "HO" => "Holland", "HN" => "Honduras", "HK" => "Hong Kong", "HU" => "Hungary", "IS" => "Iceland", "IN" => "India", "ID" => "Indonesia", "IR" => "Ireland", "IL" => "Israel", "IT" => "Italy", "CI" => "Ivory Coast", "JM" => "Jamaica", "JP" => "Japan", "JO" => "Jordan", "KZ" => "Kazakhstan", "KE" => "Kenya", "KI" => "Kiribati", "KO" => "Kosrae", "KW" => "Kuwait", "KG" => "Kyrgyzstan", "LA" => "Laos", "LV" => "Latvia", "LB" => "Lebanon", "LS" => "Lesotho", "LR" => "Liberia", "LI" => "Liechtenstein", "LT" => "Lithuania", "LU" => "Luxembourg", "MO" => "Macau", "MK" => "Macedonia", "MG" => "Madagascar", "ME" => "Madeira", "MW" => "Malawi", "MY" => "Malaysia", "MV" => "Maldives", "ML" => "Mali", "MT" => "Malta", "MH" => "Marshall Islands", "MQ" => "Martinique", "MR" => "Mauritania", "MU" => "Mauritius", "MX" => "Mexico", "MD" => "Moldova", "MC" => "Monaco", "MS" => "Montserrat", "MA" => "Morocco", "MZ" => "Mozambique", "MM" => "Myanmar", "NA" => "Namibia", "NP" => "Nepal", "NL" => "Netherlands", "AN" => "Netherlands Antilles", "NC" => "New Caledonia", "NZ" => "New Zealand", "NI" => "Nicaragua", "NE" => "Niger", "NG" => "Nigeria", "NF" => "Norfolk Island", "NB" => "Northern Ireland", "MP" => "Northern Mariana Islands", "NO" => "Norway", "OM" => "Oman", "PK" => "Pakistan", "PW" => "Palau", "PA" => "Panama", "PG" => "Papua New Guinea", "PY" => "Paraguay", "PE" => "Peru", "PH" => "Philippines", "PL" => "Poland", "PO" => "Ponape", "PT" => "Portugal", "PR" => "Puerto Rico", "QA" => "Qatar", "IE" => "Republic of Ireland", "YE" => "Republic of Yemen", "RE" => "Reunion", "RO" => "Romania", "RT" => "Rota", "RU" => "Russia", "RW" => "Rwanda", "SS" => "Saba", "SP" => "Saipan", "SA" => "Saudi Arabia", "SF" => "Scotland", "SN" => "Senegal", "SC" => "Seychelles", "SL" => "Sierra Leone", "SG" => "Singapore", "SK" => "Slovakia", "SI" => "Slovenia", "SB" => "Solomon Islands", "ZA" => "South Africa", "KR" => "South Korea", "ES" => "Spain", "LK" => "Sri Lanka", "NT" => "St. Barthelemy", "SW" => "St. Christopher", "SX" => "St. Croix", "EU" => "St. Eustatius", "UV" => "St. John", "KN" => "St. Kitts Nevis", "LC" => "St. Lucia", "MB" => "St. Maarten", "TB" => "St. Martin", "VL" => "St. Thomas", "VC" => "St. Vincent the Grenadines", "SD" => "Sudan", "SR" => "Suriname", "SZ" => "Swaziland", "SE" => "Sweden", "CH" => "Switzerland", "SY" => "Syria", "TA" => "Tahiti", "TW" => "Taiwan", "TJ" => "Tajikistan", "TZ" => "Tanzania", "TH" => "Thailand", "TI" => "Tinian", "TG" => "Togo", "TO" => "Tonga", "TL" => "Tortola", "TT" => "Trinidad Tobago", "TU" => "Truk", "TN" => "Tunisia", "TR" => "Turkey", "TC" => "Turks Caicos Islands", "TV" => "Tuvalu", "UG" => "Uganda", "UA" => "Ukraine", "UI" => "Union Island", "AE" => "United Arab Emirates", "GB" => "United Kingdom", "US" => "United States", "UY" => "Uruguay", "VI" => "US Virgin Islands", "UZ" => "Uzbekistan", "VU" => "Vanuatu", "VE" => "Venezuela", "VN" => "Vietnam", "VR" => "Virgin Gorda", "WK" => "Wake Island", "WL" => "Wales", "WF" => "Wallis Futuna Islands", "WS" => "Western Samoa", "YA" => "Yap", "YU" => "Yugoslavia", "ZR" => "Zaire", "ZM" => "Zambia", "ZW" => "Zimbabwe");
    $hclass =  (strlen($class) > 0) ? "CLASS=$class " : "" ;
    $html = "<SELECT {$hclass}NAME=$name>";
    foreach ($countrycodes as $code => $country) {
        $sel = ($defcountry == $code) ? ' SELECTED' : '' ;
        $html .= "<OPTION VALUE=\\\"$code\\\"$sel>$country</OPTION>";
    }
    $html .= '</SELECT>';
    return $html;
}

?>
