<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

    $iSearch = '<IMG SRC="/Weather/RadarImages/155_060305032900.gif"';
    
    // Go fetch the popup window from keloland.
    $kelopopup = file_get_contents("http://www.keloland.com/ClassLibrary/Page/Weather/RadarDetailPopUp.cfm?W=16&P=0");
    
    // Match by regular expression to find the <IMG> tag with the radar GIF
    preg_match('/RadarImages\/([0-9]+_[0-9]+\.gif)/i',$kelopopup,$matches);
    $gifName = $matches[1];
    
    // Go fetch the GIF for the Radar Image
    $iString = file_get_contents("http://www.keloland.com/Weather/RadarImages/$gifName");
    // Create an image and load the GIF into it.
    $img = imagecreatefromstring($iString);
    // Change the palate index #10 (The Background brown) to Black.
    imagecolorset($img,10,0,0,0);
    // Create a smaller image for scaling
    $iout = imagecreatetruecolor(240,180);
    // Scale the Radar GIF to the new Image using resampling
    if ($_REQUEST['zoom'] == 'true') {
        imagecopyresampled($iout,$img, 0,0,160,120,240,180,320,240);
    } else {
        imagecopyresampled($iout,$img, 0,0,0,0,240,180,640,480);
    }
    //Save the radar image as a PNG so we can reference it in the HTML...
    imagepng($iout,'keloradar.png');
    
    // Generate the HTML for the page.
    echo '<BODY STYLE="margin: 0px; padding: 0px;">';
    echo '<A HREF="?zoom=false">Full</A> <A HREF="?zoom=true">Zoom</A><BR>';
    echo '<meta http-equiv="refresh" content="300">';
    echo "<IMG SRC='keloradar.png'>";
    //print_r(imagecolorat($img,1,1));
    echo '</BODY>';
    
?>
