<?PHP
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------

class fmList {

    var $_list        = array();
    var $_altered     = false;
    var $_index       = -1;
    var $_source      = null;
    var $_paging      = false;
    var $_pagelen     = 15;
    var $_page        = -1;

    //------------------------------------------------------------
    //  Initializes the base variables
    //------------------------------------------------------------
    function init($listsource,$load=false) {
        // Nothing to do.
    }

    function load($words) {
        if (is_array($words)) {
            // Got an array, set it...
            $this->_list = $words;
            $this->_altered = false;
        }
    }

    function dump() {
        return $this->_list;
    }

    function add($item) {
        $this->_list[] = $item;
        $this->_altered = true;
    }

    function clear() {
        // Clears out all the words.
        $this->_list = array();
        $this->_altered = false;
    }

    function insert($item,$pos) {
        // Sanity Checking
        $chunk1 = array_slice($this->_list,0,$pos-1);
        $chunk2 = array_slice($this->_list,$pos);
        $chunk1[] = $item;
        $this->_list[] = array_merge($chunk1,$chunk2);
        $this->_altered = true;
    }

    function move($pos1,$pos2) {
        // move a word around.
        $this->_altered = true;
    }

    function delete($pos) {
        unset($this->_list[$pos]);
        $this->_altered = true;
    }

    function get($pos) {
        return $this->_list[$pos];
    }

    function set_paging($paging,$length=15) {
        $this->_paging = $paging;
        $this->_pagelen = $length;
    }

    function set_page($page) {
        $this->_page = $page;
    }

    function fetch_page() {
        // Return an array of the pages
        return array_slice($this->_list,($this->_page - 1) * $this->_pagelen, $this->_pagelen);
    }

    function count() {
        // Return a count of the words
        return count($this->_list);
    }

    function resequence() {
        if (is_array($this->_list)) {
            $this->_list = array_values($this->_list);
        }
    }
    
    function preserve() {
        $pres = "";
        $pres .= $this->_altered ? "true|" : "false|";
        $pres .= "{$this->_index}|";
        $pres .= $this->_paging ? "true|" : "false|";
        //$pres .= "{$this->_source}|";
        $pres .= "{$this->_pagelen}|";
        $pres .= "{$this->_page}|";
        $data = base64_encode(serialize($this->_list));
        $pres .= "{$data}";
        
        return $pres;
    }
    
    function restore($data) {
        // This returns a string to preserve the state of the vocab engine
        list($this->_altered,$this->_index,$this->_paging,$this->_pagelen,$this->page,$list) = explode('|',$data);
        $this->_altered = ($sm == 'true');
        $this->_paging = ($f == 'true');
        $this->_list = unserialize(base64_decode($list));
    }
}

//---------------------------------------------------------------------------------------------------------------------------------
class fmPrgLesson {

    var $_words        = null;                // fmList of words in the lesson
    var $_lessons      = null;                // List of Saved Lessons
    var $_name         = "New Lesson";        // Lesson Name
    var $_account_id   = null;                // Client's Account ID (needed for SQL Queries)
    var $_session_id   = null;                // Client's Session ID (needed for SQL Queries)
    var $_pos          = 1;                   // Position in the list of words
    var $_repeat       = 0;                   // How many times to repeat the lesson
    var $_language     = 2;                   // Language of the lesson
    //var $_error        = '';                  // Debug stuff

    //------------------------------------------------------------
    //  Initializes the base variables
    //------------------------------------------------------------
    function init($account_id,$session_id) {
        $this->_account_id = ($account_id == "none") ? null : $account_id;
        $this->_session_id = $session_id;
        $this->_words = new fmList;
        $this->_lessons = new fmList;
        // Load list of saved lessons
        $this->refresh_lessons();
    }

    function refresh_lessons() {
        global $db_link, $Timer;
        
        // If they're a guest... they have no lessons...
        if ($this->_account_id !== null) {
            $sql = "SELECT lName FROM Lesson WHERE lOwner='{$this->_account_id}'";
            $Timer->start('sql');
            $result = mysqli_query($db_link, $sql);
            $Timer->stop('sql');
            while ($row = mysqli_fetch_array($result)) {
                $lesson_array[] = $row['lName'];
            }
            $this->_lessons->load($lesson_array);
        }
    }

    function lesson_summary($premade=false) {
        global $db_link, $Timer;

        $summary = array();
        $items = array();
        if ($premade) {
            $sql = "SELECT * FROM Lesson WHERE lOwner LIKE 'PreMade%' ORDER BY lLanguage";
        } else {
            if ($this->_account_id === null) { return false; }
            $sql = "SELECT * FROM Lesson WHERE lOwner='{$this->_account_id}'";
        }
        $Timer->start('sql');
        $result = mysqli_query($db_link, $sql);
        $Timer->stop('sql');
        while ($row = mysqli_fetch_array($result)) {
            $file = unserialize(base64_decode($row['lFile']));
            $entry = array();
            $entry['ID'] = $row['lID'];
            $entry['lang'] = $row['lLanguage'];
            $entry['name'] = $row['lName'];
            $entry['words'] = count($file['words']);
            $entry['created'] = strtotime($row['lCreated']);
            $entry['used'] = $row['lCount'];
            $entry['owner'] = $row['lOwner'];
            if ($entry['name'] != '[Current]') {
                $items[] = $entry;
                $summary['l'.$entry['lang']] += 1;
            }
        }
        $summary['litems'] = $items;
        return $summary;
    }
    
    function fix_lesson_lang() {
        global $db_link, $Timer;

        $sql = "SELECT * FROM Lesson";
        $result = mysqli_query($db_link, $sql);
        while ($row = mysqli_fetch_array($result)) {
            $file = unserialize(base64_decode($row['lFile']));
            $id = $row['lID'];
            $lang = 0;
            if ($file['language'] > 0) {
                $lang = $file['language'];
            } else {
                foreach ($file['words'] as $entry) {
                    list($l,$s,$n,$f) = explode("|",$entry);
                    if ($l > 0) { $lang = $l; break;}
                }
            }
            $sqlupdate[] = "UPDATE Lesson SET lLanguage='$lang' WHERE lID='$id'";
            $errors[] = "{$file['language']} -- $lang";
        }
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        foreach ($sqlupdate as $line) {
            mysqli_query($db_link, $line);
            if (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_errno($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_errno()) ? $___mysqli_res : false)) > 0) {
                $errors[] = "$line || ".((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
            }
        }
        return $errors;
    }
    
    function update_lesson_lang() {
        global $db_link, $Timer;

        $sql = "SELECT * FROM Lesson";
        $result = mysqli_query($db_link, $sql);
        while ($row = mysqli_fetch_array($result)) {
            $file = unserialize(base64_decode($row['lFile']));
            $id = $row['lID'];
            $lang = $file['language'];
            $sqlupdate[] = "UPDATE Lesson SET lLanguage='$lang' WHERE lID='$id'";
        }
        ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        foreach ($sqlupdate as $line) {
            mysqli_query($db_link, $line);
            if (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_errno($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_errno()) ? $___mysqli_res : false)) > 0) {
                $errors[] = "$line || ".((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
            }
        }
        return $errors;
    }
    
    function blank($lang=0) {

        $this->_words->clear();
        $this->_name = '';
        $this->_savedName = '';
        $this->_pos = 1;
        $this->_repeat = 1;
        $this->_language = intval($lang);
        $this->_words->set_paging(true,15);
        $this->_words->set_page(1);
    }
    
    function load($name,$id=0) {
        global $db_link, $Timer;

        // If not logged in, use session, else use account 
        //if ($this->_account_id !== null) {
            $aID = $this->_account_id;
            $loadname = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $name) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
            $loadid = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $id) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
            if ($id == 0) {
                $sql = "SELECT * FROM Lesson WHERE lName='$loadname' AND lOwner='$aID' AND lLanguage='{$this->_language}'";
            } else {
                $sql = "SELECT * FROM Lesson WHERE lID='$loadid' AND (lOwner='$aID' OR lOwner LIKE 'PreMade%')"; //AND lNumber='$flag'
            }
            $Timer->start('sql');
            $result = mysqli_query($db_link, $sql);
            //$this->_error = mysql_error() . ' - ' . mysql_num_rows($result);
            $Timer->stop('sql');
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result);
                $file = unserialize(base64_decode($row['lFile']));
                // FIXME!!! File is not just the words
                // words, language, name, pos, repeat
                $this->_words->load($file['words']);
                $this->_name = $file['name'];
                if ($this->_name != '[Current]') {
                    $this->_savedName = $file['name'];
                }
                $this->_pos = $file['pos'];
                $this->_repeat = $file['repeat'];
                $this->_language = $row['lLanguage'];
                $this->_words->set_paging(true,15);
                $this->_words->set_page(1);
            } else {
                // Not Found! Do something!
            }
        //}
    }

    function save($name,$lang=0) {
        global $db_link, $Timer;

            // FIXME!!! File is not just the words
            // words, language, name, pos, repeat
        // Check for logged in or not....
        // If lesson name = [current] Just wax over it. ? Or not ?
        // $ls_account = $this->_account_id;
        $this->_name = $name;
        if ($name != '[Current]') {
            $this->_savedName = $name;
        }
        $lesson_name = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $this->_name) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $file = array('words' => $this->_words->dump(), 'name' => $this->_name, 'pos' => $this->_pos, 'repeat' => $this->_pos, 'language' => $this->_language);
        $ls_lesson = base64_encode(serialize($file));
        if ($lang == 0) {
            $sLang = $this->_language;
        } else {
            $sLang = intval($lang);
        }
        if ($this->_account_id !== null) {
            $aID = $this->_account_id;
            $Timer->start('sql');
            $result = mysqli_query($db_link, "SELECT count(*) as lcount FROM Lesson WHERE lName='$lesson_name' AND lLanguage='$sLang' AND lOwner='$aID'");
            $row = mysqli_fetch_array($result);
            $Timer->stop('sql');
            if ($row['lcount'] > 0) {
                $sql = "UPDATE Lesson SET lName='$lesson_name', lLastUsed=NOW(), lOwner='$aID', lFile='$ls_lesson' WHERE lName='$lesson_name' AND lLanguage='$sLang'";
            } else {
                $sql = "INSERT INTO Lesson SET lName='$lesson_name', lCreated=NOW(), lLanguage='$sLang', lLastUsed=NOW(), lOwner='$aID', lFile='$ls_lesson' ";
            }
            $Timer->start('sql');
            $result = mysqli_query($db_link, $sql);
            $Timer->stop('sql');
        }
        //echo mysql_error();
    }

    function delete($id) {
        global $db_link, $Timer;

        // Check for logged in or not....
        // If lesson name = [current] Just wax over it. ? Or not ?
        $ls_account = $this->_account_id;
        $mid = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $id) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $Timer->start('sql');
        $result = mysqli_query($db_link, "DELETE FROM Lesson WHERE lID='$mid'");
        $Timer->stop('sql');
        //echo mysql_error();
    }

    function add_word($word_id) {
        // Find the word
        // Check for access rights
        // Add the word

        // FIXME! Should not require these units...
        // FIXME! We don't use the Session to track the lesson anymore.
        global $Account, $db_link, $Timer;

        $Timer->start('sql');
        $result = mysqli_query($db_link, "SELECT wID, wLang, wNative, wForeign, wFlag2, wSHA1 FROM Language WHERE wID='$word_id'");
        $Timer->stop('sql');
        if ($result === false) {
            return false;
        } else {
            // Found the word, Check for Access Rights
            $row = mysqli_fetch_object($result);
            $al_flag = $row->wFlag2;
            $al_SHA = $row->wSHA1;
            $al_l = intval($row->wLang);
            $al_f = $row->wForeign;
            $al_n = $row->wNative;

            if ($al_flag <= 0) {
                if ($Account->has_access(0,$this->_language)) {
                    // User has access to this language, add the word.
                    $wline = "$al_l|$al_SHA|$al_n|$al_f";
                    $this->_words->add($wline);
                }
            } else {
                if ($this->_language != $al_l) {
                    //$dv_status1 = "Error #03-$id: Wrong Language [$al_l:{$Session->get('/program/lesson/language')]";
                } else {
                    // Add the word!
                    $wline = "$al_l|$al_SHA|$al_n|$al_f";
                    $this->_words->add($wline);
                }
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            return true;
        }
    }

    function del_word($word_id) {
        $cnt = $this->_words->count();
        for ($i=0;$i<$cnt;$i++) {
            $line = $this->_words->get($i);
            list($yl_l,$yl_s,$yl_n,$yl_f) = explode("|",$line);
            if ($word_id == $yl_s) { $this->_words->delete($i); break; }
        }
    }

    function get_page() {
        return $this->_words->fetch_page();
    }
    
    function count() {
        return $this->_words->count();
    }
    
    function get_word($pos) {
        //echo "prg_lesson.php:\$this->get_word(\$pos);  [$pos]<BR>";
        return $this->_words->get($pos);
    }
    
    function resequence() {
        $this->_words->resequence();
    }
    
    function preserve() {
        // This returns a string to preserve the state of the vocab engine
        $pres = "";
        $pres .= "{$this->_name}|";
        $pres .= "{$this->_savedName}|";
        $pres .= "{$this->_pos}|";
        $pres .= "{$this->_repeat}|";
        $pres .= "{$this->_language}|";
        $data = base64_encode($this->_words->preserve());
        $pres .= "$data";
        
        return $pres;
    }
    
    function restore($data) {
        // This returns a string to preserve the state of the vocab engine
        list($r_name,$r_savedName,$r_pos,$r_repeat,$r_language,$list) = explode('|',$data);
        $this->_name = $r_name;
        $this->_savedName = $r_savedName;
        $this->_pos = intval($r_pos);
        $this->_repeat = intval($r_repeat);
        if (($r_language < 2) or ($r_language > 4)) { $r_language = 2; }
        $this->_language = intval($r_language);
        $this->_words->restore(base64_decode($list));
    }
}

/*
$account = $PERS["account"];
//$Timer->start('sql');
//$Timer->stop('sql');
$p3 = mysql_num_rows($result);
$p1 = $dv_spos;
$_l_ar = array();
while ($row = mysql_fetch_object($result)) {
    $_l_ar[] = $row->lName;
}
//$p3 = count($ls_words);
echo "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=6 WIDTH=480>";
echo "<TR>";
echo "<TD COLSPAN=2 VALIGN=TOP ALIGN=LEFT WIDTH=200>";

echo "<DIV CLASS=hr_grey>";
echo "  <TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=200>";
echo "  <TR><TD ALIGN=LEFT><DIV CLASS=t><B>&nbsp;Your Saved Lessons</B></DIV></TD>";
echo "  <TD ALIGN=RIGHT><DIV CLASS=t>$p1 of $p3&nbsp;</DIV></TD></TR></TABLE>";
echo "<BR>";
echo "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=200>";
echo "<TR><TD VALIGN=TOP WIDTH=20></TD>";
echo "<TD VALIGN=BOTTOM COLSPAN=2><DIV CLASS=sql_result>Saved Lessons</DIV></TD>";
echo "<TR><TD VALIGN=TOP WIDTH=14></TD>";
$sc_cnt = 1;
// Limits for page view.... use fmList.
$lar_count = count($_l_ar);
$lar_total = 500;
$lar_from = 0;
$lar_to = 3;
foreach ($_l_ar as $value) {
    echo "<TR>";
    echo "<TD VALIGN=BOTTOM WIDTH=20><DIV CLASS=sql_result>&nbsp;";
    echo "<A HREF=program.php>del</A>&nbsp;</DIV></TD>";
    echo "<TD VALIGN=BOTTOM><DIV CLASS=sql_result>";
    echo "$value";
    echo "</DIV></TD>";
    echo "<TD VALIGN=TOP WIDTH=20>";
    echo "  <FORM ACTION=program.php METHOD=POST NAME=loadlesson_$ss_cnt>";
    echo "  <INPUT TYPE=HIDDEN NAME=loadlesson VALUE=\"$value\">";
    echo "  <INPUT CLASS=fmbutton1 TYPE=SUBMIT SRC=icons/button.png VALUE=\"&nbsp;Load&nbsp\" onclick=\"document.location.href='program.php';\">";
    echo "  </FORM>";
    echo "</TD>";
    if ($sc_cnt == 1) {echo "<TD CLASS=scsm1 WIDTH=14><A HREF=program.php?spos=prev><IMG SRC=icons/up.gif></A></TD>"; }
    if (($sc_cnt == $p4) and ($sc_cnt != 1)) {
        echo "<TD CLASS=scsm3 WIDTH=14><DIV CLASS=scsm2><IMG SRC=icons/blank8x8.gif HEIGHT=100%><BR></DIV>";
        if ($sc_cnt == 15) { echo "<A HREF=program.php?spos=next><IMG SRC=icons/down.gif></A>"; }
        echo "</TD>";
    }
    if (($sc_cnt < $p4) and ($sc_cnt > 1)) {echo "<TD CLASS=scsm2 WIDTH=14><DIV CLASS=scsm2><IMG SRC=icons/blank8x8.gif HEIGHT=100%><BR></DIV></TD>"; }
    echo "</TR>";
    $sc_cnt++;
}
echo "</TABLE>";
echo "</DIV><BR>";

echo "<FORM METHOD=POST ACTION=program.php NAME=savelessonform>";
echo "<INPUT TYPE=HIDDEN NAME=lessonsave VALUE=\"true\">";
echo "<DIV CLASS=hr_yellow>";
echo "  <TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=200>";
echo "  <TR><TD ALIGN=LEFT><DIV CLASS=t><B>&nbsp;Save Your Current Lesson</B></DIV></TD>";
echo "  <TD ALIGN=RIGHT><DIV CLASS=t>&nbsp;</DIV></TD></TR></TABLE>";
echo "<BR>";
echo "  <TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=200>";
echo "  <TR><TD VALIGN=TOP>&nbsp;&nbsp;<INPUT CLASS=sql_editb NAME=lesson_name SIZE=25></TD></TR>";
echo "  <TR><TD VALIGN=BOTTOM ALIGN=RIGHT><INPUT CLASS=fmbutton1 TYPE=SUBMIT SRC=icons/button.png NAME=ls_save VALUE=\"&nbsp;Save&nbsp\"></TD></TR>";
echo "  </TABLE>";
echo "</DIV>";
echo "</TD>";

echo "</TD>";
echo "<TD COLSPAN=2 VALIGN=TOP ALIGN=RIGHT WIDTH=250>";

echo "<DIV CLASS=hr_green>";
echo "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=250>";
echo "<TR><TD ALIGN=LEFT><DIV CLASS=t><B>&nbsp;Your Current Lesson</B></DIV></TD>";
$lessonlist->set_page($dv_lpos);
$wpage = $lessonlist->fetch_page();
$pcount = count($wpage);
$ptotal = $lessonlist->count();
$pfrom = ($dv_lpos - 1) * 15 + 1;
$pto = $pfrom + $pcount;
if ($pto > $ptotal) { $pto = $ptotal; }
echo "<TD ALIGN=RIGHT><DIV CLASS=t>$pfrom-$pto of $ptotal&nbsp;</DIV></TD></TR></TABLE>";
echo "<BR>";
echo "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=200>";
echo "<TR><TD VALIGN=TOP WIDTH=20></TD>";
echo "<TD VALIGN=BOTTOM><DIV CLASS=sql_result>Foreign Word</DIV></TD>";
echo "<TR><TD VALIGN=TOP WIDTH=14></TD>";
$sc_cnt = 1;
foreach ($wpage as $line) {
    list($yl_l,$yl_s,$yl_n,$yl_f) = explode("|",$line);
    echo "<TR>";
    echo "<TD VALIGN=TOP WIDTH=20><DIV CLASS=sql_result>&nbsp;";
    echo "<A HREF=program.php?dl=$yl_s>del</A>&nbsp;</DIV></TD>";
    echo "<TD VALIGN=BOTTOM><DIV CLASS=sql_result>";
    echo "$yl_f";
    echo "</DIV></TD>";
    if ($sc_cnt == 1) {echo "<TD CLASS=scsm1 WIDTH=14><A HREF=program.php?lpos=prev><IMG SRC=icons/up.gif></A></TD>"; }
    if (($sc_cnt == $pcount) and ($sc_cnt != 1)) {
        echo "<TD CLASS=scsm3 WIDTH=14><DIV CLASS=scsm2><IMG SRC=icons/blank8x8.gif HEIGHT=100%><BR></DIV>";
        if ($sc_cnt == 15) { echo "<A HREF=program.php?lpos=next><IMG SRC=icons/down.gif></A>"; }
        echo "</TD>";
    }
    if (($sc_cnt < $pcount) and ($sc_cnt > 1)) {echo "<TD CLASS=scsm2 WIDTH=14><DIV CLASS=scsm2><IMG SRC=icons/blank8x8.gif HEIGHT=100%><BR></DIV></TD>"; }
    echo "</TR>";
    $sc_cnt++;
}            echo "</TABLE>";
echo "</DIV>";
echo "</TD>";
echo "</TR></TABLE>";
*/


?>
