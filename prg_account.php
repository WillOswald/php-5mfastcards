<?PHP
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------

class fmAccount {

    var $_ContactInfo        = array();  // Name, addie, e-mail, etc...
    var $_Tracking           = array();  // IP Adresses, Browser Ident, Warn Level,
    var $_Visits             = 0;
    var $_LastVisit          = 0;
    var $_Created            = null;
    //var $_Session            = string;
    var $_Guest              = true;
    var $_Access             = "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNGGNNNNNNNNN";
    var $_AudioSettings      = 0;
    var $_Account            = "none";
    var $_Debug              = '';

    //------------------------------------------------------------
    //  Initializes the base variables
    //------------------------------------------------------------
    function init($sessionid = null) {
        // Initialize some stuff

        global $db_link, $Timer;
        if ($sessionid == null) {
            $this->_ContactInfo['FName'] = 'Guest';
            $this->_ContactInfo['LName'] = '';
            $this->_ContactInfo['email'] = '';
            $this->_Account = 'none';
            $this->_Guest = true;
            $this->_Access = "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNGGNNNNNNNNN";
        } else {
            $sql = "SELECT * FROM Account WHERE uSessionID='$sessionid'";
            $Timer->start('sql');
            $result = mysqli_query($db_link, $sql);
            $Timer->stop('sql');
            if (!empty($result) && mysqli_num_rows($result) > 0) {
                // FIXME - What if this brings back more than one account?
                // FIXME - What if their account has expired?
                $row = mysqli_fetch_object($result);
                $this->_Guest = false;
                $this->_Account = $row->uSHA1;
                $this->_Access = $row->uAccess;
                $this->_ContactInfo['FName'] = $row->uFName;
                $this->_ContactInfo['LName'] = $row->uLName;
                $this->_ContactInfo['Address1'] = $row->uAddress1;
                $this->_ContactInfo['Address2'] = $row->uAddress2;
                $this->_ContactInfo['POBox'] = $row->uPOBox;
                $this->_ContactInfo['City'] = $row->uCity;
                $this->_ContactInfo['State'] = $row->uState;
                $this->_ContactInfo['Zip'] = $row->uZip;
                $this->_ContactInfo['Country'] = $row->uCountry;
                $this->_ContactInfo['Province'] = $row->uProvince;
                $this->_Created = strtotime($row->uCreated);
                $this->_LastVisit = strtotime($row->uAccessed);
                $this->_Visits = trim($row->uVisits);
                $this->_ContactInfo['email'] = $row->uEmail;
                $this->_AudioSettings = $row->uAudioSettings;
                $sql = "UPDATE Account SET uAccessed=NOW() WHERE uSessionID='$sessionid'";
                $Timer->start('sql');
                $result = mysqli_query($db_link, $sql);
                $Timer->stop('sql');
            } else {
                $this->_ContactInfo['FName'] = 'Guest';
                $this->_ContactInfo['LName'] = '';
                $this->_ContactInfo['email'] = '';
                $this->_Account = 'none';
                $this->_Guest = true;
                $this->_Access = "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNGGNNNNNNNNN";
            }
        }
    }
    // Need function for Login, Session, Logout, Validate, Update, Create etc...

    function getinfo($info) {
        return $this->_ContactInfo[$info];
    }

    function is_guest() {
        return $this->_Guest;
    }
    function getID() {
        return $this->_Account;
    }

    function listAdmins() {
        global $db_link, $Timer;
        
        $owners = array();
        $result = mysqli_query($db_link, "SELECT uFName,uLName,uSHA1 FROM Account WHERE uAccess LIKE '______________________________________G_'");
        while ($row = mysqli_fetch_assoc($result)) {
            $data = array();
            $data['name'] = $row['uFName'] . ' ' . $row['uLName'];
            $data['id'] = $row['uSHA1'];
            $owners[] = $data;
        }
        return $owners;
    }
    
    function finger($id) {
        global $db_link, $Timer;
        
        $owners = array();
        $eid = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $id) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $result = mysqli_query($db_link, "SELECT * FROM Account WHERE uSHA1 = '$eid'");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            $row = mysqli_fetch_assoc($result);
            $data = array();
            foreach ($row as $field => $value) {
                $data[$field] = $value;
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
        }
        return $data;
    }
    
    function login($email, $password, $sessionid) {
        global $db_link, $Timer;
        
        // Login to the Account System.
        if (($email == '') or ($password == '')) {
            return 1;
        }
        if (!$this->is_guest()) {$this->logout();}
        $lg_email = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $email) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $lg_pass = sha1($password);
        
        $sql = "SELECT * FROM Account WHERE uEmail='$lg_email'";
        $Timer->start('sql');
        $result = mysqli_query($db_link, $sql);
        $Timer->stop('sql');
        if (!$result->num_rows) {
            return 1;
        } else {
            $row = mysqli_fetch_object($result);
            if (trim($row->uPassword) != trim($lg_pass)) {
                return 2;
            } else {
                $this->_Guest = false;
                $this->_Account = $row->uSHA1;
                $this->_Access = $row->uAccess;
                $this->_ContactInfo['FName'] = $row->uFName;
                $this->_ContactInfo['LName'] = $row->uLName;
                $this->_ContactInfo['Address1'] = $row->uAddress1;
                $this->_ContactInfo['Address2'] = $row->uAddress2;
                $this->_ContactInfo['POBox'] = $row->uPOBox;
                $this->_ContactInfo['City'] = $row->uCity;
                $this->_ContactInfo['State'] = $row->uState;
                $this->_ContactInfo['Zip'] = $row->uZip;
                $this->_ContactInfo['Country'] = $row->uCountry;
                $this->_ContactInfo['Province'] = $row->uProvince;
                $this->_Created = strtotime($row->uCreated);
                $this->_LastVisit = strtotime($row->uAccessed);
                $this->_Visits = trim($row->uVisits);
                $this->_ContactInfo['email'] = $email;
                $this->_AudioSettings = $row->uAudioSettings;
                $sql = "UPDATE Account SET uSessionID='$sessionid', uAccessed=NOW(), uVisits=uVisits+1 WHERE uEmail='$lg_email'";
                $Timer->start('sql');
                mysqli_query($db_link, $sql);
                $Timer->stop('sql');
                $sql = "select sSessionID,sSessionData FROM Session WHERE sSessionID='{$sessionid}'";
                $Timer->start('sql');
                $sresult = mysqli_query($db_link, $sql);
                $Timer->stop('sql');
                $srow = @mysqli_fetch_assoc($sresult);
                $sdata = unserialize(base64_decode($srow['sSessionData']));
                $id = $sdata['log']['logid'];
                $sql = "UPDATE log_enter SET member='1' WHERE logID='$id'";
                $Timer->start('sql');
                mysqli_query($db_link, $sql);
                $Timer->stop('sql');
            }
        }
        return 0;
    }

    function logout() {
        global $db_link, $Timer;
        // Logout from the Account System.

        $lg_email = $this->_ContactInfo['email'];
        $sql = "UPDATE Account SET uSessionID='' WHERE uEmail='$lg_email'";
        $Timer->start('sql');
        mysqli_query($db_link, $sql);
        $Timer->stop('sql');
        $this->_Account = 'none';
        $this->_Guest = true;
        $this->_Access = "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNGGNNNNNNNNN";
        $this->_ContactInfo['FName'] = 'Guest';
        $this->_ContactInfo['LName'] = '';
        $this->_ContactInfo['email'] = '';
        //$PERS['program_position'] = "100";
        //$PERS['signup'] = "700";
    }
    
    function register_account($fields) {
        global $db_link, $Timer;
        // Create a new account 
        $data = $fields;
        $data['uPassword'] = trim(sha1($data["uPassword"]));
        $data['uSHA1'] = trim(sha1("{$data['uEmail']}/{$data['uPassword']}/{$data["uFName"]}/{$data["uLName"]}"));
        $data['uAccess'] = 'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNGGNNNNNNNNN';
        $data['uVisits'] = '0';
        $sql = 'INSERT INTO Account SET';
        foreach ($data as $field => $value) {
            $mvalue = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $value) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
            $sql .= " $field='$mvalue',";
        }
        $sql .= " uCreated=NOW(),";
        $sql .= " uAccessed=NOW()";
        // SQL is prepared, now insert the record....
        $Timer->start('sql');
        mysqli_query($db_link, $sql);
        $Timer->stop('sql');
        // Check to see if there were any errors...
    }
    
    function is_registered($email) {
        global $db_link, $Timer;
        $memail = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $email) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $Timer->start('sql');
        $result = mysqli_query($db_link, "SELECT * from Account WHERE uEmail='$memail'");
        $Timer->stop('sql');
        if ($result !== false) {
            return (mysqli_num_rows($result) > 0);
				}
    }
    
    function LessonStorage() {
        $lessonmax = 0; // Free Account
        $splev = $this->req_access(1);
        $frlev = $this->req_access(2);
        $gelev = $this->req_access(3);
        $wlev = $this->req_access(31);
        $lessonmax += (($splev == 'P') or ($splev == 'G') or ($splev == 'C')) ? 50 : (($wlev == 'S') ? 0 : 5);
        $lessonmax += (($frlev == 'P') or ($frlev == 'G') or ($frlev == 'C')) ? 50 : (($wlev == 'S') ? 0 : 5);
        $lessonmax += (($gelev == 'P') or ($gelev == 'G') or ($gelev == 'C')) ? 50 : (($wlev == 'S') ? 0 : 5);
        return $lessonmax;
    }

    function grant($lang=0,$type,$level=0) {
        global $db_link, $Timer;
        
        if ($level == 0) {
            if ($lang == 2) {$clevel = 1;} // Spanish
            if ($lang == 3) {$clevel = 3;} // German
            if ($lang == 4) {$clevel = 2;} // French
        } else {
            $clevel = $level;
        }
        $this->_Access[$clevel-1] = $type;
        mysqli_query($db_link, "UPDATE Account SET uAccess='{$this->_Access}' WHERE uSHA1='{$this->_Account}'");
        return ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
    }
    
    function req_access($level) {
        // Request access for a specified level.
        return $this->_Access[$level-1];
    }

    function has_access($level=0,$lang=0) {
        // Request access for a specified level.
        if ($level == 0) {
            if ($lang == 2) {$clevel = 1;} // Spanish
            if ($lang == 3) {$clevel = 3;} // German
            if ($lang == 4) {$clevel = 2;} // French
        } else {
            $clevel = $level;
        }
        return (($this->_Access[$clevel-1] == "G") or ($this->_Access[$clevel-1] == "P") or ($this->_Access[$clevel-1] == "C"));
    }

    function get_audio_settings() {
        // Request Current Audio Settings.
        return $this->_AudioSettings;
    }

    function set_audio_settings($setting) {
        // Request Current Audio Settings.

        global $db_link, $Timer;
        $sql = "UPDATE Account SET uAudioSettings=$setting WHERE uSHA1='{$this->_Account}'";
        $Timer->start('sql');
        mysqli_query($db_link, $sql);
        $Timer->stop('sql');

    }
}

$Access_Codes = Array(
    'N' => 'None',
    'G' => 'Granted',
    'S' => 'Suspended',
    'D' => 'Deleted',
    'P' => 'Purchased',
    'C' => 'Gift Certificate'
);

$Access_Translate = Array(
    1 => 'Spanish Vocabulary',
    2 => 'French Vocabulary',
    3 => 'German Vocabulary',
    4 => 'Russian Vocabulary',
    5 => 'Italian Vocabulary',
    6 => 'English Vocabulary',
    7 => '--',
    8 => '--',
    9 => '--',
    10 => '--',
    11 => '--',
    12 => '--',
    13 => '--',
    14 => '--',
    15 => '--',
    16 => '--',
    17 => '--',
    18 => '--',
    19 => '--',
    20 => '--',
    21 => '--',
    22 => '--',
    23 => '--',
    24 => '--',
    25 => '--',
    26 => '--',
    27 => '--',
    28 => '--',
    29 => '--',
    30 => 'Demo Access',
    31 => 'Web Page Access',
    32 => '--',
    33 => '--',
    34 => 'Report Access',
    35 => 'Web Edit Access',
    36 => 'Account Access',
    37 => 'Program Access',
    38 => 'Log Access',
    39 => 'Administrative Access',
    40 => 'Database Access'
);

?>
