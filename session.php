<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

class fmSession {

    var $_SessionID          = "none";
    var $_SessionData        = array();
    var $_Server             = 'none';
    var $_UserAgent          = 'none';
    var $_UserIP             = '0.0.0.0';
    var $_CookieName         = 'default';
    var $_debug              = 'No Debugging Information';

    //------------------------------------------------------------
    //  Initializes the base variables
    //------------------------------------------------------------
    function init($cookie_name, $session=false) {
        global $db_link, $Timer;

        // FIXME!! Need to compensate against Browser ID, IP, Cookies, etc for Spiders/Bots
        
        // Return Value... 0=New 1=expired
        $ret = 0;
        // Initialize some stuff, check cookie data, pull from database.
        $this->_CookieName = $cookie_name;
        $this->post('/log/cookie',$cookie_name);
        $this->_Server = $_SERVER['SERVER_NAME'];
        //echo "INIT - CookieData = $cookiedata<BR>INIT - CookieLen  = ".strlen($cookiedata)."<BR>";
        if (!isset($_COOKIE[$this->_CookieName])) {
            // No Session Found.... Start a new one...
            $this->new_session();
            return $ret;
        } else {
            // Session Exists... Load and update expiration.
            if ($session === false) {
                $this->_SessionID = $_COOKIE[$this->_CookieName];
            } else {
                $this->_SessionID = $session;
            }
            $fetch_sql = "select sID,sSessionID,sSessionData,sExpires,sCreated,(NOW() >= sExpires) as Expired,(NOW() - sExpires) as ExpiredAt from Session WHERE sSessionID='{$this->_SessionID}'";
            $Timer->start('sql');
            $result = mysqli_query($db_link, $fetch_sql);
            $Timer->stop('sql');
            if (mysqli_num_rows($result) == 0) {
                // Not Found... Bad Cookie or Session Expired
                $this->new_session();
                return $ret;
            } else {
                $row = mysqli_fetch_object($result);
                // is session expired?
                //$this->post('/debug/session/expired',$row->Expired);
                //$this->post('/debug/session/expiredAt',$row->ExpiredAt);
                if (intval($row->Expired) == 1) {
                    // Expired Session... Now what?
                    $expiredTime = $row->ExpiredAt;
                    if ($expiredTime < 3600) {
                        // Expired within the past Hour
                        $ret = 1;
                    }
                    $this->destroy($cookie_name);
                    $this->new_session();
                    return $ret;
                } else {
                    // Session Valid... restore session data and update expires.
                    // Check to see if session has expired... Notify User somehow...
                    $this->_SessionData = unserialize(base64_decode($row->sSessionData));
                    $this->_SessionID = $row->sSessionID;
                    $this->_Created = $row->sCreated;
                    @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
                    $update_sql = "UPDATE Session SET sExpires=NOW() + INTERVAL 25 MINUTE WHERE sSessionID='{$this->_SessionID}'";
                    $Timer->start('sql');
                    mysqli_query($GLOBALS["___mysqli_ston"], $update_sql);
                    $Timer->stop('sql');
                    $id = $this->get("/log/logid");
                    $sql = "UPDATE log_enter SET clicks = clicks + 1 WHERE logID='$id'";
                    $Timer->start('sql');
                    mysqli_query($db_link, $sql);
                    $Timer->stop('sql');
                }
            }
        }
        return $ret;
    }


    function destroy($cookiename) {
        global $db_link, $Timer;

        $this->_SessionID = $_COOKIE[$this->_CookieName];
        $fetch_sql = "SELECT count(*) as total FROM Session WHERE sSessionID='{$this->_SessionID}'";
        $Timer->start('sql');
        $result = mysqli_query($db_link, $fetch_sql);
        $Timer->stop('sql');
        $session_count = mysqli_fetch_array($result);
        if ($session_count['total'] > 0) {
            // Got a session... delete it and remove cookie.
            $fetch_sql = "DELETE FROM Session WHERE sSessionID='{$this->_SessionID}'";
            $Timer->start('sql');
            $result = mysqli_query($db_link, $fetch_sql);
            $Timer->stop('sql');
            setcookie($this->_CookieName, $cookiedata, 0, '/', $this->_Server);
        }
    }

    function new_session() {
        global $db_link, $Timer;

        // Create a new session
        $this->_UserIP = $_SERVER['REMOTE_ADDR'];
        $this->_UserAgent = $_SERVER['HTTP_USER_AGENT'];
        $Time = strval(time());
        $encode = "{$this->_UserIP}:{$this->_UserAgent}:$Time";
        $this->_SessionID = sha1($encode);

        // Pre-init some stuff...
        $this->post("/log/logstate",'new');  //New, Res, Complete
        $this->post("/log/entry_time", time());
        // Check for User Agent...
        if ($_SERVER['HTTP_USER_AGENT'] != '') {
            $result = $this->agentID($_SERVER['HTTP_USER_AGENT']);  //FIXME!! Get the referrer as well and log it.
            $ip = $_SERVER['REMOTE_ADDR'];
            $port = $_SERVER['REMOTE_PORT'];
            $t = time();
            $bid = $this->addBrowser($result['browser']['name'],$result['browser']['ver']);
            $oid = $this->addOS($result['os']['id']);
            //echo "<PRE>".print_r($result,true)."<PRE>";
            $mid = $this->addMoz($result['moz']['name'],$result['moz']['ver']);
            @$rid = $this->addRefer($_SERVER['HTTP_REFERER']);
            $sql = "INSERT INTO log_enter SET ip_address=INET_ATON('$ip'), entry_time='$t', ip_port='$port',browser_id='$bid',os_id='$oid',moz_id='$mid',refered='$rid',clicks='0'";
            mysqli_query($db_link, $sql);
            //echo mysql_error();
            $idresult = mysqli_query($db_link, "select LAST_INSERT_ID() as ID;");
            $row = mysqli_fetch_assoc($idresult);
            $id = $row['ID'];
            ((mysqli_free_result($idresult) || (is_object($idresult) && (get_class($idresult) == "mysqli_result"))) ? true : false);
            $this->post("/log/logstate",'res');  //New, Res, Complete
            $this->post("/log/logid",$id);  //ID of the Log Entry
        }
        // Initialize the session and create database row...
        $data = base64_encode(serialize($this->_SessionData));
        $current_time = strftime("%Y-%m-%d %T");
        $sql = "INSERT INTO Session SET sSessionID='{$this->_SessionID}', sCreated=NOW(), sExpires=NOW() + INTERVAL 25 MINUTE, sSessionData='$data' ";
        $Timer->start('sql');
        mysqli_query($db_link, $sql);
        $Timer->stop('sql');

        // Store the Cookie that holds the session ID for database retrieval later...
        $cookiedata = $this->_SessionID;
        $expires = time() + 1500; // +60 Minutes        31536000 = +1 year
        //setcookie($this->_CookieName, $cookiedata, $expires, '/', $this->_Server);
        setcookie($this->_CookieName, $cookiedata, 0, '/', $this->_Server);
        return $this->_SessionID;
    }

    function save() {
        global $db_link, $Timer;

        // Save the session data to disk.
        $data = base64_encode(serialize($this->_SessionData));
        $update_sql = "UPDATE Session SET sExpires=NOW() + INTERVAL 25 MINUTE, sSessionData='$data' WHERE sSessionID='{$this->_SessionID}'";
        $Timer->start('sql');
        mysqli_query($GLOBALS["___mysqli_ston"], $update_sql);
        $Timer->stop('sql');
        if ($this->_debug) {
            $this->_debug = '<B>Session-Function:</B> fmSession->save()<BR>';
            $this->_debug .= "<B>Session-ID:</B> '{$this->_SessionID}'<BR>";
            $this->_debug .= '<B>MySQL-Affected-Rows:</B> ' .mysqli_affected_rows($GLOBALS["___mysqli_ston"]) . '<BR>';
            $this->_debug .= '<B>MySQL-Error:</B> (' .((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_errno($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_errno()) ? $___mysqli_res : false)) .') ' . ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)) . '<BR>';
        }
    }

    function getID() {
        return $this->_SessionID;
    }

    function post($url, $data=NULL) {
        if ($data === NULL) {
            // URL formatted posting of session variables  "/program/location/ipage=4"
            //list($var,$data) = preg_split("/=/", $url);
            $dl = strpos($url,'=');
            if (is_int($dl)) {
                $var = substr($url,0,$dl);
                $data = substr($url,$dl+1);
            } else {
                $var = $url;
            }
        } else {
            $var = $url;
        }
        $curr_var = "this->_SessionData";
        $tree = preg_split("/\\//", $var);
        foreach ($tree as $name) {
            if ($name != '') { $curr_var .= "[$name]"; }
        }
        $print_data = print_r($data,true);
        $type_data = gettype($data);
        //echo "<FONT FACE=Courier><FONT COLOR=#A00000>[$var]</FONT> <FONT COLOR=#0000A0>[$print_data] - [$type_data]</FONT> $$curr_var = \$data; </FONT><BR>";
        if ($data == '') { $data = "''"; }
        @eval("$$curr_var = \$data;");
    }

    function get($url) {
        // URL formatted fetching of session variables
        $curr_var = "this->_SessionData";
        $tree = preg_split("/\\//", $url);
        foreach ($tree as $name) {
            if ($name != '') { $curr_var .= "[$name]"; }
        }
        $evalcode = "\$data = $$curr_var;";
        @eval($evalcode);
        return $data;
    }
    
    function agentID($agent) {
        // Breakdown the Browser ID
        $match_moz = "/^([\w-]+)[\/]([\d\.]+)/i";
        $match_os = "/(Windows [0-9]+|Windows NT [0-9]+[\.]?[0-9]+|Linux|SunOS|Mac_PowerPC|PPC Mac OS X|Win98|FreeBSD|NetBSD)/i";
        //$match_browser = "/(MSIE [\d\.]+|Konqueror\/[\d\.rc-]+|MS FrontPage[\s\d\.]+|Opera\/[\d\.rc-]+|WebTV\/[\d\.]+|Safari\/[\d\.rc-]+|Firefox\/[\d\.rc-]+|Netscape\/[\d\.rc-]+)/i";
        $match_browser = "/(MSIE|Konqueror|MS FrontPage|Opera|WebTV|Safari|Firefox|Netscape|Links)[\/ ][\(]{0,1}([\d\.pre]+)/i";
        $match_gecko = "/Gecko\/[\d\.rc-]+/i";
        $match_guid = "/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i";
        
        $count_moz = preg_match_all($match_moz,$agent,$matches);
        if ($count_moz > 0) {
            $ret_moz = array('id' => $matches[0][0],'name' => $matches[1][0],'ver' => $matches[2][0]);
        } else {
            $ret_moz = array('id' => 'Unknown','name' => 'Unknown','ver' => '--');
        }
        $count_os = preg_match_all($match_os,$agent,$matches);
        if ($count_os > 0) {
            $ret_os = array('id' => $matches[0][0]);
        } else {
            $ret_os = 'Unknown';
        }
        $count_browser = preg_match_all($match_browser,$agent,$matches);
        if ($count_browser > 0) {
            $ret_browser = array('id' => $matches[0][0],'name' => $matches[1][0],'ver' => $matches[2][0]);
        } else {
            $ret_browser = array('id' => 'Unknown','name' => 'Unknown','ver' => '--');
        }
        $ret = array('moz' => $ret_moz,'os' => $ret_os,'browser' => $ret_browser);
        return $ret;
    }

    function addBrowser($bname,$bver) {
        global $db_link, $Timer;
        
        $ebName = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $bname) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $ebVer = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $bver) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $result = mysqli_query($db_link, "SELECT * from log_browser WHERE browser='$ebName' AND ver='$ebVer'");
        $count = mysqli_num_rows($result);
        if ($count > 0) {
            $row = mysqli_fetch_assoc($result);
            $id = $row['ID'];
        } else {
            mysqli_query($db_link, "INSERT INTO log_browser SET browser='$ebName', ver='$ebVer'");
            $result = mysqli_query($db_link, "select LAST_INSERT_ID() as ID;");
            $row = mysqli_fetch_assoc($result);
            $id = $row['ID'];
        }
        return $id;
    }

    function addOS($oname) {
        global $db_link, $Timer;
        
        $eoName = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $oname) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $result = mysqli_query($db_link, "SELECT * from log_os WHERE os='$eoName'");
        $count = mysqli_num_rows($result);
        if ($count > 0) {
            $row = mysqli_fetch_assoc($result);
            $id = $row['ID'];
        } else {
            mysqli_query($db_link, "INSERT INTO log_os SET os='$eoName'");
            $result = mysqli_query($db_link, "select LAST_INSERT_ID() as ID;");
            $row = mysqli_fetch_assoc($result);
            $id = $row['ID'];
        }
        return $id;
    }

    function addMoz($bname,$bver) {
        global $db_link, $Timer;
        
        $ebName = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $bname) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $ebVer = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $bver) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $result = mysqli_query($db_link, "SELECT * from log_moz WHERE name='$ebName' AND ver='$ebVer'");
        $count = mysqli_num_rows($result);
        if ($count > 0) {
            $row = mysqli_fetch_assoc($result);
            $id = $row['ID'];
        } else {
            mysqli_query($db_link, "INSERT INTO log_moz SET name='$ebName', ver='$ebVer'");
            $result = mysqli_query($db_link, "select LAST_INSERT_ID() as ID;");
            $row = mysqli_fetch_assoc($result);
            $id = $row['ID'];
        }
        return $id;
    }
    
    function addRefer($url) {
        global $db_link, $Timer;
        
        // Extract URL Domain and Args from URL
        if ($url != '') {
            preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$url,$matches);
            $domain = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[1]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
            $args = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $matches[2]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
            //echo "<PRE>$url\n".print_r($matches,true)."</PRE>";
            // Post them to the logs
            $result = mysqli_query($db_link, "SELECT * from log_refer WHERE domain='$domain' AND argv='$args'");
            $count = mysqli_num_rows($result);
            if ($count > 0) {
                $row = mysqli_fetch_assoc($result);
                $id = $row['ID'];
            } else {
                mysqli_query($db_link, "INSERT INTO log_refer SET domain='$domain', argv='$args'");
                $result = mysqli_query($db_link, "select LAST_INSERT_ID() as ID;");
                $row = mysqli_fetch_assoc($result);
                $id = $row['ID'];
            }
        } else {
            $id = 0;
        }
        return $id;
    }
}


/*
require_once('functions.php');


fmsession('start');
//echo ":{$PERS['account']}:";
//    Handle all the session variables here. Including Account.
if (!isset($PERS['program_position'])) { $PERS['program_position'] = "100"; }

if (!isset($PERS['account'])) { $PERS['account'] = "Guest"; }
if (!isset($PERS['signup'])) { $PERS['signup'] = "700"; }

if (!isset($PERS['program'])) {
    $pb_set["repeat"] = 1;
    $pb_set["delay"] = 5;
    $pb_set["size"] = 1;
    $pb_set["volume"] = 70;
    $pb_set["color1"] = "#040673";
    $pb_set["color2"] = "#0691CA";
    $pb_set["theme"] = "none";
    $pb_set["search"] = "";
    $pb["settings"] = $pb_set;
    $pb_les["words"] = array();  // format of lang|wSHA1|wNative|wForeign
    $pb_les["language"] = 2;
    $pb_les["name"] = "New Lesson";
    $pb_les["pos"] = 0;
    $pb_les["repeat"] = 1;
    $pb["lesson"] = $pb_les;
    $pb_v["lpos"] = 1;
    $pb_v["dpos"] = 1;
    $pb_v["spos"] = 1;
    $pb_v["ipage"] = 1;
    $pb_v["opage"] = 1;
    $pb["dview"] = $pb_v;
} else {
    $pb = $PERS['program'];
}

if ($_REQUEST["p"] == "1")   { $PERS['program_position'] = "1"; }
if ($_REQUEST["p"] == "100") { $PERS['program_position'] = "100"; }
if ($_REQUEST["p"] == "101") { $PERS['program_position'] = "101"; }
if ($_REQUEST["p"] == "102") { $PERS['program_position'] = "102"; }
if ($_REQUEST["p"] == "103") { $PERS['program_position'] = "103"; }
if ($_REQUEST["p"] == "104") { $PERS['program_position'] = "104"; }
if ($_REQUEST["p"] == "119") { $PERS['program_position'] = "119"; }
if ($_REQUEST["p"] == "991") { $PERS['program_position'] = "991"; }
$pr_pos = intval($PERS["program_position"]);

// Language Changing
if ($_REQUEST["language"] == "true") {
    if ($_REQUEST["sr_lang"] != "") {
        if ($_REQUEST["sr_lang"] == "Spanish") { $pb['lesson']['language'] = 2; }
        if ($_REQUEST["sr_lang"] == "German") { $pb['lesson']['language'] = 3; }
        if ($_REQUEST["sr_lang"] == "French") { $pb['lesson']['language'] = 4; }
        //echo $pb['lesson']['language'];
    }
}


// Count the words in the database
$pb_lang = $pb['lesson']['language'];
$Timer->start('sql');
if ($search != "") {
    $cresult = mysql_query("SELECT count(wID) as Total FROM Language WHERE ((wNative LIKE '%$search%') OR (wForeign LIKE '%$search%')) AND wLang='$pb_lang'",$db_link);
} else {
    $cresult = mysql_query("SELECT count(wID) as Total FROM Language WHERE wLang='$pb_lang'",$db_link);
}
$Timer->stop('sql');
$row = mysql_fetch_object($cresult);
$dbcount = $row->Total;
unset($row);
mysql_free_result($cresult);
//echo "_DB_$dbcount";

if ($_REQUEST["search"] == "true") {
    if ($_REQUEST["sr_n"] != "") {
        $pb['settings']['search'] = $_REQUEST["sr_n"];
        $pb["dview"]["dpos"] = 0;
    }
}
// Reset the search Criteria
if ($_REQUEST["clearsearch"] == "true") {
    $pb['settings']['search'] = "";
}

if ($_REQUEST["lessonsave"] == "true") {
    // Check for logged in or not....
    $lesson_name = $_REQUEST["lesson_name"];
    $ls_account = $PERS['account'];
    $PERS['program']['lesson']['name'] = $leson_name;
    $pb['lesson']['name'] = $leson_name;
    $lesson_name = mysql_escape_string($lesson_name);
    $ls_lesson = base64_encode(serialize($PERS['program']['lesson']));
    $Timer->start('sql');
    $result = mysql_query("SELECT * FROM Lesson WHERE lName='$lesson_name'",$db_link);
    $Timer->stop('sql');
    if (mysql_num_rows($result) > 0) {
        $sql = "UPDATE Lesson SET lName='$lesson_name', lCreated=NOW(), lLastUsed=NOW(), lOwner='$ls_account', lFile='$ls_lesson' WHERE lName='$lesson_name'";
    } else {
        $sql = "INSERT INTO Lesson SET lName='$lesson_name', lCreated=NOW(), lLastUsed=NOW(), lOwner='$ls_account', lFile='$ls_lesson' ";
    }
    $Timer->start('sql');
    $result = mysql_query($sql,$db_link);
    $Timer->stop('sql');
    //echo mysql_error();
}

    $pb_v = $pb["dview"];
    if ($_REQUEST["dpos"] == "prev") {$pb_v["dpos"] = $pb_v["dpos"] - 15; }
    if ($_REQUEST["dpos"] == "next") {$pb_v["dpos"] = $pb_v["dpos"] + 15; }
    if ($pb_v["dpos"] <= 0) { $pb_v["dpos"] = 1; }
    if ($pb_v["dpos"] >= $dbcount) { $pb_v["dpos"] = $dbcount - 1; }

    if ($_REQUEST["lpos"] == "prev") {$pb_v["lpos"] = $pb_v["lpos"] - 1; }
    if ($_REQUEST["lpos"] == "next") {$pb_v["lpos"] = $pb_v["lpos"] + 1; }
    if ($pb_v["lpos"] <= 0) { $pb_v["lpos"] = 1; }
    if ($pb_v["lpos"] > intval(count($pb['lesson']['words']) / 15) +1) { $pb_v["lpos"] = intval(count($pb['lesson']['words'])/15) + 1; }

    if ($_REQUEST["spos"] == "prev") {$pb_v["spos"] = $pb_v["spos"] - 1; }
    if ($_REQUEST["spos"] == "next") {$pb_v["spos"] = $pb_v["spos"] + 1; }
    if ($pb_v["spos"] <= 0) { $pb_v["spos"] = 1; }
    if ($pb_v["spos"] >= 5) { $pb_v["spos"] = 5; }
    $pb["dview"] = $pb_v;

if (isset($_REQUEST["ipage"])) {
    $pb_v = $pb["dview"];
    $pb_v["ipage"] = 1;
    if ($_REQUEST["ipage"] == "1") $pb_v["ipage"] = 1;
    if ($_REQUEST["ipage"] == "2") $pb_v["ipage"] = 2;
    if ($_REQUEST["ipage"] == "3") $pb_v["ipage"] = 3;
    if ($_REQUEST["ipage"] == "4") $pb_v["ipage"] = 4;
    if ($_REQUEST["ipage"] == "5") $pb_v["ipage"] = 5;
    $pb["dview"] = $pb_v;
}

if (isset($_REQUEST["opage"])) {
    $pb_v = $pb["dview"];
    $pb_v["opage"] = 1;
    if ($_REQUEST["opage"] == "1") $pb_v["opage"] = 1;
    if ($_REQUEST["opage"] == "2") $pb_v["opage"] = 2;
    $pb["dview"] = $pb_v;
}

if ($_REQUEST["options"] == "set") {
    $pb_set = $pb["settings"];
    // Sanity Checks
    $st_volume = intval($_REQUEST["set_volume"]);
    $st_delay = intval($_REQUEST["set_delay"]);
    $st_repeat = intval($_REQUEST["set_repeat"]);
    $st_size = intval($_REQUEST["set_size"]);
    if (($st_size < 1) or ($st_size > 4)) { $st_size = 1; }
    if (($st_delay < 1) or ($st_delay > 3)) { $st_delay = 1; }
    if (($st_repeat < 1) or ($st_repeat > 5)) { $st_repeat = 5; }
    if (($st_volume < 10) or ($st_volume > 100)) { $st_volume = 70; }
    $pb_set["volume"] = $st_volume;
    $pb_set["delay"] = $st_delay;
    $pb_set["repeat"] = $st_repeat;
    $pb_set["size"] = $st_size;
    $pb["settings"] = $pb_set;
    $set = ($st_volume << 16) | ($st_delay << 8) | ($st_repeat << 4) | $st_size;
    $sql = "UPDATE Account SET uAudioSettings=$set";
    $Timer->start('sql');
    mysql_query($sql,$db_link);
    $Timer->stop('sql');
}

if ($_REQUEST["sgnup"] == "701") {
//echo "yep";
    $acct = $PERS['acct'];
    $acct["email"] = $_REQUEST['sgn_email'];
    $acct["pass"] = $_REQUEST['sgn_pass'];
    $PERS['acct'] = $acct;
    $PERS['signup'] = "701";
}

if ($_REQUEST["sgnup"] == "702") {
    $acct = $PERS['acct'];
    $acct["fname"] = $_REQUEST['sgn_fname'];
    $acct["lname"] = $_REQUEST['sgn_lname'];
    $acct["addr1"] = $_REQUEST['sgn_addr1'];
    $acct["addr2"] = $_REQUEST['sgn_addr2'];
    $acct["city"] = $_REQUEST['sgn_city'];
    $acct["state"] = $_REQUEST['sgn_state'];
    $acct["zip"] = $_REQUEST['sgn_zip'];

    $PERS['acct'] = $acct;
    $PERS['signup'] = "702";

    if (isset($_REQUEST["back"])) {
        $acct = $PERS['acct'];
        $PERS['signup'] = "700";
    }
}

if ($_REQUEST["sgnup"] == "703") {
    $acct = $PERS['acct'];
    $acct["heard"] = $_REQUEST['sgn_heard'];

    $PERS['acct'] = $acct;
    $PERS['signup'] = "703";

    if (isset($_REQUEST["back"])) {
        $acct = $PERS['acct'];
        $PERS['signup'] = "701";
    }
}

if ($_REQUEST["login"] == "true") {
    $lg_email = $_REQUEST["log_email"];
    $lg_pass = trim(enc_sha1($_REQUEST["log_pass"]));
    $Timer->start('sql');
    $sql = "SELECT uPassword, uSHA1, uFName, uLName, uAccess, uAudioSettings FROM Account WHERE uEmail='$lg_email'";
    $result = mysql_query($sql,$db_link);
    $Timer->stop('sql');
    if (mysql_num_rows($result) == 0) {
        $lg_stat = "Invalid E-Mail or Password...";
    } else {
        $row = mysql_fetch_object($result);
        //echo "<FONT SIZE=2><PRE>";
        //echo $row->uPassword;
        //echo "<BR>$lg_pass<BR>";
        //echo $_REQUEST["log_pass"];
        //echo "</PRE></FONT>";
        if ($row->uPassword != $lg_pass) {
            $lg_stat = "Invalid E-Mail or Password....";
        } else {
            $PERS["account"] = $row->uSHA1;
            $PERS["account_access"] = $row->uAccess;
            $PERS["account_f"] = $row->uFName;
            $PERS["account_l"] = $row->uLName;
            $set = $row->uAudioSettings;
            $pb_set["volume"] = ($set >> 16) & 255;
            $pb_set["delay"] = ($set >> 8) & 15;
            $pb_set["repeat"] = ($set >> 4) & 15;
            $pb_set["size"] = $set & 15;
            $pb["settings"] = $pb_set;
        }
    }

}

if ($_REQUEST["login"] == "false") {
    $PERS["account"] = "Guest";
    $PERS["account_access"] = $default_access;
    $PERS['program_position'] = "100";
    $PERS['signup'] = "700";
    unset($PERS["account_f"]);
    unset($PERS["account_l"]);
}

if ($PERS["account"] == "Guest") {
    $PERS["account_access"] = $default_access;
} else {
    if (!isset($PERS["account_access"])) {
        // Set Access Rights by Database
        $sha = $PERS["account"];
        $sql = "SELECT * FROM Account WHERE uSHA1='$sha'";
        $Timer->start('sql');
        $result = mysql_query($sql,$db_link);
        $Timer->stop('sql');
        $row = mysql_fetch_object($result);
        $PERS["account_access"] = $row->uAccess;
        mysql_free_result($result);
    }
    $sql = "UPDATE Account SET uAccessed=now()";
    $Timer->start('sql');
    mysql_query($sql,$db_link);
    $Timer->stop('sql');
}

    $lesson = $pb['lesson'];
    $settings = $pb['settings'];
    $dview = $pb['dview'];

    $pb_lang = $lesson['language'];
    $ls_words = $lesson['words'];
    $ls_name = $lesson['name'];
    $dv_dpos = $dview['dpos'];
    $dv_lpos = $dview['lpos'];
    $dv_spos = $dview['spos'];

$PERS['program'] = $pb;
//fmsession('stop');

*/

?>
