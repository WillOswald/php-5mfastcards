## MySQL on appfog
MySQL configuration on appfog is loaded from an environment variable.  See: https://docs.appfog.com/services/mysql

##Todo
* Most or all of the analytic data about users (referers, os, browser version etc.) can be managed more easily with Google Analytics.
* the db_sanity function should not be run each request
