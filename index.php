<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | General Includes
// +----------------------------------------------------------------------

    include_once("functions.php");
        $Timer->start('init');
    include_once("db.php");
    include_once("session.php");
    include_once("prg_account.php");
    include_once("main_lib.php");

    // generate Tables...
    db_sanity();
    
// +----------------------------------------------------------------------
// | What's the name of this script?
// +----------------------------------------------------------------------
    // FIXME: Need to log Referer and such...
    // FIXME: Output the javascript to grab info about the browser...
    
    $full_pagename = $_SERVER['SCRIPT_FILENAME'];
    $ar = preg_split('/\//',$full_pagename);
    $current_page = $ar[count($ar)-1];

// +----------------------------------------------------------------------
// | Cache Control
// +----------------------------------------------------------------------
    // FIXME: Change cache control to expire in 15 minutes.
    // Get Modified date from file, 
    $head_modified = gmdate('D, j M Y H:i:s',filemtime($current_page)) . ' GMT';
    $head_expires_days = 7; //(86400 * $head_expires_days)
    $head_expires = gmdate('D, j M Y H:i:s',time()+900) . ' GMT';
        
    header ("Content-type: text/html");
    header("Expires: $head_expires");
    header("Last-Modified: $head_modified");
    // For IE
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

// +----------------------------------------------------------------------
// | Create and validate the current session  
// | FIXME!! Filter out bots from sessions, but still log them.
// +----------------------------------------------------------------------
    $Session = new fmSession;
    // Here we need to FIX the AGENT stuff and NO Session for the Bots
    $Timer->start('session');
    $Session->init('FMC_FlashCard');
    $Timer->stop('session');
    // Set the location of the place where we are...
    $Session->post("/w2/n/np","1000");
    $Session->post("/w2/n/adfc","none:");
    $Session->save();

    @$Account = new fmAccount;
    $Account->init($Session->getID());

// +----------------------------------------------------------------------
// | Misc Functions
// +----------------------------------------------------------------------
    function make_seed() {
        list($usec, $sec) = explode(' ', microtime());
        return (float) $sec + ((float) $usec * 100000);
    }

// +----------------------------------------------------------------------
// | Login / Logout
// +----------------------------------------------------------------------
    $lgMessage = '';
    if (@$_REQUEST['lgAct'] == 'login') {
        $email = $_REQUEST['lgEmail'];
        $pass = $_REQUEST['lgPass'];
        $lcode = $Account->login($email,$pass,$Session->getID());
        switch ($lcode) {
            case 1: // Account not found...
                $lgMessage = 'Account not found.';
                break;
            case 2: // Password Incorrect...
                $lgMessage = 'Incorrect password.';
                break;
            default:
                $lgMessage = '';
                break;
        }
    }
    if (@$_REQUEST['lgAct'] == 'logout') {
        $Account->logout();
        $lgMessage = 'You have been logged out.';
    }

// +----------------------------------------------------------------------
// | Other Requests
// +----------------------------------------------------------------------
    if (isset($_REQUEST['lang'])) {
        include_once("prg_lesson.php");
        include_once("prg_vocabulary.php");
        $Lesson = new fmPrgLesson;
        $aid = $Account->getID();
        $sid = $Session->getID();
        $Lesson->init($aid,$sid);
        $Lesson->restore($Session->get("/w2/l"));
        
        $Vocabulary = new fmPrgVocabulary;
        if (strlen($Session->get("/w2/v")) > 1)
        $Vocabulary->restore($Session->get("/w2/v"));
        
        $lang = 2;
        if ($_REQUEST['lang'] == 'spanish') { $lang = 2; }
        if ($_REQUEST['lang'] == 'french') { $lang = 4; }
        if ($_REQUEST['lang'] == 'german') { $lang = 3; }
        $Lesson->_language = $lang;
        $Vocabulary->vLanguage = $lang;
        $Session->post("/w2/l=".$Lesson->preserve());
        $Session->post("/w2/v=".$Vocabulary->preserve());
        $Session->save();
    }
// +----------------------------------------------------------------------
// | Setup Main HTML Body
// +----------------------------------------------------------------------
    
    // +----------------------------------------------------------------------
    // | Read File in...
    // +----------------------------------------------------------------------
    $htxt_filename = substr($current_page,0,strpos($current_page,'.')).'.htxt';
    if (file_exists($htxt_filename)) {
        $htxt = file($htxt_filename);
        $content = false; $htxt_navpos = 0;
        while (!$content) {
            $line = array_shift($htxt);
            if (trim($line) == '') {
                break;
            } else {
                preg_match('/^Filename: (.+\.php)/i',$line,$m_file);
                preg_match('/^Title: (.+)/i',$line,$m_title);
                preg_match('/^Description: (.+)/i',$line,$m_desc);
                preg_match('/^Keywords: (.+)/i',$line,$m_keywords);
                preg_match('/^NavPos: ([0-9]+)/i',$line,$m_navpos);
                if (count($m_file) > 0) { $htxt_file = $m_file[1]; }
                if (count($m_title) > 0) { $htxt_title = $m_title[1]; }
                if (count($m_desc) > 0) { $htxt_desc = $m_desc[1]; }
                if (count($m_keywords) > 0) { $htxt_keywords = $m_keywords[1]; }
                if (count($m_navpos) > 0) { $htxt_navpos = $m_navpos[1]; }
                //echo "<PRE>"; var_dump($m_title); echo "</PRE>";
            }
        }
        if (intval($htxt_navpos) != 0) {
            // Set Position in the session.
            $Session->post("/w2/n/np",$htxt_navpos);
        }
    } else {
        $htxt_out = file('404.html');
    }
    
    // +----------------------------------------------------------------------
    // | Content from the htxt file
    // +----------------------------------------------------------------------
    $links = array(
        '(%textdfc%);/main.php?act=dfc&ibTag=2140;Dynamic Fast Cards;-none-',
        '(%countVocabulary%);-none-;'.countVocabulary().';-none-',
        '%(Dynamic Fast Cards)%;/main.php?act=dfc&ibTag=2140;\\1;Enter The Dynamic Fast Cards Program',
        '(5Muses Software L.L.C.);/about.php;\\1;About 5Muses Softare L.L.C.',
        '%(tutorial)%;/tutorial.php;\\1;Dynamic Fast Cards Tutorial',
        '(%5m%);-none-;5Muses Software L.L.C.;-none-',
        '(%vocab%);-none-;Vocabulary;-none-',
        '%(German vocabulary)%;/main.php?redir=ge_ad;\\1;Build Your German Vocabulary',
        '%(Spanish vocabulary)%;/main.php?redir=sp_ad;\\1;Build Your Spanish Vocabulary',
        '%(French vocabulary)%;/main.php?redir=fr_ad;\\1;Build Your French Vocabulary',
        '%(Learn German|German)%;/400free.php?lang=german;\\1;Learn German Today!',
        '%(Learn French|French)%;/400free.php?lang=french;\\1;Learn French Today!',
        '%(free account)%;/signup.php;\\1;Sign up for a free account today!',
        '%(Learn Spanish|Spanish)%;/400free.php?lang=spanish;\\1;Learn Spanish Today!');
        
        //'(%signupform%);'.$sgForm.';\\1');
        //free account
    
    reset($htxt); $htxt_out = '';
    foreach ($htxt as $line) {
        $htxt_out .= $line;
    }
    
    foreach ($links as $entity) {
        list($search,$link,$replace,$title) = explode(';',$entity);
        if ($link == '-none-') {
            $htxt_out = preg_replace("/$search/i",$replace,$htxt_out);
        } else {
            $htxt_out = preg_replace("/$search/i","<A CLASS=\"mlink\" TITLE=\"$title\" HREF=\"$link\">$replace</A>",$htxt_out);
        }
    }
    
    // Catch the Signup Sheet
    if (stristr($htxt_out,'%signupform%') !== false) {
        include('signupform.php');
        $htxt_out = str_replace('%signupform%',$sgForm,$htxt_out);
        $htxt_out = str_replace('%signupmessage%',$sgMessage,$htxt_out);
    }
    
    // How about some random number generators?
    if (stristr($htxt_out,'%rand(') !== false) {
        $htxt_out = preg_replace("/%rand\((\d+)\)%/ie","rand(1,\\1)",$htxt_out);
    }
    
    // This is for the purchasing lines.
    if (stristr($htxt_out,'%purchase(') !== false) {
        include_once("prg_records.php");
        // Grab the stuff from the Purchasing Table.
        preg_match_all("/%purchase\((\d+)\)%/i",$htxt_out,$matches);
        $result = mysqli_query($db_link, "SELECT * from p_itemcodes ORDER BY item_amount");
        if (($result !== false) && (mysqli_num_rows($result) > 0)) {
            $products = array();
            while ($row = mysqli_fetch_assoc($result)) {
                // Fill an array
                $prod = array();
                $prod['id'] = $row['item_number'];
                list($pre,$post) = explode(' - ',$row['item_name']);
                $prod['name'] = $post;
                $prod['price'] = $row['item_amount'];
                $prod['button'] = paypal_button($Account->getID(),$row['item_number']);
                $products[$row['item_number']] = $prod;
            }
            ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
            foreach ($matches[1] as $match) {
                $phtml = "";
                foreach ($products as $pID => $pData) {
                    if (substr($pID,0,strlen($match)) == $match) {
                        // Found a matching Entry... 
                        $phtml .= '<TR><TD STYLE="border-bottom:1px dashed #CCC;">'.$pData['name'].'</TD><TD STYLE="color:green;border-bottom:1px dashed #CCC;">\$'.$pData['price'].' <SPAN STYLE="font-size:70%;color:#888;border-bottom:1px dashed #CCC;">USD</SPAN></TD><TD STYLE="border-bottom:1px dashed #CCC;"><IMG SRC="images/bn5.gif">'."</TD></TR>\n"; //$pData['button']
                    }
                }
                // Replace the tags
                $htxt_out = preg_replace("/%purchase\((".$match.")\)%/i",$phtml,$htxt_out);
                
            }
        } else {
            // No Products? This is bad.
        }
    }
    
    // Catch the Tutorial Bit
    if (stristr($htxt_out,'%dfc_tutorial%') !== false) {
        if (isset($_REQUEST['section'])) {
            $reqSection = floatval($_REQUEST['section']);
            $Session->post("/w2/n/tut=$reqSection");
            $section = strval($reqSection);
        } else {
            $section = $Session->get('/w2/n/tut');
            if ($section == '') {
                $section = '1';
                $Session->post('/w2/n/tut=1');
            }
        }
        $c = ($_REQUEST['i'] == 'c') ? 'true' : 'false';
        $htxt_out = str_replace('%dfc_tutorial%','<SCRIPT TYPE="text/javascript">var hIndexClosed = '.$c.';</SCRIPT>%dfc_tutorial%',$htxt_out);
        if (file_exists("tut_s{$section}.html")) {
            $secFile = file_get_contents("tut_s{$section}.html");
            $htxt_out = str_replace('%dfc_tutorial%',$secFile,$htxt_out);
        } else {
            $htxt_out = str_replace('%dfc_tutorial%','<I>Under Construction</I>',$htxt_out);
        }
    }
    
    
    // +----------------------------------------------------------------------
    // | Title and Header
    // +----------------------------------------------------------------------
    
    $html = '';
    $html .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
    $html .= '<HTML>';
    $html .= '<HEAD>';
    $html .= "\n<TITLE>$htxt_title</TITLE>\n";
    // META Tags? Do I even need them?
    $html .= "<meta name=\"description\" content=\"$htxt_desc\">\n";
    $html .= "<meta name=\"keywords\" content=\"$htxt_keywords\">\n";
    // Only if necessary...
    $html .= '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">';
    $html .= '    fmc_init = function() { return true; }';
    $html .= '</SCRIPT>';
    if ($Session->get('/log/logstate') != 'complete') {
        $html .= '<SCRIPT LANGUAGE="JavaScript" SRC="id.js" TYPE="text/javascript"></SCRIPT>';
        $html .= '</HEAD>';
        $html .= '<BODY BGCOLOR=#FFFFFF onload="idBrowser();fmc_init();">';
    } else {
        $html .= '</HEAD>';
        $html .= '<BODY BGCOLOR=#FFFFFF onload="fmc_init();">';
    }
//     $html .= '<STYLE TYPE="text/css">';
//     $html .= 'a.elink:link, a.elink:visited { color: #000000; text-decoration: none; }';
//     $html .= 'a.elink:hover, a.elink:active { color: #3F4AFC; text-decoration: none; }';
//     $html .= '</STYLE>';
//     $html .= $css;
    // +----------------------------------------------------------------------
    // | Cascading Style Sheets
    // +----------------------------------------------------------------------
    $html .= '<LINK HREF="css.php?sheet=global" REL=stylesheet TYPE="text/css">';
    $html .= '<LINK HREF="css.php?sheet=html" REL=stylesheet TYPE="text/css">';
    // This is the main div enclosure for the margins
    // +----------------------------------------------------------------------
    // | Content
    // +----------------------------------------------------------------------
    $html .= '<CENTER><DIV ID="indexContent" STYLE="padding:0px 5px 0px 5px;margin: 8px 8px 8px 3px;width:980px;text-align:justify;border-left:1px solid #C0C0C0;border-right:1px solid #C0C0C0;">';
    
    $html .= fmLibBanner('index2');
    
    
    //include_once("prg_flash.php");
    //$FlashCards = new fmFlashCard;
    //$html .= stripslashes($FlashCards->makeToolbar(2));
    
    
    
    $html .= '<DIV STYLE="background-image: url(\'images/parthpath2.jpg\'); background-repeat: no-repeat;">';
    $html .= "<BR>";
    $html .= '<TABLE WIDTH=100%>';
    $html .= '<TR>';
    $html .= '<TD VALIGN=TOP CLASS="nav" STYLE="width:161px;">';
    // +----------------------------------------------------------------------
    // | Navigation Bar
    // +----------------------------------------------------------------------
    $navitems = array(
        'index.php' => 'Welcome',
        '1' => '-',
        'learnspanish.php' => 'Learn Spanish',
        'learngerman.php' => 'Learn German',
        'learnfrench.php' => 'Learn French',
        'vocabulary.php' => 'Vocabulary',
        'tutorial.php' => 'Tutorial',
        '2' => '-',
        'purchasing.php' => 'Purchasing',
        '3' => '-',
        'news.php' => 'Latest News',
        'history.php' => 'History',
        'about.php' => 'About Us',
        'contact.php' => 'Contact Us',
        '4' => '-',
        'support.php' => 'Support');
    foreach ($navitems as $nFile => $nTitle) {
        if ($nTitle == '-') {
            $html .= '<DIV CLASS="navspacer"><IMG SRC=images/1px.gif></DIV>';
        } else {
            $class = ($htxt_file == $nFile) ? 'navitem2sel' : 'navitem2';
            $html .= '<A CLASS="'.$class.' spaced1" HREF="'.$nFile.'">'.$nTitle.'</A>'; 
        }
    }
    if ($Account->has_access(39)) {
        // Hello there mr admin.
        $html .= '<DIV CLASS="navspacer"><IMG SRC=images/1px.gif></DIV>';
        $html .= '<A CLASS="navitema spaced1" TITLE="Admin Panel" HREF="http://www.5muses.net/288012_dfc_5mc/dfc_admin.php">Admin</A>'; 
    }
    // Users online...
    $uonline = users_online();
    $ucount = visit_count();
    $total = intval($ucount['total']) + 39000;
    
    $email = '';
    
    // Login Section
    if ($Account->is_guest()) {
        $html .= '<BR><BR><BR><DIV CLASS="spaced1" STYLE="border-bottom:1px solid #888;color:#A0A0A0;">Member Login</DIV><SPAN STYLE="color:#A0A0A0;">';
        $html .= '<FORM METHOD=POST NAME=login>';
        $html .= '<INPUT TYPE=HIDDEN NAME=lgAct VALUE=login>';
        $html .= '<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH=100%>';
        if ($lgMessage != '') {
            $html .= '<TR><TD CLASS="login1" STYLE="color:#F44">'.$lgMessage.'</TD></TR>';
            if ($_REQUEST['lgAct'] == 'login') {
                $html .= '<TR><TD CLASS="login1" STYLE="color:#F44"><A CLASS=mlink STYLE="font-size:7pt;color:#888;" HREF="forgot.php?email='.base64_encode($email).'" TITLE="Click Here if you forgot your password">Forogt Password?</A></TD></TR>';
            }
        }
        $html .= '<TR><TD CLASS="login1">e-mail:</TD></TR><TR><TD><INPUT CLASS="login1" NAME=lgEmail VALUE="'.$email.'"></TD></TR>';
        $html .= '<TR><TD CLASS="login1">password:</TD></TR><TR><TD><INPUT CLASS="login1" TYPE=PASSWORD NAME=lgPass></TD></TR>';
        $html .= '<TR><TD CLASS="login1" ALIGN=RIGHT><INPUT TYPE=SUBMIT CLASS="submit1" VALUE=Login></TD></TR>';
        $html .= '</TABLE>';
        $html .= '</FORM>';
        $html .= '<A CLASS=mlink STYLE="font-size:7pt;color:#888;letter-spacing:.05em;" HREF="signup.php?email='.base64_encode($email).'" TITLE="Sign up for a free account today!">Signup for a free account!</A></SPAN>';
    } else {
        $html .= '<BR><BR><BR><DIV CLASS="spaced1" STYLE="border-bottom:1px solid #888;color:#A0A0A0;">Logout</DIV><SPAN STYLE="color:#A0A0A0;">';
        $html .= 'You are logged in as<BR>';
        $html .= $Account->_ContactInfo['FName'] . ' ' . $Account->_ContactInfo['LName'];
        $html .= '</SPAN>';
        $html .= '<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH=100%>';
        $html .= '<TR><TD CLASS="login1" ALIGN=RIGHT><FORM METHOD=POST NAME=logout><INPUT TYPE=HIDDEN NAME=lgAct VALUE=logout><INPUT TYPE=SUBMIT CLASS="submit1" VALUE=Logout></FORM></TD></TR>';
        $html .= '</TABLE>';
    }

    // Users Online    
    $html .= '<BR><BR><BR><DIV CLASS="spaced1" STYLE="border-bottom:1px solid #888;color:#A0A0A0;">Users Online</DIV><SPAN STYLE="color:#A0A0A0;">';
    $html .= "{$uonline['guests']} Guests<BR>";
    $html .= "{$uonline['members']} Members<BR></SPAN>";
    
    // Statistics
    $html .= '<BR><BR><BR><DIV CLASS="spaced1" STYLE="border-bottom:1px solid #888;color:#A0A0A0;">Statistics</DIV><SPAN STYLE="color:#A0A0A0;">';
    $html .= "{$ucount['account']} Members<BR>";
    $html .= "$total Visits Total<BR>";
    $html .= "{$ucount['today']} Visits Today<BR></SPAN>";
    

    $html .= '</TD>';
    $html .= '<TD VALIGN=TOP STYLE="width:804px;">';
    
    $html .= $htxt_out;
    $html .= '</TD>';
    $html .= '</TR>';
    $html .= '</TABLE>';
    
    // +----------------------------------------------------------------------
    // | Copyright, Terms of Use, Privacy Policy
    // +----------------------------------------------------------------------
    $html .= "<BR>";
    
    $html .= "<CENTER><DIV STYLE=\"color:#AAAAAA;font-size:9px;width:750px;border-top:1px solid #888888;\">&copy;2005, 5 Muses Software L.L.C.  All Rights Reserved. &nbsp;&nbsp;:&nbsp;&nbsp;<A HREF=\"termsofuse.php\">Terms Of Use</A>&nbsp;&nbsp;:&nbsp;&nbsp;<A HREF=\"privacypolicy.php\">Privacy Policy</A>&nbsp;&nbsp;:&nbsp;&nbsp;<A HREF=\"sitemap.php\">Site Map</A></DIV></CENTER>";
    $html .= '<a class="firefox" href="http://www.spreadfirefox.com/?q=affiliates&amp;id=105352&amp;t=86"></a>'; //<img border="0" alt="Get Firefox!" title="Get Firefox!" align=right src=" http://sfx-images.mozilla.org/affiliates/Buttons/125x50/takebacktheweb_125x50.png"/>
    $html .= "</DIV></CENTER>";
    
    /*

    
    * Eliminate the guesswork in pronouncing new words. Our native speakers have the preferred dialects - simply listen to them and imitate the sounds.
    * See and hear all of your required vocabulary in less time time than is currently possible with any other method.
    * Associate the abstract meaning of the nearest English equivalent to the foreign vocabulary terms fast.
    * Our program will allow you to quiz yourself when you think it is necessary.

    I. Success with memorization by using all of your senses at once, repetitively

    At least half of your learning, like it or not, is memorization.
    
    */
    $html .= "</DIV>"; // For Parth Path
    $html .= "</DIV>";

    $html .= '<DIV STYLE="display:none;">';
    $html .= '<IMG SRC="images/firefox2.png">';
    $html .= '</DIV>';
//     $html .= '
//   <EMBED SRC="thunder.swf" WIDTH="100" HEIGHT="100"
//    PLAY="true" ALIGN="" LOOP="false" QUALITY="high"
//    TYPE="application/x-shockwave-flash"
//    PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer">
//   </EMBED>
// 
// ';

// +----------------------------------------------------------------------
// | Save the session information. (if any)
// +----------------------------------------------------------------------
    $Session->save();

// +----------------------------------------------------------------------
// | Output the Main HTML
// +----------------------------------------------------------------------
    echo $html;

        /*
        $url = $_SERVER['HTTP_REFERER'];
        preg_match('/^http:\/\/([\d\w.-]+)\/(.*)/i',$url,$matches);
        $domain = mysql_escape_string($matches[1]);
        $args = mysql_escape_string($matches[2]);
        echo "<PRE>$url\n".print_r($matches,true)."</PRE>";
        */
// +----------------------------------------------------------------------
// | End of file
// +----------------------------------------------------------------------
?>
