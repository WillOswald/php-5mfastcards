<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//


    //------------------------------------------------------------
    //  Signal Handler
    //------------------------------------------------------------
    function sig_handler($signo)
    {
        switch ($signo) {
            case SIGTERM:
                log_syslog('PHPrSyslogD','Caught SIGTERM - Exiting...');
                @socket_close($socket);
                exit;
                break;
            case SIGHUP:
                log_syslog('PHPrSyslogD','Caught SIGHUP - Socket Cleanup.');
                @socket_close($socket);
                $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
                if(!socket_bind($socket, "0.0.0.0", 514)) {
                    log_syslog('PHPrSyslogD','Err: (socket_bind) - '.socket_strerror(socket_last_error()));
                    die(1);
                }
                break;
            default:
                // handle all other signals
                log_syslog('PHPrSyslogD',"Caught Unhandled SIGNAL - $signo");
        }
    
    }
    
    //------------------------------------------------------------
    //  Signal Handler
    //------------------------------------------------------------
    function log_syslog($tag,$msg) {
        if (file_exists('/usr/bin/logger')) {
            $output = shell_exec("/usr/bin/logger -p local1.info -t $tag -- $msg");
        }
    }
    
    
    //------------------------------------------------------------
    //  Program Initialize.
    //------------------------------------------------------------
    
    // setup signal handlers
    pcntl_signal(SIGTERM, "sig_handler");
    pcntl_signal(SIGHUP,  "sig_handler");
    pcntl_signal(SIGUSR1, "sig_handler");    
    $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    
    if(!socket_bind($socket, "0.0.0.0", 514)) {
        log_syslog('PHPrSyslogD','Err: (socket_bind) - '.socket_strerror(socket_last_error()));
        die(1);
    }
    //------------------------------------------------------------
    //  Create and bind the socket
    //------------------------------------------------------------
    // Loop to receive the logs stuff
    log_syslog('PHPrSyslogD','Ver 0.8b Init: Ready for connections.');
    while (true) {
        socket_recvfrom($socket, $buf, 65535, 0, $clientIP, $clientPort);
        if($buf === false) { 
            $error = socket_strerror(socket_last_error());
            break;
        } else {
            if(strlen($buf) === 0) { 
                log_syslog('PHPrSyslogD','Err: (socket_read) - '.socket_strerror(socket_last_error()));
            }
        }
        $prefix = strpos($buf,':');
        if ($prefix !== false) {
            // So far so good...
            $line = str_replace('"','\"',substr($buf,$prefix+2));
            if (file_exists('/usr/bin/logger')) {
                $output = shell_exec("/usr/bin/logger -p local1.info -t $clientIP -- \"$line\"");
            }
        }
        $buf = '';
    }
    
?>
