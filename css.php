<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

// +----------------------------------------------------------------------
// | Version
// +----------------------------------------------------------------------
    $css_manager = array();
    $css_manager['global'] = 2;
    $css_manager['dfc'] = 1;
    $css_manager['html'] = 4;
    
    $css_global =  '<LINK HREF="css.php?sheet=global" REL=stylesheet TYPE="text/css">';
    $css_dfc =     '<LINK HREF="css.php?sheet=dfc" REL=stylesheet TYPE="text/css">';
    $css_html =    '<LINK HREF="css.php?sheet=html" REL=stylesheet TYPE="text/css">';
    
// +----------------------------------------------------------------------
// | Send the Headers and Stylesheet
// +----------------------------------------------------------------------
    $file = $_REQUEST['sheet'];
    foreach ($css_manager as $name => $version) {
        if ($name == $file) {
            // Found the one we want
            $filename = $name . '_' . sprintf("%04d",$version) . '.css';
            $head_modified = gmdate('D, j M Y H:i:s',filemtime($filename)) . ' GMT';
            $head_expires_hours = 6;
            $head_expires = gmdate('D, j M Y H:i:s',time()+(3600 * $head_expires_hours)) . ' GMT';
            header ("Content-type: text/css");
            header("Expires: $head_expires");
            header("Last-Modified: $head_modified");
            echo "/* \n    Stylesheet (C) 2005 5Muses Software L.L.C\n    Version 1.0.{$css_manager['html']}\n*/\n";
            readfile($filename);
        }
    }

// +----------------------------------------------------------------------
// | 
// +----------------------------------------------------------------------


?>
