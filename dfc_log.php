<?php
// +----------------------------------------------------------------------
// | PHP Source                                                           
// +----------------------------------------------------------------------
// | Copyright (C) 2005 by Chris Holland <anheuser50@yahoo.com>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

    // logger for the Admin Panel
    include_once("db.php");
    include_once("session.php");
    
    header ("Content-type: image/gif");
    //header ("Content-type: image/gif");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    
    $AdminSession = new fmSession;
    $Timer->start('session');
    $AdminSession->init('FMC_AdminPanel');
    $Timer->stop('session');
    
    $rpcElement = $_REQUEST['element'];
    $rpcValue = $_REQUEST['value'];
    if ($rpcElement == 'lgData') {
        // rpcValue = sres|bres|cres|browserID
        list($sres,$bres,$cres,$agent) = explode('|',$rpcValue);

        // Log is "new" or "res" or "complete"                    
        $state = $AdminSession->get("/log/logstate");
        $AdminSession->post("/log/logerror",":$state:");// if new...
        if ($state == 'new') {
            $result = $AdminSession->agentID($agent);
            $ip = $_SERVER['REMOTE_ADDR'];
            $port = $_SERVER['REMOTE_PORT'];
            $bid = $AdminSession->addBrowser($result['browser']['name'],$result['browser']['ver']);
            $oid = $AdminSession->addOS($result['os']);
            $mid = $AdminSession->addMoz($result['moz']['name'],$result['moz']['ver']);
            $sql = "INSERT INTO log_enter SET ip_address='$ip', entry_time=NOW(), ip_port='$port',browser_id='$bid',os_id='$oid',moz_id='$mid'";
            mysqli_query($db_link, $sql);
            $idresult = mysqli_query($db_link, "select LAST_INSERT_ID() as ID;");
            $row = mysqli_fetch_assoc($idresult);
            $id = $row['ID'];
            ((mysqli_free_result($idresult) || (is_object($idresult) && (get_class($idresult) == "mysqli_result"))) ? true : false);
            $AdminSession->post("/log/logstate",'res');  //New, Res, Complete
            $AdminSession->post("/log/logid",$id);  //ID of the Log Entry
        }
        $state = $AdminSession->get("/log/logstate");
        if ($state == 'res') {
            $id = $AdminSession->get("/log/logid");
            // Breakdown the resolution
            $sres_p = ($sres != 'xx');
            $bres_p = ($bres != 'x');
            $cres_p = ($cres != 'x');
            $sx=0;$sy=0;$sd=0;$bx=0;$by=0;$cx=0;$cy=0;
            if ($sres_p) { list($sx,$sy,$sd) = explode('x',$sres); }
            if ($bres_p) { list($bx,$by) = explode('x',$bres); }
            if ($cres_p) { list($cx,$cy) = explode('x',$cres); }
            $sql = "UPDATE log_enter SET s_res_x='$sx',s_res_y='$sy',s_res_d='$sd',c_res_x='$cx',c_res_y='$cy',b_res_x='$bx',b_res_y='$by' WHERE logID='$id'";
            mysqli_query($db_link, $sql);
            $AdminSession->post("/log/logerror",((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));// if new...
            $AdminSession->post("/log/logstate",'complete');  //New, Res, Complete
        }
    } else {
        $AdminSession->post("/log/log_element",":$rpcElement:");// if new...
        $AdminSession->post("/log/log_value",":$rpcValue:");// if new...
    }
    $AdminSession->save();
// Need to log this as well...
//      http://www.google.com/search?hl=en&q=5+muses&btnG=Google+Search

    echo base64_decode("R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==");
?>
