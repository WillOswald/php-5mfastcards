<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------
//

include_once("functions.php");
include_once("prg_config.php");

$db_link = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dfc_config['mysql_host'],
    $dfc_config['mysql_user'],  $dfc_config['mysql_pass'], $dfc_config['mysql_db']));

$db_name = $dfc_config['mysql_db'];
$db_tables["Language"] = "CREATE TABLE $db_name.Language (
        wID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        wIndex bigint,
        wLang smallint,
        wNative char(40),
        wForeign char(40),
        wNPrefix char(10),
        wNObject char(30),
        wFPrefix char(10),
        wFObject char(30),
        wSize bigint,
        wDuration bigint,
        wFlag1 integer,
        wFlag2 integer,
        wFlag3 integer,
        wFlag4 integer,
        wSHA1 char(40),
        index (wIndex),
        index (wSHA1),
        index (wNative),
        index (wForeign),
        index (wFlag1),
        index (wFlag2),
        index (wFlag3),
        index (wFlag4),
        index (wLang)
        )";

$db_tables["Lesson"] = "CREATE TABLE $db_name.Lesson (
        lID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        lName char(80),
        lGroup bigint,
        lCreated datetime,
        lLastUsed datetime,
        lOwner char(40),
        lFile char(40),
        index(lName),
        index(lGroup),
        index(lOwner)
        )";

$db_tables["Statistics"] = "CREATE TABLE $db_name.Statistics (
        sID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        sTime datetime,
        sPage char(80),
        sHits bigint,
        sScriptTime bigint,
        sSQLTime bigint,
        index(sPage),
        index(sTime),
        index(sTime,sPage)
        )";

$db_tables["Account"] = "CREATE TABLE $db_name.Account (
        uID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        uUID varchar(120),
        uUsername varchar(20),
        uPassword char(40),
        uFName varchar(20),
        uLName varchar(20),
        uAddress1 varchar(40),
        uAddress2 varchar(40),
        uPOBox varchar(10),
        uCity varchar(30),
        uState varchar(2),
        uCountry varchar(40),
        uProvince varchar(40),
        uDOB date,
        uAccess varchar(40),
        uAudioSettings bigint,
        uAudioCaps varchar(80),
        uCreated datetime,
        uAccessed datetime,
        uTimeOnline bigint,
        uVisits bigint,
        uSpeed integer,
        uSpeedSetting integer,
        uSHA1 char(40),
        index (uUID),
        index (uUsername),
        index (uLName,uFName),
        index (uCreated)
        )";

$db_tables["Referral"] = "CREATE TABLE $db_name.Referral (
        rID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        rReferrer varchar(20),
        rWhen datetime,
        rAccount char(40),
        index (rReferrer),
        index (rWhen)
        )";

$db_tables["Visitors"] = "CREATE TABLE $db_name.Visitors (
        vID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        vIP bigint,
        vReferrer char(80),
        vLocation char(3),
        index(vIP)
        )";

$db_tables["Session"] = "CREATE TABLE $db_name.Session (
        sID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        sSessionID char(40),
        sSessionData blob,
        sExpires datetime,
        sCreated datetime,
        index(sSessionID),
        index(sExpires)
        )";

        // Subject, Owner, PriorityMaj, PriorityMin, Started, Completed, Updated, Notes, 
$db_tables["Tasks"] = "CREATE TABLE $db_name.Tasks (
        taskID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        subject varchar(160),
        owner char(40),
        priorityA tinyint,
        priorityB tinyint,
        started datetime, 
        updated datetime, 
        completed datetime,
        deleted tinyint unsigned,
        notes blob,
        index(subject),
        index(owner),
        index(started),
        index(priorityA,priorityB)
        )";

$db_tables["log_enter"] = "CREATE TABLE $db_name.log_enter (
        logID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        entry_time bigint,
        ip_address bigint unsigned,
        ip_port smallint unsigned,
        s_res_x smallint DEFAULT 0,
        s_res_y smallint DEFAULT 0,
        s_res_d smallint DEFAULT 0,
        b_res_x smallint DEFAULT 0,
        b_res_y smallint DEFAULT 0,
        c_res_x smallint DEFAULT 0,
        c_res_y smallint DEFAULT 0,
        browser_id bigint,
        os_id integer,
        moz_id integer,
        index(entry_time),
        index(ip_address),
        index(browser_id)
        )";

$db_tables["log_browser"] = "CREATE TABLE $db_name.log_browser (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        browser varchar(60),
        ver varchar(20)
        )";

$db_tables["log_refer"] = "CREATE TABLE $db_name.log_refer (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        domain varchar(80),
        argv varchar(255),
        index(domain)
        )";

$db_tables["log_os"] = "CREATE TABLE $db_name.log_os (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        os varchar(40)
        )";
        
$db_tables["log_moz"] = "CREATE TABLE $db_name.log_moz (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        name varchar(60),
        ver varchar(20)
        )";

$db_tables["log_dnscache"] = "CREATE TABLE $db_name.log_dnscache (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        ip_address bigint unsigned,
        ip_hostname varchar(127),
        index(ip_address)
        )";

$db_tables["stat_ranking"] = "CREATE TABLE $db_name.stat_ranking (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        search_engine bigint unsigned,
        keyword bigint unsigned,
        site_domain varchar(64),
        site_title varchar(255),
        site_url varchar(255),
        site_desc varchar(255),
        rank integer,
        pulled date,
        index(pulled),
        index(search_engine,keyword,rank),
        index(keyword,rank),
        index(site_url),
        index(site_domain)
        )";

$db_tables["stat_counts"] = "CREATE TABLE $db_name.stat_counts (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        keyword bigint unsigned,
        search_engine bigint unsigned,
        total bigint unsigned,
        pulled date,
        index(pulled)
        )";

$db_tables["stat_keywords"] = "CREATE TABLE $db_name.stat_keywords (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        keyword varchar(80),
        index(keyword)
        )";

$db_tables["stat_searchengines"] = "CREATE TABLE $db_name.stat_searchengines (
        ID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        name varchar(80),
        url varchar(255)
        )";
        
$db_tables["Financial"] = "CREATE TABLE $db_name.Financial (
        fID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        fAccount char(40),
        fAmount decimal(12,2),
        fSource integer,
        fProduct integer,
        fDate datetime,
        index(fAccount),
        index(fDate)
        )";

$db_tables["WebContent"] = "CREATE TABLE $db_name.WebContent (
        wID bigint unsigned NOT NULL auto_increment PRIMARY KEY,
        wItem char(40),
        wPage char(40),
        wContent blob,
        index(wItem)
        )";

function countVocabulary() {
    global $db_link;
    
    $result = mysqli_query($db_link, "SELECT count(*) as total from Language");
    $row = mysqli_fetch_assoc($result);
    return $row['total'];
}

//TODO this should not be called so frequently. Ideally this would be removed
//or moved to an admin area to be called manually
function db_sanity($verb=false) {
    global $db_link, $db_name,$db_tables;

    // Check DB
		// it's really not necessary and probably bad to send USE queries.
		// the db is defined in the connection initialization
    if(DB_CONF_TYPE != 'appfog'){
        $qresult = ((bool)mysqli_query( $db_link, "USE $db_name"));
        if ($qresult == false) {
            mysqli_query($GLOBALS["___mysqli_ston"], "CREATE DATABASE $db_name") or die("Couldn't Create Database: $db_name");
        }
    }

    // Check Tables
    $current = array();
    $tresult = mysqli_query($db_link, "SHOW TABLES FROM $db_name");
    while ($row = mysqli_fetch_object($tresult)) {
        @$current[] = $row->Name;
    }
    if ($tresult) { ((mysqli_free_result($tresult) || (is_object($tresult) && (get_class($tresult) == "mysqli_result"))) ? true : false); }
    foreach($db_tables as $table => $sql) {
        if ($verb) echo "<DIV STYLE=\"border:1px solid blue;\">[$table] - ";
        if (!array_key_exists($table,$current)) {
            mysqli_query($db_link, $sql);
            if ($verb) echo "Creating...<BR>";
            if ($verb) echo ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
            if ($verb) echo "<BR>$sql</DIV>";
        } else {
            if ($verb) echo "Exists</DIV>";
        }
    }

}

function db_import_csv($file, $langid) {
    // Imports CSV files into the database.
    global $db_link, $db_name,$db_tables;

    $csv = file($file);
    $count = explode(",",array_shift($csv));
    $rows = count($csv);
    echo "File: $file<BR>";
    echo "Fields: $count<BR>";
    echo "Rows: $rows<BR><BR>";
    foreach($csv as $line) {
        // for each line...
        $items = explode(",","$line,");
        $l_id = $items[0];
        $l_index = $items[1];
        $l_eng = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $items[2]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $l_for = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $items[3]) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
        $l_id2 = $items[7];
        $l_id3 = $items[8];
        if ($l_id2 == "" ) { $l_id2 = "-1"; }
        if (intval($l_id2) == 0 ) { $l_id2 = "-1"; }
        if ($l_id3 != "" ) { $l_id3 = "1"; } else { $l_id3 = "-1"; }
        $sql = "INSERT INTO $db_name.Language SET ";
        $sql .= "wIndex = '$l_index', ";
        $sql .= "wLang = '$langid', ";
        $sql .= "wNative = '$l_eng', ";
        $sql .= "wForeign = '$l_for', ";
        $sql .= "wFlag1 = '$l_id2', ";
        $sql .= "wFlag2 = '$l_id3' ";
        mysqli_query($db_link, $sql);
        $err = ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
        if (strlen($err) > 1) { echo "<PRE>$err\n$sql</PRE><HR>"; }
    }

}

function db_400() {
    // Imports CSV files into the database.
    global $db_link, $db_name,$db_tables;

    srand ((float) microtime() * 10000000);
    $lang2 = array();
    $lang3 = array();
    $lang4 = array();
    // Fetch the indicies from each language...
    $result = mysqli_query($db_link, "SELECT wID, wIndex, wFlag1, wLang FROM $db_name.Language WHERE (wLang = 2) and (wFlag1 > 0) ORDER BY wFlag1");
    while ($row = mysqli_fetch_object($result)) {
        $lang2["{$row->wID}"] = $row->wFlag1;
    }
    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);

    $result = mysqli_query($db_link, "SELECT wID, wIndex, wFlag1, wLang FROM $db_name.Language WHERE (wLang = 3) and (wFlag1 > 0) ORDER BY wFlag1");
    while ($row = mysqli_fetch_object($result)) {
        $lang3[$row->wID] = $row->wFlag1;
    }
    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);

    $result = mysqli_query($db_link, "SELECT wID, wIndex, wFlag1, wLang FROM $db_name.Language WHERE (wLang = 4) and (wFlag1 > 0) ORDER BY wFlag1");
    while ($row = mysqli_fetch_object($result)) {
        $lang4[$row->wID] = $row->wFlag1;
    }
    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);

    $rkey2 = array_rand($lang2, 400);
    $rkey3 = array_rand($lang3, 400);
    $rkey4 = array_rand($lang4, 400);

    mysqli_query($db_link, "UPDATE Language SET wFlag2=-1");

    foreach($rkey2 as $value) {
        mysqli_query($db_link, "UPDATE Language SET wFlag2=1 WHERE wID=$value");
    }
    foreach($rkey3 as $value) {
        mysqli_query($db_link, "UPDATE Language SET wFlag2=1 WHERE wID=$value");
    }
    foreach($rkey4 as $value) {
        mysqli_query($db_link, "UPDATE Language SET wFlag2=1 WHERE wID=$value");
    }
}

function db_sha1() {
    // Make SHA1 Hash for Language Database
    global $db_link, $db_name,$db_tables;

    $result = mysqli_query($db_link, "SELECT wID, wIndex, wLang FROM $db_name.Language");
    $count = mysqli_num_rows($result);
    $base = intval($count / 100);
    $i=0;
    echo "<PRE>";
    echo "[...:....|....:....|....:....|....:....|....:....|....:....|....:....|....:....|....:....|....:....]\n";
    while ($row = mysqli_fetch_object($result)) {
        $wID = $row->wID;
        $sha1text = "{$row->wIndex}:{$row->wLang}:5muses";
        $sha1 = enc_sha1($sha1text);
        mysqli_query($db_link, "UPDATE $db_name.Language SET wSHA1='$sha1' WHERE wID=$wID");
        $i++;
        if ($i == $base) { echo "X"; $i = 0; }
    }
    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);

}

function db_calc_audio_time() {
    // Make SHA1 Hash for Language Database
    global $db_link, $db_name,$db_tables;

    $result = mysqli_query($db_link, "SELECT wSHA1 FROM $db_name.Language");
    $count = mysqli_num_rows($result);
    $base = intval($count / 100);
    $i=0;
    echo "<PRE>";
    echo "[...:....|....:....|....:....|....:....|....:....|....:....|....:....|....:....|....:....|....:....]\n";
    while ($row = mysqli_fetch_object($result)) {
        $sha1 = $row->wSHA1;
        $file = "audio/$sha1";
        $shell = madplay($file);
        list($l1,$l2) = explode("(",$shell);
        list($time,$l2) = explode(")",$l2);
        list($h,$m,$s) = explode(":",$time);
        $runtime = (intval($h) * 3600000) + (intval($m) * 60000) + (floatval($s) * 1000);
        //echo "<PRE>[$sha1] $time -- $runtime</PRE>";
        mysqli_query($db_link, "UPDATE $db_name.Language SET wDuration='$runtime' WHERE wSHA1='$sha1'");
        //echo mysql_error();
        //echo "<BR>";
        $i++;
        if ($i == $base) { echo "X"; $i = 0; }
    }
    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);

}
function rename_audio($lang) {
    // Rename the MP3 files
    global $db_link, $db_name,$db_tables;
    foreach (glob("incoming/*.mp3") as $filename) {
        list($dir,$name) = explode("/",$filename);
        list($name,$ext) = explode(".",$name);
        $result = mysqli_query($db_link, "SELECT wSHA1 FROM $db_name.Language WHERE (wLang=$lang) and (wIndex=$name)");
        if (!$result) {
            echo "Failure on $lang:$name<BR>";
        } else {
            $row = mysqli_fetch_object($result);
            $sha1 = $row->wSHA1;
            rename("incoming/$name.mp3","incoming/$sha1");
        }
    }
}

?>
