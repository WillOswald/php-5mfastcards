<?php
// +----------------------------------------------------------------------
// | PHP Source
// +----------------------------------------------------------------------
// | Copyright (C) 2004 by ,,, <anheuser50@fuzz>
// +----------------------------------------------------------------------
// |
// | Copyright: See COPYING file that comes with this distribution
// +----------------------------------------------------------------------

    header ("Content-type: image/png");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

// Grab the color to paint the "translucent" image
    $dir = $_REQUEST['d'];
    $color = $_REQUEST['c'];
    if ($color[0] == '#') {$color = substr($color,1);}
// Decode the color into RGB
    $cr = intval(substr($color,0,2),16);
    $cg = intval(substr($color,2,2),16);
    $cb = intval(substr($color,4,2),16);
// Create the Image and 2 colors, transparent and main
    $image = imagecreate(8,8);
    $tcolor = imagecolorallocate($image, 1, 1, 1);
    $mcolor = imagecolorallocate($image, $cr, $cg, $cb);

// Fill with the transparent color
    imagefill($image, 0, 0, $tcolor);

// Draw the Arrow
    if ($dir == 'down') {
        imageline ( $image, 0, 2, 7, 2, $mcolor);
        imageline ( $image, 1, 3, 6, 3, $mcolor);
        imageline ( $image, 2, 4, 5, 4, $mcolor);
        imageline ( $image, 3, 5, 4, 5, $mcolor);
    }
    if ($dir == 'right') {
        imageline ( $image, 2, 0, 2, 7, $mcolor);
        imageline ( $image, 3, 1, 3, 6, $mcolor);
        imageline ( $image, 4, 2, 4, 5, $mcolor);
        imageline ( $image, 5, 3, 5, 4, $mcolor);
    }
    if ($dir == 'space') {
        // Draw Nothing
    }

// Decalre the transparent color as "transparent";
    imagecolortransparent($image, $tcolor);

// Output the image
    imagepng($image);

?>
